//
//  SortController.swift
//  MatchRX
//
//  Created by Vignesh on 3/15/18.
//  Copyright © 2018 HTC. All rights reserved.
//

import UIKit

class SortController: UIViewController {
    
    @IBOutlet var tapView: UIView!
    
    var sortDictionary: NSMutableDictionary = NSMutableDictionary.init()
    var sortKeyDictionary: NSMutableDictionary = NSMutableDictionary.init()

    override func viewDidLoad() {
        super.viewDidLoad()
        self.view.backgroundColor = UIColor.black.withAlphaComponent(0.5)
        self.view.isOpaque = false
        
        let tapGesture = UITapGestureRecognizer.init(target: self, action: #selector(self.onViewTapped))
        tapGesture.numberOfTapsRequired = 1
        tapView.addGestureRecognizer(tapGesture)
        
        sortKeyDictionary.setValue(NSArray.init(objects: 100,101), forKey: "updated_at")
        sortKeyDictionary.setValue(NSArray.init(objects: 102,103), forKey: "unit_price")
        sortKeyDictionary.setValue(NSArray.init(objects: 104,105), forKey: "package_strength")
        sortKeyDictionary.setValue(NSArray.init(objects: 107,106), forKey: "post_title")
        sortKeyDictionary.setValue(NSArray.init(objects: 108,109), forKey: "wac_discount")
        sortKeyDictionary.setValue(NSArray.init(objects: 110,111), forKey: "expire_date")
        
        let keyAray = NSArray.init(objects: "updated_at", "updated_at", "unit_price", "unit_price", "package_strength", "package_strength", "post_title", "post_title", "wac_discount", "wac_discount", "expire_date", "expire_date")
        for i in 0..<keyAray.count {
            (self.view.viewWithTag(i+100) as! UIButton).titleLabel?.text = (keyAray.object(at: i) as! String)
            let label = UILabel.init()
            label.tag = i+200
            label.text = (keyAray.object(at: i) as! String)
            label.isHidden = true
            self.view.addSubview(label)
        }
        
        self.loadSelectedFilter()
        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    //MARK: - Instance functions.
    
    //Dismisses the Sort controller on tapping on the view.
    func onViewTapped() {
        self.dismiss(animated: true, completion: nil)
    }
    
    //Loads the selected filters.
    func loadSelectedFilter() {
        sortDictionary = GlobalData.sharedInstance.sortDictionary
        let key = sortDictionary.allKeys
        let selectedData = sortKeyDictionary.object(forKey: key[0] as! String)
        let dataArray = selectedData as! Array<Int>
        var index = 0
        if sortDictionary.allValues[0] as! String == "asc" {
            index = dataArray[1]
            if key[0] as! String == "post_title" {
                index = dataArray[0]
            }
        }
        else {
            index = dataArray[0]
            if key[0] as! String == "post_title" {
                index = dataArray[1]
            }
        }
        let sender = self.view.viewWithTag(index) as! UIButton
        if !sender.isSelected {
            for i in 100..<112 {
                (self.view.viewWithTag(i) as! UIButton).isSelected = false
            }
            sender.isSelected = true
        }
    }
    
    //Selects the clicked button to sort
    @IBAction func onSortByClicked(sender: UIButton) {
        if !sender.isSelected {
            for i in 100..<112 {
                (self.view.viewWithTag(i) as! UIButton).isSelected = false
            }
            sender.isSelected = true
        }
        sortDictionary.removeAllObjects()
        var value = "asc"
        if sender.tag % 2 == 0 {
            value = "desc"
        }
        if sender.tag == 106 {
            value = "desc"
        }
        else if sender.tag == 107 {
            value = "asc"
        }
        
        let text = (self.view.viewWithTag(sender.tag+100) as! UILabel).text
        sortDictionary.setValue(value, forKey: text!)
        let sortBy = NSMutableDictionary.init()
        sortBy.setValue(value, forKey: text!)
        GlobalData.sharedInstance.sortDictionary.removeAllObjects()
        GlobalData.sharedInstance.sortDictionary = sortBy
        sortDictionary = sortBy
        self.dismiss(animated: true, completion: nil)
        NavigationModel.sharedInstance.buyListControl?.reloadListData()
    }
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
