//
//  ConfirmReceiptTableViewCell.swift
//  MatchRX
//
//  Created by Vignesh on 5/31/18.
//  Copyright © 2018 HTC. All rights reserved.
//

import UIKit

class ConfirmReceiptTableViewCell: UITableViewCell {
    
    @IBOutlet weak var baseView: UIView!
    @IBOutlet weak var acceptBtn: UIButton!
    @IBOutlet weak var rejectBtn: UIButton!
    @IBOutlet weak var ndcNameLbl: UILabel!
    @IBOutlet weak var ndcNumberLbl: UILabel!
    @IBOutlet weak var ndcImageView: UIImageView!
    @IBOutlet weak var expiryDateLbl: UILabel!
    @IBOutlet weak var lotNumberLbl: UILabel!
    @IBOutlet weak var fullPackView: UIView!
    @IBOutlet weak var partialPackView: UIView!
    @IBOutlet weak var quantityLbl: UILabel!
    @IBOutlet weak var partialLbl: UILabel!
    @IBOutlet weak var priceLbl: UILabel!
    @IBOutlet weak var statusBtnView: UIView!
    @IBOutlet weak var outOfStockView: UIView!
    @IBOutlet weak var editBtn: UIButton!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
        // Configure the view for the selected state
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        baseView.layer.borderWidth = 0.8
        baseView.layer.borderColor = UIColor.lightGray.cgColor
    }
}


