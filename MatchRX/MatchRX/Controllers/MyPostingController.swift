//
//  MyPostingController.swift
//  MatchRX
//
//  Created by Vignesh on 12/15/17.
//  Copyright © 2017 HTC. All rights reserved.
//

import UIKit

class MyPostingController: UIViewController,UITableViewDelegate,UITableViewDataSource, NetworkManagerDelegate, URLSessionDelegate, EditPostingDelegate, UITextFieldDelegate {

    var textBorderColor = UIColor(red: 207.0/255.0, green: 216.0/255.0, blue: 220.0/255.0, alpha: 1.0).cgColor
    
    @IBOutlet var tableView: UITableView!
    @IBOutlet var searchTxtField: UITextField!
    @IBOutlet var wacDiscountView: UIView!
    @IBOutlet var wacDiscountViewHeightConstraint: NSLayoutConstraint!
    @IBOutlet var wacAlertLbl: UILabel!
    @IBOutlet var informationLbl: UILabel!
    var editControlforMyPost: EditPostingControl!
    var refreshControl: UIRefreshControl!
    var isRefreshHidden: Bool?
    var isPaginationCalled: Bool = false
    let alertMessage = "Postings with WAC discount of less than 10% will not display in the marketplace. Adjust the WAC discount to 10% or more."
    
    /*func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        editControlforMyPost = storyboard.instantiateViewController(withIdentifier: "editPostControl") as! EditPostingControl
        NavigationModel.sharedInstance.homeControl?.contentNavController.pushViewController(editControlforMyPost, animated: true)
        NavigationModel.sharedInstance.homeControl?.showBackIconWithTitle(title: "Edit Posting")
        editControlforMyPost.postId = "296"
    }*/
    
    //MARK: - Global declarations
    var marketPlaceDataModelArr: NSMutableArray?
    var detailControl: ItemDetailViewController!
    var myPostingListArray: NSMutableArray = NSMutableArray.init()
    var pageCount: Int = 0
    var dataPerPage: Int = 20
    var selectedIndex: Int?
    
    var imageCache = NSCache<AnyObject, UIImage>()
    var operationQueue = OperationQueue()
    var operationListArray = Array<String>()
    
    //MARK: - Overriding functions
    
    override func viewDidLoad() {
        super.viewDidLoad()
        searchTxtField.layer.borderColor = textBorderColor
        searchTxtField.layer.borderWidth = 1.0
        
        refreshControl = UIRefreshControl()
        refreshControl.attributedTitle = NSAttributedString(string: "")
        refreshControl.addTarget(self, action: #selector(self.refreshTableView), for: UIControlEvents.valueChanged)
        isRefreshHidden = true
        tableView.addSubview(refreshControl)
        
        wacDiscountView.isHidden = true
        wacDiscountViewHeightConstraint.constant = 0.0
        fetchMyPostingListData()
        // Do any additional setup after loading the view.
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        tableView.delegate = self
        tableView.dataSource = self
    }
    
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    //MARK: - IBAction functions.
    
    @IBAction func onSearchClicked(sender: UIButton) {
//        wacDiscountView.isHidden = true
//        wacDiscountViewHeightConstraint.constant = 0.0
    }
    
    @IBAction func onSortClicked(sender: UIButton) {
//        wacDiscountView.isHidden = false
//        wacDiscountViewHeightConstraint.constant = 54.0
    }
    
    //MARK: - Instance functions
    
    
    //Returns the request parameters with the added filter and sort keys.
    func getRequestData() -> NSMutableDictionary{
        let requestDict = NSMutableDictionary.init()
        let filterDict = NSMutableDictionary.init()
        filterDict.setValue("A", forKey: "package_option")
        filterDict.setValue("A", forKey: "original_package")
        filterDict.setValue("A", forKey: "brand")
        filterDict.setValue("A", forKey: "expire_date")
        filterDict.setValue("A", forKey: "wac_discount")
        filterDict.setValue("A", forKey: "created_at")
        
        let sortDict = NSMutableDictionary.init()
        sortDict.setValue("desc", forKey: "updated_at")
        
        requestDict.setValue(filterDict, forKey: "filter")
//        requestDict.setValue(sortDict, forKey: "sort")
        requestDict.setValue("", forKey: "search")
        requestDict.setValue(dataPerPage, forKey: "per_page")
        requestDict.setValue(pageCount, forKey: "page")
        return requestDict
    }
    
    
    
    //Refresh the table view to get the new values.
    func refreshTableView() {
        if NetworkManager.sharedInstance.isReachable(){
            isRefreshHidden = false
            pageCount = 0
            myPostingListArray.removeAllObjects()
            self.fetchMyPostingListData()
        }
        else{
            refreshControl.endRefreshing()
            tableView.setContentOffset(CGPoint.zero, animated: true)
            AlertControl.sharedInstance.presentAlertView(alertTitle: "Kindly check your internet connectivity", alertMessage: "", delegate: nil)
        }
    }
    
    //Creates the request to fetch the data.
    func fetchMyPostingListData() {
        
        if NetworkManager.sharedInstance.isReachable(){
            _ = NetworkManager.sharedInstance.checkTypeOfNetwork()
            if NetworkManager.sharedInstance.isReachableViaWiFi || NetworkManager.sharedInstance.isReachableViaCellular{
                LoadingView.shareInstance.showLodingView(parentView: (NavigationModel.sharedInstance.homeControl?.view!)!)
                NetworkManager.sharedInstance.manageAPIRequest(requestType: .ServiceTypeMyPostings, requestString: RequestDataModel.sharedInstance.makeRequestForMyPostingList(requestDict: self.getRequestData()), delegate: self)
            }
        }
        else{
            AlertControl.sharedInstance.presentAlertView(alertTitle: "Kindly check your internet connectivity", alertMessage: "", delegate: nil)
        }
    }
    
    //Downloads the image from URL
    func downloadImage(drugModel: BuyListDataModel) {
        let config = URLSessionConfiguration.default
        let session = URLSession(configuration: config, delegate: self, delegateQueue: nil)
        let getImageTask = session.downloadTask(with: URL(string: drugModel.medispanImageURL!)!) { (url, response, error) in
            if url != nil{
                let data = try? Data(contentsOf: url!)
                let downloadedImage  = UIImage(data: data!)
                if (downloadedImage != nil) {
                    //                self.imageCache.setObject(downloadedImage!, forKey: productObj.productSKUId as AnyObject)
                    self.imageCache.setObject(downloadedImage!, forKey: drugModel.postId!
                        as AnyObject)
                    //self.operationListArray.remove(at:productObj.productSKUId!)
                }
                //            self.productListView.reloadData()
                //            self.productListView.performSelector(onMainThread: #selector(self.productListView.reloadData), with: nil, waitUntilDone: false)
                self.tableView.performSelector(onMainThread: #selector(UITableView.reloadData), with: nil, waitUntilDone: false)
            }
            
        }
        getImageTask.resume()
    }
    
    //Deletes the selected item from list.
    func onDeleteClicked(sender: UIButton) {
        let dataModel = myPostingListArray.object(at: sender.tag) as! BuyListDataModel
        selectedIndex = sender.tag
        let alertControl = UIAlertController(title: "Do you want to delete this posting?", message: "", preferredStyle: UIAlertControllerStyle.alert)
        alertControl.addAction(UIAlertAction(title:"YES", style: UIAlertActionStyle.default, handler: {actions in self.deleteDrug(dataModel: dataModel)}))
        alertControl.addAction(UIAlertAction(title: "NO", style: UIAlertActionStyle.cancel, handler: nil))
        UIApplication.topViewController()?.present(alertControl, animated: true, completion: nil)
    }
    
    //Deletes the selected item from list.
    func deleteDrug(dataModel: BuyListDataModel) {
        if NetworkManager.sharedInstance.isReachable(){
            _ = NetworkManager.sharedInstance.checkTypeOfNetwork()
            if NetworkManager.sharedInstance.isReachableViaWiFi || NetworkManager.sharedInstance.isReachableViaCellular{
                LoadingView.shareInstance.showLodingView(parentView: self.view.window!)
                NetworkManager.sharedInstance.manageAPIRequest(requestType: .ServiceTypeDeletePost, requestString: "\(dataModel.postId!)", delegate: self)
            }
        }
        else{
            AlertControl.sharedInstance.presentAlertView(alertTitle: "Kindly check your internet connectivity", alertMessage: "", delegate: nil)
        }
    }
    
    //MARK: - Network manager delegate function
    
    //Returns the web service call
    func webServiceResponse(for requestType: ServiceRequestType, responseData Data: Any!, withStatusCode statusCode: Int, errorMessage message: Any!) {
        if !isRefreshHidden! {
            isRefreshHidden = true
            refreshControl.endRefreshing()
        }
        if requestType == .ServiceTypeMyPostings {
            isPaginationCalled = false
        }
        LoadingView.shareInstance.hideLoadingView()
        if message as? Error != nil {
            AlertControl.sharedInstance.presentAlertView(alertTitle: "Something went wrong", alertMessage: "", delegate: self)
        }
        else{
            if !NetworkManager.sharedInstance.checkStatusCode(statusCode){
                let error = NetworkManager.sharedInstance.getErrorModel(requestType: requestType, httpStatusCode: statusCode, responseData: Data)
                AlertControl.sharedInstance.presentAlertView(alertTitle: (error.errorMessage!), alertMessage: "", delegate: self)
            }
            else{
                if requestType == .ServiceTypeMyPostings{
                    let responseArray = ResponseManager.sharedInstance.getMyPostingsListData(responseData: Data)
                    for i in 0..<responseArray.count {
                        myPostingListArray.add(responseArray.object(at: i))
                    }
                    if responseArray.count > 0 {
                        tableView.reloadData()
                    }
                    /*if requestType == .ServiceTypeDeletePost {
                        AlertControl.sharedInstance.presentAlertView(alertTitle: "The item was successfully deleted", alertMessage: "", delegate: self)
                    }*/
                }
                else if requestType == .ServiceTypeDeletePost {
                    AlertControl.sharedInstance.presentAlertView(alertTitle: "The item was successfully deleted", alertMessage: "", delegate: self)
                    myPostingListArray.removeObject(at: selectedIndex!)
                    tableView.reloadData()
//                    self.reloadListData()
                }
            }
        }
        if myPostingListArray.count == 0 {
            self.view.bringSubview(toFront: informationLbl)
        }
        else {
            self.view.sendSubview(toBack: informationLbl)
        }
    }
    
    //MARK: - Edit posting delegate called.
    func reloadListData() {
        myPostingListArray.removeAllObjects()
        pageCount = 0
        tableView.reloadData()
        self.fetchMyPostingListData()
    }
    
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
     // Get the new view controller using segue.destinationViewController.
     // Pass the selected object to the new view controller.
     }
     */
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 150.0
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return myPostingListArray.count;
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "MyPostCellId", for: indexPath) as!MyPostingListViewCell
        
        if indexPath.row <= myPostingListArray.count {
            let dataModel = myPostingListArray.object(at: indexPath.row) as! BuyListDataModel
            
            if indexPath.row == 0 {
                if dataModel.isWacDiscountLessThanTenExists! {
                    wacDiscountView.isHidden = false
                    wacAlertLbl.text = alertMessage
                    self.view.layoutIfNeeded()
                    wacDiscountViewHeightConstraint.constant = 54.0
                }
                else {
                    wacDiscountView.isHidden = true
                    wacDiscountViewHeightConstraint.constant = 0.0
                }
            }
            
            cell.stripLbl.isHidden = true
            cell.shippingPromoView.isHidden = true
            if dataModel.imageCount != 0 {
                cell.stripLbl.isHidden = false
            }
            cell.drugNameLbl.text = dataModel.drugName
            cell.ndcNumberLotNumberLbl.text = "NDC# \(dataModel.ndcNumber!) | Lot# \(dataModel.lotNumber!)"
            cell.expiryDateLbl.text = dataModel.expiryDate
            cell.strengthLbl.text = dataModel.strength
            cell.packagingLbl.text = dataModel.packaging
            cell.discountLbl.text = dataModel.discount!+"%"
            
            //            let currentPrice = String(format: "%.2f", arguments: [dataModel.currentPrice!])
            let revisedPrice = String(format: "%.2f", arguments: [dataModel.revisedPrice!])
            //            cell.priceLbl.text = "$\(dataModel.currentPrice!)"
            cell.priceLbl.text = "$\(revisedPrice)"
            
            
            
            cell.modifiedDateLbl.text = dataModel.modifiedDate
//            cell.quantityAvailableLbl.text = "\(dataModel.packageQuantity!)"
            cell.quantityAvailableLbl.text = "\(dataModel.availableQuantity!)"
            cell.deleteBtn.tag = indexPath.row
            cell.deleteBtn.addTarget(self, action: #selector(self.onDeleteClicked(sender:)), for: .touchUpInside)
            
            if !dataModel.isModified! {
                cell.baseViewHeight.constant = 0.0
            }
            else {
                cell.baseViewHeight.constant = 20.0
            }
            
            if dataModel.packageOption == "F" {
                cell.partialImgView.isHidden = true
                cell.packagingLbl.frame.origin.x = 0.0
            }
            else {
                cell.partialImgView.isHidden = false
                cell.packagingLbl.frame.origin.x = 10.0
            }
            
            if dataModel.shippingAmount != "0" {
                cell.shippingPromoView.isHidden = false
                cell.shippingPromoLbl.text = "$"+dataModel.shippingAmount!
            }
            else {
                cell.shippingPromoView.isHidden = true
            }
            
            if dataModel.medispanImageURL != nil {
                if ((imageCache.object(forKey: dataModel.postId! as AnyObject)) != nil){
                    cell.drugImageView.image = imageCache.object(forKey: dataModel.postId! as Int as AnyObject)
                }
                else {
                    if !((operationListArray.contains(String(dataModel.postId!)))){
                        
                        let operation : BlockOperation = BlockOperation(block: {
                            self.downloadImage(drugModel: dataModel)
                            //                            self.downloadImageForProduct(productObj: productObj)
                        })
                        operation.completionBlock = {
//                            print("Operation completed")
                        }
                        
                        operationQueue.addOperation(operation)
                        
                        operationListArray.insert(String(dataModel.postId!), at: (operationListArray.count))
                    }
                }
            }
            
        }
        if indexPath.row == myPostingListArray.count-1 && !isPaginationCalled{
            print("Index path count is: \(indexPath.row) and Array count is: \(myPostingListArray.count-1)")
            //            let dataModel = myPostingListArray.object(at: indexPath.row) as! BuyListDataModel
            pageCount += 1
            self.fetchMyPostingListData()
            isPaginationCalled = true
        }
        return cell;
    }
    
    /*func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        print("Displaying row no: \(indexPath.row)")
        if indexPath.row == myPostingListArray.count-1 {
//            let dataModel = myPostingListArray.object(at: indexPath.row) as! BuyListDataModel
            pageCount += 1
            self.fetchMyPostingListData()
        }
    }*/
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        selectedIndex = indexPath.row
        let dataModel = myPostingListArray.object(at: indexPath.row) as! BuyListDataModel
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let editPostControl = storyboard.instantiateViewController(withIdentifier: "editPostControl") as! EditPostingControl
        NavigationModel.sharedInstance.homeControl?.contentNavController.pushViewController(editPostControl, animated: true)
        NavigationModel.sharedInstance.homeControl?.showBackIconWithTitle(title: "EDIT POSTING")
        editPostControl.parentDelegate = self
        editPostControl.postId = "\(dataModel.postId!)"
    }
    
    //MARK: - Text field delegate functions.
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        self.view.endEditing(true)
        return true
    }
    
}

