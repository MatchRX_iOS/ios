//
//  CartFooterTableViewCell.swift
//  MatchRX
//
//  Created by Poonkathirvelan on 19/02/18.
//  Copyright © 2018 HTC. All rights reserved.
//

import UIKit

class CartFooterTableViewCell: UITableViewCell {

    @IBOutlet weak var baseContentView: UIView!
    @IBOutlet weak var groundRadio: UIImageView!
    @IBOutlet weak var TwoDayRadioImg: UIImageView!
    @IBOutlet weak var priorityRadio: UIImageView!
    @IBOutlet weak var footerViewTempViewCon: NSLayoutConstraint!
    @IBOutlet weak var footerTempView: UIView!
    @IBOutlet weak var groundView: UIView!
    @IBOutlet weak var twoDayView: UIView!
    @IBOutlet weak var priorityOvernightView: UIView!
    @IBOutlet weak var groundTitleLbl: UILabel!
    @IBOutlet weak var groundDeliveryLbl: UILabel!
    @IBOutlet weak var twoDayTitleLbl: UILabel!
    @IBOutlet weak var twoDayDeliveryLbl: UILabel!
    @IBOutlet weak var priorityTitleLbl: UILabel!
    @IBOutlet weak var priorityDeliveryLbl: UILabel!
    @IBOutlet weak var itemTotalLbl: UILabel!
    @IBOutlet weak var shippingCostLbl: UILabel!
    @IBOutlet weak var sellerTotalLbl: UILabel!
    @IBOutlet weak var sellerNameLbl: UILabel!
    @IBOutlet weak var fedexImgView: UIImageView!
    @IBOutlet weak var groundBtn: UIButton!
    @IBOutlet weak var twoDayBtn: UIButton!
    @IBOutlet weak var priorityBtn: UIButton!
    @IBOutlet weak var matchRXOfferLbl: UILabel!
    @IBOutlet weak var matchRXLblContentViewHeight: NSLayoutConstraint!
    @IBOutlet weak var fedexLblContentViewHeight: NSLayoutConstraint!
    @IBOutlet weak var fedexOfferLbl: UILabel!
    @IBOutlet weak var dashedImgView: UIView!
    @IBOutlet weak var groundViewHeightConstraint: NSLayoutConstraint!
    @IBOutlet weak var twoDayHeightConstratint: NSLayoutConstraint!
    @IBOutlet weak var fedexOfferInfoViewHeightConstraint: NSLayoutConstraint!
    @IBOutlet weak var matchRXOfferInfoContentViewHeightConstraint: NSLayoutConstraint!
    @IBOutlet weak var matchRxOfferLblViewHeightConstraint: NSLayoutConstraint!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        
        // Initialization code
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    

    @IBAction func shipingMethodSelctd(_ sender: UIButton) {
        
         groundRadio.image = UIImage(named: "Radio off.png")
        TwoDayRadioImg.image = UIImage(named: "Radio off.png")
         priorityRadio.image = UIImage(named: "Radio off.png")
        switch sender.tag {
        case 500:
            groundRadio.image = UIImage(named: "Radio on.png")
            break
            
        case 501:
            TwoDayRadioImg.image = UIImage(named: "Radio on.png")
            break
            
        case 502:
            priorityRadio.image = UIImage(named: "Radio on.png")
            break
            
        default:
            groundRadio.image = UIImage(named: "Radio off.png")
            break
        }
    }
}
