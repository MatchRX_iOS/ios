//
//  MenuListCell.swift
//  ServiceTest
//
//  Created by Vignesh on 10/13/17.
//  Copyright © 2017 HTC. All rights reserved.
//

import UIKit

class MenuListCell: UITableViewCell {

    
    //MARK: - Global variable declaration
    
    @IBOutlet var menuNameLabel: UILabel!
    @IBOutlet var menuImage: UIImageView!
    @IBOutlet var countLbl: UILabel!
    
    //MARK: - Override functions
    
    override func awakeFromNib() {
        super.awakeFromNib()
//        self.countLbl.layer.cornerRadius = self.countLbl.frame.size.width/2.0
        self.countLbl.layer.cornerRadius = 5.0
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    override func layoutIfNeeded() {
        super.layoutIfNeeded()
        self.countLbl.layer.cornerRadius = self.countLbl.frame.size.width/2.0
    }
    override func layoutSubviews() {
        super.layoutSubviews()
        self.countLbl.layer.cornerRadius = self.countLbl.frame.size.width/2.0
    }

}
