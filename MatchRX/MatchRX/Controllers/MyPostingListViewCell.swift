//
//  MyPostingListViewCell.swift
//  MatchRX
//
//  Created by Poonkathirvelan on 30/01/18.
//  Copyright © 2018 HTC. All rights reserved.
//

import UIKit

class MyPostingListViewCell: UITableViewCell {
    
    //MARK: - IBOutlet references
    @IBOutlet weak var baseView: UIView!
    @IBOutlet weak var drugImageView: UIImageView!
    @IBOutlet weak var stripLbl: UILabel!
    @IBOutlet weak var drugNameLbl: UILabel!
    @IBOutlet weak var ndcNumberLotNumberLbl: UILabel!
    @IBOutlet weak var expiryDateLbl: UILabel!
    @IBOutlet weak var strengthLbl: UILabel!
    @IBOutlet weak var packagingLbl: UILabel!
    @IBOutlet weak var partialImgView: UIImageView!
    @IBOutlet weak var quantityAvailableLbl: UILabel!
    @IBOutlet weak var ratingLbl: UILabel!
    @IBOutlet weak var shippingPromoLbl: UILabel!
    @IBOutlet weak var discountLbl: UILabel!
    @IBOutlet weak var priceLbl: UILabel!
    @IBOutlet weak var deleteBtn: UIButton!
    @IBOutlet weak var shippingPromoView: UIView!
    @IBOutlet weak var ratingView: UIView!
    @IBOutlet weak var modifiedDateLbl: UILabel!
    @IBOutlet weak var infoImgView: UIImageView!
    @IBOutlet weak var footerView: UIView!
    @IBOutlet weak var baseViewHeight: NSLayoutConstraint!

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    override func layoutSubviews() {
        self.baseView.layer.masksToBounds = false
        self.baseView.layer.shadowOffset = CGSize(width: 3, height: 3)
        self.baseView.layer.shadowRadius = 1
        self.baseView.layer.shadowOpacity = 0.2
    }
}
