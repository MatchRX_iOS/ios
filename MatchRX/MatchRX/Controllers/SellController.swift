//
//  SellController.swift
//  MatchRX
//
//  Created by Vignesh on 12/5/17.
//  Copyright © 2017 HTC. All rights reserved.
//

import UIKit

class SellController: UIViewController {
    
    @IBOutlet var postButton: UIButton!
    @IBOutlet var myPostingButton: UIButton!
    @IBOutlet var selectedImage: UIImageView!
    @IBOutlet var leadingConstraint: NSLayoutConstraint!
    @IBOutlet var contentView: UIView!
    
    var newPostControl : NewPostingController!
    var myPostingControl: MyPostingController!
    var selectedButton: UIButton?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        newPostControl = storyboard.instantiateViewController(withIdentifier: "postNewControl") as! NewPostingController
        newPostControl.view.frame = contentView.bounds
        contentView.addSubview(newPostControl.view)
        
        
//        myPostingControl = storyboard.instantiateViewController(withIdentifier: "myPostingControl") as! MyPostingController
//        myPostingControl.view.frame = contentView.bounds
//        contentView.addSubview(myPostingControl.view)
//        contentView.bringSubview(toFront: newPostControl.view)
        
        selectedButton = postButton
        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    //MARK: - Instance functions
    
    //Navigates to selected controller.
    @IBAction func loadSelectedController(sender: UIButton){
        leadingConstraint.constant = sender.frame.origin.x
        selectedButton = sender
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        if sender == postButton {
           // contentView.bringSubview(toFront: newPostControl.view)
            if(myPostingControl != nil){
                myPostingControl.view.removeFromSuperview();
                myPostingControl = nil;
            }
            if(newPostControl == nil){
            newPostControl = storyboard.instantiateViewController(withIdentifier: "postNewControl") as! NewPostingController
            newPostControl.view.frame = contentView.bounds
            contentView.addSubview(newPostControl.view)
            }
        }
        else{
           // contentView.bringSubview(toFront: myPostingControl.view)
            if(newPostControl != nil){
                newPostControl.view.removeFromSuperview();
                newPostControl = nil;
            }
            
            if(myPostingControl == nil){
            myPostingControl = storyboard.instantiateViewController(withIdentifier: "myPostingControl") as! MyPostingController
            myPostingControl.view.frame = contentView.bounds
            contentView.addSubview(myPostingControl.view)
            //contentView.bringSubview(toFront: newPostControl.view)
        }
        }
    }
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
