//
//  SellerConfirmationControl.swift
//  MatchRX
//
//  Created by Vignesh on 5/4/18.
//  Copyright © 2018 HTC. All rights reserved.
//

import UIKit

class SellerConfirmationControl: UIViewController, UITableViewDelegate, UITableViewDataSource, NetworkManagerDelegate, URLSessionDelegate, ReviseItemDelegate, DropDownViewDelegate, UITextFieldDelegate {
    
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var listView: UIView!
    @IBOutlet weak var availableView: UIView!
    @IBOutlet weak var editedView: UIView!
    @IBOutlet weak var outOfStockView: UIView!
    @IBOutlet weak var orderID_DateLbl: UILabel!
    @IBOutlet weak var confirmStatusBtn: UIButton!
    @IBOutlet weak var confirmOrderBtn: UIButton!
    @IBOutlet weak var nextButton: UIButton!
    @IBOutlet var oneLbl: UILabel!
    @IBOutlet var twoLbl: UILabel!
    @IBOutlet var wizardTick1Img: UIImageView!
    @IBOutlet var wizardTick2Img: UIImageView!
    @IBOutlet weak var schedulePickupBtn: UIButton!
    @IBOutlet weak var shippingDateBtn1: UIButton!
    @IBOutlet weak var shippingDateBtn2: UIButton!
    @IBOutlet weak var avail_itemTotalLbl: UILabel!
    @IBOutlet weak var avail_shippingChargeLbl: UILabel!
    @IBOutlet weak var avail_matchRXLbl: UILabel!
    @IBOutlet weak var avail_matchMoneyLbl: UILabel!
    @IBOutlet weak var avail_appliedMatchMoneyLbl: UILabel!
    @IBOutlet weak var avail_appliedInsuranceLbl: UILabel!
    @IBOutlet weak var avail_creditTotalLbl: UILabel!
    @IBOutlet weak var matchMoneyTxtField: UITextField!
    @IBOutlet weak var fedexInsuranceBtn: UIButton!
    @IBOutlet weak var avail_insuranceLbl: UILabel!
    @IBOutlet weak var avail_shippingMethodLbl: UILabel!
    @IBOutlet weak var cancelMatchMoneyBtn: UIButton!
    @IBOutlet weak var applyMaxBtn: UIButton!
    @IBOutlet weak var edit_ImgView: UIImageView!
    @IBOutlet weak var edit_ReviseLbl: UILabel!
    @IBOutlet weak var edit_OutOfStockLbl: UILabel!
    @IBOutlet weak var edit_itemTotalLbl: UILabel!
    @IBOutlet weak var edit_shippingLbl: UILabel!
    @IBOutlet weak var edit_matchRXFeeLbl: UILabel!
    @IBOutlet weak var edit_creditTotalLbl: UILabel!
    @IBOutlet weak var avail_SubmitBtn: UIButton!
    @IBOutlet weak var edit_SubmitBtn: UIButton!
    @IBOutlet weak var downloadPDFView: UIView!
    @IBOutlet weak var downloadContentView: UIView!
    @IBOutlet weak var submit_MessageView: UIView!
    @IBOutlet weak var submit_CancellationView: UIView!
    @IBOutlet weak var submit_ReviseView: UIView!
    
    
    var dataArray: NSMutableArray = NSMutableArray.init()
    var orderId: String?
    var selectedIndex: Int?
    var orderDetailsDataModel: OrderDetailsDataModel?
    var confirmOrderType: OrderedItemStatus?
    
    var imageCache = NSCache<AnyObject, UIImage>()
    var operationQueue = OperationQueue()
    var operationListArray = Array<String>()
    
    let DEFAULT_WIZARD_BG_COLOR = UIColor(red: 238.0/255.0, green: 241.0/255.0, blue: 243.0/255.0, alpha: 1.0)
    let SELECTED_WIZARD_BG_COLOR = UIColor(red: 246.0/255.0, green: 208.0/255.0, blue: 181.0/255.0, alpha: 1.0)
    let DEFAULT_WIZARD_LABEL_BG_COLOR = UIColor.clear
    let SELECTED_WIZARD_LABEL_BG_COLOR = UIColor(red: 255.0/255.0, green: 142.0/255.0, blue: 0.0/255.0, alpha: 1.0)
    let DEFAULT_WIZARD_LABEL_TEXT_COLOR = UIColor(red: 164.0/255.0, green: 164.0/255.0, blue: 164.0/255.0, alpha: 1.0)
    let SELECTED_WIZARD_LABEL_TEXT_COLOR = UIColor(red: 255.0/255.0, green: 255.0/255.0, blue: 255.0/255.0, alpha: 1.0)
    let DEFAULT_WIZARD_LABEL_BORDER_COLOR = UIColor(red: 164.0/255.0, green: 164.0/255.0, blue: 164.0/255.0, alpha: 1.0).cgColor
    let SELECTED_WIZARD_LABEL_BORDER_COLOR = UIColor.clear.cgColor
    

    override func viewDidLoad() {
        super.viewDidLoad()
//        self.view.bringSubview(toFront: listView)
        oneLbl.layer.cornerRadius = oneLbl.frame.size.width/2.0
        twoLbl.layer.cornerRadius = twoLbl.frame.size.width/2.0
        oneLbl.layer.borderColor = SELECTED_WIZARD_LABEL_BORDER_COLOR
        twoLbl.layer.borderColor = DEFAULT_WIZARD_LABEL_BORDER_COLOR
        twoLbl.layer.borderWidth = 1.0
        schedulePickupBtn.layer.borderWidth = 1.0
        schedulePickupBtn.layer.borderColor = DEFAULT_WIZARD_LABEL_BORDER_COLOR
        NavigationModel.sharedInstance.homeControl?.hamburgerView.isHidden = true
        NavigationModel.sharedInstance.homeControl?.headerLabel.frame.size.width = 250.0
//        NavigationModel.sharedInstance.homeControl?.backView.isHidden = true
        NavigationModel.sharedInstance.homeControl?.headerMenuView.isHidden = true
        
        self.fetchOrderDetails()
        
        
        // Do any additional setup after loading the view.
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        for views in downloadContentView.subviews {
            if views.isKind(of: UILabel.self) {
                views.sizeToFit()
            }
        }
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    //MARK: - Instance functions.
    
    //Prepares request to fetch the data from service.
    func fetchOrderDetails() {
        if NetworkManager.sharedInstance.isReachable(){
            _ = NetworkManager.sharedInstance.checkTypeOfNetwork()
            if NetworkManager.sharedInstance.isReachableViaWiFi || NetworkManager.sharedInstance.isReachableViaCellular{
                LoadingView.shareInstance.showLodingView(parentView: (NavigationModel.sharedInstance.homeControl?.view!)!)
                NetworkManager.sharedInstance.manageAPIRequest(requestType: .ServiceTypeOrderDetails, requestString: orderId!, delegate: self)
            }
        }
        else{
            AlertControl.sharedInstance.presentAlertView(alertTitle: "Kindly check your internet connectivity", alertMessage: "", delegate: nil)
        }
    }
    
    //Serialises the data fetched from service.
    func loadWithSerialisedData() {
        var pickupSlots: ShippingDates!
//        slotData = orderDetailsDataModel?.shippingSlotArray.firstObject as! ShippingDates
        
        orderID_DateLbl.text = "Order# \(orderDetailsDataModel?.orderNumber ?? "") | Order Date \(orderDetailsDataModel?.orderDate ?? "")"
        if orderDetailsDataModel?.shippingMethods.object(forKey: "Ground") != nil {
            let shippingDetails = orderDetailsDataModel?.shippingMethods.object(forKey: "Ground") as! CartShippingDetailsDataModel
            if shippingDetails.isSelected! {
                avail_shippingMethodLbl.text = shippingDetails.shippingName
            }
        }
        else if orderDetailsDataModel?.shippingMethods.object(forKey: "2-Day") != nil {
            let shippingDetails = orderDetailsDataModel?.shippingMethods.object(forKey: "2-Day") as! CartShippingDetailsDataModel
            if shippingDetails.isSelected! {
                avail_shippingMethodLbl.text = shippingDetails.shippingName
            }
        }
        else if orderDetailsDataModel?.shippingMethods.object(forKey: "Overnight") != nil {
            let shippingDetails = orderDetailsDataModel?.shippingMethods.object(forKey: "Overnight") as! CartShippingDetailsDataModel
            if shippingDetails.isSelected! {
                avail_shippingMethodLbl.text = shippingDetails.shippingName
            }
        }
        avail_insuranceLbl.text = "$"+String(format:"%.2f", arguments:[(orderDetailsDataModel?.fedexInsuranceAmount!)!])
        avail_appliedInsuranceLbl.text = "-$"+String(format:"%.2f", arguments:[(orderDetailsDataModel?.fedexInsuranceAmount!)!])
        if (orderDetailsDataModel?.fedexInsuranceAmount)! > 0.00 {
            fedexInsuranceBtn.isSelected = false
        }
        else {
            fedexInsuranceBtn.isSelected = true
            fedexInsuranceBtn.isUserInteractionEnabled = false
        }
        
        if (orderDetailsDataModel?.matchMoney)! == 0 {
            matchMoneyTxtField.isUserInteractionEnabled = false
            applyMaxBtn.isUserInteractionEnabled = false
        }
        
        if (orderDetailsDataModel?.shippingSlotArray.count)! > 0 {
            let shippingSlot1 = orderDetailsDataModel?.shippingSlotArray.firstObject as! ShippingDates
            let shippingDate = shippingSlot1.shippingDate
            let keyArray = shippingSlot1.keys?.components(separatedBy: ",")
            schedulePickupBtn.setTitle(shippingSlot1.shippingTimeSlot.object(forKey: keyArray![0]) as? String, for: .normal)
            shippingDateBtn1.setTitle(shippingDate, for: .normal)
            shippingDateBtn1.isSelected = true
            if (orderDetailsDataModel?.shippingSlotArray.count)! > 1{
                shippingDateBtn2.isHidden = false
                let shippingSlot2 = orderDetailsDataModel?.shippingSlotArray.lastObject as! ShippingDates
                let shippingDate2 = shippingSlot2.shippingDate
                let keyArray = shippingSlot2.keys?.components(separatedBy: ",")
                schedulePickupBtn.setTitle(shippingSlot2.shippingTimeSlot.object(forKey: keyArray![0]) as? String, for: .normal)
                shippingDateBtn2.setTitle(shippingDate2, for: .normal)
            }
            else {
                shippingDateBtn2.isHidden = true
            }
        }
        dataArray.removeAllObjects()
        dataArray.addObjects(from: orderDetailsDataModel?.orderItemsArray as! [Any])
        tableView.reloadData()
        nextButton.isUserInteractionEnabled = true
        nextButton.alpha = 1.0
        confirmOrderBtn.isUserInteractionEnabled = true
        confirmStatusBtn.isUserInteractionEnabled = true
    }
    
    
    
    //Downloads the image from URL
    func downloadImage(itemModel: OrderDetailsDataModel) {
        let config = URLSessionConfiguration.default
        let session = URLSession(configuration: config, delegate: self, delegateQueue: nil)
        let getImageTask = session.downloadTask(with: URL(string: itemModel.medispanImageURL!)!) { (url, response, error) in
            if url != nil{
                let data = try? Data(contentsOf: url!)
                let downloadedImage  = UIImage(data: data!)
                if (downloadedImage != nil) {
                    self.imageCache.setObject(downloadedImage!, forKey: itemModel.orderId!
                        as AnyObject)
                }
                self.tableView.performSelector(onMainThread: #selector(UITableView.reloadData), with: nil, waitUntilDone: false)
            }
            
        }
        getImageTask.resume()
    }
    
    //Shows terms and conditions.
    @IBAction func showTermsAndConditions() {
        AlertControl.sharedInstance.presentAlertView(alertTitle: "", alertMessage: "All shipments are insured up to $100 by FedEx. You may purchase additional FedEx insurance for the full item total, maximum insurable value is $50,000.", delegate: nil)
    }
    
    //On Next button clicked will decide which view to be displayed.
    @IBAction func onNextButtonClicked(sender: UIButton) {
        self.onHeaderButtonClicked(sender: confirmOrderBtn)
    }
    
    //Navigates to Buy marketplace
    @IBAction func onContinueShoppingClicked(sender: UIButton) {
        NavigationModel.sharedInstance.homeControl?.contentNavController.popToRootViewController(animated: true)
        NavigationModel.sharedInstance.homeControl?.headerLabel.text = "BUY"
        NavigationModel.sharedInstance.homeControl?.screenTitle = "BUY"
        NavigationModel.sharedInstance.homeControl?.headerMenuView.isHidden = false
        NavigationModel.sharedInstance.homeControl?.hamburgerView.isHidden = false
        NavigationModel.sharedInstance.homeControl?.backView.isHidden = true
        NavigationModel.sharedInstance.homeControl?.headerLabel.frame.size.width = 160.0
        NavigationModel.sharedInstance.homeControl?.showBuyMenu()
    }
    
    //Navigates to My Orders
    @IBAction func onGoToMyOrdersClicked(sender: UIButton) {
        NavigationModel.sharedInstance.homeControl?.contentNavController.popToRootViewController(animated: true)
        NavigationModel.sharedInstance.homeControl?.headerLabel.text = "MY ORDERS"
        NavigationModel.sharedInstance.homeControl?.screenTitle = "MY ORDERS"
        NavigationModel.sharedInstance.homeControl?.headerMenuView.isHidden = false
        NavigationModel.sharedInstance.homeControl?.hamburgerView.isHidden = false
        NavigationModel.sharedInstance.homeControl?.backView.isHidden = true
        NavigationModel.sharedInstance.homeControl?.headerLabel.frame.size.width = 160.0
        NavigationModel.sharedInstance.homeControl?.showMyOrders()
    }
    
    //Submits the order
    @IBAction func onSubmitOrderClicked(sender: UIButton) {
        let currentDateFormat = DateFormatter()
        let expectedDateFormat = DateFormatter()
        currentDateFormat.dateFormat = "MM/dd/YYYY (EEE)"
        expectedDateFormat.dateFormat = "YYYY-MM-dd"
        var shippingDate = ""
        var slotData: ShippingDates!
        
        let dataDict = NSMutableDictionary.init()
        dataDict.setValue("sale", forKey: "type")
        dataDict.setValue(orderDetailsDataModel?.globalOrderId, forKey: "order_id")
        if confirmOrderType == OrderedItemStatus.Available {
            let matchMoney = Double((avail_appliedMatchMoneyLbl.text?.trimmingCharacters(in: CharacterSet.init(charactersIn: "$")))!)
            if matchMoney! > 0 {
                if matchMoney! > (orderDetailsDataModel?.matchMoney!)! {
//                    AlertControl.sharedInstance.presentAlertView(alertTitle: "You can avail maximum of $\(String(format:"%.2f", arguments:[(orderDetailsDataModel?.matchMoney)!]))", alertMessage: "", delegate: nil)
                    AlertControl.sharedInstance.presentAlertView(alertTitle: "The amount of MatchMoney applied cannot exceed the amount available", alertMessage: "", delegate: nil)
                    return
                }
                if matchMoney! > (orderDetailsDataModel?.matchRXFee!)! {
//                    AlertControl.sharedInstance.presentAlertView(alertTitle: "You can avail maximum of $\(String(format:"%.2f", arguments:[(orderDetailsDataModel?.matchRXFee)!]))", alertMessage: "", delegate: nil)
                    AlertControl.sharedInstance.presentAlertView(alertTitle: "The amount of MatchMoney applied cannot exceed the MatchRX fee", alertMessage: "", delegate: nil)
                    return
                }
            }
//            var selectedDate = ""
            if shippingDateBtn1.isSelected {
//                selectedDate = shippingDateBtn1.title(for: .normal)!
                let date = currentDateFormat.date(from: shippingDateBtn1.title(for: .normal)!)
                shippingDate = expectedDateFormat.string(from: date!)
                slotData = orderDetailsDataModel?.shippingSlotArray.firstObject as! ShippingDates
            }
            else if shippingDateBtn2.isSelected {
//                selectedDate = shippingDateBtn2.title(for: .normal)!
                slotData = orderDetailsDataModel?.shippingSlotArray.lastObject as! ShippingDates
                let date = currentDateFormat.date(from: shippingDateBtn2.title(for: .normal)!)
                shippingDate = expectedDateFormat.string(from: date!)
            }
            
            shippingDate = slotData.receivedDate!
            let pickUpSlots = slotData.shippingTimeSlot
            let pickupSchedule = pickUpSlots.allKeys(for: schedulePickupBtn.title(for: .normal)!)
            
            dataDict.setValue("Fedex Schedule", forKey: "status")
            if fedexInsuranceBtn.isSelected {
                dataDict.setValue(orderDetailsDataModel?.fedexInsuranceAmount, forKey: "fedex_shipping_insurance")
            }
            dataDict.setValue(shippingDate, forKey: "shipping_date")
            dataDict.setValue(schedulePickupBtn.title(for: .normal), forKey: "schedule_pickup_dropoff")
            dataDict.setValue(pickupSchedule.first as? String, forKey: "schedule_pickup_dropoff")
            dataDict.setValue(avail_appliedMatchMoneyLbl.text?.trimmingCharacters(in: CharacterSet.init(charactersIn: "$")), forKey: "match_money")
            
        }
        else if confirmOrderType == OrderedItemStatus.Revised {
            let orderArray = NSMutableArray.init()
            for i in 0..<dataArray.count {
                let orderDataDict = NSMutableDictionary.init()
                let orderData = dataArray.object(at: i) as! OrderDetailsDataModel
                
                orderDataDict.setValue(orderData.orderId, forKey: "id")
                
                if orderData.itemStatus == OrderedItemStatus.Available {
                    orderDataDict.setValue("Available", forKey: "confirmation_status")
                }
                else if orderData.itemStatus == OrderedItemStatus.Revised {
                    orderDataDict.setValue("Revised", forKey: "confirmation_status")
                }
                else if orderData.itemStatus == OrderedItemStatus.OutOfStock {
                    orderDataDict.setValue("Outofstock", forKey: "confirmation_status")
                }
                
                if orderData.packageOption?.lowercased() == "f" {
                    orderDataDict.setValue(orderData.modifiedQuantity, forKey: "quantity")
                }
                else {
                    orderDataDict.setValue(1, forKey: "quantity")
                    orderDataDict.setValue(orderData.modifiedPackageSize, forKey: "package_size")
                }
                orderArray.add(orderDataDict)
            }
            dataDict.setValue(orderArray, forKey: "orderitems")
            dataDict.setValue("Seller Revised", forKey: "status")
        }
        else {
            dataDict.setValue("Canceled", forKey: "status")
        }
        if NetworkManager.sharedInstance.isReachable(){
            _ = NetworkManager.sharedInstance.checkTypeOfNetwork()
            if NetworkManager.sharedInstance.isReachableViaWiFi || NetworkManager.sharedInstance.isReachableViaCellular{
                LoadingView.shareInstance.showLodingView(parentView: (NavigationModel.sharedInstance.homeControl?.view!)!)
                NetworkManager.sharedInstance.manageAPIRequest(requestType: .ServiceTypeSellerConfirmation, requestString: RequestDataModel.sharedInstance.makeRequestForSellerConfirmation(requestDict: dataDict), delegate: self)
            }
        }
        else{
            AlertControl.sharedInstance.presentAlertView(alertTitle: "Kindly check your internet connectivity", alertMessage: "", delegate: nil)
        }
    }
    
    //Cancels the applied match money.
    @IBAction func onCancelMatchMoneyClicked(sender: UIButton) {
        avail_appliedMatchMoneyLbl.text = "$0.00"
        cancelMatchMoneyBtn.isHidden = true
        matchMoneyTxtField.text = ""
        let creditTotal = self.calculateCreditTotal()
        avail_creditTotalLbl.text = "$"+String(format: "%.2f", arguments:[creditTotal])
    }
    
    
    //Loads the data for dropdown to select the slot.
    @IBAction func onScheduleDropdownClicked(sender: UIButton) {
        var slotData: ShippingDates!
        if shippingDateBtn1.isSelected {
            slotData = orderDetailsDataModel?.shippingSlotArray.firstObject as! ShippingDates
        }
        else {
            slotData = orderDetailsDataModel?.shippingSlotArray.lastObject as! ShippingDates
        }
        let pickUpSlots = slotData.shippingTimeSlot
//        let keyArray = pickUpSlots.allKeys
        let dropDownData = NSMutableArray()
//        let keyString = "dropoff,08:00,08:30,09:00,09:30,10:00,10:30,11:00,11:30,12:00,12:30,13:00,13:30,14:00,14:30,15:00,15:30,16:00"
//        let keyArray = keyString.components(separatedBy: ",")
        let keyArray = slotData.keys?.components(separatedBy: ",")
        for i in 0..<(keyArray?.count)! {
            let model = DropDownModel()
            model.modelDataString = pickUpSlots.object(forKey: keyArray![i]) as? NSString
            model.modelDataString = pickUpSlots.value(forKey: keyArray![i] ) as? NSString
            model.modelIdString = keyArray![i] as NSString
            dropDownData.add(model)
        }
        
        DropDownView.defaultDropDownControl.showDropDownForData(dropDownData ,DropDownType.SingleSelectionDefault, CGRect.zero, (NavigationModel.sharedInstance.homeControl?.view)!, self as DropDownViewDelegate, [])
        DropDownView.defaultDropDownControl.dropDownListSearch?.isHidden = true
    }
    
    //Provide the maximum match money can be availed.
    @IBAction func onApplyMaxBtnClicked(sender: UIButton) {
        if (orderDetailsDataModel?.matchMoney)! < (orderDetailsDataModel?.matchRXFee)! {
            matchMoneyTxtField.text = String(format: "%.2f", arguments:[(orderDetailsDataModel?.matchMoney)!])
            avail_appliedMatchMoneyLbl.text = "$"+String(format: "%.2f", arguments:[(orderDetailsDataModel?.matchMoney)!])
        }
        else {
            matchMoneyTxtField.text = String(format: "%.2f", arguments:[(orderDetailsDataModel?.matchRXFee)!])
            avail_appliedMatchMoneyLbl.text = "$"+String(format: "%.2f", arguments:[(orderDetailsDataModel?.matchRXFee)!])
        }
        cancelMatchMoneyBtn.isHidden = false
        let creditTotal = self.calculateCreditTotal()
        avail_creditTotalLbl.text = "$"+String(format: "%.2f", arguments:[creditTotal])
    }
    
    //Selects the shipping date button.
    @IBAction func onShippingDateBtnClicked(sender: UIButton) {
        if !sender.isSelected {
            if sender == shippingDateBtn1 {
                shippingDateBtn1.isSelected = true
                shippingDateBtn2.isSelected = false
            }
            else {
                shippingDateBtn1.isSelected = false
                shippingDateBtn2.isSelected = true
            }
        }
    }
    
    @IBAction func onFedexInsuranceBtnClicked(sender: UIButton) {
        if sender.isSelected {
           sender.isSelected = false
            avail_appliedInsuranceLbl.text = "$0.00"
        }
        else {
            sender.isSelected = true
            avail_appliedInsuranceLbl.text = "-$"+String(format: "%.2f", arguments:[(orderDetailsDataModel?.fedexInsuranceAmount)!])
        }
        let creditTotal = self.calculateCreditTotal()
        avail_creditTotalLbl.text = "$"+String(format: "%.2f", arguments:[creditTotal])
    }
    
    @IBAction func onHeaderButtonClicked(sender: UIButton) {
        if !sender.isSelected {
            confirmStatusBtn.isSelected = false
            confirmOrderBtn.isSelected = false
            if sender == confirmStatusBtn {
                oneLbl.backgroundColor = SELECTED_WIZARD_LABEL_BG_COLOR
                twoLbl.backgroundColor = DEFAULT_WIZARD_LABEL_BG_COLOR
                oneLbl.textColor = SELECTED_WIZARD_LABEL_TEXT_COLOR
                twoLbl.textColor = DEFAULT_WIZARD_LABEL_TEXT_COLOR
                oneLbl.layer.borderColor = SELECTED_WIZARD_LABEL_BORDER_COLOR
                twoLbl.layer.borderColor = DEFAULT_WIZARD_LABEL_BORDER_COLOR
                confirmStatusBtn.backgroundColor = SELECTED_WIZARD_BG_COLOR
                confirmOrderBtn.backgroundColor = DEFAULT_WIZARD_BG_COLOR
                wizardTick2Img.isHidden = false
                wizardTick1Img.isHidden = true
                self.view.bringSubview(toFront: listView)
            }
            else {
                oneLbl.backgroundColor = DEFAULT_WIZARD_LABEL_BG_COLOR
                twoLbl.backgroundColor = SELECTED_WIZARD_LABEL_BG_COLOR
                oneLbl.textColor = DEFAULT_WIZARD_LABEL_TEXT_COLOR
                twoLbl.textColor = SELECTED_WIZARD_LABEL_TEXT_COLOR
                oneLbl.layer.borderColor = DEFAULT_WIZARD_LABEL_BORDER_COLOR
                twoLbl.layer.borderColor = SELECTED_WIZARD_LABEL_BORDER_COLOR
                confirmStatusBtn.backgroundColor = DEFAULT_WIZARD_BG_COLOR
                confirmOrderBtn.backgroundColor = SELECTED_WIZARD_BG_COLOR
                wizardTick1Img.isHidden = false
                wizardTick2Img.isHidden = true
                self.showConfirmOrderView()
            }
            sender.isSelected = true
        }
        sender.backgroundColor = SELECTED_WIZARD_BG_COLOR
    }
    
    //Decides which confirm order view has to be displayed.
    func showConfirmOrderView() {
        var availableCount = 0
        var outOfStockCount = 0
        var shippingPrefix = "-$"
        var matchRXPrefix = "-$"
        
        if orderDetailsDataModel?.shippingCharge == 0.0 {
            shippingPrefix = "$"
        }
        if orderDetailsDataModel?.matchRXFee == 0.0 {
            matchRXPrefix = "-$"
        }
        
        for i in 0..<dataArray.count {
            let dataModel = dataArray.object(at: i) as! OrderDetailsDataModel
            if dataModel.itemStatus == OrderedItemStatus.Available {
                availableCount += 1
            }
            else if dataModel.itemStatus == OrderedItemStatus.OutOfStock {
                outOfStockCount += 1
            }
        }
        var itemTotal = 0.00
        self.view.bringSubview(toFront: availableView)
        for i in 0..<dataArray.count {
            let orderData = dataArray.object(at: i) as! OrderDetailsDataModel
            itemTotal += orderData.modifiedPrice!
        }
        if availableCount == dataArray.count {
            confirmOrderType = OrderedItemStatus.Available
            var appliedMatchMoney = "0.00"
            if !(matchMoneyTxtField.text?.isEmpty)! {
                appliedMatchMoney = matchMoneyTxtField.text!
            }
//            let appliedMatchMoney = matchMoneyTxtField.text?.trimmingCharacters(in: CharacterSet.init(charactersIn: "($) "))
//            let shippingCharge = ((orderDetailsDataModel?.sellerShippingFee)!+(orderDetailsDataModel?.additionalSellerShippingFee)!+(orderDetailsDataModel?.additionalSellerPickupFee)!-(orderDetailsDataModel?.refundSellerShippingFee)!)+((orderDetailsDataModel?.fedexShippingInsurance)!+(orderDetailsDataModel?.additionalFedexShippingInsurance)!-(orderDetailsDataModel?.refundFededShippingInsurance)!)
            
            avail_matchMoneyLbl.text = "$"+String(format:"%.2f", arguments:[(orderDetailsDataModel?.matchMoney!)!])
            avail_itemTotalLbl.text = "$"+String(format:"%.2f", arguments:[itemTotal])
            avail_shippingChargeLbl.text = shippingPrefix+String(format:"%.2f", arguments:[(orderDetailsDataModel?.shippingCharge)!])
            avail_matchRXLbl.text = matchRXPrefix+String(format:"%.2f", arguments:[(orderDetailsDataModel?.matchRXFee!)!])
            avail_appliedMatchMoneyLbl.text = "$"+String(format:"%.2f", arguments:[Double(appliedMatchMoney)!])
            if fedexInsuranceBtn.isSelected {
                avail_appliedInsuranceLbl.text = "-$"+String(format:"%.2f", arguments:[(orderDetailsDataModel?.fedexInsuranceAmount!)!])
            }
            else {
                avail_appliedInsuranceLbl.text = "$0.00"
            }
            let creditTotal = self.calculateCreditTotal()
            avail_creditTotalLbl.text = "$"+String(format: "%.2f", arguments:[creditTotal])
        }
        else if outOfStockCount == dataArray.count {
//            self.view.bringSubview(toFront: outOfStockView)
            confirmOrderType = OrderedItemStatus.OutOfStock
            self.view.bringSubview(toFront: editedView)
            edit_SubmitBtn.setTitle("Submit Cancellation", for: .normal)
            edit_ImgView.image = #imageLiteral(resourceName: "order_cancel.png")
            edit_ReviseLbl.isHidden = true
            edit_OutOfStockLbl.isHidden = false
            edit_itemTotalLbl.text = "$0.00"
            edit_shippingLbl.text = "$0.00"
            edit_matchRXFeeLbl.text = "$0.00"
            edit_creditTotalLbl.text = "$0.00"
        }
        else {
            let matchRXFee = (itemTotal*(orderDetailsDataModel?.matchRXFeePercentage)!)/100.0
            confirmOrderType = OrderedItemStatus.Revised
            self.view.bringSubview(toFront: editedView)
            edit_SubmitBtn.setTitle("Submit Revision", for: .normal)
            edit_ImgView.image = #imageLiteral(resourceName: "order_warning.png")
            edit_ReviseLbl.isHidden = false
            edit_OutOfStockLbl.isHidden = true
            edit_itemTotalLbl.text = "$"+String(format:"%.2f", arguments:[itemTotal])
            edit_shippingLbl.text = shippingPrefix+String(format:"%.2f", arguments:[(orderDetailsDataModel?.shippingCharge)!])
//            edit_matchRXFeeLbl.text = "-$"+String(format:"%.2f", arguments:[(orderDetailsDataModel?.matchRXFee!)!])
            edit_matchRXFeeLbl.text = matchRXPrefix+String(format:"%.2f", arguments:[matchRXFee])
            let shippingCharge = Double((edit_shippingLbl.text?.trimmingCharacters(in: CharacterSet.init(charactersIn: "-$")))!)
//            let matchRXFee = Double((edit_matchRXFeeLbl.text?.trimmingCharacters(in: CharacterSet.init(charactersIn: "-$")))!)
//            let creditTotal = itemTotal-(shippingCharge!+matchRXFee!)
            let creditTotal = itemTotal-(shippingCharge!+matchRXFee)
            edit_creditTotalLbl.text = "$"+String(format: "%.2f", arguments:[creditTotal])
        }
    }
    
    //When order item status clicked
    func onItemStatusChanged(sender: UIButton) {
//        let indexPath = IndexPath(row: sender.tag, section: 0)
        let orderData = dataArray.object(at: sender.tag) as! OrderDetailsDataModel
        let cell = sender.superview?.superview?.superview?.superview as! SellerConfirmationTableViewCell
//        let cell = tableView.cellForRow(at: indexPath) as! SellerConfirmationTableViewCell
        selectedIndex = sender.tag
        if !sender.isSelected {
            cell.availableBtn.isSelected = false
            cell.reviseBtn.isSelected = false
            cell.outOfStockBtn.isSelected = false
            if sender == cell.availableBtn {
                cell.availableBtn.isSelected = true
                self.onAvailableClicked(dataModel: orderData, cell: cell)
            }
            else if sender == cell.reviseBtn {
                cell.reviseBtn.isSelected = true
                self.reviseItemDetails(dataModel: orderData, selectedCell: cell)
            }
            else {
                cell.outOfStockBtn.isSelected = true
                self.outOfStockClicked(dataModel: orderData, cell: cell)
            }
//            tableView.reloadData()
        }
    }
    
    func calculateCreditTotal() -> Double{
        var itemTotal = 0.00
        var insurance = 0.00
        let txtFieldValue = avail_appliedMatchMoneyLbl.text?.trimmingCharacters(in: CharacterSet.init(charactersIn: "$"))
        //        let matchmoney: String = avail_appliedMatchMoneyLbl.text?.remove(at: (avail_appliedMatchMoneyLbl.text?.startIndex)!)
        //        let matchMoney = Double(avail_appliedMatchMoneyLbl.text?.dropFirst() as? String ?? "0.00")
        let matchMoney = Double(txtFieldValue!)
        if fedexInsuranceBtn.isSelected {
            insurance = (orderDetailsDataModel?.fedexInsuranceAmount)!
        }
        for i in 0..<dataArray.count {
            let orderData = dataArray.object(at: i) as! OrderDetailsDataModel
            itemTotal += orderData.modifiedPrice!
        }
        let creditTotal = (itemTotal+matchMoney!)-((orderDetailsDataModel?.shippingCharge)!+(orderDetailsDataModel?.matchRXFee)!+insurance)
        return creditTotal
    }
    
    //On clicking the available button all the original values has to be restored.
    func resetToOriginalValues() {
        
    }
    
    //Initiates the revise control on revise button clicked
    func reviseItemDetails(dataModel: OrderDetailsDataModel, selectedCell: SellerConfirmationTableViewCell) {
        let storyBoard = UIStoryboard(name: "Main", bundle: nil)
        let reviseControl = storyBoard.instantiateViewController(withIdentifier: "reviseControl") as! ReviseControl
        reviseControl.parentDelegate = self
        reviseControl.dataModel = dataModel
        reviseControl.sellerCell = selectedCell
        NavigationModel.sharedInstance.homeControl?.present(reviseControl, animated: true, completion: nil)
    }
    
    //On available button clicked.
    func onAvailableClicked(dataModel: OrderDetailsDataModel, cell: SellerConfirmationTableViewCell) {
        dataModel.itemStatus = OrderedItemStatus.Available
        dataModel.modifiedQuantity = dataModel.originalQuantity
        dataModel.modifiedPackageSize = dataModel.originalPackageSize
        
        cell.availableBtn.isSelected = true
        cell.reviseBtn.isSelected = false
        cell.outOfStockBtn.isSelected = false
        if dataModel.packageOption?.lowercased() == "f" {
            dataModel.modifiedPrice = dataModel.originalPackPrice!*Double(dataModel.modifiedQuantity!)
            cell.quantityLbl.text = "\(dataModel.modifiedQuantity!)"
            cell.priceLbl.text = "$"+String(format:"%.2f", arguments:[dataModel.modifiedPrice!])
        }
        else {
            dataModel.modifiedPrice = dataModel.unitPrice!*Double(dataModel.modifiedPackageSize!)
            cell.partialLbl.text = "Partial: \(dataModel.modifiedPackageSize!)/\(dataModel.actualPackageSize!)"
            cell.priceLbl.text = "$"+String(format:"%.2f", arguments:[dataModel.modifiedPrice!])
        }
        dataArray.replaceObject(at: selectedIndex!, with: dataModel)
    }
    
    //Sets the selected item as out of stock
    func outOfStockClicked(dataModel: OrderDetailsDataModel, cell: SellerConfirmationTableViewCell) {
        dataModel.itemStatus = OrderedItemStatus.OutOfStock
        if dataModel.packageOption?.lowercased() == "f" {
            dataModel.modifiedPrice = 0.00
            dataModel.modifiedQuantity = 0
            cell.quantityLbl.text = "\(dataModel.modifiedQuantity!)"
            cell.priceLbl.text = "$"+String(format:"%.2f", arguments:[dataModel.modifiedPrice!])
        }
        else {
            dataModel.modifiedPrice = 0.00
            dataModel.modifiedPackageSize = 0
            cell.partialLbl.text = "Partial: \(dataModel.modifiedPackageSize!)/\(dataModel.actualPackageSize!)"
            cell.priceLbl.text = "$"+String(format:"%.2f", arguments:[dataModel.modifiedPrice!])
        }
        dataArray.replaceObject(at: selectedIndex!, with: dataModel)
    }
    
    
    //Returns the number of specific letter
    func SpecificLetterCount(str:String, char:Character) ->Int {
        let letters = Array(str); var count = 0
        for letter in letters {
            if letter == char {
                count += 1
            }
        }
        return count
    }
    
    //Check whether it is a valid decimal number.
    func isValidDecimalNumber(number: String) -> Bool {
        var maximumLength = 6
        if number.prefix(1) == "." {
            return false
        }
        if number.contains(".") {
            maximumLength = 9
            let decimalCount = self.SpecificLetterCount(str: number, char: ".")
            if decimalCount > 1 {
                return false
            }
            if (number.components(separatedBy: ".")[1]).count > 2{
                return false
            }
        }
        if number.count > maximumLength {
            return false
        }
        return true
    }
    
    //MARK: - Drop down delegate
    
    func selectedDropDownData(_ dropDownDataObject: DropDownModel) {
        schedulePickupBtn.setTitle(dropDownDataObject.modelDataString as String?, for: .normal)
        DropDownView.defaultDropDownControl.hideDropDown()
    }
    
    func selectedDropDownDataArray(_ dropDownDataArray: NSMutableArray) {
        
        print("")
    }
    
    func selectedDropDownDataString(_ selectedDropDownDataString: NSString) {
        
        print("")
    }
    
    
    
    
    //MARK: - Revise control delegate
    func revisedItemDetails(itemDetails: OrderDetailsDataModel, forCell: SellerConfirmationTableViewCell) {
        var flag = false
        if itemDetails.packageOption?.lowercased() == "f" {
            if itemDetails.originalQuantity == itemDetails.modifiedQuantity {
                flag = true
            }
        }
        else {
            if itemDetails.originalPackageSize == itemDetails.modifiedPackageSize {
                flag = true
            }
        }
        if flag {
            forCell.availableBtn.isSelected = true
            forCell.reviseBtn.isSelected = false
            forCell.outOfStockBtn.isSelected = false
            itemDetails.itemStatus = OrderedItemStatus.Available
            itemDetails.modifiedQuantity = itemDetails.originalQuantity
            itemDetails.modifiedPackageSize = itemDetails.originalPackageSize
        }
        else {
            if itemDetails.packageOption?.lowercased() == "f" {
                itemDetails.modifiedPrice = itemDetails.originalPackPrice!*Double(itemDetails.modifiedQuantity!)
                forCell.quantityLbl.text = "\(itemDetails.modifiedQuantity!)"
                forCell.priceLbl.text = "$"+String(format:"%.2f", arguments:[itemDetails.modifiedPrice!])
            }
            else {
                itemDetails.modifiedPrice = itemDetails.unitPrice!*Double(itemDetails.modifiedPackageSize!)
//                forCell.partialLbl.text = "\(itemDetails.modifiedPackageSize!)"
                forCell.partialLbl.text = "Partial: \(itemDetails.modifiedPackageSize!)/\(itemDetails.actualPackageSize!)"
                forCell.priceLbl.text = "$"+String(format:"%.2f", arguments:[itemDetails.modifiedPrice!])
            }
            itemDetails.itemStatus = OrderedItemStatus.Revised
        }
        dataArray.replaceObject(at: selectedIndex!, with: itemDetails)
    }
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */
    
    //MARK: - Table view delegate and datasource functions.
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return dataArray.count
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 135.0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "sellerConfirmationCell", for: indexPath) as! SellerConfirmationTableViewCell
        if indexPath.row < dataArray.count {
            let dataModel = dataArray.object(at: indexPath.row) as! OrderDetailsDataModel
            cell.ndcNameLbl.text = dataModel.ndcName
            cell.ndcNumberLbl.text = dataModel.ndcNumber
            cell.expiryDateLbl.text = dataModel.expiryDate
            cell.lotNumberLbl.text = dataModel.lotNumber
            cell.availableBtn.tag = indexPath.row
            cell.reviseBtn.tag = indexPath.row
            cell.outOfStockBtn.tag = indexPath.row
            cell.availableBtn.addTarget(self, action: #selector(onItemStatusChanged(sender:)), for: .touchUpInside)
            cell.reviseBtn.addTarget(self, action: #selector(onItemStatusChanged(sender:)), for: .touchUpInside)
            cell.outOfStockBtn.addTarget(self, action: #selector(onItemStatusChanged(sender:)), for: .touchUpInside)
            if dataModel.packageOption?.lowercased() == "f" {
                cell.partialPackView.isHidden = true
                cell.fullPackView.isHidden = false
                cell.quantityLbl.text = "\(dataModel.modifiedQuantity!)"
                if dataModel.originalQuantity! < 2 {
                    cell.reviseBtn.isUserInteractionEnabled = false
                    cell.reviseBtn.alpha = 0.5
                }
                else {
                    cell.reviseBtn.isUserInteractionEnabled = true
                    cell.reviseBtn.alpha = 1.0
                }
            }
            else {
                cell.partialPackView.isHidden = false
                cell.fullPackView.isHidden = true
                cell.partialLbl.text = "Partial: \(dataModel.modifiedPackageSize!)/\(dataModel.actualPackageSize!)"
                if dataModel.originalPackageSize! < 2 {
                    cell.reviseBtn.isUserInteractionEnabled = false
                    cell.reviseBtn.alpha = 0.5
                }
                else {
                    cell.reviseBtn.isUserInteractionEnabled = true
                    cell.reviseBtn.alpha = 1.0
                }
            }
            cell.priceLbl.text = "$"+String(format: "%.2f", arguments:[dataModel.modifiedPrice!])
            
            if dataModel.medispanImageURL != nil {
                if ((imageCache.object(forKey: dataModel.orderId! as AnyObject)) != nil){
                    cell.ndcImageView.image = imageCache.object(forKey: dataModel.orderId! as Int64 as AnyObject)
                }
                else {
                    if !((operationListArray.contains(String(dataModel.orderId!)))){
                        
                        let operation : BlockOperation = BlockOperation(block: {
                            self.downloadImage(itemModel: dataModel)
                        })
                        operation.completionBlock = {
                            //                            print("Operation completed")
                        }
                        
                        operationQueue.addOperation(operation)
                        
                        operationListArray.insert(String(dataModel.orderId!), at: (operationListArray.count))
                    }
                }
            }
            
        }
        return cell
    }
    
    //MARK: - Table view delegate
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        let combinedString = textField.text?.replacingCharacters(in: Range(range, in: textField.text!)!, with: string)
        if self.isValidDecimalNumber(number: combinedString!) {
            //                let matchMoney = Double(combinedString!)
            self.perform(#selector(self.resizeTxtField), with: nil, afterDelay: 0.3)
            return true
        }
        else {
            return false
        }
    }
    
    func resizeTxtField() {
        self.view.layoutIfNeeded()
    }
    
    func textFieldShouldEndEditing(_ textField: UITextField) -> Bool {
        if (textField.text?.isEmpty)! {
            textField.text = "0.00"
        }
        else {
            if (textField.text?.contains("."))!{
                let decimal = textField.text?.components(separatedBy: ".")[1]
                if (decimal?.count)! < 2 {
                    var decimalValue: String = "00"
                    if decimal?.count == 1 {
                        decimalValue = "0"
                    }
                    textField.text = textField.text!+decimalValue
                }
            }
            else if (textField.text?.count)! > 0{
                textField.text = textField.text!+".00"
            }
        }
        avail_appliedMatchMoneyLbl.text = "$\(textField.text!)"
        cancelMatchMoneyBtn.isHidden = false
        let creditTotal = self.calculateCreditTotal()
        avail_creditTotalLbl.text = "$"+String(format: "%.2f", arguments:[creditTotal])
        return true
    }
    
    
    //MARK: - Network manager delegate functions.

    //Returns the web service call
    func webServiceResponse(for requestType: ServiceRequestType, responseData Data: Any!, withStatusCode statusCode: Int, errorMessage message: Any!) {
        LoadingView.shareInstance.hideLoadingView()
        if message as? Error != nil {
            AlertControl.sharedInstance.presentAlertView(alertTitle: "Something went wrong", alertMessage: "", delegate: self)
        }
        else{
            if !NetworkManager.sharedInstance.checkStatusCode(statusCode){
                let error = NetworkManager.sharedInstance.getErrorModel(requestType: requestType, httpStatusCode: statusCode, responseData: Data)
                AlertControl.sharedInstance.presentAlertView(alertTitle: (error.errorMessage!), alertMessage: "", delegate: self)
            }
            else{
                if requestType == .ServiceTypeOrderDetails {
                    orderDetailsDataModel = ResponseManager.sharedInstance.getOrderDetails(responseData: Data, orderType: .Sales)
                    self.loadWithSerialisedData()
                }
                else if requestType == .ServiceTypeSellerConfirmation {
                    NavigationModel.sharedInstance.homeControl?.backView.isHidden = true
                    if confirmOrderType == OrderedItemStatus.Available {
                        self.view.bringSubview(toFront: downloadPDFView)
                    }
                    else if confirmOrderType == OrderedItemStatus.Revised {
                        self.view.bringSubview(toFront: submit_MessageView)
                        self.submit_MessageView.bringSubview(toFront: submit_ReviseView)
                    }
                    else {
                        self.view.bringSubview(toFront: submit_MessageView)
                        self.submit_MessageView.bringSubview(toFront: submit_CancellationView)
                    }
                }
            }
        }
    }
}
