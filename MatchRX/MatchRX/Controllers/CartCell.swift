//
//  CartCell.swift
//  MatchRX
//
//  Created by Vignesh on 2/16/18.
//  Copyright © 2018 HTC. All rights reserved.
//

import UIKit

class CartCell: UITableViewCell {
    
    //MARK: - IBOutlet references
    
    @IBOutlet weak var medispanImageView: UIImageView!
    @IBOutlet weak var ndcName: UILabel!
    @IBOutlet weak var ndcNum_ManufacturerNameLbl: UILabel!
    @IBOutlet weak var partialPackView: UIView!
    @IBOutlet weak var quantityView: UIView!
    @IBOutlet weak var deleteButton: UIButton!
    @IBOutlet weak var priceLbl: UILabel!
    @IBOutlet weak var quantityButton: UIButton!
    @IBOutlet weak var quantityLbl: UILabel!
    @IBOutlet weak var partialLbl: UILabel!
    @IBOutlet weak var baseView: UIView!
    
    //MARK: - Overriding native functions.
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    override func layoutSubviews() {
        self.baseView.layer.masksToBounds = false
        self.baseView.layer.shadowOffset = CGSize(width: 3, height: 3)
        self.baseView.layer.shadowRadius = 1
        self.baseView.layer.shadowOpacity = 0.2
        
        
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
