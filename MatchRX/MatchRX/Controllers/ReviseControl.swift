//
//  ReviseControl.swift
//  MatchRX
//
//  Created by Vignesh on 5/14/18.
//  Copyright © 2018 HTC. All rights reserved.
//

import UIKit

protocol ReviseItemDelegate {
    func revisedItemDetails(itemDetails: OrderDetailsDataModel, forCell: SellerConfirmationTableViewCell)
}

class ReviseControl: UIViewController, UITextFieldDelegate {
    
    
    @IBOutlet weak var fullPackView: UIView!
    @IBOutlet weak var partialPackView: UIView!
    @IBOutlet weak var partialTxtField: UITextField!
    @IBOutlet weak var maxQuantityLbl: UILabel!
    @IBOutlet weak var fullQuantityLbl: UILabel!
    @IBOutlet weak var addQuantityBtn: UIButton!
    @IBOutlet weak var subtractQuantityBtn: UIButton!
    @IBOutlet weak var plusImgView: UIImageView!
    @IBOutlet weak var minusImgView: UIImageView!
    @IBOutlet weak var okButton: UIButton!
    
    var dataModel: OrderDetailsDataModel = OrderDetailsDataModel()
    var sellerCell: SellerConfirmationTableViewCell = SellerConfirmationTableViewCell()
    var parentDelegate: ReviseItemDelegate!

    override func viewDidLoad() {
        super.viewDidLoad()
        fullPackView.layer.cornerRadius = 5.0
        partialPackView.layer.cornerRadius = 5.0
        partialTxtField.layer.borderColor = UIColor.init(red: 255.0/255, green: 141.0/255, blue: 0.0/255, alpha: 1.0).cgColor
        if dataModel.packageOption?.lowercased() == "f" {
            fullPackView.isHidden = false
            partialPackView.isHidden = true
            fullQuantityLbl.text = "\(dataModel.originalQuantity!-1)"
            plusImgView.image = #imageLiteral(resourceName: "plus_default.png")
            addQuantityBtn.isEnabled = false
            if dataModel.originalQuantity!-1 > 1 {
                minusImgView.image = #imageLiteral(resourceName: "minus_enabled.png")
                subtractQuantityBtn.isEnabled = true
            }
            else {
                minusImgView.image = #imageLiteral(resourceName: "minus_disabled.png")
                subtractQuantityBtn.isEnabled = false
            }
        }
        else {
            fullPackView.isHidden = true
            partialPackView.isHidden = false
            partialTxtField.text = "\(dataModel.originalPackageSize!-1)"
            maxQuantityLbl.text = "(Max \(dataModel.originalPackageSize!-1))"
        }
        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func onCancelClicked(sender: UIButton) {
        self.dismiss(animated: true, completion: nil)
        /*if dataModel.packageOption?.lowercased() == "f" {
            dataModel.modifiedQuantity = Int(fullQuantityLbl.text!)
        }
        else {
            dataModel.modifiedPackageSize = Int(partialTxtField.text!)
        }*/
        self.parentDelegate.revisedItemDetails(itemDetails: dataModel, forCell: sellerCell)
    }
    
    @IBAction func onOkClicked(sender: UIButton) {
        self.dismiss(animated: true, completion: nil)
        if dataModel.packageOption?.lowercased() == "f" {
            dataModel.modifiedQuantity = Int(fullQuantityLbl.text!)
        }
        else {
            dataModel.modifiedPackageSize = Int(partialTxtField.text!)
        }
        self.parentDelegate.revisedItemDetails(itemDetails: dataModel, forCell: sellerCell)
    }
    
    @IBAction func onAddButtonClicked(sender: UIButton) {
        if addQuantityBtn.isEnabled {
//            dataModel.modifiedQuantity! += 1
            let qty = Int(fullQuantityLbl.text!)!+1
//            fullQuantityLbl.text = "\(dataModel.modifiedQuantity!)"
            fullQuantityLbl.text = "\(qty)"

            if qty < dataModel.originalQuantity!-1 {
                plusImgView.image = #imageLiteral(resourceName: "plus_enabled.png")
                addQuantityBtn.isEnabled = true
            }
            else {
                plusImgView.image = #imageLiteral(resourceName: "plus_default.png")
                addQuantityBtn.isEnabled = false
            }
            if qty > 1 {
                minusImgView.image = #imageLiteral(resourceName: "minus_enabled.png")
                subtractQuantityBtn.isEnabled = true
            }
            else {
                minusImgView.image = #imageLiteral(resourceName: "minus_disabled.png")
                subtractQuantityBtn.isEnabled = false
            }
        }
    }
    
    @IBAction func onSubtractButtonClicked(sender: UIButton) {
        if subtractQuantityBtn.isEnabled {
//            dataModel.modifiedQuantity! -= 1
            let qty = Int(fullQuantityLbl.text!)!-1
            fullQuantityLbl.text = "\(qty)"
            if qty > 1 {
                minusImgView.image = #imageLiteral(resourceName: "minus_enabled.png")
                subtractQuantityBtn.isEnabled = true
            }
            else {
                minusImgView.image = #imageLiteral(resourceName: "minus_disabled.png")
                subtractQuantityBtn.isEnabled = false
            }
            if qty < dataModel.originalQuantity!-1 {
                plusImgView.image = #imageLiteral(resourceName: "plus_enabled.png")
                addQuantityBtn.isEnabled = true
            }
            else {
                plusImgView.image = #imageLiteral(resourceName: "plus_default.png")
                addQuantityBtn.isEnabled = false
            }
        }
    }
    
    //MARK: - Text filed delegate methods.
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        if string.isEmpty {
            let reviseQty = textField.text?.replacingCharacters(in: Range(range, in: textField.text!)!, with: string)
            if (reviseQty?.isEmpty)! {
                okButton.isUserInteractionEnabled = false
                okButton.alpha = 0.5
            }
            else if Int(reviseQty!)! < 1 {
                okButton.isUserInteractionEnabled = false
                okButton.alpha = 0.5
            }
            else {
                okButton.isUserInteractionEnabled = true
                okButton.alpha = 1.0
            }
            return true
        }
//        let combinedString = textField.text?.replacingCharacters(in: Range(range, in: textField.text!)!, with: string)
        var text: String = string
        text = "\(textField.text ?? "")\(string)"
        if (text.rangeOfCharacter(from: CharacterSet.init(charactersIn: "0123456789").inverted) != nil)
        {
            return false
        }
        if Int(text)! > (dataModel.originalPackageSize!-1) {
            return false
        }
        if text.count > "\(dataModel.originalPackageSize!-1)".count {
            return false
        }
        if Int(text)! < 1 {
            okButton.isUserInteractionEnabled = false
            okButton.alpha = 0.5
        }
        else {
            okButton.isUserInteractionEnabled = true
            okButton.alpha = 1.0
        }
        return true
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
