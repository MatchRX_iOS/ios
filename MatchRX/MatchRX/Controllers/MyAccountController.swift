//
//  MyAccountController.swift
//  MatchRX
//
//  Created by Vignesh on 11/21/17.
//  Copyright © 2017 HTC. All rights reserved.
//

import UIKit

class MyAccountController: UIViewController, NetworkManagerDelegate, UITableViewDelegate, UITableViewDataSource {
    
    //MARK: - IBOutlet references
    
    @IBOutlet var pharmacyNameLbl: UILabel!
    @IBOutlet var userNameLbl: UILabel!
    @IBOutlet var deaNumberLbl: UILabel!
    @IBOutlet var emailIDLbl: UILabel!
    @IBOutlet var selectedImage: UIImageView!
    @IBOutlet var pharmacyInfoTblView: UITableView!
    @IBOutlet var leadingConstraint: NSLayoutConstraint!
    @IBOutlet var imageWidthConstraint: NSLayoutConstraint!
    
    var whiteView: UIView!
    var pharmacyDetailsArray: NSMutableArray!
    var ownerDetailsArray: NSMutableArray!
    var licenseDetailsArray: NSMutableArray!
    var dataArray: NSMutableArray!
    var selectedButton: UIButton?

    override func viewDidLoad() {
        super.viewDidLoad()
        pharmacyDetailsArray = NSMutableArray.init()
        ownerDetailsArray = NSMutableArray.init()
        licenseDetailsArray = NSMutableArray.init()
        dataArray = NSMutableArray.init()
        
        deaNumberLbl.layer.borderWidth = 1.0
        deaNumberLbl.layer.cornerRadius = 4.0
        deaNumberLbl.layer.borderColor = UIColor(red: 255.0/255.0, green: 148.0/255.0, blue: 0.0/255.0, alpha: 1.0).cgColor
        selectedButton = self.view.viewWithTag(100) as? UIButton
//        whiteView = UIView.init(frame: self.view.bounds)
//        whiteView.backgroundColor = UIColor.white
//        self.view.addSubview(whiteView)
//        self.view.bringSubview(toFront: whiteView)
        LoadingView.shareInstance.showWhiteView(parentView: self.view)
        
        let leftSwipeGesture = UISwipeGestureRecognizer.init(target: self, action: #selector(self.onSwipe(gesture:)))
        leftSwipeGesture.direction = UISwipeGestureRecognizerDirection.left
        pharmacyInfoTblView.addGestureRecognizer(leftSwipeGesture)
        
        let rightSwipeGesture = UISwipeGestureRecognizer.init(target: self, action: #selector(self.onSwipe(gesture:)))
        rightSwipeGesture.direction = UISwipeGestureRecognizerDirection.right
        pharmacyInfoTblView.addGestureRecognizer(rightSwipeGesture)
        
        if NetworkManager.sharedInstance.isReachable(){
            _ = NetworkManager.sharedInstance.checkTypeOfNetwork()
            if NetworkManager.sharedInstance.isReachableViaWiFi || NetworkManager.sharedInstance.isReachableViaCellular{
                LoadingView.shareInstance.showLodingView(parentView: (NavigationModel.sharedInstance.homeControl?.view.window)!)
                NetworkManager.sharedInstance.manageAPIRequest(requestType: .ServiceTypePharmacyInfo, requestString: RequestDataModel.sharedInstance.getPharmacyInformation(), delegate: self)
            }
        }
        else{
            AlertControl.sharedInstance.presentAlertView(alertTitle: "Kindly check your internet connectivity", alertMessage: "", delegate: nil)
        }
        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    //MARK: - Instance functions
    
    func onSwipe(gesture: UISwipeGestureRecognizer){
        let tag = selectedButton?.tag
        if gesture.direction == UISwipeGestureRecognizerDirection.right {
            print("Right swipe")
            if tag != 100{
                selectedButton = self.view.viewWithTag(tag!-1) as? UIButton
                UIView.animate(withDuration: 0.1, delay: 0.0, options: .transitionFlipFromLeft, animations: {
                    self.menuButtonTapped(sender: self.selectedButton!)
                    self.view.layoutIfNeeded()
                }) { (animationComplete) in
                    print("The animation is complete!")
                }
            }
        }
        else{
            print("Left")
            if tag != 102{
                selectedButton = self.view.viewWithTag(tag!+1) as? UIButton
                UIView.animate(withDuration: 0.1, delay: 0.0, options: .transitionFlipFromRight, animations: {
                    self.menuButtonTapped(sender: self.selectedButton!)
                    self.view.layoutIfNeeded()
                }) { (animationComplete) in
                    print("The animation is complete!")
                }
            }
        }
    }
    
    //Decides which tab is selected.
    @IBAction func menuButtonTapped(sender: UIButton){
        selectedButton = sender
        if dataArray.count > 0 {
            dataArray.removeAllObjects()
            dataArray = nil
        }
        leadingConstraint.constant = sender.frame.origin.x
//        imageWidthConstraint.constant = sender.frame.size.width
        switch sender.tag {
        case 100:
            dataArray = NSMutableArray.init(array: pharmacyDetailsArray)
            break
            
        case 101:
            dataArray = NSMutableArray.init(array: ownerDetailsArray)
            break
            
        case 102:
            dataArray = NSMutableArray.init(array: licenseDetailsArray)
            break
            
        default:
            break
        }
        if dataArray.count != 0 {
            self.pharmacyInfoTblView.reloadData()
            self.pharmacyInfoTblView.scrollToRow(at: IndexPath(row: 0, section: 0), at: UITableViewScrollPosition.top, animated: false)
        }
    }
    
    //Serializes the pharmacy model object, appends the cell type and loads in array
    func preparePharmacyList(pharmacyModel: PharmacyDataModel){
        
        let legalBusinessModel = PharmacyDataModel()
        legalBusinessModel.celltype = CellType.cellTypeSingleColumn
        legalBusinessModel.firstLabelText = "Legal Business Name"
        legalBusinessModel.firstTxtFieldText = pharmacyModel.legalBusinessName
        pharmacyDetailsArray.add(legalBusinessModel)
        
        let doingBusinessModel = PharmacyDataModel()
        doingBusinessModel.celltype = CellType.cellTypeSingleColumn
        doingBusinessModel.firstLabelText = "Doing Business As (DBA)"
        doingBusinessModel.firstTxtFieldText = pharmacyModel.doingBusinessName
        pharmacyDetailsArray.add(doingBusinessModel)
        
        let shippingAddress1 = PharmacyDataModel()
        shippingAddress1.celltype = CellType.cellTypeSingleColumn
        shippingAddress1.firstLabelText = "Shipping Address 1"
        shippingAddress1.firstTxtFieldText = pharmacyModel.shippingAddress1
        pharmacyDetailsArray.add(shippingAddress1)
        
        let shippingAddress2 = PharmacyDataModel()
        shippingAddress2.celltype = CellType.cellTypeSingleColumn
        shippingAddress2.firstLabelText = "Shipping Address 2"
        shippingAddress2.firstTxtFieldText = pharmacyModel.shippingAddress2
        pharmacyDetailsArray.add(shippingAddress2)
        
        let shippingModel1 = PharmacyDataModel()
        shippingModel1.celltype = CellType.cellTypeDoubleColumn
        shippingModel1.firstLabelText = "Shipping City"
        shippingModel1.secondLabelText = "Shipping State"
        shippingModel1.firstTxtFieldText = pharmacyModel.shippingCity
        shippingModel1.secondTxtFieldText = pharmacyModel.shippingState
        pharmacyDetailsArray.add(shippingModel1)
        
        let shippingModel2 = PharmacyDataModel()
        shippingModel2.celltype = CellType.cellTypeDoubleColumn
        shippingModel2.firstLabelText = "Shipping Zip Code"
        shippingModel2.secondLabelText = "Phone"
        shippingModel2.firstTxtFieldText = pharmacyModel.shippingZipCode
        shippingModel2.secondTxtFieldText = pharmacyModel.shippingPhone
        pharmacyDetailsArray.add(shippingModel2)
        
        let faxModel = PharmacyDataModel()
        faxModel.celltype = CellType.cellTypeSingleColumn
        faxModel.firstLabelText = "Fax"
        faxModel.firstTxtFieldText = pharmacyModel.shippingFax
        pharmacyDetailsArray.add(faxModel)
        
        let mailingAddress1 = PharmacyDataModel()
        mailingAddress1.celltype = CellType.cellTypeSingleColumn
        mailingAddress1.firstLabelText = "Mailing Address 1"
        mailingAddress1.firstTxtFieldText = pharmacyModel.mailingAddress1
        pharmacyDetailsArray.add(mailingAddress1)
        
        let mailingAddress2 = PharmacyDataModel()
        mailingAddress2.celltype = CellType.cellTypeSingleColumn
        mailingAddress2.firstLabelText = "Mailing Address 2"
        mailingAddress2.firstTxtFieldText = pharmacyModel.mailingAddress2
        pharmacyDetailsArray.add(mailingAddress2)
        
        let mailingModel = PharmacyDataModel()
        mailingModel.celltype = CellType.cellTypeDoubleColumn
        mailingModel.firstLabelText = "Mailing City"
        mailingModel.secondLabelText = "Mailing State"
        mailingModel.firstTxtFieldText = pharmacyModel.mailingCity
        mailingModel.secondTxtFieldText = pharmacyModel.mailingState
        pharmacyDetailsArray.add(mailingModel)
        
        let mailZipCode = PharmacyDataModel()
        mailZipCode.celltype = CellType.cellTypeSingleColumn
        mailZipCode.firstLabelText = "Mailing Zip Code"
        mailZipCode.firstTxtFieldText = pharmacyModel.mailingZipCode
        pharmacyDetailsArray.add(mailZipCode)
        
        let firstNameModel = PharmacyDataModel()
        firstNameModel.celltype = CellType.cellTypeSingleColumn
        firstNameModel.firstLabelText = "First Name"
        firstNameModel.firstTxtFieldText = pharmacyModel.ownerFirstName
        ownerDetailsArray.add(firstNameModel)
        
        let lastNameModel = PharmacyDataModel()
        lastNameModel.celltype = CellType.cellTypeSingleColumn
        lastNameModel.firstLabelText = "Last Name"
        lastNameModel.firstTxtFieldText = pharmacyModel.ownerLastName
        ownerDetailsArray.add(lastNameModel)
        
        let mobileNoModel = PharmacyDataModel()
        mobileNoModel.celltype = CellType.cellTypeSingleColumn
        mobileNoModel.firstLabelText = "Mobile Number"
        mobileNoModel.firstTxtFieldText = pharmacyModel.ownerMobileNumber
        ownerDetailsArray.add(mobileNoModel)
        
        let emailIDModel = PharmacyDataModel()
        emailIDModel.celltype = CellType.cellTypeSingleColumn
        emailIDModel.firstLabelText = "Email Address"
        emailIDModel.firstTxtFieldText = pharmacyModel.ownerEmailID
        ownerDetailsArray.add(emailIDModel)
        
        let NCPDPModel = PharmacyDataModel()
        NCPDPModel.celltype = CellType.cellTypeSingleColumn
        NCPDPModel.firstLabelText = "NCPDP Number"
        NCPDPModel.firstTxtFieldText = pharmacyModel.ncpdpNumber
        licenseDetailsArray.add(NCPDPModel)
        
        let DEALicenseModel = PharmacyDataModel()
        DEALicenseModel.celltype = CellType.cellTypeSingleColumn
        DEALicenseModel.firstLabelText = "DEA License Number"
        DEALicenseModel.firstTxtFieldText = pharmacyModel.deaNumber
        licenseDetailsArray.add(DEALicenseModel)
        
        let DEAExpiryModel = PharmacyDataModel()
        DEAExpiryModel.celltype = CellType.cellTypeSingleColumn
        DEAExpiryModel.firstLabelText = "DEA Expiration Date"
        DEAExpiryModel.firstTxtFieldText = pharmacyModel.deaExpiryDate
        licenseDetailsArray.add(DEAExpiryModel)
        
        let npiModel = PharmacyDataModel()
        npiModel.celltype = CellType.cellTypeSingleColumn
        npiModel.firstLabelText = "NPI Number"
        npiModel.firstTxtFieldText = "\(pharmacyModel.npiNumber ?? 0)"
        licenseDetailsArray.add(npiModel)
        
        let stateLicenseModel = PharmacyDataModel()
        stateLicenseModel.celltype = CellType.cellTypeSingleColumn
        stateLicenseModel.firstLabelText = "State License Number"
        stateLicenseModel.firstTxtFieldText = pharmacyModel.stateLicenseNumber
        licenseDetailsArray.add(stateLicenseModel)
        
        let stateLicenseExpModel = PharmacyDataModel()
        stateLicenseExpModel.celltype = CellType.cellTypeSingleColumn
        stateLicenseExpModel.firstLabelText = "State License Expires"
        stateLicenseExpModel.firstTxtFieldText = pharmacyModel.licenseExpiryDate
        licenseDetailsArray.add(stateLicenseExpModel)
        
        dataArray = NSMutableArray.init(array: pharmacyDetailsArray)
        
        if dataArray.count != 0 {
            self.pharmacyInfoTblView.reloadData()
        }
    }
    
    //MARK: - Table view delegate and datasoure implementation
    
    //Returns the number of sections in tableview
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    //Returns the number of rows in tableview
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return dataArray.count
    }
    
    //Returns the height of the row
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 70.0
    }
    
    //Creates and manages the cell
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        var singleColumnCell:SingleColumnTextFieldCell
        singleColumnCell = tableView.dequeueReusableCell(withIdentifier: "singleColumnCell") as! SingleColumnTextFieldCell
        //        singleColumnCell = tableView.dequeueReusableCell(withIdentifier: "singleColumnCell", for: indexPath) as! SingleColumnTextFieldCell
//        singleColumnCell = SingleColumnTextFieldCell(style: UITableViewCellStyle.default, reuseIdentifier: "singleColumnCell")
        if dataArray.count > indexPath.row {
            let pharmacyModel: PharmacyDataModel = dataArray.object(at: indexPath.row) as! PharmacyDataModel
            if pharmacyModel.celltype == CellType.cellTypeSingleColumn{
                singleColumnCell.labelValue.text = pharmacyModel.firstLabelText
                singleColumnCell.textFieldValue.text = pharmacyModel.firstTxtFieldText
//                singleColumnCell.labelValue.text = "pharmacyModel.firstLabelText"
//                singleColumnCell.textFieldValue.text = "pharmacyModel.firstTxtFieldText"
            }
            else if pharmacyModel.celltype == CellType.cellTypeDoubleColumn{
                let doubleColumnCell = tableView.dequeueReusableCell(withIdentifier: "doubleColumnCell") as! DoubleColumnTextFieldCell
                //                let doubleColumnCell = tableView.dequeueReusableCell(withIdentifier: "doubleColumnCell", for: indexPath) as! DoubleColumnTextFieldCell
//                let doubleColumnCell = DoubleColumnTextFieldCell(style: UITableViewCellStyle.default, reuseIdentifier: "doubleColumnCell")
                doubleColumnCell.firstlabelValue.text = pharmacyModel.firstLabelText
                doubleColumnCell.secondLabelValue.text = pharmacyModel.secondLabelText
                doubleColumnCell.firstTextFieldValue.text = pharmacyModel.firstTxtFieldText
                doubleColumnCell.secondTextFieldValue.text = pharmacyModel.secondTxtFieldText
                doubleColumnCell.layoutIfNeeded()
                return doubleColumnCell
            }
        }
        singleColumnCell.layoutIfNeeded()
        return singleColumnCell
    }
    
    //MARK: - Network manager delegate functions.
    
    //Returns the response
    func webServiceResponse(for requestType: ServiceRequestType, responseData Data: Any!, withStatusCode statusCode: Int, errorMessage message: Any!) {
        LoadingView.shareInstance.hideLoadingView()
        if message as? Error != nil {
            AlertControl.sharedInstance.presentAlertView(alertTitle: "Something went wrong", alertMessage: "", delegate: self)
        }
        else{
            if !NetworkManager.sharedInstance.checkStatusCode(statusCode){
                let error = NetworkManager.sharedInstance.getErrorModel(requestType: requestType, httpStatusCode: statusCode, responseData: Data)
                AlertControl.sharedInstance.presentAlertView(alertTitle: (error.errorMessage!), alertMessage: "", delegate: self)
            }
            else{
                let pharmacyObjectModel = ResponseManager.sharedInstance.getPharmacyData(responseData: Data)
//                whiteView.removeFromSuperview()
//                whiteView = nil
                LoadingView.shareInstance.hideWhiteView()
                pharmacyNameLbl.text = pharmacyObjectModel.legalBusinessName
                userNameLbl.text = "\(pharmacyObjectModel.loggedUserFirstName ?? "") \(pharmacyObjectModel.loggedUserLastName ?? "")"
                deaNumberLbl.text = "DEA# \(pharmacyObjectModel.deaNumber ?? "")"
                emailIDLbl.text = pharmacyObjectModel.userEmailID
                self.preparePharmacyList(pharmacyModel: pharmacyObjectModel)
                
            }
        }
    }
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
