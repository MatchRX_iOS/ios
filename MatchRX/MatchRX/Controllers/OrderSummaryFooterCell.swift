//
//  OrderSummaryFooterCell.swift
//  MatchRX
//
//  Created by Poonkathirvelan on 21/02/18.
//  Copyright © 2018 HTC. All rights reserved.
//

import UIKit

class OrderSummaryFooterCell: UITableViewCell {

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    
    @IBOutlet weak var shippingTypeLbl: UILabel!
    @IBOutlet weak var shippingTypeImg: UIImageView!
    @IBOutlet weak var shippingAmt: UILabel!
    @IBOutlet weak var shippingCostLbl: UILabel!
    @IBOutlet weak var totalAmtLbl: UILabel!
    @IBOutlet weak var itemTotalLbl: UILabel!
    @IBOutlet weak var sellerNameLbl: UILabel!
    

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
