//
//  DoubleColumnTextFieldCell.swift
//  MatchRX
//
//  Created by Vignesh on 11/23/17.
//  Copyright © 2017 HTC. All rights reserved.
//

import UIKit

class DoubleColumnTextFieldCell: UITableViewCell {

    @IBOutlet var firstlabelValue: UILabel!
    @IBOutlet var secondLabelValue: UILabel!
    @IBOutlet var firstTextFieldValue: UITextField!
    @IBOutlet var secondTextFieldValue: UITextField!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
