//
//  BuyListViewController.swift
//  MatchRX
//
//  Created by Poonkathirvelan on 29/01/18.
//  Copyright © 2018 HTC. All rights reserved.
//

import UIKit

class BuyListViewController: UIViewController,UITableViewDelegate,UITableViewDataSource, NetworkManagerDelegate, URLSessionDelegate, EditPostingDelegate, ItemDetailsDelegate {

    //MARK: - IBOutlet references
    @IBOutlet  var buyListView: UITableView!
    @IBOutlet weak var multipleSellerView: UIView!
    @IBOutlet weak var multipleSellerViewHeightConstraint: NSLayoutConstraint!
    @IBOutlet weak var sellerBtn: UIButton!
    @IBOutlet weak var marketplaceBtn: UIButton!
    @IBOutlet weak var contentView: UIView!
    @IBOutlet weak var messageLbl: UILabel!
    
    let MULITPLE_SELLER_TAB_VIEW_HEIGHT: CGFloat = 35.0
    
    var alertMessageControl: StaticMessageController!
    var refreshControl: UIRefreshControl!
    var isRefreshHidden: Bool?
    var isActiveMenu: String?
    //MARK: - Global declarations
    var marketPlaceDataModelArr: NSMutableArray?
    var detailControl: ItemDetailViewController!
    var buyListArray: NSMutableArray = NSMutableArray.init()
    var sellerSwitchArray: NSMutableArray = NSMutableArray.init()
    var pageCount: Int = 0
    var dataPerPage: Int = 20
    
    var imageCache = NSCache<AnyObject, UIImage>()
    var operationQueue = OperationQueue()
    var operationListArray = Array<String>()
    var selectedIndexPath: IndexPath?
    var isPaginationCalled: Bool = false
    var postSellerArray = NSMutableArray.init()
    
    //MARK: - Overriding functions
    
    override func viewDidLoad() {
        super.viewDidLoad()
//        fetchMatketplaceDataService()
        // Do any additional setup after loading the view.
        refreshControl = UIRefreshControl()
        refreshControl.attributedTitle = NSAttributedString(string: "")
        refreshControl.addTarget(self, action: #selector(self.refreshTableView), for: UIControlEvents.valueChanged)
        isRefreshHidden = true
        buyListView.addSubview(refreshControl)
        
        multipleSellerViewHeightConstraint.constant = 0.0
        
//        self.showHideMultipleSellerTabView()
        
    }
    
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    //MARK: - IBAction functions
    
    //On clicking the seller tab
    @IBAction func onSellerTabClicked(sender: UIButton) {
        if sender.isSelected {
            sender.isSelected = false
            sender.backgroundColor = UIColor.white
            sender.setTitleColor(UIColor(red: 88.0/255.0, green: 89.0/255.0, blue: 91.0/255.0, alpha: 1.0), for: .normal)
//            sender.titleLabel?.textColor = UIColor(red: 88.0/255.0, green: 89.0/255.0, blue: 91.0/255.0, alpha: 1.0)
        }
        else {
            sender.isSelected = true
            sender.backgroundColor = UIColor(red: 255.0/255.0, green: 148.0/255.0, blue: 0.0/255.0, alpha: 1.0)
            sender.setTitleColor(UIColor.white, for: .selected)
        }
        let sellerArray = NSMutableArray.init()
        if marketplaceBtn.isSelected {
            let sellerDict = NSMutableDictionary.init()
            sellerDict.setValue("all", forKey: "seller")
            sellerArray.add(sellerDict)
        }
        if sellerBtn.isSelected {
            let sellerDict = NSMutableDictionary.init()
            sellerDict.setValue(sellerBtn.tag, forKey: "seller")
            sellerArray.add(sellerDict)
        }
        if sellerArray.count == 0 {
            buyListArray.removeAllObjects()
            buyListView.reloadData()
            buyListView.isHidden = true
            messageLbl.text = "No items displayed"
        }
        else {
            postSellerArray = sellerArray
//            GlobalData.sharedInstance.sellerArray = sellerArray
            pageCount =  0
            buyListArray.removeAllObjects()
            buyListView.reloadData()
            buyListView.isHidden = false
            self.fetchMatketplaceDataService()
        }
    }
    
    //Scroll the list to the top
    @IBAction func onScrolToTopClicked(sender: UIButton) {
        buyListView.setContentOffset(CGPoint.zero, animated: true)
    }
    
    //Refresh the table view to get the new values.
    func refreshTableView() {
        if NetworkManager.sharedInstance.isReachable(){
            isRefreshHidden = false
            pageCount = 0
            buyListArray.removeAllObjects()
            self.fetchMatketplaceDataService()
        }
        else{
            refreshControl.endRefreshing()
            buyListView.setContentOffset(CGPoint.zero, animated: true)
            AlertControl.sharedInstance.presentAlertView(alertTitle: "Kindly check your internet connectivity", alertMessage: "", delegate: nil)
        }
    }
    
    //MARK: - Instance functions
    
    
    func showHideMultipleSellerTabView() {
        let cartArray = GlobalData.sharedInstance.marketplaceModel.cartArray
        for i in 0..<cartArray.count {
            let cartModel = cartArray.object(at: i) as! CartSellerDataModel
            if cartModel.isActive! {
                sellerBtn.tag = cartModel.sellerId!
                let sellerName = "\(cartModel.sellerName!)"
                sellerBtn.setTitle(sellerName, for: .normal)
                sellerBtn.isSelected = true
                sellerBtn.backgroundColor = UIColor(red: 255.0/255.0, green: 148.0/255.0, blue: 0.0/255.0, alpha: 1.0)
                sellerBtn.setTitleColor(UIColor.white, for: .selected)
                multipleSellerViewHeightConstraint.constant = MULITPLE_SELLER_TAB_VIEW_HEIGHT
                break
            }
        }
    }
    
    //Initializes the Edit posting controller.
    func onEditPostingClicked(sender: UIButton) {
        let dataModel = buyListArray.object(at: sender.tag) as! BuyListDataModel
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let editPostControl = storyboard.instantiateViewController(withIdentifier: "editPostControl") as! EditPostingControl
        NavigationModel.sharedInstance.homeControl?.contentNavController.pushViewController(editPostControl, animated: true)
        NavigationModel.sharedInstance.homeControl?.showBackIconWithTitle(title: "EDIT POSTING")
        editPostControl.parentDelegate = self
        editPostControl.postId = "\(dataModel.postId!)"
    }
    
    //Returns the request parameters with the added filter and sort keys.
    func getRequestData() -> NSMutableDictionary{
        let requestDict = NSMutableDictionary.init()
        let filterDict = NSMutableDictionary.init()
        
        filterDict.setValue("A", forKey: "package_option")
        filterDict.setValue("A", forKey: "original_package")
        filterDict.setValue("A", forKey: "brand")
        filterDict.setValue("A", forKey: "expire_date")
        filterDict.setValue("A", forKey: "wac_discount")
        filterDict.setValue("A", forKey: "created_at")
        
        let sortDict = NSMutableDictionary.init()
        sortDict.setValue("desc", forKey: "updated_at")
        
//        requestDict.setValue(filterDict, forKey: "filter")
//        requestDict.setValue(sortDict, forKey: "sort")
        requestDict.setValue(GlobalData.sharedInstance.filterDictionary, forKey: "filter")
        requestDict.setValue(GlobalData.sharedInstance.sortDictionary, forKey: "sort")
        requestDict.setValue(GlobalData.sharedInstance.searchText, forKey: "search")
        if !(isActiveMenu?.isEmpty)! {
            requestDict.setValue(isActiveMenu, forKey: "is_active_menu")
        }
        requestDict.setValue(dataPerPage, forKey: "per_page")
        requestDict.setValue(pageCount, forKey: "page")
        if GlobalData.sharedInstance.cartArray.count > 0 {
            requestDict.setValue(GlobalData.sharedInstance.cartArray, forKey: "cart_seller_ids")
        }
//        if sellerSwitchArray.count > 0 {
//            requestDict.setValue(sellerSwitchArray, forKey: "posts_from_seller")
//        }
//        else {
            if GlobalData.sharedInstance.sellerArray.count > 0 {
                if postSellerArray.count == 0 {
                    requestDict.setValue(GlobalData.sharedInstance.sellerArray, forKey: "posts_from_seller")
                }
                else {
                    requestDict.setValue(postSellerArray, forKey: "posts_from_seller")
                }
            }
//        }
        return requestDict
    }
    
    
    //Creates the request to fetch the data.
    func fetchMatketplaceDataService() {
        
        if NetworkManager.sharedInstance.isReachable(){
            _ = NetworkManager.sharedInstance.checkTypeOfNetwork()
            if NetworkManager.sharedInstance.isReachableViaWiFi || NetworkManager.sharedInstance.isReachableViaCellular{
                LoadingView.shareInstance.showLodingView(parentView: (NavigationModel.sharedInstance.homeControl?.view!)!)
                NetworkManager.sharedInstance.manageAPIRequest(requestType: .ServiceTypeBuyMarketplace, requestString: RequestDataModel.sharedInstance.makeRequestForBuyList(requestDict: self.getRequestData()), delegate: self)
            }
        }
        else{
            AlertControl.sharedInstance.presentAlertView(alertTitle: "Kindly check your internet connectivity", alertMessage: "", delegate: nil)
        }
    }
    
    //Loads the response data
    func loadMarketplaceData(marketPlaceData: MarketPlaceDataModel, requestType: ServiceRequestType) {
        NavigationModel.sharedInstance.homeControl?.cartCountLbl.text = "\(GlobalData.sharedInstance.cartCount!)"
        if GlobalData.sharedInstance.cartCount! > 0 {
            NavigationModel.sharedInstance.homeControl?.cartCountLbl.text = "\(GlobalData.sharedInstance.cartCount!)"
            NavigationModel.sharedInstance.homeControl?.cartCountLbl.isHidden = false
        }
        else {
            NavigationModel.sharedInstance.homeControl?.cartCountLbl.text = ""
            NavigationModel.sharedInstance.homeControl?.cartCountLbl.isHidden = true
        }
        let responseArray = marketPlaceData.marketPlaceArray
        for i in 0..<responseArray.count {
            //                    let dataModel = responseArray.object(at: i) as! BuyListDataModel
            buyListArray.add(responseArray.object(at: i))
        }
        if responseArray.count > 0{
            buyListView.reloadData()
            if pageCount == 0 {
                buyListView.scrollsToTop = true
                let indexPath = IndexPath(row: 0, section: 0)
                buyListView.scrollToRow(at: indexPath, at: .top, animated: true)
            }
        }
        else if buyListArray.count == 0{
            buyListView.isHidden = true
            buyListView.reloadData()
            messageLbl.text = marketPlaceData.message!
        }
        if buyListArray.count > 0 {
            buyListView.isHidden = false
        }
        else {
            buyListView.isHidden = true
            buyListView.reloadData()
        }
        
        //        let cartArray = marketPlaceData.cartArray
        let cartArray = GlobalData.sharedInstance.marketplaceModel.cartArray
        var lastActive: CartSellerDataModel!
        var activeObject: CartSellerDataModel!
        if cartArray.count == 0 {
            multipleSellerViewHeightConstraint.constant = 0
        }
        for i in 0..<cartArray.count {
            let cartModel = cartArray.object(at: i) as! CartSellerDataModel
            if cartModel.isLastActive {
                lastActive = cartModel
            }
            if cartModel.isActive! {
                if cartModel.commonSellerId == "all" {
                    marketplaceBtn.isSelected = true
                    marketplaceBtn.backgroundColor = UIColor(red: 255.0/255.0, green: 148.0/255.0, blue: 0.0/255.0, alpha: 1.0)
                    marketplaceBtn.setTitleColor(UIColor.white, for: .selected)
                }
                else {
                    activeObject = cartModel
                }
                multipleSellerViewHeightConstraint.constant = MULITPLE_SELLER_TAB_VIEW_HEIGHT
                //                break
            }
        }
        if (activeObject != nil) {
            sellerBtn.tag = Int(activeObject.commonSellerId!)!
            let sellerName = "\(activeObject.sellerName!)"
            sellerBtn.setTitle(sellerName, for: .normal)
            sellerBtn.isSelected = true
            sellerBtn.backgroundColor = UIColor(red: 255.0/255.0, green: 148.0/255.0, blue: 0.0/255.0, alpha: 1.0)
            sellerBtn.setTitleColor(UIColor.white, for: .selected)
        }
        else if (lastActive != nil) {
            sellerBtn.tag = Int(lastActive.commonSellerId!)!
            let sellerName = "\(lastActive.sellerName!)"
            sellerBtn.setTitle(sellerName, for: .normal)
            if !marketplaceBtn.isSelected {
                sellerBtn.isSelected = true
                sellerBtn.backgroundColor = UIColor(red: 255.0/255.0, green: 148.0/255.0, blue: 0.0/255.0, alpha: 1.0)
                sellerBtn.setTitleColor(UIColor.white, for: .selected)
            }
        }
        else {
            sellerBtn.isSelected = true
            sellerBtn.backgroundColor = UIColor(red: 255.0/255.0, green: 148.0/255.0, blue: 0.0/255.0, alpha: 1.0)
            sellerBtn.setTitleColor(UIColor.white, for: .selected)
        }
    }
    
    //Downloads the image from URL
    func downloadImage(drugModel: BuyListDataModel) {
        let config = URLSessionConfiguration.default
        let session = URLSession(configuration: config, delegate: self, delegateQueue: nil)
        let getImageTask = session.downloadTask(with: URL(string: drugModel.medispanImageURL!)!) { (url, response, error) in
            if url != nil{
                let data = try? Data(contentsOf: url!)
                let downloadedImage  = UIImage(data: data!)
                if (downloadedImage != nil) {
                    self.imageCache.setObject(downloadedImage!, forKey: drugModel.postId!
                        as AnyObject)
                }
                self.buyListView.performSelector(onMainThread: #selector(UITableView.reloadData), with: nil, waitUntilDone: false)
            }
            
        }
        getImageTask.resume()
    }
    
    //MARK: - Network manager delegate function
    
    //Returns the web service call
    func webServiceResponse(for requestType: ServiceRequestType, responseData Data: Any!, withStatusCode statusCode: Int, errorMessage message: Any!) {
        if !isRefreshHidden! {
            isRefreshHidden = true
            refreshControl.endRefreshing()
        }
        if requestType == .ServiceTypeBuyMarketplace {
            isPaginationCalled = false
        }
        LoadingView.shareInstance.hideLoadingView()
        if message as? Error != nil {
            AlertControl.sharedInstance.presentAlertView(alertTitle: "Something went wrong", alertMessage: "", delegate: self)
        }
        else{
            if !NetworkManager.sharedInstance.checkStatusCode(statusCode){
                let error = NetworkManager.sharedInstance.getErrorModel(requestType: requestType, httpStatusCode: statusCode, responseData: Data)
                AlertControl.sharedInstance.presentAlertView(alertTitle: (error.errorMessage!), alertMessage: "", delegate: self)
            }
            else{
                let marketPlaceData = ResponseManager.sharedInstance.getMarketPlaceListData(responseData: Data, requestType: requestType)
                self.loadMarketplaceData(marketPlaceData: marketPlaceData, requestType: requestType)
            }
        }
    }
    
    
    //MARK: - Edit posting delegate called.
    func reloadListData() {
        buyListArray.removeAllObjects()
        buyListView.reloadData()
        pageCount = 0
        multipleSellerViewHeightConstraint.constant = 0.0
//        postSellerArray.removeAllObjects()
//        marketplaceBtn.isSelected = false
//        marketplaceBtn.backgroundColor = UIColor.white
//        marketplaceBtn.setTitleColor(UIColor(red: 88.0/255.0, green: 89.0/255.0, blue: 91.0/255.0, alpha: 1.0), for: .normal)
//        buyListView.scrollsToTop = true
//        buyListView.setContentOffset(CGPoint.zero, animated: true)
//        let indexPath = IndexPath(row: 0, section: 0)
//        buyListView.scrollToRow(at: indexPath, at: .top, animated: true)
        self.fetchMatketplaceDataService()
    }
    
    //MARK: - Items detail delegate function implementation.
    func loadResponseData(responseData: MarketPlaceDataModel, requestType: ServiceRequestType) {
        buyListArray.removeAllObjects()
        buyListView.reloadData()
        pageCount = 0
        buyListView.scrollsToTop = true
        postSellerArray.removeAllObjects()
        marketplaceBtn.isSelected = false
        marketplaceBtn.backgroundColor = UIColor.white
        marketplaceBtn.setTitleColor(UIColor(red: 88.0/255.0, green: 89.0/255.0, blue: 91.0/255.0, alpha: 1.0), for: .normal)
        self.loadMarketplaceData(marketPlaceData: responseData, requestType: requestType)
    }
    
    func reloadBuyListFromItemDetail() {
        self.reloadListData()
    }
    
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
     // Get the new view controller using segue.destinationViewController.
     // Pass the selected object to the new view controller.
     }
     */
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if indexPath.row <= buyListArray.count {
            let dataModel = buyListArray.object(at: indexPath.row) as! BuyListDataModel
            if dataModel.isMyposting! || !(dataModel.lastBoughtDate?.isEmpty)! {
                return 150.0
            }
            else {
                return 130.0
            }
        }
        return 130.0
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return buyListArray.count;
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "buycellId", for: indexPath) as!BuyListViewCell
        
        if indexPath.row < buyListArray.count {
            let dataModel = buyListArray.object(at: indexPath.row) as! BuyListDataModel
            if dataModel.isInCart! {
                cell.baseView.backgroundColor = UIColor(red: 255.0/255.0, green: 239.0/255.0, blue: 222.0/255.0, alpha: 1.0)
            }
            else {
                cell.baseView.backgroundColor = UIColor.white
            }
            cell.editBtn.isHidden = true
            cell.wishlishBtn.isHidden = true
            cell.stripLbl.isHidden = true
            cell.ratingView.isHidden = true
            cell.shippingPromoView.isHidden = true
            cell.yourPostingLbl.isHidden = true
            if dataModel.imageCount != 0 {
                cell.stripLbl.isHidden = false
            }
            cell.drugNameLbl.text = dataModel.drugName
            cell.ndcNumberManufacturerLbl.text = "\(dataModel.ndcNumber!) | \(dataModel.manufacturerName!)"
            cell.expiryDateLbl.text = dataModel.expiryDate
            cell.strengthLbl.text = dataModel.strength
            cell.packagingLbl.text = dataModel.packaging
            cell.discountLbl.text = "\(dataModel.discount!)%"
//            cell.priceLbl.text = "$\(dataModel.currentPrice!)"
            
//            let currentPrice = String(format: "%.2f", arguments: [dataModel.currentPrice!])
            let revisedPrice = String(format: "%.2f", arguments: [dataModel.revisedPrice!])
            //            cell.priceLbl.text = "$\(dataModel.currentPrice!)"
            cell.priceLbl.text = "$\(revisedPrice)"
            
            if dataModel.packageOption == "F" {
                cell.partialImgView.isHidden = true
                cell.packagingLbl.frame.origin.x = 0.0
            }
            else {
                cell.partialImgView.isHidden = false
                cell.packagingLbl.frame.origin.x = 10.0
            }
            
            if dataModel.shippingAmount != "0" {
                cell.shippingPromoView.isHidden = false
                cell.shippingPromoLbl.text = "$"+dataModel.shippingAmount!
            }
            else {
                cell.shippingPromoView.isHidden = true
            }
            
            if dataModel.isMyposting! {
                cell.baseViewHeight.constant = 20.0;
                cell.footerView.backgroundColor = cell.MODIFIED_BG_COLOR
                cell.footerLbl.text = dataModel.modifiedDate
                cell.bottomTitleLbl.text = "Qty Available"
//                cell.bottomDataLbl.text = "\(dataModel.packageQuantity!)"
                cell.bottomDataLbl.text = "\(dataModel.availableQuantity!)"
                cell.yourPostingLbl.isHidden = false
                cell.editBtn.isHidden = false
                cell.wishlishBtn.isHidden = true
                cell.editBtn.tag = indexPath.row
                cell.editBtn.addTarget(self, action: #selector(self.onEditPostingClicked(sender:)), for: .touchUpInside)
                /*if !dataModel.isModified! {
                    cell.footerIcon.isHidden = true
                    cell.footerLbl.isHidden = true
                }
                else {
                    cell.footerIcon.isHidden = false
                    cell.footerLbl.isHidden = false
                }*/
            }
            else if !(dataModel.lastBoughtDate?.isEmpty)! {
                cell.baseViewHeight.constant = 20.0;
                cell.footerLbl.text = dataModel.lastBoughtDate
                cell.footerView.backgroundColor = cell.LAST_BOUGHT_BG_COLOR
            }
            else {
                cell.baseViewHeight.constant = 0;
            }
            
            
            if !dataModel.isMyposting! {
                cell.ratingLbl.text = "\(dataModel.rating!)"
                cell.ratingView.isHidden = false
//                cell.bottomTitleLbl.text = "Est. Delivery"
//                cell.bottomDataLbl.text = dataModel.estimatedDeliveryDate
                cell.bottomTitleLbl.text = ""
                cell.bottomDataLbl.text = ""
                if dataModel.isInWishList! {
                    cell.editBtn.isHidden = true
                    cell.wishlishBtn.isHidden = false
                    cell.wishlishBtn.isSelected = true
                }
                else {
//                    cell.wishlishBtn.isSelected = false
                    cell.wishlishBtn.isHidden = true
                    cell.editBtn.isHidden = true
                }
            }
            
            if dataModel.medispanImageURL != nil {
                if ((imageCache.object(forKey: dataModel.postId! as AnyObject)) != nil){
                    cell.drugImageView.image = imageCache.object(forKey: dataModel.postId! as Int as AnyObject)
                }
                else {
                    if !((operationListArray.contains(String(dataModel.postId!)))){
                        
                        let operation : BlockOperation = BlockOperation(block: {
                            self.downloadImage(drugModel: dataModel)
                        })
                        operation.completionBlock = {
//                            print("Operation completed")
                        }
                        
                        operationQueue.addOperation(operation)
                        
                        operationListArray.insert(String(dataModel.postId!), at: (operationListArray.count))
                    }
                }
            }
            if indexPath.row == buyListArray.count-1 && !isPaginationCalled {
                pageCount += 1
                self.fetchMatketplaceDataService()
                isPaginationCalled = true
            }
        }
        return cell;
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        selectedIndexPath = indexPath
        let dataModel = buyListArray.object(at: indexPath.row) as! BuyListDataModel
        let storyboard = UIStoryboard(name: "Secondary", bundle: nil)
        let detailControl1 = storyboard.instantiateViewController(withIdentifier: "detailControl") as! ItemDetailViewController
        NavigationModel.sharedInstance.homeControl?.contentNavController.pushViewController(detailControl1, animated: true)
        NavigationModel.sharedInstance.homeControl?.showBackIconWithTitle(title: "ITEM DETAILS")
        detailControl1.postId = "\(dataModel.postId!)"
        detailControl1.parentDelegate = self
        detailControl1.getItemDetailsForItem(itemId: "\(dataModel.postId!)")
    }
}
