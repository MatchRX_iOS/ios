//
//  OrderSummaryCell.swift
//  MatchRX
//
//  Created by Vignesh on 2/19/18.
//  Copyright © 2018 HTC. All rights reserved.
//

import UIKit

class OrderSummaryCell: UITableViewCell {
    
    //MARK: - IBOutlet references
    
    @IBOutlet weak var ndcName: UILabel!
    @IBOutlet weak var ndcNumber: UILabel!
    @IBOutlet weak var priceLbl: UILabel!
    @IBOutlet weak var quantityLbl: UILabel!
    @IBOutlet weak var totalAmountLbl: UILabel!
    
    //MARK: - Overriding native functions.

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
