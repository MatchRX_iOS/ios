//
//  WishlistController.swift
//  MatchRX
//
//  Created by Vignesh on 2/28/18.
//  Copyright © 2018 HTC. All rights reserved.
//

import UIKit

class WishlistController: UIViewController, UITableViewDelegate, UITableViewDataSource, NetworkManagerDelegate, URLSessionDelegate {
    
    //MARK: - Global references
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var informationLbl: UILabel!
    @IBOutlet weak var messageLbl: UILabel!
    
    var wishlistArray = NSMutableArray.init()
    var refreshControl: UIRefreshControl!
    var isRefreshHidden: Bool?
    var isPaginationCalled: Bool = false

    var pageCount: Int = 0
    var dataPerPage: Int = 20
    var selectedIndex: Int!
    
    var imageCache = NSCache<AnyObject, UIImage>()
    var operationQueue = OperationQueue()
    var operationListArray = Array<String>()

    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        
        refreshControl = UIRefreshControl()
        refreshControl.attributedTitle = NSAttributedString(string: "")
        refreshControl.addTarget(self, action: #selector(self.refreshTableView), for: UIControlEvents.valueChanged)
        isRefreshHidden = true
        tableView.addSubview(refreshControl)
        self.fetchWishlistedItems()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    //MARK: - Instance functions.
    
    //Refresh the table view to get the new values.
    func refreshTableView() {
        if NetworkManager.sharedInstance.isReachable(){
            isRefreshHidden = false
            pageCount = 0
            wishlistArray.removeAllObjects()
            tableView.reloadData()
            self.fetchWishlistedItems()
        }
        else{
            refreshControl.endRefreshing()
            tableView.setContentOffset(CGPoint.zero, animated: true)
            AlertControl.sharedInstance.presentAlertView(alertTitle: "Kindly check your internet connectivity", alertMessage: "", delegate: nil)
        }
        
    }
    
    //Returns the request parameters with the added filter and sort keys.
    func getRequestData() -> NSMutableDictionary{
        let requestDict = NSMutableDictionary.init()
        requestDict.setValue(dataPerPage, forKey: "per_page")
        requestDict.setValue(pageCount, forKey: "current_page")
        return requestDict
    }
    
    //Creates a request to fetch the wishlisted items.
    func fetchWishlistedItems() {
        if NetworkManager.sharedInstance.isReachable(){
            _ = NetworkManager.sharedInstance.checkTypeOfNetwork()
            if NetworkManager.sharedInstance.isReachableViaWiFi || NetworkManager.sharedInstance.isReachableViaCellular{
                LoadingView.shareInstance.showLodingView(parentView: (NavigationModel.sharedInstance.homeControl?.view!)!)
                NetworkManager.sharedInstance.manageAPIRequest(requestType: .ServiceTypeFetchWislist, requestString: RequestDataModel.sharedInstance.makeRequestToFetchWishlist(requestDict: self.getRequestData()), delegate: self)
            }
        }
        else{
            AlertControl.sharedInstance.presentAlertView(alertTitle: "Kindly check your internet connectivity", alertMessage: "", delegate: nil)
        }
    }
    
    //Downloads the image from URL
    func downloadImage(drugModel: WishlistDataModel) {
        let config = URLSessionConfiguration.default
        let session = URLSession(configuration: config, delegate: self, delegateQueue: nil)
        let getImageTask = session.downloadTask(with: URL(string: drugModel.ndcImageURL!)!) { (url, response, error) in
            if url != nil{
                let data = try? Data(contentsOf: url!)
                let downloadedImage  = UIImage(data: data!)
                if (downloadedImage != nil) {
                    //                self.imageCache.setObject(downloadedImage!, forKey: productObj.productSKUId as AnyObject)
                    self.imageCache.setObject(downloadedImage!, forKey: drugModel.wishlistId!
                        as AnyObject)
                    //self.operationListArray.remove(at:productObj.productSKUId!)
                }
                //            self.productListView.reloadData()
                //            self.productListView.performSelector(onMainThread: #selector(self.productListView.reloadData), with: nil, waitUntilDone: false)
                self.tableView.performSelector(onMainThread: #selector(UITableView.reloadData), with: nil, waitUntilDone: false)
            }
            
        }
        getImageTask.resume()
    }

    //Deletes the selected item from list.
    func onDeleteClicked(sender: UIButton) {
        let dataModel = wishlistArray.object(at: sender.tag) as! WishlistDataModel
        selectedIndex = sender.tag
        let alertControl = UIAlertController(title: "Do you want to delete this item from your wishlist?", message: "", preferredStyle: UIAlertControllerStyle.alert)
        alertControl.addAction(UIAlertAction(title:"YES", style: UIAlertActionStyle.default, handler: {actions in self.deleteWishlistedItem(dataModel: dataModel)}))
        alertControl.addAction(UIAlertAction(title: "NO", style: UIAlertActionStyle.cancel, handler: nil))
        UIApplication.topViewController()?.present(alertControl, animated: true, completion: nil)
    }
    
    //Deletes the selected item from list.
    func deleteWishlistedItem(dataModel: WishlistDataModel) {
        if NetworkManager.sharedInstance.isReachable(){
            _ = NetworkManager.sharedInstance.checkTypeOfNetwork()
            if NetworkManager.sharedInstance.isReachableViaWiFi || NetworkManager.sharedInstance.isReachableViaCellular{
                let ndcDict = NSMutableDictionary.init()
                ndcDict.setValue(dataModel.ndcNumber, forKey: "ndc_number")
                let dataArray = NSMutableArray.init(object: ndcDict)
                let wishlistDelete = NSMutableDictionary.init()
                wishlistDelete.setValue(dataArray, forKey: "wishlistdelete")
                wishlistDelete.setValue(false, forKey: "deletedall")
                let requestData = NSMutableDictionary.init()
                requestData.setValue(wishlistDelete, forKey: "data")
                LoadingView.shareInstance.showLodingView(parentView: self.view.window!)
                NetworkManager.sharedInstance.manageAPIRequest(requestType: .ServiceTypeDeleteFromWishlist, requestString: RequestDataModel.sharedInstance.makeRequestToDeleteFromWishlist(requestDict: requestData ), delegate: self)
            }
        }
        else{
            AlertControl.sharedInstance.presentAlertView(alertTitle: "Kindly check your internet connectivity", alertMessage: "", delegate: nil)
        }
    }
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */
    
    // MARK: - UITableview delegate and datasource implementation.
    
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 90.0
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return wishlistArray.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "wishlistCell", for: indexPath) as! WishlistCell
        
        if indexPath.row <= wishlistArray.count {
            let dataModel = wishlistArray.object(at: indexPath.row) as! WishlistDataModel
            
            if dataModel.ndcImageURL != nil {
                if ((imageCache.object(forKey: dataModel.wishlistId! as AnyObject)) != nil){
                    cell.ndcImageView.image = imageCache.object(forKey: dataModel.wishlistId! as Int as AnyObject)
                }
                else {
                    if !((operationListArray.contains(String(dataModel.wishlistId!)))){
                        
                        let operation : BlockOperation = BlockOperation(block: {
                            self.downloadImage(drugModel: dataModel)
                            //                            self.downloadImageForProduct(productObj: productObj)
                        })
                        operation.completionBlock = {
                            //                            print("Operation completed")
                        }
                        
                        operationQueue.addOperation(operation)
                        
                        operationListArray.insert(String(dataModel.wishlistId!), at: (operationListArray.count))
                    }
                }
            }
            
            cell.ndcNameLbl.text = dataModel.ndcName
            cell.ndcNumberLbl.text = dataModel.formattedNdcNumber
            cell.quantityLbl.text = "Qty Available: \(dataModel.quantity!)"
            cell.dateLbl.text = "Wishlist Date: \(dataModel.wishlistedDate!)"
            
            cell.deleteButton.tag = indexPath.row
            cell.deleteButton.addTarget(self, action: #selector(self.onDeleteClicked(sender:)), for: .touchUpInside)
            
        }
//        if indexPath.row == wishlistArray.count-1 && !isPaginationCalled{
//            pageCount += 1
//            self.fetchWishlistedItems()
//            isPaginationCalled = true
//        }
        
        return cell;
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let dataModel = wishlistArray.object(at: indexPath.row) as! WishlistDataModel
        NavigationModel.sharedInstance.homeControl?.searchTxtField.text = dataModel.formattedNdcNumber
        NavigationModel.sharedInstance.homeControl?.searchedNDC = dataModel.formattedNdcNumber
        NavigationModel.sharedInstance.homeControl?.onSearchIconClicked(sender: UIButton.init())
        NavigationModel.sharedInstance.homeControl?.presentBuyController()
    }
    
    //MARK: - Network manager delegate function implementation.
    
    //Returns the web service response
    func webServiceResponse(for requestType: ServiceRequestType, responseData Data: Any!, withStatusCode statusCode: Int, errorMessage message: Any!) {
        if !isRefreshHidden! {
            isRefreshHidden = true
            refreshControl.endRefreshing()
        }
        if requestType == .ServiceTypeFetchWislist {
            isPaginationCalled = false
        }
        LoadingView.shareInstance.hideLoadingView()
        if message as? Error != nil {
            AlertControl.sharedInstance.presentAlertView(alertTitle: "Something went wrong", alertMessage: "", delegate: self)
        }
        else{
            if !NetworkManager.sharedInstance.checkStatusCode(statusCode){
                let error = NetworkManager.sharedInstance.getErrorModel(requestType: requestType, httpStatusCode: statusCode, responseData: Data)
                AlertControl.sharedInstance.presentAlertView(alertTitle: (error.errorMessage!), alertMessage: "", delegate: self)
            }
            else{
                if requestType == .ServiceTypeFetchWislist {
                    let wishlistData = ResponseManager.sharedInstance.fetchWishlistData(responseData: Data)
                    let responseArray = wishlistData.wishlistArray
                    for i in 0..<responseArray.count {
                        wishlistArray.add(responseArray.object(at: i))
                    }
                    if responseArray.count > 0 {
                        tableView.reloadData()
                        tableView.scrollsToTop = true
                    }
                }
                else if requestType == .ServiceTypeDeleteFromWishlist {
                    wishlistArray.removeObject(at: selectedIndex)
                    tableView.reloadData()
                    let wishlistData = ResponseManager.sharedInstance.deleteFromWishlist(responseData: Data)
                    AlertControl.sharedInstance.presentAlertView(alertTitle: wishlistData.message!, alertMessage: "", delegate: nil)
                }
                if wishlistArray.count == 0 {
                    self.view.bringSubview(toFront: informationLbl)
                    tableView.isHidden = true
                    self.view.sendSubview(toBack: tableView)
                }
                else {
                    self.view.sendSubview(toBack: informationLbl)
                    self.view.bringSubview(toFront: tableView)
                    tableView.isHidden = false
                }
            }
        }
    }
}
