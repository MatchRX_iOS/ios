//
//  CompareListViewCell.swift
//  MatchRX
//
//  Created by Poonkathirvelan on 30/01/18.
//  Copyright © 2018 HTC. All rights reserved.
//

import UIKit

class CompareListViewCell: UITableViewCell {

    //MARK: - IBOutlet references
    @IBOutlet weak var baseView: UIView!
    @IBOutlet weak var baseViewHeight: NSLayoutConstraint!
    @IBOutlet weak var diplayBaseView: UIView!
    @IBOutlet weak var drugImageView: UIImageView!
    @IBOutlet weak var stripLbl: UILabel!
    @IBOutlet weak var drugNameLbl: UILabel!
    @IBOutlet weak var ndcNumberManufacturerLbl: UILabel!
    @IBOutlet weak var expiryDateLbl: UILabel!
    @IBOutlet weak var strengthLbl: UILabel!
    @IBOutlet weak var packagingLbl: UILabel!
    @IBOutlet weak var partialImgView: UIImageView!
    @IBOutlet weak var bottomTitleLbl: UILabel!
    @IBOutlet weak var bottomDataLbl: UILabel!
    @IBOutlet weak var ratingLbl: UILabel!
    @IBOutlet weak var shippingPromoLbl: UILabel!
    @IBOutlet weak var discountLbl: UILabel!
    @IBOutlet weak var priceLbl: UILabel!
    @IBOutlet weak var footerLbl: UILabel!
    @IBOutlet weak var yourPostingLbl: UILabel!
    @IBOutlet weak var wishlishBtn: UIButton!
    @IBOutlet weak var editBtn: UIButton!
    @IBOutlet weak var shippingPromoView: UIView!
    @IBOutlet weak var ratingView: UIView!
    @IBOutlet weak var footerView: UIView!
    @IBOutlet weak var footerIcon: UIImageView!
    
    let MODIFIED_BG_COLOR = UIColor(red: 44.0/255.0, green: 185.0/255.0, blue: 227.0/255.0, alpha: 1.0)
    let LAST_BOUGHT_BG_COLOR = UIColor(red: 56.0/255.0, green: 142.0/255.0, blue: 60.0/255.0, alpha: 1.0)
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    override func layoutSubviews() {
        self.baseView.layer.masksToBounds = false
        self.baseView.layer.shadowOffset = CGSize(width: 3, height: 3)
        self.baseView.layer.shadowRadius = 1
        self.baseView.layer.shadowOpacity = 0.2
    }

}
