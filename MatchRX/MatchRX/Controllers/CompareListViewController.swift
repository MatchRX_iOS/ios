//
//  CompareListViewController.swift
//  MatchRX
//
//  Created by Poonkathirvelan on 30/01/18.
//  Copyright © 2018 HTC. All rights reserved.
//

import UIKit

class CompareListViewController: UIViewController,UITableViewDelegate,UITableViewDataSource, NetworkManagerDelegate, URLSessionDelegate {

    //MARK: - IBOutlet references
    @IBOutlet weak var baseView: UIView!
    @IBOutlet weak var listContentView: UIView!
    @IBOutlet weak var baseViewHeight: NSLayoutConstraint!
    @IBOutlet weak var diplayBaseView: UIView!
    @IBOutlet weak var drugImageView: UIImageView!
    @IBOutlet weak var stripLbl: UILabel!
    @IBOutlet weak var drugNameLbl: UILabel!
    @IBOutlet weak var ndcNumberManufacturerLbl: UILabel!
    @IBOutlet weak var expiryDateLbl: UILabel!
    @IBOutlet weak var strengthLbl: UILabel!
    @IBOutlet weak var packagingLbl: UILabel!
    @IBOutlet weak var partialImgView: UIImageView!
    @IBOutlet weak var bottomTitleLbl: UILabel!
    @IBOutlet weak var bottomDataLbl: UILabel!
    @IBOutlet weak var ratingLbl: UILabel!
    @IBOutlet weak var shippingPromoLbl: UILabel!
    @IBOutlet weak var discountLbl: UILabel!
    @IBOutlet weak var priceLbl: UILabel!
    @IBOutlet weak var footerLbl: UILabel!
    @IBOutlet weak var yourPostingLbl: UILabel!
    @IBOutlet weak var wishlishBtn: UIButton!
    @IBOutlet weak var editBtn: UIButton!
    @IBOutlet weak var shippingPromoView: UIView!
    @IBOutlet weak var ratingView: UIView!
    @IBOutlet weak var footerView: UIView!
    @IBOutlet weak var footerIcon: UIImageView!
    @IBOutlet weak var selctedItembaseView: UIView!
    @IBOutlet weak var SelectedItemContentView: UIView!
    @IBOutlet weak var compareListTblView: UITableView!
    
    
    let MODIFIED_BG_COLOR = UIColor(red: 44.0/255.0, green: 185.0/255.0, blue: 227.0/255.0, alpha: 1.0)
    let LAST_BOUGHT_BG_COLOR = UIColor(red: 56.0/255.0, green: 142.0/255.0, blue: 60.0/255.0, alpha: 1.0)
    
    var refreshControl: UIRefreshControl!
    var isRefreshHidden: Bool?
    //MARK: - Global declarations
    var compareListArray: NSMutableArray = NSMutableArray.init()
    var pageCount: Int = 0
    var dataPerPage: Int = 20
    var isPaginationCalled: Bool = false
    var imageCache = NSCache<AnyObject, UIImage>()
    var operationQueue = OperationQueue()
    var operationListArray = Array<String>()
    
    var detailControl: ItemDetailViewController!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        
        baseView.layer.masksToBounds = false
        baseView.layer.shadowOffset = CGSize(width: 0, height: 3)
        baseView.layer.shadowRadius = 12
        baseView.layer.shadowOpacity = 0.8
        
        
        
        refreshControl = UIRefreshControl()
        refreshControl.attributedTitle = NSAttributedString(string: "")
        refreshControl.addTarget(self, action: #selector(self.refreshTableView), for: UIControlEvents.valueChanged)
        isRefreshHidden = true
        compareListTblView.addSubview(refreshControl)
        self.fetchComparePriceData()
    }

    override func viewDidLayoutSubviews() {
        // Do any additional setup after loading the view.
        self.SelectedItemContentView.layer.masksToBounds = false
        self.SelectedItemContentView.layer.shadowOffset = CGSize(width: 0, height: 3)
        self.SelectedItemContentView.layer.shadowRadius = 2
        self.SelectedItemContentView.layer.shadowOpacity = 0.5
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    
    //MARK: - Instance functions
    
    //Returns the request parameters with the added filter and sort keys.
    func getRequestData() -> NSMutableDictionary{
        let requestDict = NSMutableDictionary.init()
        let filterDict = NSMutableDictionary.init()
        
        filterDict.setValue("A", forKey: "package_option")
        filterDict.setValue("A", forKey: "original_package")
        filterDict.setValue("A", forKey: "brand")
        filterDict.setValue("A", forKey: "expire_date")
        filterDict.setValue("A", forKey: "wac_discount")
        filterDict.setValue("A", forKey: "created_at")
        
        let sortDict = NSMutableDictionary.init()
        sortDict.setValue("desc", forKey: "updated_at")
        
        requestDict.setValue(filterDict, forKey: "filter")
        requestDict.setValue(sortDict, forKey: "sort")
        requestDict.setValue("", forKey: "search")
        requestDict.setValue(dataPerPage, forKey: "per_page")
        requestDict.setValue(pageCount, forKey: "page")
//        requestDict.setValue(GlobalData.sharedInstance.sellerArray, forKey: "posts_from_seller")
        return requestDict
    }
    
    
    //Loads the current post data.
    func loadCurrentPostData (compareDataModel: ComparePriceDataModel) {
        baseView.isHidden = false
        let dataModel = compareDataModel.currentPostArray.object(at: 0) as! BuyListDataModel
        if dataModel.isInCart! {
            self.SelectedItemContentView.backgroundColor = UIColor(red: 255.0/255.0, green: 239.0/255.0, blue: 222.0/255.0, alpha: 1.0)
        }
        else {
            self.SelectedItemContentView.backgroundColor = UIColor.white
        }
//        editBtn.isHidden = true
//        wishlishBtn.isHidden = true
        stripLbl.isHidden = true
        ratingView.isHidden = true
        shippingPromoView.isHidden = true
        yourPostingLbl.isHidden = true
        if dataModel.imageCount != 0 {
            stripLbl.isHidden = false
        }
        drugNameLbl.text = dataModel.drugName
        ndcNumberManufacturerLbl.text = "\(dataModel.ndcNumber!) | \(dataModel.manufacturerName!)"
        expiryDateLbl.text = dataModel.expiryDate
        strengthLbl.text = dataModel.strength
        packagingLbl.text = dataModel.packaging
        discountLbl.text = "\(dataModel.discount!)%"
        
        //            let currentPrice = String(format: "%.2f", arguments: [dataModel.currentPrice!])
        let revisedPrice = String(format: "%.2f", arguments: [dataModel.revisedPrice!])
        //            cell.priceLbl.text = "$\(dataModel.currentPrice!)"
        priceLbl.text = "$\(revisedPrice)"
        
        if dataModel.packageOption == "F" {
            partialImgView.isHidden = true
            packagingLbl.frame.origin.x = 0.0
        }
        else {
            partialImgView.isHidden = false
            packagingLbl.frame.origin.x = 10.0
        }
        
        if dataModel.shippingAmount != "0" {
            shippingPromoView.isHidden = false
            shippingPromoLbl.text = "$"+dataModel.shippingAmount!
        }
        else {
            shippingPromoView.isHidden = true
        }
        
        if dataModel.isMyposting! {
            baseViewHeight.constant = 20.0;
            footerView.backgroundColor = MODIFIED_BG_COLOR
            footerLbl.text = dataModel.modifiedDate
            bottomTitleLbl.text = "Qty Available"
//            bottomDataLbl.text = "\(dataModel.packageQuantity!)"
            bottomDataLbl.text = "\(dataModel.availableQuantity!)"
            yourPostingLbl.isHidden = false
//            wishlishBtn.isHidden = true
           /* if !dataModel.isModified! {
                footerIcon.isHidden = true
                footerLbl.isHidden = true
            }
            else {
                footerIcon.isHidden = false
                footerLbl.isHidden = false
            }*/
        }
        else if !(dataModel.lastBoughtDate?.isEmpty)! {
            baseViewHeight.constant = 20.0;
            footerLbl.text = dataModel.lastBoughtDate
            footerView.backgroundColor = LAST_BOUGHT_BG_COLOR
        }
        else {
            baseViewHeight.constant = 0;
        }
        
        
        if !dataModel.isMyposting! {
//            bottomTitleLbl.text = "Est. Delivery"
//            bottomDataLbl.text = dataModel.estimatedDeliveryDate
            bottomTitleLbl.text = ""
            bottomDataLbl.text = ""
            ratingLbl.text = "\(dataModel.rating!)"
            ratingView.isHidden = false
            if dataModel.isInWishList! {
//                editBtn.isHidden = true
//                cell.wishlishBtn.isHidden = false
                wishlishBtn.isHidden = false
                wishlishBtn.isSelected = true
            }
            else {
                //                    cell.wishlishBtn.isSelected = false
                wishlishBtn.isHidden = true
//                editBtn.isHidden = true
            }
        }
        
        let url = URL(string: dataModel.medispanImageURL!)
        let data = try? Data(contentsOf: url!)
        
        if let imageData = data {
            let image = UIImage(data: imageData)
            drugImageView.image = image
        }
        
        /*if dataModel.medispanImageURL != nil {
            if ((imageCache.object(forKey: dataModel.postId! as AnyObject)) != nil){
                drugImageView.image = imageCache.object(forKey: dataModel.postId! as Int as AnyObject)
            }
            else {
                if !((operationListArray.contains(String(dataModel.postId!)))){
                    
                    let operation : BlockOperation = BlockOperation(block: {
                        self.downloadImage(drugModel: dataModel)
                    })
                    operation.completionBlock = {
                        //                            print("Operation completed")
                    }
                    
                    operationQueue.addOperation(operation)
                    
                    operationListArray.insert(String(dataModel.postId!), at: (operationListArray.count))
                }
            }
        }*/
    }
    
    //Loads the data in tableview.
    func loadComparePriceListData(compareListData: ComparePriceDataModel) {
        let responseArray = compareListData.matchesArray
        for i in 0..<responseArray.count {
            compareListArray.add(responseArray.object(at: i))
        }
        if responseArray.count > 0{
            compareListTblView.reloadData()
        }
        if compareListArray.count == 0 {
            compareListTblView.isHidden = true
        }
        else {
            compareListTblView.isHidden = false
        }
    }
    
    //Creates the request to fetch the data.
    func fetchComparePriceData() {
        
        if NetworkManager.sharedInstance.isReachable(){
            _ = NetworkManager.sharedInstance.checkTypeOfNetwork()
            if NetworkManager.sharedInstance.isReachableViaWiFi || NetworkManager.sharedInstance.isReachableViaCellular{
//                LoadingView.shareInstance.showWhiteView(parentView: (self.view))
                LoadingView.shareInstance.showLodingView(parentView: (NavigationModel.sharedInstance.homeControl?.view!)!)
                NetworkManager.sharedInstance.manageAPIRequest(requestType: .ServiceTypeComparePricing, requestString:RequestDataModel.sharedInstance.makeRequestForBuyList(requestDict: self.getRequestData()), delegate: self)
            }
        }
        else{
            AlertControl.sharedInstance.presentAlertView(alertTitle: "Kindly check your internet connectivity", alertMessage: "", delegate: nil)
        }
    }
    
    //Refresh the table view to get the new values.
    func refreshTableView() {
        if NetworkManager.sharedInstance.isReachable(){
            isRefreshHidden = false
            pageCount = 0
            compareListArray.removeAllObjects()
            compareListTblView.scrollsToTop = true
            self.fetchComparePriceData()
        }
        else{
            refreshControl.endRefreshing()
            compareListTblView.setContentOffset(CGPoint.zero, animated: true)
            AlertControl.sharedInstance.presentAlertView(alertTitle: "Kindly check your internet connectivity", alertMessage: "", delegate: nil)
        }
    }
    
    //Downloads the image from URL
    func downloadImage(drugModel: BuyListDataModel) {
        let config = URLSessionConfiguration.default
        let session = URLSession(configuration: config, delegate: self, delegateQueue: nil)
        let getImageTask = session.downloadTask(with: URL(string: drugModel.medispanImageURL!)!) { (url, response, error) in
            if url != nil{
                let data = try? Data(contentsOf: url!)
                let downloadedImage  = UIImage(data: data!)
                if (downloadedImage != nil) {
                    self.imageCache.setObject(downloadedImage!, forKey: drugModel.postId!
                        as AnyObject)
                }
                self.compareListTblView.performSelector(onMainThread: #selector(UITableView.reloadData), with: nil, waitUntilDone: false)
            }
            
        }
        getImageTask.resume()
    }
    
    
    
    //MARK: - Network manager delegate function
    
    //Returns the web service call
    func webServiceResponse(for requestType: ServiceRequestType, responseData Data: Any!, withStatusCode statusCode: Int, errorMessage message: Any!) {
        if !isRefreshHidden! {
            isRefreshHidden = true
            refreshControl.endRefreshing()
        }
        if requestType == .ServiceTypeComparePricing {
            isPaginationCalled = false
        }
//        LoadingView.shareInstance.hideWhiteView()
        LoadingView.shareInstance.hideLoadingView()
        if message as? Error != nil {
            AlertControl.sharedInstance.presentAlertView(alertTitle: "Something went wrong", alertMessage: "", delegate: self)
        }
        else{
            if !NetworkManager.sharedInstance.checkStatusCode(statusCode){
                let error = NetworkManager.sharedInstance.getErrorModel(requestType: requestType, httpStatusCode: statusCode, responseData: Data)
                AlertControl.sharedInstance.presentAlertView(alertTitle: (error.errorMessage!), alertMessage: "", delegate: self)
            }
            else{
                let comparePriceData = ResponseManager.sharedInstance.getComparePriceData(responseData: Data)
                self.loadCurrentPostData(compareDataModel: comparePriceData)
                self.loadComparePriceListData(compareListData: comparePriceData)
            }
        }
    }
    
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if indexPath.row <= compareListArray.count {
            let dataModel = compareListArray.object(at: indexPath.row) as! BuyListDataModel
            if dataModel.isMyposting! || !(dataModel.lastBoughtDate?.isEmpty)! {
                return 150.0
            }
            else {
                return 130.0
            }
        }
        return 130.0
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return compareListArray.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "ComparetblCellId", for: indexPath) as!CompareListViewCell ;
        if indexPath.row < compareListArray.count {
            let dataModel = compareListArray.object(at: indexPath.row) as! BuyListDataModel
            if dataModel.isInCart! {
                cell.baseView.backgroundColor = UIColor(red: 255.0/255.0, green: 239.0/255.0, blue: 222.0/255.0, alpha: 1.0)
            }
            else {
                cell.baseView.backgroundColor = UIColor.white
            }
//            cell.editBtn.isHidden = true
            cell.wishlishBtn.isHidden = true
            cell.stripLbl.isHidden = true
            cell.ratingView.isHidden = true
            cell.shippingPromoView.isHidden = true
            cell.yourPostingLbl.isHidden = true
            if dataModel.imageCount != 0 {
                cell.stripLbl.isHidden = false
            }
            cell.drugNameLbl.text = dataModel.drugName
            cell.ndcNumberManufacturerLbl.text = "\(dataModel.ndcNumber!) | \(dataModel.manufacturerName!)"
            cell.expiryDateLbl.text = dataModel.expiryDate
            cell.strengthLbl.text = dataModel.strength
            cell.packagingLbl.text = dataModel.packaging
            cell.discountLbl.text = "\(dataModel.discount!)%"
            //            cell.priceLbl.text = "$\(dataModel.currentPrice!)"
            
            //            let currentPrice = String(format: "%.2f", arguments: [dataModel.currentPrice!])
            let revisedPrice = String(format: "%.2f", arguments: [dataModel.revisedPrice!])
            //            cell.priceLbl.text = "$\(dataModel.currentPrice!)"
            cell.priceLbl.text = "$\(revisedPrice)"
            
            if dataModel.packageOption == "F" {
                cell.partialImgView.isHidden = true
                cell.packagingLbl.frame.origin.x = 0.0
            }
            else {
                cell.partialImgView.isHidden = false
                cell.packagingLbl.frame.origin.x = 10.0
            }
            
            if dataModel.shippingAmount != "0" {
                cell.shippingPromoView.isHidden = false
                cell.shippingPromoLbl.text = "$"+dataModel.shippingAmount!
            }
            else {
                cell.shippingPromoView.isHidden = true
            }
            
            if dataModel.isMyposting! {
                cell.baseViewHeight.constant = 20.0;
                cell.footerView.backgroundColor = cell.MODIFIED_BG_COLOR
                cell.footerLbl.text = dataModel.modifiedDate
                cell.bottomTitleLbl.text = "Qty Available"
//                cell.bottomDataLbl.text = "\(dataModel.packageQuantity!)"
                cell.bottomDataLbl.text = "\(dataModel.availableQuantity!)"
                cell.yourPostingLbl.isHidden = false
//                cell.editBtn.isHidden = false
//                cell.wishlishBtn.isHidden = true
//                cell.editBtn.tag = indexPath.row
//                cell.editBtn.addTarget(self, action: #selector(self.onEditPostingClicked(sender:)), for: .touchUpInside)
                /*if !dataModel.isModified! {
                    cell.footerIcon.isHidden = true
                    cell.footerLbl.isHidden = true
                }
                else {
                    cell.footerIcon.isHidden = false
                    cell.footerLbl.isHidden = false
                }*/
            }
            else if !(dataModel.lastBoughtDate?.isEmpty)! {
                cell.baseViewHeight.constant = 20.0;
                cell.footerLbl.text = dataModel.lastBoughtDate
                cell.footerView.backgroundColor = cell.LAST_BOUGHT_BG_COLOR
            }
            else {
                cell.baseViewHeight.constant = 0;
            }
            
            print("My posting: \(dataModel.isMyposting!)")
            if !dataModel.isMyposting! {
//                cell.bottomTitleLbl.text = "Est. Delivery"
//                cell.bottomDataLbl.text = dataModel.estimatedDeliveryDate
                cell.bottomTitleLbl.text = ""
                cell.bottomDataLbl.text = ""
                cell.ratingLbl.text = "\(dataModel.rating!)"
                cell.ratingView.isHidden = false
                if dataModel.isInWishList! {
//                    cell.editBtn.isHidden = true
                    cell.wishlishBtn.isHidden = false
                    cell.wishlishBtn.isSelected = true
                }
                else {
                    //                    cell.wishlishBtn.isSelected = false
                    cell.wishlishBtn.isHidden = true
//                    cell.editBtn.isHidden = true
                }
            }
            
            if dataModel.medispanImageURL != nil {
                if ((imageCache.object(forKey: dataModel.postId! as AnyObject)) != nil){
                    cell.drugImageView.image = imageCache.object(forKey: dataModel.postId! as Int as AnyObject)
                }
                else {
                    if !((operationListArray.contains(String(dataModel.postId!)))){
                        
                        let operation : BlockOperation = BlockOperation(block: {
                            self.downloadImage(drugModel: dataModel)
                        })
                        operation.completionBlock = {
                            //                            print("Operation completed")
                        }
                        
                        operationQueue.addOperation(operation)
                        
                        operationListArray.insert(String(dataModel.postId!), at: (operationListArray.count))
                    }
                }
            }
            if indexPath.row == compareListArray.count-1 && !isPaginationCalled {
                pageCount += 1
                self.fetchComparePriceData()
                isPaginationCalled = true
            }
        }
        return cell;
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let dataModel = compareListArray.object(at: indexPath.row) as! BuyListDataModel
        let storyboard = UIStoryboard(name: "Secondary", bundle: nil)
        detailControl = storyboard.instantiateViewController(withIdentifier: "detailControl") as! ItemDetailViewController
        NavigationModel.sharedInstance.homeControl?.contentNavController.pushViewController(detailControl, animated: true)
        NavigationModel.sharedInstance.homeControl?.showBackIconWithTitle(title: "ITEM DETAILS")
        detailControl.postId = "\(dataModel.postId!)"
        detailControl.isComparePriceHidden = true
        detailControl.getItemDetailsForItem(itemId: "\(dataModel.postId!)")
    }
    
}
