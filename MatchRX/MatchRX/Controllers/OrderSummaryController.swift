//
//  OrderSummaryController.swift
//  MatchRX
//
//  Created by Vignesh on 2/19/18.
//  Copyright © 2018 HTC. All rights reserved.
//

import UIKit

class OrderSummaryController: UIViewController,UITableViewDelegate,UITableViewDataSource {
    
    //MARK: - IBOutlet references
    @IBOutlet weak var summaryTableView: UITableView!
    @IBOutlet weak var bankInfoLbl: UILabel!
    
    //MARK: - Global variables declarations
    var orderArray = NSMutableArray.init()
    var summaryDataModel = OrderSummaryDataModel()
    @IBOutlet weak var tableviewSumHeightCons: NSLayoutConstraint!
    //@IBOutlet weak var bottomViewHeightCons: NSLayoutConstraint!
    
    //MARK: - Default funtions
    
    override func viewDidLoad() {
        super.viewDidLoad()
        NavigationModel.sharedInstance.homeControl?.headerLabel.frame.origin.x = 20.0
        self.loadOrderSummaryWithData()
        
        
        // Do any additional setup after loading the view.
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    //MARK: - Instance functions.
    
    //Binds the data with table view.
    func loadOrderSummaryWithData(){
        orderArray = summaryDataModel.orderSummaryArray
//        bankInfoLbl.text = summaryDataModel.bankDetails
        bankInfoLbl.text = "Seller will confirm availability of the order within 2 business days. Upon confirmation, MatchRX will debit bank account \(summaryDataModel.bankDetails ?? "") for each order separately."
        summaryTableView.reloadData()
    }
    
    @IBAction func onContinueShoppingClicked(sender: UIButton) {
//        self.dismiss(animated: true, completion: nil)
        NavigationModel.sharedInstance.homeControl?.onBackClicked(sender: UIButton.init())
        NavigationModel.sharedInstance.homeControl?.headerLabel.frame.origin.x = 55.0
        NavigationModel.sharedInstance.homeControl?.headerLabel.text = "BUY"
        NavigationModel.sharedInstance.homeControl?.screenTitle = "BUY"
        NavigationModel.sharedInstance.homeControl?.cartCountLbl.text = ""
        NavigationModel.sharedInstance.homeControl?.cartCountLbl.isHidden = true
        NavigationModel.sharedInstance.homeControl?.headerMenuView.isHidden = false
        NavigationModel.sharedInstance.homeControl?.hamburgerView.isHidden = false
        NavigationModel.sharedInstance.homeControl?.backView.isHidden = true
        NavigationModel.sharedInstance.homeControl?.showBuyMenu()
    }
    
    //MARK: - Table view delegate and data source functions.
    
    //Returns the number of sections in tableview
    func numberOfSections(in tableView: UITableView) -> Int {
        return orderArray.count
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 50.0
    }
    
    func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        let dataModel = orderArray.object(at: section) as! OrderSummaryDataModel
        let sellerName = "\(dataModel.sellerName!) - Order # "
//        return sellerName
        return ""
    }
    
    
    //Returns the number of rows in the section
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        let dataModel = orderArray.object(at: section) as! OrderSummaryDataModel
        return dataModel.cartItemArray.count
        //        return 1
    }
    
    func tableView(_ tableView: UITableView, viewForFooterInSection section: Int) -> UIView? {
        let  footerCell = tableView.dequeueReusableCell(withIdentifier: "orderSumFooterCell") as! OrderSummaryFooterCell
        let dataModel = orderArray.object(at: section) as! OrderSummaryDataModel
        footerCell.itemTotalLbl.text = "$\(String(format: "%.2f", arguments: [dataModel.itemTotalAmount!]))"
        footerCell.shippingCostLbl.text = "Shipping Cost - \(dataModel.shippingMethod!)"
        footerCell.sellerNameLbl.text = "\(dataModel.sellerName!) Total"
        if dataModel.shippingCost! > 0 {
            footerCell.shippingAmt.text = "$\(String(format: "%.2f", arguments: [dataModel.shippingCost!]))"
        }
        else {
            footerCell.shippingAmt.text = "Free"
        }
        footerCell.totalAmtLbl.text = "$\(String(format: "%.2f", arguments: [dataModel.totalAmount!]))"
        return footerCell
    }
    
    
    //Returns the cell for the indexpath.
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "orderSummaryCell", for: indexPath) as! OrderSummaryCell
        if orderArray.count != 0{
            let dataModel = orderArray.object(at: indexPath.section) as! OrderSummaryDataModel
            if indexPath.row < dataModel.cartItemArray.count {
                let orderData = dataModel.cartItemArray.object(at: indexPath.row) as! OrderItemModel
                
                let price = String(format: "%.2f", arguments: [orderData.itemPrice!])
                let totalAmount = String(format: "%.2f", arguments: [orderData.totalPrice!])
                cell.ndcName.text = orderData.postTitle
                cell.ndcNumber.text = orderData.ndcNumber
//                cell.priceLbl.text = "\(orderData.itemPrice!)"
//                cell.totalAmountLbl.text = "\(orderData.totalPrice!)"
                cell.priceLbl.text = "$\(price)"
                cell.totalAmountLbl.text = "$\(totalAmount)"
                if orderData.packageOption == "F" {
                    cell.quantityLbl.text = "\(orderData.packageQuantity!)"
                }
                else {
//                    cell.quantityLbl.text = "Partial: \(orderData.packageSize!)/\(orderData.actualPackageSize!)"
                    cell.quantityLbl.text = "Partial: \(orderData.packaging!)"
                }
            }
        }
        return cell
    }
    
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        
        // if tempHeight > self.tableviewHeightCons.constant {
        self.tableviewSumHeightCons.constant =  self.tableviewSumHeightCons.constant + cell.bounds.height
        // }
    }
    func tableView(_ tableView: UITableView, willDisplayFooterView view: UIView, forSection section: Int) {
        
        // if tempHeight > self.tableviewHeightCons.constant {
        self.tableviewSumHeightCons.constant = self.tableviewSumHeightCons.constant + view.bounds.height
        // }
        //self.tableviewHeightCons.constant = self.tableviewHeightCons.constant + view.bounds.height
        
    }
    
    func tableView(_ tableView: UITableView, willDisplayHeaderView view: UIView, forSection section: Int) {
        self.self.tableviewSumHeightCons.constant = self.tableviewSumHeightCons.constant + view.bounds.height
        let dataModel = orderArray.object(at: section) as! OrderSummaryDataModel
        let sellerName = "\(dataModel.sellerName!) - Order # "
        
        let header: UITableViewHeaderFooterView = view as! UITableViewHeaderFooterView
        header.contentView.backgroundColor = UIColor.init(red: 240/255, green: 240/255, blue: 240/255, alpha: 1.0); //UIColor.colorWithHexString(hexStr: "#408000")
        header.textLabel?.textColor = UIColor.init(red: 33/255, green: 33/255, blue: 33/255, alpha: 1.0)
        header.textLabel?.backgroundColor = UIColor.clear
//        header.textLabel?.text = sellerName
        header.textLabel?.font = UIFont(name: "Roboto-Medium", size: 14.0)
        header.textLabel?.sizeToFit()
        //UIFont.fontNames(forFamilyName: "Roboto-Medium");
        
        let headerLbl = UILabel.init(frame: CGRect(x: 15.0, y: view.frame.height-50, width: 110, height: 50))
        headerLbl.textColor = UIColor.init(red: 33/255, green: 33/255, blue: 33/255, alpha: 1.0)
        headerLbl.backgroundColor = UIColor.clear
        headerLbl.font = UIFont(name: "Roboto-Medium", size: 14.0)
        headerLbl.text = sellerName
//        headerLbl.sizeToFit()
        header.addSubview(headerLbl)
        
        //        let buttonTap:UIButton = UIButton(frame: CGRect(x: 150, y: view.frame.height-35, width: view.frame.width-160, height: 35))
        let buttonTap:UIButton = UIButton()
        buttonTap.tag = section;
        buttonTap.titleLabel?.font = UIFont(name: "Roboto-Light", size: 14.0)
        buttonTap.setTitleColor(UIColor.init(red: 33/255, green: 147/255, blue: 240/255, alpha: 1.0), for: .normal)
        buttonTap.setTitle("\(dataModel.orderNumber!)", for: .normal)
        buttonTap.sizeToFit()
        self.view.layoutIfNeeded()
//        buttonTap.addTarget(self, action: #selector(self.navigateToSellerMarketPlace(sender:)), for: .touchUpInside)
//        buttonTap.frame = CGRect(x: view.frame.size.width-buttonTap.frame.size.width-5.0, y: view.frame.height-35, width: buttonTap.frame.width, height: 35)
        buttonTap.frame = CGRect(x: (headerLbl.frame.size.width)+20.0, y: view.frame.height-50, width: buttonTap.frame.width, height: 50)
        header.addSubview(buttonTap)
        
        
        
        
    }
    
}

