//
//  SellerConfirmationTableViewCell.swift
//  MatchRX
//
//  Created by Vignesh on 5/9/18.
//  Copyright © 2018 HTC. All rights reserved.
//

import UIKit

class SellerConfirmationTableViewCell: UITableViewCell {
    
    @IBOutlet weak var baseView: UIView!
    @IBOutlet weak var availableBtn: UIButton!
    @IBOutlet weak var reviseBtn: UIButton!
    @IBOutlet weak var outOfStockBtn: UIButton!
    @IBOutlet weak var ndcNameLbl: UILabel!
    @IBOutlet weak var ndcNumberLbl: UILabel!
    @IBOutlet weak var ndcImageView: UIImageView!
    @IBOutlet weak var expiryDateLbl: UILabel!
    @IBOutlet weak var lotNumberLbl: UILabel!
    @IBOutlet weak var fullPackView: UIView!
    @IBOutlet weak var partialPackView: UIView!
    @IBOutlet weak var quantityLbl: UILabel!
    @IBOutlet weak var partialLbl: UILabel!
    @IBOutlet weak var priceLbl: UILabel!

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

    override func layoutSubviews() {
        super.layoutSubviews()
        baseView.layer.borderWidth = 0.8
        baseView.layer.borderColor = UIColor.lightGray.cgColor
    }
}
