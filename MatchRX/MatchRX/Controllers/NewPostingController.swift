//
//  NewPostingController.swift
//  MatchRX
//
//  Created by Vignesh on 12/5/17.
//  Copyright © 2017 HTC. All rights reserved.
//
import UIKit
import AVFoundation
import Photos

enum VerificationType {
    case defaultType
    case NDCVerificaiton
    case lotNumberVerification
}

enum PaymentType {
    case SellerPays
    case BuyerPays
    case Default
}

enum ActionType {
    case DefaultAction
    case FullPackClicked
    case PartialPackClicked
    case SealedClicked
    case NonSealedClicked
    case PackQtyEquals1Clicked
    case PackQtyEqualsGreaterThan1Clicked
}

class NewPostingController: UIViewController, ScanControlDelegate, DropDownViewDelegate, imageCaptureDelegate, UITextFieldDelegate, DatePickerDelegate, UITextViewDelegate, NetworkManagerDelegate {
    let QUANTITY_SOLD_ALERT_VIEW_HEIGHT: CGFloat = 70.0
    let ORIGINALPKGALERTVIEWHEIGHT = 45.0
    let MINIMUM_PRICE_VIEW_HEIGHT = 33.0
    let MAXIMUM_PRICE_ALERT_VIEW_HEIGHT: CGFloat = 24.0
    let MINIMUM_PRICE_ALERT_VIEW_HEIGHT: CGFloat = 36.0
    let PARTIAL_ERROR_MESSAGE_HEIGHT: CGFloat = 85.0
    let SLIDER_MAXIMUM_IMAGE = "Grey.png"
    let SLIDER_MINIMUM_IMAGE = "Orange.png"
    let SLIDER_THUMB_IMAGE = "progress_thumb.png"
    let DEFAULT_WIZARD_BG_COLOR = UIColor(red: 238.0/255.0, green: 241.0/255.0, blue: 243.0/255.0, alpha: 1.0)
    let SELECTED_WIZARD_BG_COLOR = UIColor(red: 246.0/255.0, green: 208.0/255.0, blue: 181.0/255.0, alpha: 1.0)
    let DEFAULT_WIZARD_LABEL_BG_COLOR = UIColor.clear
    let SELECTED_WIZARD_LABEL_BG_COLOR = UIColor(red: 255.0/255.0, green: 142.0/255.0, blue: 0.0/255.0, alpha: 1.0)
    let DEFAULT_WIZARD_LABEL_TEXT_COLOR = UIColor(red: 164.0/255.0, green: 164.0/255.0, blue: 164.0/255.0, alpha: 1.0)
    let SELECTED_WIZARD_LABEL_TEXT_COLOR = UIColor(red: 255.0/255.0, green: 255.0/255.0, blue: 255.0/255.0, alpha: 1.0)
    let DEFAULT_WIZARD_LABEL_BORDER_COLOR = UIColor(red: 164.0/255.0, green: 164.0/255.0, blue: 164.0/255.0, alpha: 1.0).cgColor
    let SELECTED_WIZARD_LABEL_BORDER_COLOR = UIColor.clear.cgColor
    
    var textBorderColor = UIColor(red: 207.0/255.0, green: 216.0/255.0, blue: 220.0/255.0, alpha: 1.0).cgColor
    
    let myGroup = DispatchGroup()
    
    //MARK: - Wizard view references
    //References for controls inside top view.
    @IBOutlet var wizardButtonsView: UIView!
    @IBOutlet var oneLbl: UILabel!
    @IBOutlet var twoLbl: UILabel!
    @IBOutlet var threeLbl: UILabel!
    @IBOutlet var enterNDCBtn: UIButton!
    @IBOutlet var setPriceBtn: UIButton!
    @IBOutlet var selectShippingBtn: UIButton!
    @IBOutlet var wizardTick1Img: UIImageView!
    @IBOutlet var wizardTick2Img: UIImageView!
    @IBOutlet var wizardTick3Img: UIImageView!
    
    var selectedWizardButton: UIButton!
    
    
    //MARK: - Enter NDC screen references
    //Enter NDC screen references
    @IBOutlet var enterNDCParentView: UIView!
    @IBOutlet var scrollView: TPKeyboardAvoidingScrollView!
    @IBOutlet var priceScrollView: TPKeyboardAvoidingScrollView!
    @IBOutlet var shippingScrollView: TPKeyboardAvoidingScrollView!
    @IBOutlet var verifyNDCView: UIView!
    @IBOutlet var ndcTxtField: UITextField!
    @IBOutlet var dummyNdcTxtField: UITextField!
    @IBOutlet var expiryDateTxtField: UITextField!
    @IBOutlet var ndcNameLbl: UILabel!
    @IBOutlet var similarItemsLbl: UILabel!
    @IBOutlet var medispanImgView: UIImageView!
    @IBOutlet var strengthLbl: UILabel!
    @IBOutlet var storageLbl: UILabel!
    @IBOutlet var packagingLbl: UILabel!
    @IBOutlet var formLbl: UILabel!
    @IBOutlet var uploadBtn1: UIButton!
    @IBOutlet var uploadBtn2: UIButton!
    @IBOutlet var deleteImg1: UIButton!
    @IBOutlet var deleteImg2: UIButton!
    @IBOutlet var lotNumberTxtField: UITextField!
    @IBOutlet var fullPackBtn: UIButton!
    @IBOutlet var fullOverlayView: UIView!
    @IBOutlet var partialPackBtn: UIButton!
    @IBOutlet var partialOverlayView: UIView!
    @IBOutlet weak var packAvailableOverlayView: UIView!
    @IBOutlet var partialTxtField: UITextField!
    @IBOutlet var unitOfMeasureLbl: UILabel!
    @IBOutlet var packAvailableTxtField: UITextField!
    @IBOutlet var packAvailableBtn: UIButton!
    @IBOutlet var sealedPackBtn: UIButton!
    @IBOutlet var nonSealedPackBtn: UIButton!
    @IBOutlet var tornPkgBtn: UIButton!
    @IBOutlet var stickerBtn: UIButton!
    @IBOutlet var XContainerBtn: UIButton!
    @IBOutlet var writingContainerBtn: UIButton!
    @IBOutlet var otherBtn: UIButton!
    @IBOutlet var otherTxtView: UITextView!
    @IBOutlet var maxCountLbl: UILabel!
    @IBOutlet var sealedOverlayView: UIView!
    @IBOutlet var nonSealedOverlayView: UIView!
    @IBOutlet var packageConditionAlertLabel: UILabel!
    var selectedButton: UIButton!
    var selectedDeleteButton: UIButton!
    
    @IBOutlet var uploadView1: UIView!
    @IBOutlet var uploadView2: UIView!
    @IBOutlet var originalPkgAlertView: UIView!
    @IBOutlet weak var originalPkgAlertLbl: UILabel!
    @IBOutlet var quantitySoldAlertView: UIView!
    @IBOutlet var quantitySoldAlertLbl: UILabel!
    @IBOutlet var dummyView: UIView!
    @IBOutlet var enterNdcView: UIView!
    @IBOutlet weak var packQtyView: UIView!
    @IBOutlet weak var packAvailableView: UIView!
    @IBOutlet weak var partialAlertView: UIView!
    @IBOutlet weak var partialAlertViewHeight: NSLayoutConstraint!
    @IBOutlet weak var partialErrorMessageLbl: UILabel!
    @IBOutlet weak var originalPackageView: UIView!
    @IBOutlet weak var optionalPkgConditionView: UIView!
    
    @IBOutlet var quantitySoldAlertViewHeight: NSLayoutConstraint!
    @IBOutlet var originalPkgAlertViewHeight: NSLayoutConstraint!
    
    
    //MARK: - Set price view references.
    
    @IBOutlet var setPriceParentView: UIView!
    @IBOutlet var sliderControl: UISlider!
    @IBOutlet var floatingView: UIView!
    @IBOutlet var floatingLabel: UILabel!
    @IBOutlet var maximumBorderView: UIView!
    @IBOutlet var minimumBorderView: UIView!
    @IBOutlet weak var minFloatingLbl: UILabel!
    @IBOutlet weak var maxFloatingLbl: UILabel!
    @IBOutlet var maxPackPriceTxtField: UITextField!
    @IBOutlet var minPackPriceTxtField: UITextField!
    @IBOutlet var fixedButton: UIButton!
    @IBOutlet var decliningButton: UIButton!
    @IBOutlet var yourPriceLbl: UILabel!
    @IBOutlet var minPriceLbl: UILabel!
    @IBOutlet var maxCurrentLbl: UILabel!
    @IBOutlet var maxPriceLbl: UILabel!
    @IBOutlet var yourWacDiscountLbl: UILabel!
    @IBOutlet var minWacDiscountLbl: UILabel!
    @IBOutlet var maxWacDiscountLbl: UILabel!
    @IBOutlet var yourExpiryDateLbl: UILabel!
    @IBOutlet var minExpiryDateLbl: UILabel!
    @IBOutlet var maxExpiryDateLbl: UILabel!
    @IBOutlet var minimumPriceView: UIView!
    @IBOutlet var maxPriceOptionAlertView: UIView!
    @IBOutlet var minPriceOptionAlertView: UIView!
    @IBOutlet var maxPriceOptionAlertLbl: UILabel!
    @IBOutlet var minPriceOptionAlertLbl: UILabel!
    @IBOutlet var minPriceViewHeightConstraint: NSLayoutConstraint!
    @IBOutlet var maxPriceOptionAlertHeightConstraint: NSLayoutConstraint!
    @IBOutlet var minPriceOptionAlertHeightConstraint: NSLayoutConstraint!
    let minimumValue : Int = 10
    let maximumValue : Int = 99
    
    
    //MARK: - Select shipping view references.
    @IBOutlet var selectShippingParentView: UIView!
    @IBOutlet var shippingPaymentBtn1: UIButton!
    @IBOutlet var shippingPaymentBtn2: UIButton!
    @IBOutlet var shippingPaymentBtn3: UIButton!
    @IBOutlet var shippingPaymentLbl1: UILabel!
    @IBOutlet var shippingPaymentLbl2: UILabel!
    @IBOutlet var shippingPaymentLbl3: UILabel!
    @IBOutlet var shippingMethodBtn1: UIButton!
    @IBOutlet var shippingMethodBtn2: UIButton!
    @IBOutlet var shippingMethodBtn3: UIButton!
    @IBOutlet var shippingMethodImg1: UIImageView!
    @IBOutlet var shippingMethodImg2: UIImageView!
    @IBOutlet var shippingMethodImg3: UIImageView!
    @IBOutlet var shippingMethodLbl1: UILabel!
    @IBOutlet var shippingMethodLbl2: UILabel!
    @IBOutlet var shippingMethodLbl3: UILabel!
    @IBOutlet var overnightShippingAlertViewHeightConstraint: NSLayoutConstraint!
    @IBOutlet weak var postBtn: UIButton!
    @IBOutlet weak var postAndNewBtn: UIButton!
    var selectedPostButton: UIButton?
    
//    var scanControl: ScanController!
    var photoEditor: imageCapture = imageCapture()
    var pickerView: DatePickerView!
    var ndcDetailsModel: NDCDetailsDataModel!
    var shippingLOVModel: ShippingLOVDataModel!
    

    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.resetNDCScreenDetails()
        
       
    }
    
    override func viewDidLayoutSubviews() {
        print("Called")
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
//    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
//        if let touch = touches.first {
//            // ...
//        }
//        super.touchesBegan(touches, with: event)
//    }
    
    //MARK: - IBAction functions
    
    //Updates the price based on the slider value
    @IBAction func sliderValueChanged(slider: UISlider){
        self.onSliderValueChanged(slider: slider)
    }
    
    func onSliderValueChanged(slider: UISlider) {
        var maximumDiscount: Float = Float(maximumValue)
        var unitPrice: Double = 0.0
        var calculatedPackPrice: Double = 0.0
        var packPrice: Double?
        
        
        if fullPackBtn.isSelected {
            maximumDiscount = Float(100-(2.00*100.0/ndcDetailsModel.packPrice!))
            calculatedPackPrice = ndcDetailsModel.packPrice!
        }
        else {
            var partialPackValue: Double = 0.0
            if (partialTxtField.text?.count)! > 0 {
                partialPackValue = Double(partialTxtField.text!)!
            }
            if ndcDetailsModel.packageQuantity == 1 {
                unitPrice = ndcDetailsModel.packPrice!/Double(ndcDetailsModel.packageSize!)!
            }
            else {
                unitPrice = ndcDetailsModel.packPrice!/Double(ndcDetailsModel.packageQuantity!)
            }
            calculatedPackPrice = unitPrice*partialPackValue
            maximumDiscount = Float(100-(2.00*100.0/calculatedPackPrice))
        }
        if maximumDiscount < Float(minimumValue){
            maximumDiscount = Float(minimumValue)
        }
        else if maximumDiscount > Float(maximumValue){
            maximumDiscount = Float(maximumValue)
        }
        var discountCalculation = sliderControl.value
        if discountCalculation > maximumDiscount {
            discountCalculation = maximumDiscount
            sliderControl.value = discountCalculation
        }
        else if discountCalculation < 10.0 {
            discountCalculation = 10.0
        }
        packPrice = (calculatedPackPrice-(Double(discountCalculation)*calculatedPackPrice/100.0)).rounded(toPlaces: 2)
//        let price = packPrice?.rounded(FloatingPointRoundingRule.down)
//        packPrice = ((calculatedPackPrice-(Double(discountCalculation)*calculatedPackPrice/100.0))*100).rounded()/100
        let text = String(format: "%.2f", arguments: [packPrice!])
//        let maximumPackPrice = "\(packPrice ?? 0.00)"
        self.updateSliderControl()
        maxPackPriceTxtField.text = text
        self.updatePrices(maxPrice: text)
    }
    
    func updateSliderControl() {
        print("Slider value is: \(sliderControl.value)")
        if Int(sliderControl.value) < minimumValue{
            sliderControl.value = Float(minimumValue)
        }
        if Int(sliderControl.value) > maximumValue{
            sliderControl.value = Float(maximumValue)
        }
        
        sliderControl.maximumTrackImage(for: UIControlState.highlighted)
        let trackRect: CGRect  = sliderControl.trackRect(forBounds: sliderControl.bounds)
        let thumbRect: CGRect  = sliderControl.thumbRect(forBounds: sliderControl.bounds , trackRect: trackRect, value: sliderControl.value)
        let x = thumbRect.origin.x + sliderControl.frame.origin.x+10
        let y = sliderControl.frame.origin.y-10
        floatingView?.center = CGPoint(x: x, y: y)
        floatingLabel?.text = "\(String(Int(sliderControl.value.rounded())))%"
    }
    
    //Updates the price based on the value entered in textfield.
    func updatePriceOnTxtEntered(value: String){
//        sliderControl.value = Float(value)!
    }
    
    //Opens the scanner control
    @IBAction func presentScanControl(){
        if AVCaptureDevice.authorizationStatus(forMediaType: AVMediaTypeVideo) ==  AVAuthorizationStatus.authorized {
            // Already Authorized
            let storyboard = UIStoryboard(name: "Main", bundle: nil)
            let scanControl = storyboard.instantiateViewController(withIdentifier: "scanControl") as! ScanController
            scanControl.scanDelegate = self
            //        UIApplication.topViewController()?.present(scanControl, animated: true, completion: nil)
            NavigationModel.sharedInstance.homeControl?.present(scanControl, animated: true, completion: nil)
        } else {
            AVCaptureDevice.requestAccess(forMediaType: AVMediaTypeVideo, completionHandler: { (granted: Bool) -> Void in
                if granted == true {
                    // User granted
                    let storyboard = UIStoryboard(name: "Main", bundle: nil)
                    let scanControl = storyboard.instantiateViewController(withIdentifier: "scanControl") as! ScanController
                    scanControl.scanDelegate = self
                    //        UIApplication.topViewController()?.present(scanControl, animated: true, completion: nil)
                    NavigationModel.sharedInstance.homeControl?.present(scanControl, animated: true, completion: nil)
                } else {
                    // User Rejected
                    AlertControl.sharedInstance.presentAlertView(alertTitle: "Camera access denied", alertMessage: "Open the settings app to change privacy settings", delegate: nil)
                }
            })
        }
    }
    
    
    
    //Create web service request and verify NDC
    @IBAction func verifyNDCNumber(){
        self.resetNDCScreenDetails()
        var ndcNumber: String
        ndcNumber = ndcTxtField.text!
        ndcTxtField.resignFirstResponder()
        if ndcNumber == "" || !isValidNDCNumber(ndcNumber: ndcNumber) {
            AlertControl.sharedInstance.presentAlertView(alertTitle: "A valid NDC is required before you are allowed to proceed", alertMessage: "", delegate: nil)
            return
        }
        let hyphenCount = self.SpecificLetterCount(str: ndcNumber, char: "-")
        if ndcNumber.count-hyphenCount < 10 {
            AlertControl.sharedInstance.presentAlertView(alertTitle: "A valid NDC is required before you are allowed to proceed", alertMessage: "", delegate: nil)
            return
        }
        myGroup.enter()
        if NetworkManager.sharedInstance.isReachable(){
            _ = NetworkManager.sharedInstance.checkTypeOfNetwork()
            if NetworkManager.sharedInstance.isReachableViaWiFi || NetworkManager.sharedInstance.isReachableViaCellular{
                LoadingView.shareInstance.showLodingView(parentView: (NavigationModel.sharedInstance.homeControl?.view!)!)
                NetworkManager.sharedInstance.manageAPIRequest(requestType: .ServiceTypeLOVDetails, requestString: "", delegate: self)
            }
        }
        else{
            AlertControl.sharedInstance.presentAlertView(alertTitle: "Kindly check your internet connectivity", alertMessage: "", delegate: nil)
        }
        myGroup.notify(queue: DispatchQueue.main) {
            if NetworkManager.sharedInstance.isReachable(){
                _ = NetworkManager.sharedInstance.checkTypeOfNetwork()
                if NetworkManager.sharedInstance.isReachableViaWiFi || NetworkManager.sharedInstance.isReachableViaCellular{
                    LoadingView.shareInstance.showLodingView(parentView: self.view.window!)
                    NetworkManager.sharedInstance.manageAPIRequest(requestType: .ServiceTypeNDCVerification, requestString: RequestDataModel.sharedInstance.makeRequestForNDCVerification(ndcNumber: ndcNumber), delegate: self)
                }
            }
            else{
                AlertControl.sharedInstance.presentAlertView(alertTitle: "Kindly check your internet connectivity", alertMessage: "", delegate: nil)
            }
        }
//        myGroup.leave()
        
    }
    
    //Perform actions based on which type of pack quantity is selected
    @IBAction func onPackQuantityClicked(sender: UIButton){
        if !sender.isSelected {
            sender.isSelected = true
            if sender == fullPackBtn{
                self.view.endEditing(true)
                partialPackBtn.isSelected = false
                partialTxtField.text = ""
                packAvailableBtn.isUserInteractionEnabled = true
                sealedOverlayView.isHidden = true
                self.handleNDCScreenBusinessRules(actiontype: .FullPackClicked)
               /*if ndcDetailsModel.stateRestrictionType == .allowOnlyNonSealed{
                    packAvailableBtn.isUserInteractionEnabled = false
                    partialTxtField.text = ""
                }
                if ndcDetailsModel.stateRestrictionType == .allowOnlySealed {
                    packAvailableBtn.isUserInteractionEnabled = true
                    partialTxtField.text = ""
                }
                else if ndcDetailsModel.stateRestrictionType == .allowBoth{
                    if ndcDetailsModel.isNonSealedPartialExists! {
                        sealedPackBtn.isSelected = true
                        nonSealedOverlayView.isHidden = false
                    }
                    else {
                        sealedPackBtn.isSelected = false
                    }
                    nonSealedPackBtn.isSelected = false
//                    sealedPackBtn.alpha = 1.0
//                    sealedPackBtn.isUserInteractionEnabled = true
                    sealedOverlayView.isHidden = true
                    nonSealedPackBtn.alpha = 1.0
                    nonSealedPackBtn.isUserInteractionEnabled = true
                    packAvailableBtn.isUserInteractionEnabled = true
                    partialTxtField.text = ""
                }*/
            }
            else{
                fullPackBtn.isSelected = false
                packAvailableTxtField.text = "1"
                sealedOverlayView.isHidden = false
                packAvailableBtn.isUserInteractionEnabled = false
                self.originalPackageConditionClicked(sender: nonSealedPackBtn)
                self.handleNDCScreenBusinessRules(actiontype: .PartialPackClicked)
                partialTxtField.becomeFirstResponder()
                /*if ndcDetailsModel.isNonSealedPartialExists! {
                    originalPkgAlertLbl.text = ndcDetailsModel.nonSealedPartialErrorMessage
                    originalPkgAlertView.isHidden = false
                    originalPkgAlertViewHeight.constant = originalPkgAlertLbl.frame.size.height+21.5
                }
                else {
                    originalPkgAlertViewHeight.constant = 0
                    originalPkgAlertView.isHidden = true
                }
//                sealedPackBtn.alpha = 0.6
//                sealedPackBtn.isUserInteractionEnabled = false
                sealedOverlayView.isHidden = false
                packAvailableBtn.isUserInteractionEnabled = false
                self.originalPackageConditionClicked(sender: nonSealedPackBtn)
//                if !partialTxtField.isFirstResponder {
//                    self.view.endEditing(true)
//                }
                partialTxtField.becomeFirstResponder()*/
            }
        }
    }
    
    //Perform actions based on Original package type selected
    @IBAction func originalPackageConditionClicked(sender: UIButton){
        self.view.endEditing(true)
        if !sender.isSelected {
            sender.isSelected = true
//            packageConditionAlertLabel.isHidden = true
            if sender == sealedPackBtn{
                nonSealedPackBtn.isSelected = false
                packAvailableBtn.isUserInteractionEnabled = true
            }
            else{
                sealedPackBtn.isSelected = false
                packAvailableTxtField.text = "1"
                packAvailableBtn.isUserInteractionEnabled = false
                /*if ndcDetailsModel.stateRestrictionType == .allowBoth {
                    if ndcDetailsModel.isPartialPackExists! {
                        if ndcDetailsModel.isNonSealedPartialExists! {
                            nonSealedOverlayView.isHidden = false
                        }
                        else {
                            nonSealedOverlayView.isHidden = true
                        }
                        partialOverlayView.isHidden = false
                        partialAlertViewHeight.constant = PARTIAL_ERROR_MESSAGE_HEIGHT
                        partialErrorMessageLbl.text = ndcDetailsModel.partialPackMessage
                        partialAlertView.isHidden = false
                    }
                    else {
                        nonSealedOverlayView.isHidden = true
                        partialAlertViewHeight.constant = 0
                        partialAlertView.isHidden = true
                    }
                    nonSealedOverlayView.isHidden = true
                }*/
            }
        }
    }
    
    //Performs action based on optional package condition selected.
    @IBAction func optionalPackageConditionClicked(sender: UIButton){
        if sender.isSelected {
            sender.isSelected = false
        }
        else{
            sender.isSelected = true
//            if sender == otherBtn{
//                otherTxtView.becomeFirstResponder()
//            }
        }
        if tornPkgBtn.isSelected || stickerBtn.isSelected || XContainerBtn.isSelected || writingContainerBtn.isSelected || otherBtn.isSelected{
            otherTxtView.isEditable = true
            otherTxtView.alpha = 1.0
        }
        else {
            otherTxtView.isEditable = false
            otherTxtView.alpha = 0.6
            otherTxtView.text = ""
        }
    }
    
    //Pops a dropdown to show the quantity
    @IBAction func dropDownClicked(sender: UIButton){
        self.view.endEditing(true)
        let dropDownData = NSMutableArray()
        selectedButton = sender
        if sender == uploadBtn1 || sender == uploadBtn2 {
            if sender.image(for: .normal) == nil {
                let cameraObject1 = DropDownModel()
                cameraObject1.modelIdString = "1"
                cameraObject1.modelDataString = "Camera"
                cameraObject1.image = ""
                dropDownData.add(cameraObject1)
                
                let cameraObject2 = DropDownModel()
                cameraObject2.modelIdString = "2"
                cameraObject2.modelDataString = "Gallery"
                cameraObject2.image = ""
                dropDownData.add(cameraObject2)
                DropDownView.defaultDropDownControl.showDropDownForData(dropDownData,DropDownType.SingleSelectionDefault, CGRect.zero, (NavigationModel.sharedInstance.homeControl?.view)!, self as DropDownViewDelegate, [])
            }
            else{
                /*let optionMenu = UIAlertController(title: nil, message: "", preferredStyle: .actionSheet)
                
                // 2
                let deleteAction = UIAlertAction(title: "Delete", style: .default, handler: {
                    (alert: UIAlertAction!) -> Void in
                    sender.setImage(UIImage.init(named: "Upload_Btn.png"), for: .normal)
                })
                
                let cancelAction = UIAlertAction(title: "Cancel", style: .cancel, handler: {
                    (alert: UIAlertAction!) -> Void in
                })
                optionMenu.addAction(deleteAction)
                optionMenu.addAction(cancelAction)
//                self.present(optionMenu, animated: true, completion: nil)
                NavigationModel.sharedInstance.homeControl?.present(optionMenu, animated: true, completion: nil)*/
            }
        }
        else{
            for i in 1..<ndcDetailsModel.packAvailable!+1 {
                let model = DropDownModel()
                model.modelIdString = "\(i)" as NSString
                model.modelDataString = "\(i)" as NSString
                dropDownData.add(model)
            }
            DropDownView.defaultDropDownControl.showDropDownForData(dropDownData,DropDownType.SingleSelectionDefault, CGRect.zero, (NavigationModel.sharedInstance.homeControl?.view)!, self as DropDownViewDelegate, [])
            DropDownView.defaultDropDownControl.dropDownListSearch?.isHidden = true
        }
    }
    
    //On clicking on the wizard buttons
    @IBAction func onWizardClicked(sender: UIButton){
        self.view.endEditing(true)
        var flag: Bool = false
        if !sender.isSelected {
            switch sender {
            case enterNDCBtn:
                if (!setPriceParentView.isHidden || !selectShippingParentView.isHidden) {
                    if maxPriceOptionAlertHeightConstraint.constant > 0 || minPriceOptionAlertHeightConstraint.constant > 0 {
                        wizardTick2Img.isHidden = true
                    }
                    else {
                        wizardTick2Img.isHidden = false
                    }
                    wizardTick1Img.isHidden = true
                    if selectShippingBtn.isUserInteractionEnabled {
                        wizardTick3Img.isHidden = false
                    }
                    else {
                        wizardTick3Img.isHidden = true
                    }
                    enterNDCParentView.isHidden = false
                    setPriceParentView.isHidden = true
                    selectShippingParentView.isHidden = true
                    flag = true
                    oneLbl.backgroundColor = SELECTED_WIZARD_LABEL_BG_COLOR
                    twoLbl.backgroundColor = DEFAULT_WIZARD_LABEL_BG_COLOR
                    threeLbl.backgroundColor = DEFAULT_WIZARD_LABEL_BG_COLOR
                    oneLbl.textColor = SELECTED_WIZARD_LABEL_TEXT_COLOR
                    twoLbl.textColor = DEFAULT_WIZARD_LABEL_TEXT_COLOR
                    threeLbl.textColor = DEFAULT_WIZARD_LABEL_TEXT_COLOR
                    oneLbl.layer.borderColor = SELECTED_WIZARD_LABEL_BORDER_COLOR
                    twoLbl.layer.borderColor = DEFAULT_WIZARD_LABEL_BORDER_COLOR
                    threeLbl.layer.borderColor = DEFAULT_WIZARD_LABEL_BORDER_COLOR
                }
                
                break
                
            case setPriceBtn:
                if (!enterNDCParentView.isHidden) {
                    if ndcTxtField.text != ndcDetailsModel.formattedNDCNumber {
                        AlertControl.sharedInstance.presentAlertView(alertTitle: "Please re-enter as the NDC has changed", alertMessage: "", delegate: nil)
                        return
                    }
                }
                if (!enterNDCParentView.isHidden || !selectShippingParentView.isHidden) && self.isValidNDCScreen() {
                    wizardTick1Img.isHidden = false
                    wizardTick2Img.isHidden = true
                    if selectShippingBtn.isUserInteractionEnabled {
                        wizardTick3Img.isHidden = false
                    }
                    else {
                        wizardTick3Img.isHidden = true
                    }
                    enterNDCParentView.isHidden = true
                    setPriceParentView.isHidden = false
                    selectShippingParentView.isHidden = true
                    flag = true
                    oneLbl.backgroundColor = DEFAULT_WIZARD_LABEL_BG_COLOR
                    twoLbl.backgroundColor = SELECTED_WIZARD_LABEL_BG_COLOR
                    threeLbl.backgroundColor = DEFAULT_WIZARD_LABEL_BG_COLOR
                    oneLbl.textColor = DEFAULT_WIZARD_LABEL_TEXT_COLOR
                    twoLbl.textColor = SELECTED_WIZARD_LABEL_TEXT_COLOR
                    threeLbl.textColor = DEFAULT_WIZARD_LABEL_TEXT_COLOR
                    oneLbl.layer.borderColor = DEFAULT_WIZARD_LABEL_BORDER_COLOR
                    twoLbl.layer.borderColor = SELECTED_WIZARD_LABEL_BORDER_COLOR
                    threeLbl.layer.borderColor = DEFAULT_WIZARD_LABEL_BORDER_COLOR
                    
                    if ((maxPackPriceTxtField.text?.count) != nil) {
                        self.updatePrices(maxPrice: maxPackPriceTxtField.text!)
                    }
                    /*if ((minPackPriceTxtField.text?.count) != nil) && Double(minPackPriceTxtField.text!)! < 2.00 {
                        minPriceOptionAlertHeightConstraint.constant = MINIMUM_PRICE_ALERT_VIEW_HEIGHT
                        minPriceOptionAlertLbl.text = "Items must be listed for $2.00 or more."
                    }*/
                }
                break
                
            case selectShippingBtn:
                if (!enterNDCParentView.isHidden) {
                    if ndcTxtField.text != ndcDetailsModel.formattedNDCNumber {
                        AlertControl.sharedInstance.presentAlertView(alertTitle: "Please re-enter as the NDC has changed", alertMessage: "", delegate: nil)
                        return
                    }
                }
                if (!setPriceParentView.isHidden && self.isValidSetPriceScreen()) || (!enterNDCParentView.isHidden && self.isValidNDCScreen()){
                    wizardTick1Img.isHidden = false
                    wizardTick2Img.isHidden = false
                    wizardTick3Img.isHidden = true
                    enterNDCParentView.isHidden = true
                    setPriceParentView.isHidden = true
                    selectShippingParentView.isHidden = false
                    flag = true
                    oneLbl.backgroundColor = DEFAULT_WIZARD_LABEL_BG_COLOR
                    twoLbl.backgroundColor = DEFAULT_WIZARD_LABEL_BG_COLOR
                    threeLbl.backgroundColor = SELECTED_WIZARD_LABEL_BG_COLOR
                    oneLbl.textColor = DEFAULT_WIZARD_LABEL_TEXT_COLOR
                    twoLbl.textColor = DEFAULT_WIZARD_LABEL_TEXT_COLOR
                    threeLbl.textColor = SELECTED_WIZARD_LABEL_TEXT_COLOR
                    oneLbl.layer.borderColor = DEFAULT_WIZARD_LABEL_BORDER_COLOR
                    twoLbl.layer.borderColor = DEFAULT_WIZARD_LABEL_BORDER_COLOR
                    threeLbl.layer.borderColor = SELECTED_WIZARD_LABEL_BORDER_COLOR
                    
                    
                    let shippingModel = shippingLOVModel.shippingHandlingArray[1] as! ShippingLOVDataModel
                    var orderPrice : Float!
                    if fixedButton.isSelected {
                        orderPrice = Float(maxPackPriceTxtField.text!)!
                    }
                    else {
                        orderPrice = Float(minPackPriceTxtField.text!)!
                    }
                    if orderPrice >= shippingModel.shippingAmount! {
                        shippingPaymentBtn2.isUserInteractionEnabled = true
                        shippingPaymentLbl2.alpha = 1.0
                        shippingPaymentBtn2.alpha = 1.0
                    }
                    else {
                        shippingPaymentBtn2.isUserInteractionEnabled = false
                        shippingPaymentLbl2.alpha = 0.6
                        shippingPaymentBtn2.alpha = 0.6
                        if shippingPaymentBtn2.isSelected {
                            shippingPaymentBtn1.isSelected = true
                            shippingPaymentBtn2.isSelected = false
                            self.processShippingMethod(payee: .BuyerPays)
                        }
                    }
                    
                }
                break
                
            default:
                break
            }
            if flag {
                enterNDCBtn.backgroundColor = DEFAULT_WIZARD_BG_COLOR
                setPriceBtn.backgroundColor = DEFAULT_WIZARD_BG_COLOR
                selectShippingBtn.backgroundColor = DEFAULT_WIZARD_BG_COLOR
                sender.backgroundColor = SELECTED_WIZARD_BG_COLOR
                selectedWizardButton = sender
            }
        }
    }
    
    //On clicking delete image button.
    @IBAction func deleteClicked(sender: UIButton){
        selectedDeleteButton = sender
        let alertControl = UIAlertController(title: "Are you sure you want to delete this image?", message: "", preferredStyle: UIAlertControllerStyle.alert)
        alertControl.addAction(UIAlertAction(title:"YES", style: UIAlertActionStyle.default, handler: {actions in self.deleteImage()}))
        alertControl.addAction(UIAlertAction(title: "NO", style: UIAlertActionStyle.cancel, handler: nil))
        UIApplication.topViewController()?.present(alertControl, animated: true, completion: nil)
    }

    //Validate the form on clicking the next button.
    @IBAction func onNextButtonClicked(sender: UIButton){
        self.view.endEditing(true)
        
        switch selectedWizardButton {
        case enterNDCBtn:
            if ndcTxtField.text != ndcDetailsModel.formattedNDCNumber {
                AlertControl.sharedInstance.presentAlertView(alertTitle: "Please re-enter as the NDC has changed", alertMessage: "", delegate: nil)
                return
            }
            if self.isValidNDCScreen() {
                self.onWizardClicked(sender: setPriceBtn)
                setPriceBtn.isUserInteractionEnabled = true
            }
            break
            
        case setPriceBtn:
            if self.isValidSetPriceScreen() {
                self.onWizardClicked(sender: selectShippingBtn)
                selectShippingBtn.isUserInteractionEnabled = true
            }
            else {
                selectShippingBtn.isUserInteractionEnabled = false
            }
            break
            
        case selectShippingBtn:
            if ndcTxtField.text != ndcDetailsModel.formattedNDCNumber {
                AlertControl.sharedInstance.presentAlertView(alertTitle: "Please re-enter as the NDC has changed", alertMessage: "", delegate: nil)
                return
            }
            break
            
        default:
            break
        }
        /*if lotNumberTxtField.text?.trimmingCharacters(in: .whitespaces).count == 0 {
            AlertControl.sharedInstance.presentAlertView(alertTitle: "Please enter valid Lot Number", alertMessage: "", delegate: nil)
            return
        }
        if expiryDateTxtField.text?.trimmingCharacters(in: .whitespaces).count == 0 {
            AlertControl.sharedInstance.presentAlertView(alertTitle: "Please select valid Expiration Date", alertMessage: "", delegate: nil)
            return
        }
        if partialPackBtn.isSelected {
            if partialTxtField.text?.trimmingCharacters(in: .whitespaces).count == 0 {
                AlertControl.sharedInstance.presentAlertView(alertTitle: "Partial Packs must have at least quantity of 1", alertMessage: "", delegate: nil)
                return
            }
            else{
                if Int(partialTxtField.text!)! > ndcDetailsModel.packageSize! {
                    AlertControl.sharedInstance.presentAlertView(alertTitle: "Partial Packs must be less than or equal to Full Packs", alertMessage: "", delegate: nil)
                    return
                }
            }
        }
        if !(sealedPackBtn.isSelected || nonSealedPackBtn.isSelected) {
            AlertControl.sharedInstance.presentAlertView(alertTitle: "Please select the package condition", alertMessage: "", delegate: nil)
            return
        }
        if otherBtn.isSelected && otherTxtView.text.trimmingCharacters(in: .whitespacesAndNewlines).count < 4 {
            AlertControl.sharedInstance.presentAlertView(alertTitle: "Please describe the Package Condition", alertMessage: "", delegate: nil)
            return
        }
        AlertControl.sharedInstance.presentAlertView(alertTitle: "NDC data validated successfully. Set price and Shipping details are coming soon", alertMessage: "", delegate: nil)*/
        
    }
    
    @IBAction func onPriceOptionClicked(sender: UIButton) {
        if !sender.isSelected {
            if sender == fixedButton {
                fixedButton.isSelected = true
                decliningButton.isSelected = false
                minimumPriceView.isHidden = true
                minPriceViewHeightConstraint.constant = 0
                maxCurrentLbl.isHidden = true
                minPriceOptionAlertHeightConstraint.constant = 0.0
            }
            else {
                fixedButton.isSelected = false
                decliningButton.isSelected = true
                minimumPriceView.isHidden = false
                maxCurrentLbl.isHidden = false
                minPriceViewHeightConstraint.constant = CGFloat(MINIMUM_PRICE_VIEW_HEIGHT)
                if ((minPackPriceTxtField.text?.count) != nil) || Double(minPackPriceTxtField.text!)! < 2.00 {
                    minPriceOptionAlertHeightConstraint.constant = MINIMUM_PRICE_ALERT_VIEW_HEIGHT
                }
            }
        }
    }
    
    
    //On clicking the shipping payment method funciton
    @IBAction func onShippingPaymentButtonClicked(sender: UIButton) {
        if !sender.isSelected {
            switch sender {
            case shippingPaymentBtn1:
                shippingPaymentBtn1.isSelected = true
                shippingPaymentBtn2.isSelected = false
                shippingPaymentBtn3.isSelected = false
                break
                
            case shippingPaymentBtn2:
                shippingPaymentBtn1.isSelected = false
                shippingPaymentBtn2.isSelected = true
                shippingPaymentBtn3.isSelected = false
                break
                
            case shippingPaymentBtn3:
                shippingPaymentBtn1.isSelected = false
                shippingPaymentBtn2.isSelected = false
                shippingPaymentBtn3.isSelected = true
                break
                
            default:
                break
            }
            if sender == shippingPaymentBtn1 {
                self.processShippingMethod(payee: .BuyerPays)
            }
            else {
                self.processShippingMethod(payee: .SellerPays)
            }
        }
    }
    
    @IBAction func onShippingMethodButtonClicked(sender: UIButton) {
        if !sender.isSelected {
            switch sender {
            case shippingMethodBtn1:
                shippingMethodBtn1.isSelected = true
                shippingMethodBtn2.isSelected = false
                shippingMethodBtn3.isSelected = false
                break
                
            case shippingMethodBtn2:
                shippingMethodBtn1.isSelected = false
                shippingMethodBtn2.isSelected = true
                shippingMethodBtn3.isSelected = false
                break
                
            case shippingMethodBtn3:
                shippingMethodBtn1.isSelected = false
                shippingMethodBtn2.isSelected = false
                shippingMethodBtn3.isSelected = true
                break
                
            default:
                break
            }
        }
    }
    
    @IBAction func showToolTipAlert(sender: UIButton){
        AlertControl.sharedInstance.presentAlertView(alertTitle: "", alertMessage: "The price will first appear at the max price and decrease incrementally every week for 10 weeks by 10% of the difference in the max and min provided, until the Prescription Drug sells/transfers or the min price is reached, whichever comes first.", delegate: nil)
    }
    
    //Post an item button clicked.
    @IBAction func postAnItem(sender: UIButton) {
        selectedPostButton = sender
        let postParameters = NSMutableDictionary()
        var packageOption: String = "F"
        var packQuantity: String = "1"
        var packSize: String = ""
        var condition: String = ""
        var priceOption: String = "fixed"
        var packageCondition: String = "sealed"
        
        let shippingMethodArray = NSMutableArray.init(array: shippingLOVModel.shippingMethodsArray)
        let shippingHandlingArray = NSMutableArray.init(array: shippingLOVModel.shippingHandlingArray)
        
        var shippingHandling: String = (shippingHandlingArray[0] as! ShippingLOVDataModel).shippingHandlingName!
        var shippingMethod: String = (shippingMethodArray[0] as! ShippingLOVDataModel).shippingMethodName!
        
        if partialPackBtn.isSelected {
            packageOption = "P"
            packSize = partialTxtField.text!
        }
        else {
            packQuantity = packAvailableTxtField.text!
        }
        
        if nonSealedPackBtn.isSelected {
            packageCondition = "nonsealed"
        }
        
        for i in 700..<705 {
            if (self.view.viewWithTag(i) as! UIButton).isSelected {
                condition = condition+",\(i-699)"
            }
        }
        if condition.prefix(1) == "," {
            condition = String(condition.dropFirst())
        }
        
        if decliningButton.isSelected {
            priceOption = "declining"
        }
        
        if shippingPaymentBtn2.isSelected {
            shippingHandling = (shippingHandlingArray[1] as! ShippingLOVDataModel).shippingHandlingName!
        }
        else if shippingPaymentBtn3.isSelected{
            shippingHandling = (shippingHandlingArray[2] as! ShippingLOVDataModel).shippingHandlingName!
        }
        
        if shippingMethodBtn2.isSelected {
            shippingMethod = (shippingMethodArray[1] as! ShippingLOVDataModel).shippingMethodName!
        }
        else if shippingMethodBtn3.isSelected {
            shippingMethod = (shippingMethodArray[2] as! ShippingLOVDataModel).shippingMethodName!
        }
        
        
        postParameters.setValue(ndcDetailsModel.formattedNDCNumber, forKey: "ndc_number")
        postParameters.setValue(lotNumberTxtField.text, forKey: "lot_number")
        postParameters.setValue(expiryDateTxtField.text, forKey: "expire_date")
        postParameters.setValue(packageOption, forKey: "package_option")
        postParameters.setValue(packSize, forKey: "package_size")
        postParameters.setValue(packQuantity, forKey: "package_quantity")
        postParameters.setValue(condition, forKey: "condition")
        postParameters.setValue(priceOption, forKey: "price_option")
        postParameters.setValue(ndcDetailsModel.yourPricePerUnit, forKey: "unit_price")
        postParameters.setValue(maxPackPriceTxtField.text, forKey: "max_price")
        postParameters.setValue(minPackPriceTxtField.text, forKey: "min_price")
        postParameters.setValue(maxPackPriceTxtField.text, forKey: "current_price")
        postParameters.setValue(ndcDetailsModel.storageCondition, forKey: "storage")
        if ndcDetailsModel.priceType == "wac" {
            postParameters.setValue("0", forKey: "aawp_discount_price")
            postParameters.setValue(ndcDetailsModel.yourDiscount, forKey: "wac_discount_price")
        }
        else {
            postParameters.setValue(ndcDetailsModel.yourDiscount, forKey: "aawp_discount_price")
            postParameters.setValue("0", forKey: "wac_discount_price")
        }
        postParameters.setValue(shippingHandling, forKey: "shipping_handling_name")
        postParameters.setValue(shippingMethod, forKey: "shipping_method")
        postParameters.setValue("3", forKey: "package_unit")
        postParameters.setValue(otherTxtView.text, forKey: "seller_notes")
        postParameters.setValue(packageCondition, forKey: "package_condition")
        
        let imageDetailArray = NSMutableArray.init()
        let guid = ProcessInfo.processInfo.globallyUniqueString
        if uploadBtn1.image(for: .normal) != nil {
            let imageName = "iOS"+guid+".jpg"
            imageDetailArray.add([imageName, uploadBtn1.image(for: .normal)!])
        }
        if uploadBtn2.image(for: .normal) != nil {
            let imageName = "iOS"+guid+".jpg"
            imageDetailArray.add([imageName, uploadBtn2.image(for: .normal)!])
        }
        
        if NetworkManager.sharedInstance.isReachable(){
            _ = NetworkManager.sharedInstance.checkTypeOfNetwork()
            if NetworkManager.sharedInstance.isReachableViaWiFi || NetworkManager.sharedInstance.isReachableViaCellular{
                LoadingView.shareInstance.showLodingView(parentView: self.view.window!)
                NetworkManager.sharedInstance.manageAPIForMultiFormDataRequest(requestType: .ServiceTypePostAnItem, requestParam: postParameters, imageArray: imageDetailArray, delegate: self)
            }
        }
        else{
            AlertControl.sharedInstance.presentAlertView(alertTitle: "Kindly check your internet connectivity", alertMessage: "", delegate: nil)
        }
    }
    
    //MARK: - Instance functions
    
    //Validates the Enter NDC screen.
    func isValidNDCScreen() -> Bool {
        if lotNumberTxtField.text?.trimmingCharacters(in: .whitespaces).count == 0 {
            AlertControl.sharedInstance.presentAlertView(alertTitle: "Please enter valid Lot Number", alertMessage: "", delegate: nil)
            return false
        }
        if expiryDateTxtField.text?.trimmingCharacters(in: .whitespaces).count == 0 {
            AlertControl.sharedInstance.presentAlertView(alertTitle: "Please select valid Expiration Date", alertMessage: "", delegate: nil)
            return false
        }
        if partialPackBtn.isSelected {
            if partialTxtField.text?.trimmingCharacters(in: .whitespaces).count == 0 || Int(partialTxtField.text!)! == 0{
                AlertControl.sharedInstance.presentAlertView(alertTitle: "Partial Packs must have at least quantity of 1", alertMessage: "", delegate: nil)
                return false
            }
            else{
                var packageQty = 0
                if ndcDetailsModel.packageQuantity == 1 {
                    packageQty = Int(ndcDetailsModel.packageSize!)!
                }
                else {
                    packageQty = ndcDetailsModel.packageQuantity!
                }
                if Int(partialTxtField.text!)! > packageQty-1 {
                    AlertControl.sharedInstance.presentAlertView(alertTitle: "Partial Packs must be less than or equal to Full Packs", alertMessage: "", delegate: nil)
                    return false
                }
            }
        }
        if !(sealedPackBtn.isSelected || nonSealedPackBtn.isSelected) {
            AlertControl.sharedInstance.presentAlertView(alertTitle: "Please select the original package condition", alertMessage: "", delegate: nil)
            return false
        }
        if otherBtn.isSelected && otherTxtView.text.trimmingCharacters(in: .whitespacesAndNewlines).count < 4 {
            AlertControl.sharedInstance.presentAlertView(alertTitle: "Please describe the Package Condition", alertMessage: "", delegate: nil)
            return false
        }
        if (tornPkgBtn.isSelected || stickerBtn.isSelected || XContainerBtn.isSelected || writingContainerBtn.isSelected) && (otherTxtView.text.trimmingCharacters(in: .whitespacesAndNewlines).count > 0 && otherTxtView.text.trimmingCharacters(in: .whitespacesAndNewlines).count < 4) {
            AlertControl.sharedInstance.presentAlertView(alertTitle: "Please describe the Package Condition", alertMessage: "", delegate: nil)
            return false
        }
        return true
    }
    
    //Validates the Set price screen
    func isValidSetPriceScreen () -> Bool {
        if maxPriceOptionAlertHeightConstraint.constant > 0 || minPriceOptionAlertHeightConstraint.constant > 0 {
            AlertControl.sharedInstance.presentAlertView(alertTitle: "Please make the required corrections to proceed further.", alertMessage: "", delegate: nil)
            return false
        }
        return true
    }
    
    
    //Changes the userinteraction and alpha based on which field edited.
    func handleUserInteractions(flag: Bool, type: VerificationType){
        let alphaValue: CGFloat?
        if flag{
            alphaValue = 1.0
        }
        else {
            alphaValue = 0.6
        }
        /*switch type {
        case .defaultType:
            packView.isUserInteractionEnabled = flag
            packView.alpha = alphaValue!
            originalPackageView.isUserInteractionEnabled = flag
            originalPackageView.alpha = alphaValue!
            optionalPkgConditionView.isUserInteractionEnabled = flag
            optionalPkgConditionView.alpha = alphaValue!
            break
            
        case .NDCVerificaiton:
            packView.isUserInteractionEnabled = flag
            packView.alpha = alphaValue!
            originalPackageView.isUserInteractionEnabled = flag
            originalPackageView.alpha = alphaValue!
            optionalPkgConditionView.isUserInteractionEnabled = flag
            optionalPkgConditionView.alpha = alphaValue!
            break
            
        case .lotNumberVerification:
            packView.isUserInteractionEnabled = flag
            packView.alpha = alphaValue!
            originalPackageView.isUserInteractionEnabled = flag
            originalPackageView.alpha = alphaValue!
            optionalPkgConditionView.isUserInteractionEnabled = flag
            optionalPkgConditionView.alpha = alphaValue!
            break
            
        default:
            break
        }*/
        packQtyView.isUserInteractionEnabled = flag
        packQtyView.alpha = alphaValue!
        partialAlertView.alpha = alphaValue!
        quantitySoldAlertView.alpha = alphaValue!
        packAvailableView.isUserInteractionEnabled = flag
        packAvailableView.alpha = alphaValue!
        originalPackageView.isUserInteractionEnabled = flag
        originalPackageView.alpha = alphaValue!
        optionalPkgConditionView.isUserInteractionEnabled = flag
        optionalPkgConditionView.alpha = alphaValue!
    }
    
    //Shows dashed line over view
    func showDashedOverlayForView(sender: UIView, flag: Bool){
        
        let dashedBorder = CAShapeLayer()
        dashedBorder.strokeColor = UIColor.gray.cgColor
        dashedBorder.lineDashPattern = [2, 2]
        dashedBorder.fillColor = nil
        dashedBorder.path = UIBezierPath(rect: sender.bounds).cgPath
        dashedBorder.frame = sender.bounds
        sender.layer.borderColor = UIColor.lightGray.cgColor
        
        
//        let lineBorder = CAShapeLayer()
//        lineBorder.strokeColor = UIColor.gray.cgColor
//        lineBorder.lineDashPattern = [0, 0]
//        lineBorder.fillColor = nil
//        lineBorder.path = UIBezierPath(rect: sender.bounds).cgPath
//        lineBorder.frame = sender.bounds
//        sender.layer.borderColor = UIColor.lightGray.cgColor
        
        if flag{
            sender.layer.borderWidth = 0.0
            sender.layer.addSublayer(dashedBorder)
        }
        else{
//            sender.layer.insertSublayer(lineBorder, at: 1)
            //            sender.layer.sublayers?.forEach { $0.removeFromSuperlayer() }
            sender.layer.borderWidth = 1.0
        }
    }
    
    //Draws corner border for all the textfields and textview
    func drawBorder(){
        for views in enterNdcView.subviews{
            if views.isKind(of: UIView.self){
                for secondView in views.subviews{
                    if secondView.isKind(of: UITextField.self) || views.isKind(of: UITextView.self){
                        secondView.layer.borderColor = textBorderColor
                        secondView.layer.borderWidth = 1.0
                    }
                    for thirdView in secondView.subviews{
                        if thirdView.isKind(of: UITextField.self) || views.isKind(of: UITextView.self){
                            thirdView.layer.borderColor = textBorderColor
                            thirdView.layer.borderWidth = 1.0
                        }
                    }
                }
            }
        }
        otherTxtView.layer.borderColor = textBorderColor
        otherTxtView.layer.borderWidth = 1.0
    }
    
    //On clicking done button in Date picker
    @objc func donePicker()
    {
        expiryDateTxtField?.resignFirstResponder()
    }
    
    func allowBothPackage(actionType: ActionType) {
        if ndcDetailsModel.isPartialPackExists! {
            partialOverlayView.isHidden = false
            partialErrorMessageLbl.text = ndcDetailsModel.partialPackMessage
            self.view.layoutIfNeeded()
            partialAlertViewHeight.constant = partialErrorMessageLbl.frame.size.height+50.0
            partialAlertView.isHidden = false
        }
        else {
            partialOverlayView.isHidden = true
            partialAlertViewHeight.constant = 0
            partialAlertView.isHidden = true
        }
        if ndcDetailsModel.isNonSealedPartialExists! {
            if fullPackBtn.isSelected {
                sealedOverlayView.isHidden = true
                nonSealedOverlayView.isHidden = false
                sealedPackBtn.isSelected = true
                nonSealedPackBtn.isSelected = false
            }
            else {
                nonSealedOverlayView.isHidden = true
                sealedOverlayView.isHidden = false
//                originalPkgAlertViewHeight.constant = 0
//                originalPkgAlertView.isHidden = true
            }
            originalPkgAlertLbl.text = ndcDetailsModel.nonSealedPartialErrorMessage
            originalPkgAlertView.isHidden = false
            self.view.layoutIfNeeded()
            originalPkgAlertViewHeight.constant = originalPkgAlertLbl.frame.size.height+21.5
            if actionType == .PackQtyEquals1Clicked || actionType == .DefaultAction || actionType == .FullPackClicked{
                sealedPackBtn.isSelected = true
            }
            else {
                sealedPackBtn.isSelected = false
            }
            
        }
        else {
            originalPkgAlertViewHeight.constant = 0
            originalPkgAlertView.isHidden = true
            if actionType == .PackQtyEqualsGreaterThan1Clicked {
                nonSealedOverlayView.isHidden = false
                originalPkgAlertView.isHidden = false
                self.view.layoutIfNeeded()
                originalPkgAlertViewHeight.constant = originalPkgAlertLbl.frame.size.height+21.5
            }
            else {
                nonSealedOverlayView.isHidden = true
                originalPkgAlertView.isHidden = true
                originalPkgAlertViewHeight.constant = 0.0
            }
            if actionType == .FullPackClicked || actionType == .PackQtyEquals1Clicked{
                sealedPackBtn.isSelected = false
                nonSealedPackBtn.isSelected = false
            }
            else if actionType == .PartialPackClicked {
                sealedPackBtn.isSelected = false
                nonSealedPackBtn.isSelected = true
            }
        }
    }
    
    func allowOnlySealedPackage(actionType: ActionType) {
        partialOverlayView.isHidden = false
        nonSealedOverlayView.isHidden = false
        /*if actionType == .PackQtyEqualsGreaterThan1Clicked {
            originalPkgAlertView.isHidden = false
            originalPkgAlertViewHeight.constant = originalPkgAlertLbl.frame.size.height+21.5
        }
        else {
            originalPkgAlertView.isHidden = true
            originalPkgAlertViewHeight.constant = 0.0
        }*/
    }
    
    func allowOnlyNonSealedPackage(actionType: ActionType) {
        sealedOverlayView.isHidden = false
        sealedPackBtn.isSelected = false
        nonSealedOverlayView.isHidden = true
        nonSealedPackBtn.isSelected = true
        packAvailableBtn.isUserInteractionEnabled = false
        if ndcDetailsModel.isPartialPackExists! {
            partialOverlayView.isHidden = false
            partialErrorMessageLbl.text = ndcDetailsModel.partialPackMessage
            self.view.layoutIfNeeded()
            partialAlertViewHeight.constant = partialErrorMessageLbl.frame.size.height+50.0
            partialAlertView.isHidden = false
        }
        else {
            partialOverlayView.isHidden = true
            partialAlertViewHeight.constant = 0
            partialAlertView.isHidden = true
        }
        if ndcDetailsModel.isNonSealedPartialExists! {
            fullOverlayView.isHidden = false
            fullPackBtn.isSelected = false
            partialPackBtn.isSelected = true
            originalPkgAlertLbl.text = ndcDetailsModel.nonSealedPartialErrorMessage
            originalPkgAlertView.isHidden = false
            self.view.layoutIfNeeded()
            originalPkgAlertViewHeight.constant = originalPkgAlertLbl.frame.size.height+21.5
        }
        else {
            nonSealedOverlayView.isHidden = true
            sealedOverlayView.isHidden = false
            originalPkgAlertViewHeight.constant = 0
            originalPkgAlertView.isHidden = true
        }
    }
    
    func handleNDCScreenBusinessRules(actiontype: ActionType) {
        switch ndcDetailsModel.stateRestrictionType {
        case .allowBoth?:
            self.allowBothPackage(actionType: actiontype)
            break
            
        case .allowOnlySealed?:
            self.allowOnlySealedPackage(actionType: actiontype)
            break
            
        case StateRestriction.allowOnlyNonSealed?:
            self.allowOnlyNonSealedPackage(actionType: actiontype)
            break
            
        default:
            break
        }
        if Int(ndcDetailsModel.packageSize!)! == 1 {
            partialOverlayView.isHidden = false
        }
    }
    
    func bindVerifiedNDCDetails(){
        if ndcDetailsModel.packAvailable == 0 {
            AlertControl.sharedInstance.presentAlertView(alertTitle: ndcDetailsModel.quantitySoldMessage!, alertMessage: "", delegate: nil)
            self.resetNDCScreenDetails()
            return
        }
        ndcTxtField.text = ndcDetailsModel.formattedNDCNumber
        ndcNameLbl.text = ndcDetailsModel.ndcName
        similarItemsLbl.text = "\(ndcDetailsModel.similarItems ?? 0) Similar items posted"
//        medispanImgView.image = UIImage.init(named: ndcDetailsModel.medispanImageURL!)
        let url = URL(string: ndcDetailsModel.medispanImageURL!)
        let data = try? Data(contentsOf: url!)
        
        if let imageData = data {
            let image = UIImage(data: imageData)
            medispanImgView.image = image
        }
        strengthLbl.text = "\(ndcDetailsModel.packageStrength!) \(ndcDetailsModel.strengthUnitOfMeasure ?? "")"
        var storage: String = ""
        if ndcDetailsModel.storageCondition == "R" {
            storage = "Refrigerated"
            overnightShippingAlertViewHeightConstraint.constant = 60
        }
        else if ndcDetailsModel.storageCondition == "N" {
            storage = "Normal"
            overnightShippingAlertViewHeightConstraint.constant = 0
        }
        else{
            storage = "Frozen"
            overnightShippingAlertViewHeightConstraint.constant = 60
        }
        storageLbl.text = storage
//        packagingLbl.text = "\(ndcDetailsModel.packageType ?? "") (\(ndcDetailsModel.packageSize ?? "") \(ndcDetailsModel.packageUnitOfMeasure ?? ""))"
        packagingLbl.text = ndcDetailsModel.packaging
        if ndcDetailsModel.packageQuantity == 1 {
            maxCountLbl.text = "MAX:\(Int(ndcDetailsModel.packageSize!)!-1)"
        }
        else {
            maxCountLbl.text = "MAX:\(ndcDetailsModel.packageQuantity!-1)"
        }
        unitOfMeasureLbl.text = ndcDetailsModel.packageUnitOfMeasure
        formLbl.text = ndcDetailsModel.packageDosageForm
        
        
        if (ndcDetailsModel.quantitySoldMessage?.count)! > 0 {
            quantitySoldAlertLbl.text = ndcDetailsModel.quantitySoldMessage
            self.view.layoutIfNeeded()
            quantitySoldAlertViewHeight.constant = quantitySoldAlertLbl.frame.size.height+34.0
        }
        else {
            quantitySoldAlertViewHeight.constant = 0
        }
        
        
        if ndcDetailsModel.minimumPrice?.trimmingCharacters(in: .whitespaces).count == 0 || Double(ndcDetailsModel.minimumPrice!) == 0 {
            minPriceLbl.text = ""
        }
        else {
            let minPrice = String(format: "%.2f", arguments: [Double(ndcDetailsModel.minimumPrice!)!])
//            minPriceLbl.text = "$ "+ndcDetailsModel.minimumPrice!
            minPriceLbl.text = "$ \(minPrice)"
        }
        if ndcDetailsModel.maximumPrice?.trimmingCharacters(in: .whitespaces).count == 0 || Double(ndcDetailsModel.maximumPrice!) == 0 {
            maxPriceLbl.text = ""
        }
        else {
            let maxPrice = String(format: "%.2f", arguments: [Double(ndcDetailsModel.maximumPrice!)!])
            maxPriceLbl.text = "$ \(maxPrice)"
//            maxPriceLbl.text = "$ "+ndcDetailsModel.maximumPrice!
        }
        if ndcDetailsModel.minimumDiscount?.trimmingCharacters(in: .whitespaces).count == 0 || Int(ndcDetailsModel.minimumDiscount!) == 0  {
            minWacDiscountLbl.text = ""
        }
        else {
            let minDiscount = Int((Double(ndcDetailsModel.minimumDiscount!)?.rounded(FloatingPointRoundingRule.down))!)
//            let minDiscount = String(format: "%.2f", arguments: [(Double(ndcDetailsModel.minimumDiscount!)?.rounded(FloatingPointRoundingRule.down))!])
            minWacDiscountLbl.text = "\(minDiscount)%"
//            minWacDiscountLbl.text = ndcDetailsModel.minimumDiscount!+"%"
        }
        if ndcDetailsModel.maximumDiscount?.trimmingCharacters(in: .whitespaces).count == 0 || Int(ndcDetailsModel.maximumDiscount!) == 0  {
            maxWacDiscountLbl.text = ""
        }
        else {
            let maxDiscount = Int((Double(ndcDetailsModel.maximumDiscount!)?.rounded(FloatingPointRoundingRule.down))!)
//            let maxDiscount = String(format: "%.2f", arguments: [(Double(ndcDetailsModel.maximumDiscount!)?.rounded(FloatingPointRoundingRule.down))!])
            maxWacDiscountLbl.text = "\(maxDiscount)%"
//            maxWacDiscountLbl.text = ndcDetailsModel.maximumDiscount!+"%"
        }
        minExpiryDateLbl.text = ndcDetailsModel.minimumExpiryDate
        maxExpiryDateLbl.text = ndcDetailsModel.maximumExpiryDate
        
        
        self.handleNDCScreenBusinessRules(actiontype: .DefaultAction)
        self.processShippingMethod(payee: .Default)
    }
    
    func processShippingMethod(payee: PaymentType) {
        if ndcDetailsModel != nil {
            if ndcDetailsModel.storageCondition == "R" || ndcDetailsModel.storageCondition == "F" {
                shippingMethodBtn1.isSelected = false
                shippingMethodBtn2.isSelected = false
                shippingMethodBtn3.isSelected = true
                shippingMethodBtn1.isUserInteractionEnabled = false
                shippingMethodBtn2.isUserInteractionEnabled = false
                shippingMethodBtn3.isUserInteractionEnabled = true
                shippingMethodBtn1.alpha = 0.6
                shippingMethodBtn2.alpha = 0.6
                shippingMethodBtn3.alpha = 1.0
                shippingMethodImg1.alpha = 0.6
                shippingMethodImg2.alpha = 0.6
                shippingMethodImg3.alpha = 1.0
                shippingMethodLbl1.alpha = 0.6
                shippingMethodLbl2.alpha = 0.6
                shippingMethodLbl3.alpha = 1.0
            }
            else {
                if payee == .SellerPays {
//                    shippingMethodBtn2.isSelected = false
//                    shippingMethodBtn3.isSelected = false
//                    shippingMethodBtn3.isSelected = true
                    shippingMethodBtn1.isUserInteractionEnabled = true
                    shippingMethodBtn2.isUserInteractionEnabled = true
                    shippingMethodBtn3.isUserInteractionEnabled = true
                    shippingMethodBtn1.alpha = 1.0
                    shippingMethodBtn2.alpha = 1.0
                    shippingMethodBtn3.alpha = 1.0
                    shippingMethodImg1.alpha = 1.0
                    shippingMethodImg2.alpha = 1.0
                    shippingMethodImg3.alpha = 1.0
                    shippingMethodLbl1.alpha = 1.0
                    shippingMethodLbl2.alpha = 1.0
                    shippingMethodLbl3.alpha = 1.0
                }
                else if payee == .BuyerPays {
                    shippingMethodBtn1.isSelected = true
                    shippingMethodBtn2.isSelected = false
                    shippingMethodBtn3.isSelected = false
                    shippingMethodBtn2.isUserInteractionEnabled = true
                    shippingMethodBtn2.isUserInteractionEnabled = false
                    shippingMethodBtn3.isUserInteractionEnabled = false
                    shippingMethodBtn1.alpha = 1.0
                    shippingMethodBtn2.alpha = 0.6
                    shippingMethodBtn3.alpha = 0.6
                    shippingMethodLbl2.alpha = 1.0
                    shippingMethodLbl2.alpha = 0.6
                    shippingMethodLbl3.alpha = 0.6
                }
            }
        }
        else {
            if payee == .Default || payee == .BuyerPays{
                shippingPaymentBtn1.isSelected = true
                shippingPaymentBtn2.isSelected = false
                shippingPaymentBtn3.isSelected = false
                shippingPaymentBtn2.isUserInteractionEnabled = false
                shippingPaymentBtn2.alpha = 0.6
                shippingPaymentLbl2.alpha = 0.6
                
                shippingMethodBtn1.isSelected = true
                shippingMethodBtn2.isSelected = false
                shippingMethodBtn3.isSelected = false
                shippingMethodBtn2.isUserInteractionEnabled = true
                shippingMethodBtn2.isUserInteractionEnabled = false
                shippingMethodBtn3.isUserInteractionEnabled = false
                shippingMethodBtn1.alpha = 1.0
                shippingMethodBtn2.alpha = 0.6
                shippingMethodBtn3.alpha = 0.6
                shippingMethodLbl2.alpha = 1.0
                shippingMethodLbl2.alpha = 0.6
                shippingMethodLbl3.alpha = 0.6
            }
            else if payee == .SellerPays {
                shippingMethodBtn1.isSelected = true
                shippingMethodBtn2.isSelected = false
                shippingMethodBtn3.isSelected = false
                shippingMethodBtn2.isUserInteractionEnabled = true
                shippingMethodBtn2.isUserInteractionEnabled = true
                shippingMethodBtn3.isUserInteractionEnabled = true
                shippingMethodBtn1.alpha = 1.0
                shippingMethodBtn2.alpha = 1.0
                shippingMethodBtn3.alpha = 1.0
                shippingMethodLbl2.alpha = 1.0
                shippingMethodLbl2.alpha = 1.0
                shippingMethodLbl3.alpha = 1.0
            }
        }
    }
    
    func deleteImage(){
        if selectedDeleteButton == deleteImg1 {
//            uploadBtn1.setImage(UIImage.init(named: "Upload_Btn.png"), for: .normal)
            uploadBtn1.setImage(nil, for: .normal)
            uploadBtn1.layer.borderWidth = 0.0
            //            self.showDashedOverlayForView(sender: uploadBtn1, flag: true)
        }
        else{
//            uploadBtn2.setImage(UIImage.init(named: "Upload_Btn.png"), for: .normal)
            uploadBtn2.setImage(nil, for: .normal)
            uploadBtn2.layer.borderWidth = 0.0
            //            self.showDashedOverlayForView(sender: uploadBtn2, flag: true)
        }
        selectedDeleteButton.isHidden = true
    }
    
    //Verifies whether the entered ndc number is valid or not
    func isValidNDCNumber(ndcNumber: String) -> Bool{
        let maximumLength = 13
        if ndcNumber.count > maximumLength{
            return false
        }
        if ndcNumber.prefix(1) == "-"{
            return false
        }
        if ndcNumber.count == maximumLength && ndcNumber.suffix(1) == "-"{
            return false
        }
        let consecutiveCount = self.consectiveCountOf(letter: "-", inString: ndcNumber)
        if consecutiveCount > 1 {
            return false
        }
        let hyphenCount = self.SpecificLetterCount(str: ndcNumber, char: "-")
        if hyphenCount > 2 {
            return false
        }
        if (ndcNumber.rangeOfCharacter(from: CharacterSet.init(charactersIn: "0123456789-").inverted) != nil)
        {
            return false
        }
        return true
    }
    
    func isValidDecimalNumber(number: String) -> Bool {
        var maximumLength = 6
        if number.prefix(1) == "." {
            return false
        }
        if number.contains(".") {
            maximumLength = 9
            let decimalCount = self.SpecificLetterCount(str: number, char: ".")
            if decimalCount > 1 {
                return false
            }
            if (number.components(separatedBy: ".")[1]).count > 2{
                return false
            }
        }
        if number.count > maximumLength {
            return false
        }
        return true
    }
    
    //Verifies whether the entered maximum price option number is a valid one
    func isValidMaxPriceOption(maxPrice: String, range: NSRange) -> Bool {
        if self.isValidDecimalNumber(number: maxPrice) {
            self.updatePrices(maxPrice: maxPrice)
        }
        else {
            return false
        }
        return true
    }
    
    func isValidMinPriceOption(minPrice: String, maxPrice: String) -> Bool {
        if self.isValidDecimalNumber(number: minPrice) {
            if minPrice.count > 0 {
                if Double(minPrice)! < 2.0 {
                    minPriceOptionAlertHeightConstraint.constant = MINIMUM_PRICE_ALERT_VIEW_HEIGHT
                    minPriceOptionAlertLbl.text = "Items must be listed for $2.00 or more."
                }
                else {
                    var maxprice: Double = 0.0
                    if (maxPrice.count) > 0 {
                        maxprice = Double(maxPrice)!
                    }
                    let maxDiscountAmount = maxprice-((20.0*maxprice)/100.0)
                    if Double(minPrice)! > maxprice-((20.0*maxprice)/100.0) {
                        minPriceOptionAlertHeightConstraint.constant = MINIMUM_PRICE_ALERT_VIEW_HEIGHT
                        minPriceOptionAlertLbl.text = "Min Price must be at least 20% less than Max Price."
                        minPriceOptionAlertLbl.text = "Min price must be at least 20% less than Max price. Enter a value not to exceed \(maxDiscountAmount)"
                    }
                    else {
                        minPriceOptionAlertHeightConstraint.constant = 0
                    }
                }
            }
            else {
                minPriceOptionAlertHeightConstraint.constant = MINIMUM_PRICE_ALERT_VIEW_HEIGHT
                minPriceOptionAlertLbl.text = "Items must be listed for $2.00 or more."
            }
        }
        else {
            return false
        }
        return true
    }
    
    func updatePrices(maxPrice: String) {
        if maxPrice.count > 0 {
            if Double(maxPrice)! < 2.0 {
                maxPriceOptionAlertHeightConstraint.constant = MAXIMUM_PRICE_ALERT_VIEW_HEIGHT
                maxPriceOptionAlertLbl.text = "Items must be listed for $2.00 or more."
            }
            else {
                
                let price: Double!
                let maxDiscountPercentage: Double!
                let discountedPrice: Double!
                var partialCount: Double = 0.0
                var discountPercentage: Int?
                var unitPrice: Double = 0.00
                let partialText = partialTxtField.text
                if ndcDetailsModel.priceType == "wac" {
                    maxDiscountPercentage = 10.0
                    
                }
                else {
                    maxDiscountPercentage = 25.0
                }
                if fullPackBtn.isSelected {
                    price = ndcDetailsModel.packPrice
                    discountedPrice = price - (maxDiscountPercentage*price)/100.0
                    discountPercentage = Int((100.0-(Double(maxPrice)!*100)/ndcDetailsModel.packPrice!).rounded())
                    if ndcDetailsModel.packageQuantity == 1 {
                        unitPrice = Double(maxPrice)!/Double(ndcDetailsModel.packageSize!)!.rounded(toPlaces: 2)
                    }
                    else {
                        unitPrice = Double(maxPrice)!/Double(ndcDetailsModel.packageQuantity!).rounded(toPlaces: 2)
                    }
                }
                else {
                    if (partialText?.count)! > 0 {
                        partialCount = Double(partialText!)!
                    }
                    if ndcDetailsModel.packageQuantity == 1 {
                        price = ndcDetailsModel.packPrice!/Double(ndcDetailsModel.packageSize!)!
                    }
                    else {
                        price = ndcDetailsModel.packPrice!/Double(ndcDetailsModel.packageQuantity!)
                    }
                    discountedPrice = partialCount*(price - (maxDiscountPercentage*price)/100.0)
                    unitPrice = Double(maxPrice)!/partialCount
                    let calculatedPrice = price*partialCount
                    discountPercentage = Int((100.0-(Double(maxPrice)!*100)/calculatedPrice).rounded())
                }
                var discountValue: String = ""
                if Double(maxPrice)! > discountedPrice {
                    maxPriceOptionAlertHeightConstraint.constant = MAXIMUM_PRICE_ALERT_VIEW_HEIGHT
                    maxPriceOptionAlertLbl.text = "Pack Price should be at least 10% below WAC."
                    discountValue = "N/A"
                }
                else {
                    maxPriceOptionAlertHeightConstraint.constant = 0.0
                    discountValue = "\(discountPercentage ?? 0)%"
                }
                let text = String(format: "%.2f", arguments: [unitPrice])
                sliderControl.value = Float(discountPercentage!)
                self.updateSliderControl()
                yourPriceLbl.text = "$ \(text)"
                ndcDetailsModel.yourPricePerUnit = String(text)
                yourWacDiscountLbl.text = discountValue
                ndcDetailsModel.yourDiscount = "\(discountPercentage ?? 10)"
                if discountPercentage! < 10 {
                    yourWacDiscountLbl.text = "N/A"
                    ndcDetailsModel.yourDiscount = "10"
                }
            }
        }
        else {
            maxPriceOptionAlertHeightConstraint.constant = MAXIMUM_PRICE_ALERT_VIEW_HEIGHT
            maxPriceOptionAlertLbl.text = "Items must be listed for $2.00 or more."
            sliderControl.value = Float(maximumValue)
            self.updateSliderControl()
        }
        if decliningButton.isSelected {
            self.isValidMinPriceOption(minPrice: minPackPriceTxtField.text!, maxPrice: maxPrice)
        }
    }
    
    //Verifies whether the copied date is a valid one.
    func isValidExpiryDate(expMonth: String, expYear: String) -> Bool{
        let calendar = Calendar.current
        
        let monthFormat = DateFormatter.init()
        monthFormat.dateFormat = "MMMM"
        let yearFormat = DateFormatter.init()
        yearFormat.dateFormat = "yyyy"
        
        let selectedMonth = monthFormat.date(from: expMonth)
        let selectedYear = yearFormat.date(from: expYear)
        
        let expiryMonth = calendar.component(.month , from: selectedMonth!)
        let expiryYear = calendar.component(.year, from: selectedYear!)
        
        let currentDate = Date()
        let currentMonth = calendar.component(.month, from: currentDate)
        let currentYear = calendar.component(.year, from: currentDate)
        
        if expiryYear >= currentYear {
            if expiryYear == currentYear{
                if expiryMonth > currentMonth{
                    return true
                }
                else{
                    return false
                }
            }
            else{
                return true
            }
        }
        else{
            return false
        }
    }
    
    //Returns the number of consecutive count of a character.
    func consectiveCountOf(letter alphabet : String, inString source : String) -> Int
    {
        var occurance = 0
        for character in source
        {
            let value = String(character)
            
            if value == alphabet
            {
                occurance += 1
            }
            else if occurance > 0   // For there would at least be one occurance
            {
                break
            }
        }
        
        if occurance == 1 {
            return 0    // For Single Occurance, Repeat Count is Zero
        }
        return occurance
    }
    
    //Returns the number of specific letter
    func SpecificLetterCount(str:String, char:Character) ->Int {
        let letters = Array(str); var count = 0
        for letter in letters {
            if letter == char {
                count += 1
            }
        }
        return count
    }
    
    //Reset the screens
    func resetNDCScreenDetails(){
        
        
        enterNDCParentView.isHidden = false
        setPriceParentView.isHidden = true
        selectShippingParentView.isHidden = true
        selectedWizardButton = enterNDCBtn
        
        setPriceBtn.isUserInteractionEnabled = false
        selectShippingBtn.isUserInteractionEnabled = false
        
        scrollView.isScrollEnabled = false
        scrollView.contentOffset = CGPoint(x:0,y:0)
        priceScrollView.contentOffset = CGPoint(x:0,y:0)
        shippingScrollView.contentOffset = CGPoint(x:0,y:0)

        oneLbl.layer.cornerRadius = oneLbl.frame.size.width/2.0
        twoLbl.layer.cornerRadius = twoLbl.frame.size.width/2.0
        threeLbl.layer.cornerRadius = threeLbl.frame.size.width/2.0
        twoLbl.layer.borderWidth = 1.0
        threeLbl.layer.borderWidth = 1.0
        twoLbl.layer.borderColor = DEFAULT_WIZARD_LABEL_BORDER_COLOR
        threeLbl.layer.borderColor = DEFAULT_WIZARD_LABEL_BORDER_COLOR
        // Do any additional setup after loading the view.
        
        self.showDashedOverlayForView(sender: uploadView1, flag: true)
        self.showDashedOverlayForView(sender: uploadView2, flag: true)
        uploadBtn1.layer.borderColor = UIColor.lightGray.cgColor
        uploadBtn2.layer.borderColor = UIColor.lightGray.cgColor
        uploadBtn1.setImage(nil, for: .normal)
        uploadBtn2.setImage(nil, for: .normal)
        uploadBtn1.layer.borderWidth = 0.0
        uploadBtn2.layer.borderWidth = 0.0
        deleteImg1.isHidden = true
        deleteImg1.isHidden = true
        
        self.drawBorder()
        sealedPackBtn.isSelected = true
        photoEditor.parentViewCont = self
        self.handleUserInteractions(flag: false, type: .defaultType)
        quantitySoldAlertViewHeight.constant = 0
        originalPkgAlertViewHeight.constant = 0
        originalPkgAlertView.isHidden = true
        
        minPriceViewHeightConstraint.constant = 0
        maxPriceOptionAlertHeightConstraint.constant = 0
        minPriceOptionAlertHeightConstraint.constant = 0
        floatingView.layer.cornerRadius = 2.0
        floatingView.layer.borderWidth = 1.0
        floatingView.layer.borderColor = UIColor.lightGray.cgColor
        sliderControl.translatesAutoresizingMaskIntoConstraints = true
        sliderControl.value = Float(maximumValue)
        sliderControl.setThumbImage(UIImage(named: SLIDER_THUMB_IMAGE), for: UIControlState.highlighted)
        sliderControl.setThumbImage(UIImage(named: SLIDER_THUMB_IMAGE), for: UIControlState.normal)
        sliderControl.setMinimumTrackImage(UIImage(named: SLIDER_MINIMUM_IMAGE), for: .normal)
        sliderControl.setMaximumTrackImage(UIImage(named: SLIDER_MAXIMUM_IMAGE), for: .normal)
        let trackRect: CGRect  = sliderControl.trackRect(forBounds: sliderControl.bounds)
        let thumbRect: CGRect  = sliderControl.thumbRect(forBounds: sliderControl.bounds , trackRect: trackRect, value: sliderControl.value)
        let x = thumbRect.origin.x + sliderControl.frame.origin.x+10
        let y = sliderControl.frame.origin.y-10
        floatingView?.center = CGPoint(x: x, y: y)
        floatingLabel?.text = "\(String(Int(sliderControl.value.rounded())))%"
        
        let minimumWidth = sliderControl.frame.size.width*CGFloat(minimumValue)/100
        minimumBorderView.frame = CGRect(x: sliderControl.frame.origin.x, y: sliderControl.frame.origin.y+10 , width: minimumWidth, height:11)
        minFloatingLbl.frame = CGRect(x: minimumBorderView.frame.origin.x+minimumBorderView.frame.size.width-5.0, y:minimumBorderView.frame.origin.y+minimumBorderView.frame.size.height+5.0, width: minFloatingLbl.frame.size.width, height: minFloatingLbl.frame.size.height)
        
        let maximumWidth = (CGFloat(sliderControl.maximumValue)-CGFloat(maximumValue))*sliderControl.frame.size.width/100
        maximumBorderView.frame = CGRect(x: sliderControl.frame.size.width+sliderControl.frame.origin.x-maximumWidth, y: sliderControl.frame.origin.y+10 , width: maximumWidth, height:11)
        maxFloatingLbl.frame = CGRect(x: maximumBorderView.frame.origin.x-maxFloatingLbl.frame.size.width+5.0, y:maximumBorderView.frame.origin.y+maximumBorderView.frame.size.height+5.0, width: maxFloatingLbl.frame.size.width, height: maxFloatingLbl.frame.size.height)
        //        sliderControl.value = 100.0
        
        ndcDetailsModel = nil
        self.processShippingMethod(payee: .Default)
        
        
        
        enterNDCBtn.backgroundColor = SELECTED_WIZARD_BG_COLOR
        setPriceBtn.backgroundColor = DEFAULT_WIZARD_BG_COLOR
        selectShippingBtn.backgroundColor = DEFAULT_WIZARD_BG_COLOR
        selectedWizardButton = enterNDCBtn
        wizardTick1Img.isHidden = true
        wizardTick2Img.isHidden = true
        wizardTick3Img.isHidden = true
        
        oneLbl.backgroundColor = SELECTED_WIZARD_LABEL_BG_COLOR
        twoLbl.backgroundColor = DEFAULT_WIZARD_LABEL_BG_COLOR
        threeLbl.backgroundColor = DEFAULT_WIZARD_LABEL_BG_COLOR
        oneLbl.textColor = SELECTED_WIZARD_LABEL_TEXT_COLOR
        twoLbl.textColor = DEFAULT_WIZARD_LABEL_TEXT_COLOR
        threeLbl.textColor = DEFAULT_WIZARD_LABEL_TEXT_COLOR
        oneLbl.layer.borderColor = SELECTED_WIZARD_LABEL_BORDER_COLOR
        twoLbl.layer.borderColor = DEFAULT_WIZARD_LABEL_BORDER_COLOR
        threeLbl.layer.borderColor = DEFAULT_WIZARD_LABEL_BORDER_COLOR
        enterNDCParentView.isHidden = false
        setPriceParentView.isHidden = true
        selectShippingParentView.isHidden = true
        setPriceBtn.isUserInteractionEnabled = false
        selectShippingBtn.isUserInteractionEnabled = false
        
        dummyView.isHidden = false
        scrollView.isScrollEnabled = false
        ndcNameLbl.text = ""
        similarItemsLbl.text = ""
        medispanImgView.image = nil
        strengthLbl.text = ""
        storageLbl.text = ""
        packagingLbl.text = ""
        formLbl.text = ""
        lotNumberTxtField.text = ""
        expiryDateTxtField.text = ""
        fullPackBtn.isSelected = true
        partialPackBtn.isSelected = false
        partialTxtField.text = ""
        partialOverlayView.isHidden = true
        fullOverlayView.isHidden = true
        packAvailableTxtField.text = "1"
        packAvailableBtn.isUserInteractionEnabled = true
        sealedPackBtn.isSelected = true
        nonSealedPackBtn.isSelected = false
        sealedOverlayView.isHidden = true
        nonSealedOverlayView.isHidden = true
        originalPkgAlertViewHeight.constant = 0
        partialAlertViewHeight.constant = 0
        partialAlertView.isHidden = true
        quantitySoldAlertViewHeight.constant = 0
        originalPkgAlertView.isHidden = true
        originalPkgAlertLbl.text = "NOTE: MatchRX policy limits the sale of any full unsealed NDC to one (1) every 90 days."
        tornPkgBtn.isSelected = false
        stickerBtn.isSelected = false
        XContainerBtn.isSelected = false
        writingContainerBtn.isSelected = false
        otherBtn.isSelected = false
        otherTxtView.text = ""
        otherTxtView.isEditable = false
        
//        minimumPriceView.isHidden = true
        minPriceViewHeightConstraint.constant = 0
        maxPriceOptionAlertHeightConstraint.constant = 0
        minPriceOptionAlertHeightConstraint.constant = 0
        fixedButton.isSelected = true
        decliningButton.isSelected = false
        maxCurrentLbl.isHidden = true
        maxPackPriceTxtField.text = ""
        minPackPriceTxtField.text = ""
        yourPriceLbl.text = ""
        minPriceLbl.text = ""
        maxPriceLbl.text = ""
        yourWacDiscountLbl.text = ""
        minWacDiscountLbl.text = ""
        maxWacDiscountLbl.text = ""
        yourExpiryDateLbl.text = ""
        minExpiryDateLbl.text = ""
        maxExpiryDateLbl.text = ""
        
    }
    
    //Load the shipping details data fetched from service
    func bindShippingLOVData(){
        if shippingLOVModel != nil {
            let shippingMethodArray = NSMutableArray.init(array: shippingLOVModel.shippingMethodsArray)
            let shippingHandlingArray = NSMutableArray.init(array: shippingLOVModel.shippingHandlingArray)
            if shippingMethodArray.count == 3 {
                for i in 0..<shippingMethodArray.count {
                    let url = URL(string: (shippingMethodArray[i] as! ShippingLOVDataModel).shippingMethodIconURL!)
                    let data = try? Data(contentsOf: url!)
                    
                    if let imageData = data {
                        let image = UIImage(data: imageData)
                        (self.view.viewWithTag(i+3000) as! UIImageView).image = image
                    }
                    
                    (self.view.viewWithTag(i+4000) as! UILabel).text = (shippingMethodArray[i] as! ShippingLOVDataModel).shippingMethodShortName
                }
            }
            if shippingHandlingArray.count == 3 {
                for i in 0..<shippingHandlingArray.count {
                    (self.view.viewWithTag(i+1000) as! UILabel).text = (shippingHandlingArray[i] as! ShippingLOVDataModel).shippingHandlingTitle
                }
            }
        }
    }
    
    //MARK: - Textfield delegate methods.
    
    //Called on focussing on textfield.
    func textFieldDidBeginEditing(_ textField: UITextField) {
        if textField == expiryDateTxtField {
            let calendar = Calendar.current
            let currentDate = Date()
            let currentYear = calendar.component(.year, from: currentDate)
            
            
            let toolBar = UIToolbar()
            toolBar.barStyle = UIBarStyle.default
            toolBar.isTranslucent = true
            // toolBar.tintColor = UIColor(red: 76/255, green: 217/255, blue: 100/255, alpha: 1)
            toolBar.sizeToFit()
            let doneButton = UIBarButtonItem.init(title: "Done", style: UIBarButtonItemStyle.plain, target: nil, action: #selector(donePicker))
            toolBar.setItems([doneButton], animated: false)
            toolBar.isUserInteractionEnabled = true
            pickerView = DatePickerView()
            pickerView.datePickerDelegate = self
            pickerView.minYear = currentYear
            pickerView.maxYear = currentYear+20
            pickerView.rowHeight = 40
            pickerView.selectToday()
            textField.inputView = pickerView
            textField.inputAccessoryView = toolBar
        }
        if textField == partialTxtField {
            self.onPackQuantityClicked(sender: partialPackBtn)
        }
    }
    
    //Called on entering the value in textfiled.
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        if textField == lotNumberTxtField && (textField.text?.replacingCharacters(in: Range(range, in: textField.text!)!, with: string).count)! < 1 {
            self.handleUserInteractions(flag: false, type: .lotNumberVerification)
        }
        else if textField == lotNumberTxtField{
            self.handleUserInteractions(flag: true, type: .lotNumberVerification)
        }
        if string.isEmpty {
            if textField == maxPackPriceTxtField {
                let combinedString = textField.text?.replacingCharacters(in: Range(range, in: textField.text!)!, with: string)
                return isValidMaxPriceOption(maxPrice: combinedString!, range: range)
            }
            if textField == minPackPriceTxtField {
                let combinedString = textField.text?.replacingCharacters(in: Range(range, in: textField.text!)!, with: string)
                return isValidMinPriceOption(minPrice: combinedString!, maxPrice: maxPackPriceTxtField.text!)
            }
            return true
        }
        var maximumLength: Int?
        if textField == ndcTxtField{
//            return isValidNDCNumber(ndcNumber: "\(textField.text ?? "")\(string)")
            var text: String = string
            text = "\(textField.text ?? "")\(string)"
            let maximumLength = 13
            if text.count > maximumLength{
                return false
            }
            if (text.rangeOfCharacter(from: CharacterSet.init(charactersIn: "0123456789-").inverted) != nil)
            {
                return false
            }
        }
        if textField == lotNumberTxtField {
            maximumLength = 12
            if (textField.text?.count)! >= maximumLength!{
                return false
            }
            var text: String = string
            if string.count > 1 {
                text = "\(textField.text ?? "")\(string)"
            }
            if (text.rangeOfCharacter(from: CharacterSet.init(charactersIn: "0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ").inverted) != nil)
            {
                return false
            }
        }
        if textField == partialTxtField {
            if ndcDetailsModel.packageQuantity == 1 {
                maximumLength = "\(Int(ndcDetailsModel.packageSize!)!)".count
            }
            else {
                let pkg = "\(ndcDetailsModel.packageQuantity!)"
                maximumLength = pkg.count
            }
            if (textField.text?.count)! >= maximumLength!{
                return false
            }
            var text: String = string
            if string.count > 1 {
                text = "\(textField.text ?? "")\(string)"
            }
            if (text.rangeOfCharacter(from: CharacterSet.init(charactersIn: "0123456789").inverted) != nil)
            {
                return false
            }
        }
        if textField == expiryDateTxtField{
            let text = "\(textField.text ?? "")\(string)"
            let dateFormater = DateFormatter.init()
            dateFormater.dateFormat = "MMMM/yyyy"
            if dateFormater.date(from: text) != nil{
                if text.components(separatedBy: "/").count == 2 {
                    let monthString = text.components(separatedBy: "/")[0]
                    let yearString = text.components(separatedBy: "/")[1]
                    let months = ["January","February","March","April", "May", "June", "July", "August", "September", "October", "November", "December"]
                    var validMonth: Bool = false
                    for i in 0..<months.count {
                        if months[i] == monthString {
                            validMonth = true
                        }
                    }
                    if !validMonth{
                        return false
                    }
                    let calendar = Calendar.current
                    let currentDate = Date()
                    let currentYear = calendar.component(.year, from: currentDate)
                    var validYear: Bool = false
                    for i in currentYear..<currentYear+20 {
                        if i == Int(yearString){
                            validYear = true
                        }
                    }
                    if !validYear {
                        return false
                    }
                    if self.isValidExpiryDate(expMonth: monthString, expYear: yearString){
                        return true
                    }
                    else{
                        return false
                    }
                }
                else {
                    return false
                }
            }
            else{
                return false
            }
            
        }
        if textField == maxPackPriceTxtField {
            let combinedString = textField.text?.replacingCharacters(in: Range(range, in: textField.text!)!, with: string)
            if self.isValidMaxPriceOption(maxPrice: combinedString!, range: range){
                
            }
            else {
                return false
            }
        }
        if textField == minPackPriceTxtField {
            let combinedString = textField.text?.replacingCharacters(in: Range(range, in: textField.text!)!, with: string)
            return isValidMinPriceOption(minPrice: combinedString!, maxPrice: maxPackPriceTxtField.text!)
        }
        
        return true
    }
    
    func textFieldShouldEndEditing(_ textField: UITextField) -> Bool {
        if textField == lotNumberTxtField && (textField.text?.count)! < 1 {
            self.handleUserInteractions(flag: false, type: .lotNumberVerification)
        }
        else if textField == lotNumberTxtField{
            self.handleUserInteractions(flag: true, type: .lotNumberVerification)
        }
        if textField == maxPackPriceTxtField || textField == minPackPriceTxtField{
            if (textField.text?.contains("."))!{
                let decimal = textField.text?.components(separatedBy: ".")[1]
                if (decimal?.count)! < 2 {
                    var decimalValue: String = "00"
                    if decimal?.count == 1 {
                        decimalValue = "0"
                    }
                    textField.text = textField.text!+decimalValue
                }
            }
            else if (textField.text?.count)! > 0{
                textField.text = textField.text!+".00"
            }
        }
        return true
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }
    
    
    //MARK: - Textview delegate
    
    //Called on clicking into the text view.
    
    func textView(_ textView: UITextView, shouldChangeTextIn range: NSRange, replacementText text: String) -> Bool {
        if text.isEmpty {
            return true
        }
        let maximumLength = 50
        var appendedText: String = text
        appendedText = "\(textView.text ?? "")\(text)"
        if (appendedText.count) > maximumLength{
            return false
        }
        return true
    }
    
    
    //MARK: - Date picker delegate
    
    //Returns the selected date.
    func selectedDate(selectedDate: String) {
        print(selectedDate)
        expiryDateTxtField.text = selectedDate
        yourExpiryDateLbl.text = selectedDate
    }
    
    //MARK: - Image capture delegate
    
    //Returns the captured image
    func capturedImage(_ imageData: Data!) {
        let selectedImageSize:Double = Double(imageData.count)/1024.0/1024.0
        print("Image Size in MB", Double(selectedImageSize))
        
        if selectedImageSize <= 10.0 {
            let image : UIImage = UIImage(data: imageData)!
            //        self.showDashedOverlayForView(sender: selectedButton, flag: false)
            if selectedButton == uploadBtn1 {
                deleteImg1.isHidden = false
                uploadBtn1.setImage(image, for: .normal)
            }
            else{
                deleteImg2.isHidden = false
                uploadBtn2.setImage(image, for: .normal)
            }
            selectedButton.layer.borderWidth = 1.0
        }
        else {
            AlertControl.sharedInstance.presentAlertView(alertTitle: "File size should not be more than 10 MB", alertMessage: "", delegate: nil)
        }
    }
    
    //MARK: - Drop down delegate
    
    func selectedDropDownData(_ dropDownDataObject: DropDownModel) {
        if selectedButton != packAvailableBtn {
            if(dropDownDataObject.modelIdString?.isEqual(to: "1"))!{
                if AVCaptureDevice.authorizationStatus(forMediaType: AVMediaTypeVideo) ==  AVAuthorizationStatus.authorized {
                    // Already Authorized
                    self.photoEditor.showcamera(NavigationModel.sharedInstance.homeControl, fromview: self.view)
                } else {
                    AVCaptureDevice.requestAccess(forMediaType: AVMediaTypeVideo, completionHandler: { (granted: Bool) -> Void in
                        if granted == true {
                            // User granted
                            self.photoEditor.showcamera(NavigationModel.sharedInstance.homeControl, fromview: self.view)
                        } else {
                            // User Rejected
                            AlertControl.sharedInstance.presentAlertView(alertTitle: "Camera access denied", alertMessage: "Open the settings app to change privacy settings", delegate: nil)
                        }
                    })
                }
                
                
                
                
            }
            else{
                PHPhotoLibrary.requestAuthorization({
                    (newStatus) in
                    if newStatus ==  PHAuthorizationStatus.authorized {
                        /* do stuff here */
                        self.photoEditor.showLibrary(NavigationModel.sharedInstance.homeControl, fromview: self.view)
                    }
                    else{
                        AlertControl.sharedInstance.presentAlertView(alertTitle: "Photo library access denied", alertMessage: "Open the settings app to change privacy settings", delegate: nil)
                    }
                })
            }
        }
        else{
             packAvailableTxtField.text = dropDownDataObject.modelDataString as String?
            nonSealedPackBtn.isSelected = false
            let packCount: Int = Int(packAvailableTxtField.text!)!
            if packCount > 1 {
                nonSealedOverlayView.isHidden = false
                self.handleNDCScreenBusinessRules(actiontype: .PackQtyEqualsGreaterThan1Clicked)
                sealedPackBtn.isSelected = true
                /*if ndcDetailsModel.stateRestrictionType == .allowOnlyNonSealed {
                    originalPkgAlertView.isHidden = true
                    originalPkgAlertViewHeight.constant = 0
                }
                else {
                    nonSealedOverlayView.isHidden = false
                    originalPkgAlertView.isHidden = false
                    originalPkgAlertViewHeight.constant = CGFloat(ORIGINALPKGALERTVIEWHEIGHT)
                    if ndcDetailsModel.isNonSealedPartialExists! {
                        originalPkgAlertLbl.text = ndcDetailsModel.nonSealedPartialErrorMessage
                    }
                }*/
            }
            else{
                /*if ndcDetailsModel.isNonSealedPartialExists! {
                    originalPkgAlertLbl.text = ndcDetailsModel.nonSealedPartialErrorMessage
                    originalPkgAlertView.isHidden = false
                    originalPkgAlertViewHeight.constant = originalPkgAlertLbl.frame.size.height+21.5
                }
                else {
                    originalPkgAlertViewHeight.constant = 0
                    originalPkgAlertView.isHidden = true
                }*/
                self.handleNDCScreenBusinessRules(actiontype: .PackQtyEquals1Clicked)
//                if ndcDetailsModel.stateRestrictionType == .allowBoth {
//                    sealedPackBtn.isSelected = false
//                }
//                if ndcDetailsModel.stateRestrictionType == .allowBoth {
//                    nonSealedOverlayView.isHidden = true
//                }
                    /*if ndcDetailsModel.isPartialPackExists! {
                        if ndcDetailsModel.isNonSealedPartialExists! {
                            nonSealedOverlayView.isHidden = false
                        }
                        else {
                            nonSealedOverlayView.isHidden = true
                        }
                        partialOverlayView.isHidden = false
                        partialAlertViewHeight.constant = PARTIAL_ERROR_MESSAGE_HEIGHT
                        partialErrorMessageLbl.text = ndcDetailsModel.partialPackMessage
                        partialAlertView.isHidden = false
                    }
                    else {
                        nonSealedOverlayView.isHidden = true
                        partialAlertViewHeight.constant = 0
                    }
                    sealedPackBtn.isSelected = false
                }*/
            }
        }
        DropDownView.defaultDropDownControl.hideDropDown()
    }
    
    func selectedDropDownDataArray(_ dropDownDataArray: NSMutableArray) {
        
        print("")
    }
    
    func selectedDropDownDataString(_ selectedDropDownDataString: NSString) {
        
        print("")
    }
    
    
    //MARK: - Scan control delegate
    
    //Returns the scanned object value
    func scannedObject(sender: String) {
        ndcTxtField.text = "\(sender)"
        self.verifyNDCNumber()
//        self.perform(#selector(self.verifyNDCNumber), with: "", afterDelay: 1.0)
    }
    
    //MARK: - Network manager delegate function
    
    //Returns the web service call
    func webServiceResponse(for requestType: ServiceRequestType, responseData Data: Any!, withStatusCode statusCode: Int, errorMessage message: Any!) {
        LoadingView.shareInstance.hideLoadingView()
        if message as? Error != nil {
            AlertControl.sharedInstance.presentAlertView(alertTitle: "Something went wrong", alertMessage: "", delegate: self)
        }
        else{
            if !NetworkManager.sharedInstance.checkStatusCode(statusCode){
                let error = NetworkManager.sharedInstance.getErrorModel(requestType: requestType, httpStatusCode: statusCode, responseData: Data)
                AlertControl.sharedInstance.presentAlertView(alertTitle: (error.errorMessage!), alertMessage: "", delegate: self)
            }
            else{
                if requestType == .ServiceTypeNDCVerification {
                    ndcDetailsModel = ResponseManager.sharedInstance.getNDCDetails(responseData: Data) as! NDCDetailsDataModel
                    if ndcDetailsModel.isSuccess! {
                        dummyView.isHidden = true
                        scrollView.isScrollEnabled = true
                        self.handleUserInteractions(flag: false, type: .NDCVerificaiton)
                        self.bindVerifiedNDCDetails()
                    }
                    else{
                        AlertControl.sharedInstance.presentAlertView(alertTitle: "This NDC appears to be invalid. Please re-enter.", alertMessage: "", delegate: nil)
                    }
                }
                else if requestType == .ServiceTypeLOVDetails {
                    shippingLOVModel = ResponseManager.sharedInstance.getLOVDetails(responseData: Data)
                    self.bindShippingLOVData()
                    myGroup.leave()
                }
                else if requestType == .ServiceTypePostAnItem {
                    AlertControl.sharedInstance.presentAlertView(alertTitle: "The item was successfully posted", alertMessage: "", delegate: nil)
                    if selectedPostButton == postBtn {
                        NavigationModel.sharedInstance.homeControl?.selectedRowAtIndex(selectedIndex: 0, menuTitle: "BUY", selectedData: Any?.self)
                    }
                    else {
                        self.resetNDCScreenDetails()
                        ndcTxtField.text = ""
                    }
                }
            }
        }
    }
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
