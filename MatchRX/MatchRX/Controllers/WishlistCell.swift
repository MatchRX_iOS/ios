//
//  WishlistCell.swift
//  MatchRX
//
//  Created by Vignesh on 3/8/18.
//  Copyright © 2018 HTC. All rights reserved.
//

import UIKit

class WishlistCell: UITableViewCell {
    
    //MARK: - Global variable references
    @IBOutlet weak var ndcImageView: UIImageView!
    @IBOutlet weak var ndcNameLbl: UILabel!
    @IBOutlet weak var ndcNumberLbl: UILabel!
    @IBOutlet weak var quantityLbl: UILabel!
    @IBOutlet weak var dateLbl: UILabel!
    @IBOutlet weak var deleteButton: UIButton!

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    override func layoutSubviews() {
        
    }

}
