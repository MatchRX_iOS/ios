//
//  ForgotPasswordController.swift
//  MatchRX
//
//  Created by Vignesh on 11/14/17.
//  Copyright © 2017 HTC. All rights reserved.
//

import UIKit

class ForgotPasswordController: UIViewController, NetworkManagerDelegate, AlertDelegate, UITextFieldDelegate {
    
    //MARK: - IBOutlet declaration
    @IBOutlet var deaNumber : UITextField!
    @IBOutlet var userName : UITextField!
    @IBOutlet var dockView: UIView!
    @IBOutlet var forgotPwdBtn: UIButton!
    
    var responseStatusCode: Int?
    var selectedTextField: UITextField!

    //MARK: - Override functions
    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationController?.isNavigationBarHidden = false
//        forgotPwdBtn.layer.cornerRadius = 4.0
        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    //Deinitializes the allocated values
    deinit {
        print("Class dismissed")
    }
    
    //MARK: - Instance functions.
    
    //Dismiss the controller and navigate back to Login controller
    @IBAction func backButtonClicked(){
        self.dismiss(animated: true, completion: nil)
    }
    
    //Validates the input and call the Forgot password API
    @IBAction func resetPasswordClicked(){
        if (selectedTextField != nil) {
            selectedTextField.resignFirstResponder()
        }
        let forgotPasswordModel = ForgotPasswordDataModel()
        forgotPasswordModel.deaNumber = deaNumber.text
        forgotPasswordModel.userName = userName.text
        if !isValidDEANumber(deaNumber: forgotPasswordModel.deaNumber!) {
            //self.displayAlertControl(alertTitle: "Please provide a valid DEA number", alertMessage: "")
            AlertControl.sharedInstance.presentAlertView(alertTitle: "Please provide a valid DEA number", alertMessage: "", delegate: nil)
            return
        }
        if !isValidUserName(userNameText: forgotPasswordModel.userName!) {
            //self.displayAlertControl(alertTitle: "Please provide a valid username", alertMessage: "")
            AlertControl.sharedInstance.presentAlertView(alertTitle: "Please provide a valid username", alertMessage: "", delegate: nil)
            return
        }
        if NetworkManager.sharedInstance.isReachable(){
            _ = NetworkManager.sharedInstance.checkTypeOfNetwork()
            if NetworkManager.sharedInstance.isReachableViaWiFi || NetworkManager.sharedInstance.isReachableViaCellular{
                LoadingView.shareInstance.showLodingView(parentView: self.view.window!)
                NetworkManager.sharedInstance.manageAPIRequest(requestType: .ServiceTypeForgotPassword, requestString: RequestDataModel.sharedInstance.forgotPassword(forgotPwdModel: forgotPasswordModel), delegate: self)
            }
        }
        else{
            AlertControl.sharedInstance.presentAlertView(alertTitle: "Kindly check your internet connectivity", alertMessage: "", delegate: nil)
        }
    }

    //Validates the entered DEA Number
    func isValidDEANumber(deaNumber: String) -> Bool{
        if deaNumber.count == 9 {
            let firstCharacter = deaNumber.prefix(1)
            let numbers = deaNumber.index(deaNumber.startIndex, offsetBy: 2)
            if (firstCharacter.rangeOfCharacter(from: CharacterSet.init(charactersIn: "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ")) == nil){
                return false
            }
            else if (deaNumber.suffix(from: numbers).rangeOfCharacter(from: CharacterSet.init(charactersIn: "0123456789").inverted) != nil){
                return false
            }
            else{
                let index = deaNumber.index(after: deaNumber.startIndex)
                let secondCharacter = String(deaNumber[index])
                if (secondCharacter.rangeOfCharacter(from: CharacterSet.init(charactersIn: "0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ")) == nil){
                    return false
                }
            }
        }
        else{
            return false
        }
        return true
    }
    
    //Validates the entered Username
    func isValidUserName(userNameText: String) -> Bool{
        if userNameText.count <= 4 {
            return false
        }
        else{
            if (userNameText.prefix(1).rangeOfCharacter(from: CharacterSet.init(charactersIn: "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz")) == nil){
                return false
            }
            let set = CharacterSet(charactersIn: "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789.-_")
            if userNameText.rangeOfCharacter(from: set.inverted) != nil {
                return false
            }
        }
        return true
    }
    
    //Present a AlertController as pop up for validation
    func displayAlertControl(alertTitle: String, alertMessage: String) {
//        let alertControl = UIAlertController(title: alertTitle, message: alertMessage, preferredStyle: UIAlertControllerStyle.alert)
//        alertControl.addAction(UIAlertAction(title:"OK", style: UIAlertActionStyle.cancel, handler: nil))
//        self.present(alertControl, animated: true, completion: nil)
        AlertControl.sharedInstance.presentAlertView(alertTitle: alertTitle, alertMessage: "", delegate: self)
    }
    
    //MARK: - Textfield delegate functions implementation
    
    func textFieldDidBeginEditing(_ textField: UITextField) {
        selectedTextField = textField
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        let characterLength: Int?
        if textField == deaNumber{
            characterLength = 9
        }
        else if textField == userName {
            characterLength = 30
        }
        else{
            characterLength = 20
        }
        if characterLength! > 0 && !string.isEmpty
        {
            let text = "\(textField.text ?? "")\(string)"
            if text.count >= characterLength!
            {
                if string.count > 1{
                    let index = text.index(text.startIndex, offsetBy: characterLength!)
                    textField.text = text.substring(to: index)
                    return false
                }
                let currentString: NSString = textField.text! as NSString
                let newString: NSString =
                    currentString.replacingCharacters(in: range, with: string) as NSString
                //                let flag = newString.length <= characterLength!
                return newString.length <= characterLength!
            }
        }
        /*if characterLength! > 0
        {
            if (textField.text?.count)! >= characterLength!
            {
                let currentString: NSString = textField.text! as NSString
                let newString: NSString =
                    currentString.replacingCharacters(in: range, with: string) as NSString
                return newString.length <= characterLength!
            }
        }*/
        return true
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        
        textField.resignFirstResponder()
        return true
    }
    
    //MARK: - Network manager delegate function
    
    //Returns the web service response data
    func webServiceResponse(for requestType: ServiceRequestType, responseData Data: Any!, withStatusCode statusCode: Int, errorMessage message: Any!) {
        LoadingView.shareInstance.hideLoadingView()
        responseStatusCode = statusCode
        if message as? Error != nil {
            AlertControl.sharedInstance.presentAlertView(alertTitle: "Something went wrong", alertMessage: "", delegate: self)
        }
        else{
            if !NetworkManager.sharedInstance.checkStatusCode(statusCode){
                let error = NetworkManager.sharedInstance.getErrorModel(requestType: requestType, httpStatusCode: statusCode, responseData: Data)
                AlertControl.sharedInstance.presentAlertView(alertTitle: (error.errorMessage!), alertMessage: "", delegate: self)
            }
            else{
                //                self.performSelector(onMainThread: #selector(self.processData(Data:)), with: Data, waitUntilDone: true)
                let forgotPwdModel = ResponseManager.sharedInstance.forgotPasswordResponse(responseData: Data)
                AlertControl.sharedInstance.presentAlertView(alertTitle: forgotPwdModel.responseMessage!, alertMessage: "", delegate: self)
            }
        }
    }
    
    //MARK: - Alert delegate functions
    
    //Dismiss the controller
    func onOkTouched() {
        if responseStatusCode == 200 {
            self.dismiss(animated: true, completion: nil)
//            self.navigationController?.popViewController(animated: true)
        }
    }
    
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
