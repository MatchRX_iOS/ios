//
//  HomeControl.swift
//  MatchRX
//
//  Created by Vignesh on 11/15/17.
//  Copyright © 2017 HTC. All rights reserved.
//

import UIKit

class HomeControl: UIViewController , menuDelegate, NetworkManagerDelegate, UITextFieldDelegate {
    
    @IBOutlet var leadingConstraint: NSLayoutConstraint!
    @IBOutlet var menuView: UIView!
    @IBOutlet var menuContentView: UIView!
    @IBOutlet var contentView: UIView!
    @IBOutlet var blurBgView: UIView!
    @IBOutlet var dullView: UIView!
    @IBOutlet var headerLabel: UILabel!
    @IBOutlet var userNameLabel: UILabel!
    @IBOutlet var pharmacyNameLabel: UILabel!
    @IBOutlet var deaNumberLabel: UILabel!
    @IBOutlet weak var backView: UIView!
    @IBOutlet weak var hamburgerView: UIView!
    @IBOutlet weak var cartCountLbl: UILabel!
    @IBOutlet weak var headerMenuView: UIView!
    @IBOutlet weak var searchView: UIView!
    @IBOutlet weak var searchTxtField: UITextField!
    @IBOutlet weak var searchBgView: UIView!
    
    
    var swipeGestureLeft: UISwipeGestureRecognizer!
    var tapGesture: UITapGestureRecognizer!
    var menuController : MenuViewController!
    var loginResponseModel: LoginResponseModel?
    var myAccountController: MyAccountController!
    var myOrdersController: MyOrdersController!
    var sellController: SellController!
    var buyController: BuyController!
    var settingsController: SettingsController!
    var wishlistController: WishlistController!
    var contentNavController = UINavigationController.init()
    var currentIndex: Int = -1
    var screenTitle: String?
    var searchedNDC: String?
    
    //MARK: - Override functions.
    
    override func viewDidLoad() {
        super.viewDidLoad()
        NavigationModel.sharedInstance.homeControl = self
        contentNavController.view.frame = contentView.bounds
        contentView.addSubview(contentNavController.view)
        self.contentNavController.isNavigationBarHidden = true
        
        self.leadingConstraint.constant = -self.view.frame.size.width
        swipeGestureLeft = UISwipeGestureRecognizer(target: self, action: #selector(self.hideMenuController))
        swipeGestureLeft.direction = UISwipeGestureRecognizerDirection.left
        menuView.addGestureRecognizer(swipeGestureLeft)
        
        tapGesture = UITapGestureRecognizer(target:self, action: #selector(self.hideMenuController))
        tapGesture.numberOfTapsRequired = 1
        dullView.addGestureRecognizer(tapGesture)
        
        let storyboard = UIStoryboard(name: "Main", bundle:nil)
        menuController = storyboard.instantiateViewController(withIdentifier: "menuController") as! MenuViewController
        menuController.parentDelegate = self
        menuController.launchMenu = loginResponseModel?.landingPage
        menuController.view.frame = menuContentView.bounds
        menuContentView.addSubview(menuController.view)
        userNameLabel.text = "Hi, \(loginResponseModel?.firstName ?? "") \(loginResponseModel?.lastName ?? "")"
        pharmacyNameLabel.text = loginResponseModel?.pharmacyName
        deaNumberLabel.text = "DEA# \(loginResponseModel?.pharmacyDEA ?? "")"
        
        cartCountLbl.layer.cornerRadius = cartCountLbl.frame.size.width/2.0
        
        if GlobalData.sharedInstance.cartCount! > 0 {
            cartCountLbl.isHidden = false
            cartCountLbl.text = "\(GlobalData.sharedInstance.cartCount!)"
        }
        else {
            cartCountLbl.text = ""
            cartCountLbl.isHidden = true
        }
        
        searchTxtField.enablesReturnKeyAutomatically = true
        // Do any additional setup after loading the view, typically from a nib.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.navigationController?.isNavigationBarHidden = true
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    //MARK: - IBAction functions.
    
    //Initiates cart controller
    @IBAction func initiateCartControl(sender: UIButton) {
        let storyboard = UIStoryboard(name: "Secondary", bundle: nil)
        let cartControl = storyboard.instantiateViewController(withIdentifier: "myCart") as! CartViewController
        self.contentNavController.view.frame = self.contentView.bounds
        self.contentNavController.isNavigationBarHidden = true
        self.contentNavController.pushViewController(cartControl, animated: true)
        self.showBackIconWithTitle(title: "MY CART")
        headerMenuView.isHidden = true
    }
    
    //Shows the menu view controller
    @IBAction func showHideMenu(){
        self.view.endEditing(true)
        self.view.bringSubview(toFront: blurBgView)
        self.view.bringSubview(toFront: menuView)
        UIView.animate(withDuration: 0.5, delay: 0.0, options: .transitionFlipFromLeft, animations: {
            self.leadingConstraint.constant = 0
            self.blurBgView.alpha = 0.4
            self.view.layoutIfNeeded()
            
            if NetworkManager.sharedInstance.isReachable(){
                _ = NetworkManager.sharedInstance.checkTypeOfNetwork()
                if NetworkManager.sharedInstance.isReachableViaWiFi || NetworkManager.sharedInstance.isReachableViaCellular{
                    LoadingView.shareInstance.showLodingView(parentView: (NavigationModel.sharedInstance.homeControl?.view!)!)
                    NetworkManager.sharedInstance.manageAPIRequest(requestType: .ServiceTypeOrderCount, requestString: "", delegate: self)
                }
            }
            else{
                AlertControl.sharedInstance.presentAlertView(alertTitle: "Kindly check your internet connectivity", alertMessage: "", delegate: nil)
            }
            
        }) { (animationComplete) in
            print("The animation is complete!")
        }
    }
    
    //Logout button action.
    @IBAction func logoutButtonClicked(){
        self.hideMenuController()
        let alertControl = UIAlertController(title: "Are you sure you want to logout?", message:"", preferredStyle:UIAlertControllerStyle.alert)
        alertControl.addAction(UIAlertAction(title: "YES", style: UIAlertActionStyle.default, handler:{ (action) in
            let defaults = UserDefaults.standard
            if ((defaults.object(forKey: "refreshToken") as? String) != nil) {
                defaults.removeObject(forKey: "refreshToken")
                defaults.synchronize()
            }
            self.dismiss(animated: true, completion: nil)
            let storyboard = UIStoryboard(name: "Main", bundle: nil)
//            self.window?.rootViewController = storyboard.instantiateInitialViewController()
            let appDelegate = UIApplication.shared.delegate as? AppDelegate
            appDelegate?.window?.rootViewController = storyboard.instantiateInitialViewController()
        }))
        alertControl.addAction(UIAlertAction(title: "NO", style: UIAlertActionStyle.cancel, handler: nil))
        self.present(alertControl, animated: true, completion: nil)
    }
    
    
    //Navigates back from the contentNavcontroller.
    @IBAction func onBackClicked(sender: UIButton) {
        headerMenuView.isHidden = false
        self.contentNavController.popViewController(animated: true)
        self.perform(#selector(self.performsBackAction), with: nil, afterDelay: 0.2)
        print("Back button hided and view controllers count is: \(self.contentNavController.viewControllers.count)")
    }
    
    func performsBackAction() {
        if self.contentNavController.viewControllers.count == 1 {
            if !(searchTxtField.text?.isEmpty)! {
                self.view.bringSubview(toFront: searchView)
            }
            backView.isHidden = true
            hamburgerView.isHidden = false
            headerLabel.text = screenTitle
            if self.contentNavController.viewControllers[0] is BuyController {
                buyController.allPostingBtn.isSelected = true
                buyController.selectedImgViewLeadingConstraint.constant = buyController.allPostingBtn.frame.origin.x
                if buyController.allPostingBtn.isSelected {
                    NavigationModel.sharedInstance.buyListControl?.isActiveMenu = ""
                    NavigationModel.sharedInstance.buyListControl?.reloadListData()
                }
                else if buyController.myPurchasesBtn.isSelected {
                    buyController.loadmyPurchases()
                }
                else {
                    buyController.loadMyWishlist()
                }
            }
        }
        else {
            headerLabel.text = self.contentNavController.topViewController?.title
        }
    }
    
    //Presents the search view controller.
    @IBAction func onSearchIconClicked(sender: UIButton) {
        self.view.bringSubview(toFront: searchView)
        self.view.bringSubview(toFront: searchBgView)
        if (searchTxtField.text?.isEmpty)! {
            searchTxtField.becomeFirstResponder()
        }
    }
    
    //Hides the search view
    @IBAction func onSearchBackClicked(sender: UIButton) {
        self.view.endEditing(true)
        if searchedNDC == searchTxtField.text || (searchTxtField.text?.isEmpty)!{
            self.view.sendSubview(toBack: searchView)
            self.view.sendSubview(toBack: searchBgView)
            GlobalData.sharedInstance.searchText = searchTxtField.text!
            if self.contentNavController.viewControllers.count == 1 && self.contentNavController.viewControllers[0] is BuyController{
                searchTxtField.text = ""
                GlobalData.sharedInstance.searchText = ""
//                self.presentBuyController()
                self.performsBackAction()
            }
            //        if self.contentNavController.viewControllers[0] is BuyController {
            ////            currentIndex = 0
            //            self.presentBuyController()
            //        }
        }
        else {
            searchTxtField.text = searchedNDC
//            GlobalData.sharedInstance.searchText = searchedNDC!
        }
    }
    
    //Cancels the text in search text box.
    @IBAction func onCancelSearchIconClicked(sender: UIButton) {
        searchTxtField.text = ""
        searchTxtField.becomeFirstResponder()
    }
    
    //MARK: - Local accessible functions.
    
    //Called on swiping the menu content view
    func respondToSelector(gesture: UIGestureRecognizer){
        self.hideMenuController()
    }
    
    //Show hide the Back icon and the title.
    func showBackIconWithTitle(title: String) {
        if !(searchTxtField.text?.isEmpty)! {
            self.view.sendSubview(toBack: searchView)
        }
        hamburgerView.isHidden = true
        backView.isHidden = false
        headerLabel.text = title
    }
    
    //Hides the menu controller on selecting an item or by swiping
    func hideMenuController(){
        UIView.animate(withDuration: 0.5, delay: 0.0, options: .transitionFlipFromRight, animations: {
            self.leadingConstraint.constant = -self.view.frame.size.width
            self.blurBgView.alpha = 0.0
            self.view.layoutIfNeeded()
        }) { (animationComplete) in
            print("The animation is complete!")
            self.view.sendSubview(toBack: self.menuView)
            self.view.sendSubview(toBack: self.blurBgView)
        }
    }
    
    //Hits the service to fetch pharmacy information 
    func presentMyAccount(){
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        myAccountController = storyboard.instantiateViewController(withIdentifier: "accountController") as! MyAccountController
        self.contentNavController = UINavigationController.init(rootViewController: myAccountController)
        self.contentNavController.view.frame = self.contentView.bounds
        self.contentNavController.isNavigationBarHidden = true
        self.contentView.addSubview(self.contentNavController.view)
    }
    
    //Present Order controller
    func presentMyOrders(landingPage: String){
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        myOrdersController = storyboard.instantiateViewController(withIdentifier: "orderController") as! MyOrdersController
        self.contentNavController = UINavigationController.init(rootViewController: myOrdersController)
        myOrdersController.landingTab = landingPage
        self.contentNavController.view.frame = self.contentView.bounds
        self.contentNavController.isNavigationBarHidden = true
        self.contentView.addSubview(self.contentNavController.view)
    }
    
    //Present the Sell controller
    func presentPostController(){
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        sellController = storyboard.instantiateViewController(withIdentifier: "sellController") as! SellController
        self.contentNavController = UINavigationController.init(rootViewController: sellController)
        self.contentNavController.view.frame = self.contentView.bounds
        self.contentNavController.isNavigationBarHidden = true
        self.contentView.addSubview(self.contentNavController.view)
    }
    
    //Present the Buy controller
    func presentBuyController() {
        if buyController != nil {
            buyController.removeFromParentViewController()
            buyController = nil
        }
        self.view.sendSubview(toBack: searchBgView)
        currentIndex = 0
        headerLabel.text = "BUY"
        screenTitle = "BUY"
        GlobalData.sharedInstance.searchText = searchTxtField.text!
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        buyController = storyboard.instantiateViewController(withIdentifier: "buyControl") as! BuyController
        self.contentNavController = UINavigationController.init(rootViewController: buyController)
        self.contentNavController.view.frame = self.contentView.bounds
        self.contentNavController.isNavigationBarHidden = true
        self.contentView.addSubview(self.contentNavController.view)
        NavigationModel.sharedInstance.buyControl = buyController
    }
    
    func presentSettingsController() {
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        settingsController = storyboard.instantiateViewController(withIdentifier: "settingsControl") as! SettingsController
        self.contentNavController = UINavigationController.init(rootViewController: settingsController)
        self.contentNavController.view.frame = self.contentView.bounds
        self.contentNavController.isNavigationBarHidden = true
        self.contentView.addSubview(self.contentNavController.view)
    }
    
    func presentWishlistController() {
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        wishlistController = storyboard.instantiateViewController(withIdentifier: "wishlistControl") as! WishlistController
        self.contentNavController = UINavigationController.init(rootViewController: wishlistController)
        self.contentNavController.view.frame = self.contentView.bounds
        self.contentNavController.isNavigationBarHidden = true
        self.contentView.addSubview(self.contentNavController.view)
    }
    
    func checkFormSubmission() {
        let dataDict = NSMutableDictionary()
        dataDict.setValue("60687023401", forKey: "ndc_number")
        dataDict.setValue("ABC123980", forKey: "lot_number")
        dataDict.setValue("07/2019", forKey: "expire_date")
        dataDict.setValue("F", forKey: "package_option")
        dataDict.setValue("2", forKey: "package_size")
        dataDict.setValue("1", forKey: "package_quantity")
        dataDict.setValue("1,2,5", forKey: "condition")
        dataDict.setValue("fixed", forKey: "price_option")
        dataDict.setValue("100", forKey: "unit_price")
        dataDict.setValue("100", forKey: "max_price")
        dataDict.setValue("50", forKey: "min_price")
        dataDict.setValue("100", forKey: "current_price")
        dataDict.setValue("50", forKey: "aawp_discount_price")
        dataDict.setValue("50", forKey: "wac_discount_price")
        dataDict.setValue("buyerpays", forKey: "shipping_handling_name")
        dataDict.setValue("FedEx Ground", forKey: "shipping_method")
//        dataDict.setValue("115", forKey: "post_id")
        dataDict.setValue("3", forKey: "package_unit")
        dataDict.setValue("Sample seller notes", forKey: "seller_notes")
        dataDict.setValue("sealed", forKey: "package_condition")
        dataDict.setValue(true, forKey: "matchrx_stickers")
        dataDict.setValue(true, forKey: "frozen_stickers")
        
        let image1 = UIImage.init(named: "Upload_Btn.png")
        let imageData1 = UIImageJPEGRepresentation(image1!, 1.0)
        let image2 = UIImage.init(named: "Upload_Btn.png")
        let imageData2 = UIImageJPEGRepresentation(image2!, 1.0)
//        let imageArray: [[(String, UIImage)]] = [[("First_image",image1!)], [("Second_image", image2!)]]
        let imageArray: Array<Any> = [["First_image", imageData1!]]
        
        /*if NetworkManager.sharedInstance.isReachable(){
            _ = NetworkManager.sharedInstance.checkTypeOfNetwork()
            if NetworkManager.sharedInstance.isReachableViaWiFi || NetworkManager.sharedInstance.isReachableViaCellular{
                LoadingView.shareInstance.showLodingView(parentView: self.view.window!)
                NetworkManager.sharedInstance.manageAPIForMultiFormDataRequest(requestType: .ServiceTypePostAnItem, requestParam: dataDict, imageArray: imageArray, delegate: self)
            }
        }
        else{
            AlertControl.sharedInstance.presentAlertView(alertTitle: "Kindly check your internet connectivity", alertMessage: "", delegate: nil)
        }*/
    }
    
    func showBuyMenu() {
        for view in contentNavController.view.subviews {
            view.removeFromSuperview()
        }
        self.presentBuyController()
    }
    
    func showMyOrders() {
        for view in contentNavController.view.subviews {
            view.removeFromSuperview()
        }
        self.presentMyOrders(landingPage: GlobalData.sharedInstance.landingPage!)
    }
    
    //MARK: - Menu delegate functions
    
    //Delegate function which returns the selected data on clicking the table view
    func selectedRowAtIndex(selectedIndex: Int, menuTitle: String, selectedData: Any?) {
        self.view.endEditing(true)
        print(menuTitle)
        self.hideMenuController()
        if selectedIndex == 1 && GlobalData.sharedInstance.stateWiseRestriction == .restrictBoth{
            AlertControl.sharedInstance.presentAlertView(alertTitle: GlobalData.sharedInstance.stateWiseRestrictionMessage!, alertMessage: "", delegate: nil)
            return
        }
        if selectedIndex != currentIndex {
            headerLabel.text = menuTitle
            screenTitle = menuTitle
            for view in contentNavController.view.subviews {
                view.removeFromSuperview()
            }
            switch selectedIndex {
            case 0:
                self.presentBuyController()
                break
                
            case 1:
                self.presentPostController()
                break
                
            case 2:
                self.presentMyOrders(landingPage: GlobalData.sharedInstance.landingPage!)
                break
                
            case 3:
                self.presentWishlistController()
                break
                
            case 4:
                self.presentMyAccount()
                break
                
            case 5:
                self.presentSettingsController()
                break
                
            default:
                self.presentSettingsController()
                break
            }
        }
        currentIndex = selectedIndex
    }
    
    //MARK: - Network manager delegate functions.
    
    //Returns the response
    func webServiceResponse(for requestType: ServiceRequestType, responseData Data: Any!, withStatusCode statusCode: Int, errorMessage message: Any!) {
        LoadingView.shareInstance.hideLoadingView()
        if message as? Error != nil {
            AlertControl.sharedInstance.presentAlertView(alertTitle: "Something went wrong", alertMessage: "", delegate: self)
        }
        else{
            if !NetworkManager.sharedInstance.checkStatusCode(statusCode){
                let error = NetworkManager.sharedInstance.getErrorModel(requestType: requestType, httpStatusCode: statusCode, responseData: Data)
                AlertControl.sharedInstance.presentAlertView(alertTitle: (error.errorMessage!), alertMessage: "", delegate: self)
            }
            else{
                if requestType == .ServiceTypeOrderCount {
                    let orderCountModel = ResponseManager.sharedInstance.getOrderCount(responseData: Data)
                    let indexPath = IndexPath(row: 2, section: 0)
                    let cell = menuController.tableView.cellForRow(at: indexPath) as! MenuListCell
                    if orderCountModel.totalCount != 0 {
                        cell.countLbl.isHidden = false
                        cell.countLbl.text = "\(orderCountModel.totalCount!)"
                        cell.countLbl.layer.cornerRadius = cell.countLbl.frame.size.width/2.0
                        self.view.layoutIfNeeded()
                    }
                    else {
                        cell.countLbl.isHidden = true
                    }
                }
                else {
                    let pharmacyModel = ResponseManager.sharedInstance.getPharmacyData(responseData: Data)
                }
            }
        }
    }
    
    //MARK: - Textfield delegate function implementation
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        searchedNDC = textField.text
        GlobalData.sharedInstance.searchText = searchTxtField.text!
        self.view.sendSubview(toBack: searchBgView)
        if self.contentNavController.viewControllers[0] is BuyController && self.contentNavController.viewControllers.count == 1{
            self.performsBackAction()
        }
        else {
            self.presentBuyController()
        }
        
        return true
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        if string.isEmpty {
            return true
        }
        let text = "\(textField.text ?? "")\(string)"
        if text.count > 100 {
            return false
        }
        return true
    }
    
    // MARK: - Navigation
    
    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
//        let destinationController = segue.destination as! UserViewController
//        destinationController.headerTitle = sender as? String
    }
    
}


