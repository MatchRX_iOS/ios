//
//  SingleColumnTextFieldCell.swift
//  MatchRX
//
//  Created by Vignesh on 11/23/17.
//  Copyright © 2017 HTC. All rights reserved.
//

import UIKit

class SingleColumnTextFieldCell: UITableViewCell {
    
    @IBOutlet var labelValue: UILabel!
    @IBOutlet var textFieldValue: UITextField!

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
