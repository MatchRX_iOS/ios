//
//  ScanController.swift
//  MatchRX
//
//  Created by Vignesh on 12/6/17.
//  Copyright © 2017 HTC. All rights reserved.
//

import UIKit
import AVFoundation

protocol ScanControlDelegate {
    func scannedObject(sender: String)
}

class ScanController: UIViewController, AVCaptureMetadataOutputObjectsDelegate {

    @IBOutlet var messageLabel:UILabel!
    @IBOutlet var topbar: UIView!
    @IBOutlet var overlayImgView: UIImageView!
    @IBOutlet var scanView: UIView!
    
    var scanDelegate: ScanControlDelegate!
    var captureSession:AVCaptureSession?
    var videoPreviewLayer:AVCaptureVideoPreviewLayer?
    var qrCodeFrameView:UIView?
    var isScanned: Bool = false
    
    let supportedCodeTypes = [AVMetadataObjectTypeUPCECode,
                              AVMetadataObjectTypeCode39Code,
                              AVMetadataObjectTypeCode39Mod43Code,
                              AVMetadataObjectTypeCode93Code,
                              AVMetadataObjectTypeCode128Code,
                              AVMetadataObjectTypeEAN8Code,
                              AVMetadataObjectTypeEAN13Code,
                              AVMetadataObjectTypeAztecCode,
                              AVMetadataObjectTypePDF417Code,
                              AVMetadataObjectTypeInterleaved2of5Code,
                              AVMetadataObjectTypeITF14Code,
                              AVMetadataObjectTypeDataMatrixCode,
                              AVMetadataObjectTypeQRCode]
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Get an instance of the AVCaptureDevice class to initialize a device object and provide the video as the media type parameter.
        let captureDevice = AVCaptureDevice.defaultDevice(withMediaType: AVMediaTypeVideo)
        
        do {
            // Get an instance of the AVCaptureDeviceInput class using the previous device object.
            let input = try AVCaptureDeviceInput(device: captureDevice)
            
            // Initialize the captureSession object.
            captureSession = AVCaptureSession()
            
            // Set the input device on the capture session.
            captureSession?.addInput(input)
            
            // Initialize a AVCaptureMetadataOutput object and set it as the output device to the capture session.
            let captureMetadataOutput = AVCaptureMetadataOutput()
            captureSession?.addOutput(captureMetadataOutput)
            
            // Set delegate and use the default dispatch queue to execute the call back
            captureMetadataOutput.setMetadataObjectsDelegate(self, queue: DispatchQueue.main)
            captureMetadataOutput.metadataObjectTypes = [AVMetadataObjectTypeUPCECode,
                                                         AVMetadataObjectTypeCode39Code,
                                                         AVMetadataObjectTypeCode39Mod43Code,
                                                         AVMetadataObjectTypeCode93Code,
                                                         AVMetadataObjectTypeCode128Code,
                                                         AVMetadataObjectTypeEAN8Code,
                                                         AVMetadataObjectTypeEAN13Code,
                                                         AVMetadataObjectTypeAztecCode,
                                                         AVMetadataObjectTypePDF417Code,
                                                         AVMetadataObjectTypeInterleaved2of5Code,
                                                         AVMetadataObjectTypeITF14Code,
                                                         AVMetadataObjectTypeDataMatrixCode,
                                                         AVMetadataObjectTypeQRCode]
            
            // Initialize the video preview layer and add it as a sublayer to the viewPreview view's layer.
            videoPreviewLayer = AVCaptureVideoPreviewLayer(session: captureSession)
            videoPreviewLayer?.videoGravity = AVLayerVideoGravityResizeAspectFill
            videoPreviewLayer?.frame = view.layer.bounds
            view.layer.addSublayer(videoPreviewLayer!)
            
            // Start video capture.
            captureSession?.startRunning()
            
            // Move the message label and top bar to the front
            view.bringSubview(toFront: messageLabel)
            view.bringSubview(toFront: topbar)
            view.bringSubview(toFront: overlayImgView)
            view.bringSubview(toFront: scanView)
            captureMetadataOutput.accessibilityFrame = overlayImgView.frame
            let visibleRect = videoPreviewLayer?.metadataOutputRectOfInterest(for: overlayImgView.frame)
            captureMetadataOutput.rectOfInterest = visibleRect!
            // Initialize QR Code Frame to highlight the QR code
            qrCodeFrameView = UIView()
            
            if let qrCodeFrameView = qrCodeFrameView {
                qrCodeFrameView.layer.borderColor = UIColor.green.cgColor
                qrCodeFrameView.layer.borderWidth = 1
//                scanView.addSubview(qrCodeFrameView)
//                scanView.bringSubview(toFront: qrCodeFrameView)
            }
            qrCodeFrameView?.frame = CGRect(x: scanView.frame.origin.x, y: scanView.frame.height/2.0, width: scanView.frame.size.width, height: 1.0)
            
        } catch {
            // If any error occurs, simply print it out and don't continue any more.
            print(error)
            return
        }
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    //Dismiss controller
    @IBAction func dismissScanController(){
        self.dismiss(animated: true, completion: nil)
    }
    
    func dismissController(scannedObject: String) {
        self.dismiss(animated: true) {
            self.scanDelegate.scannedObject(sender: scannedObject)
        }
    }
    
    // MARK: - AVCaptureMetadataOutputObjectsDelegate Methods
    
    func captureOutput(_ captureOutput: AVCaptureOutput!, didOutputMetadataObjects metadataObjects: [Any]!, from connection: AVCaptureConnection!) {
        
        // Check if the metadataObjects array is not nil and it contains at least one object.
        if metadataObjects == nil || metadataObjects.count == 0 {
            qrCodeFrameView?.frame = CGRect.zero
            messageLabel.text = "No QR/barcode is detected"
            return
        }
        
        // Get the metadata object.
        let metadataObj = metadataObjects[0] as! AVMetadataMachineReadableCodeObject
        
        if supportedCodeTypes.contains(metadataObj.type) {
            // If the found metadata is equal to the QR code metadata then update the status label's text and set the bounds
            let barCodeObject = videoPreviewLayer?.transformedMetadataObject(for: metadataObj)
            qrCodeFrameView?.frame = barCodeObject!.bounds
//            if self.scanDelegate != nil {
//                self.dismissScanController()
//                self.scanDelegate.scannedObject(sender: metadataObj.stringValue)
//            }
            if isScanned == false {
                self.dismissController(scannedObject: metadataObj.stringValue)
                isScanned = true
            }
//            self.scanDelegate = nil
            if metadataObj.stringValue != nil {
                messageLabel.text = metadataObj.stringValue
            }
        }
    }

}
