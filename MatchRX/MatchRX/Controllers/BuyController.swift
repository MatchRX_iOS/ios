//
//  BuyController.swift
//  MatchRX
//
//  Created by Vignesh on 1/18/18.
//  Copyright © 2018 HTC. All rights reserved.
//

import UIKit

class BuyController: UIViewController {
    
    var editPostControl: EditPostingControl!
    var buyListViewCtrl: BuyListViewController!
    
    @IBOutlet weak var headerView: UIView!
    @IBOutlet weak var filterSortView: UIView!
    @IBOutlet weak var contentView: UIView!
    @IBOutlet weak var selectedImageView: UIImageView!
    @IBOutlet weak var selectedImgViewLeadingConstraint: NSLayoutConstraint!
    
    @IBOutlet weak var allPostingBtn: UIButton!
    @IBOutlet weak var myPurchasesBtn: UIButton!
    @IBOutlet weak var myWishlistBtn: UIButton!
    @IBOutlet weak var filterCountLbl: UILabel!
    var filterCount = 0

    override func viewDidLoad() {
        super.viewDidLoad()
        headerView.layer.masksToBounds = false
        headerView.layer.shadowOffset = CGSize(width: 0, height: 3)
        headerView.layer.shadowRadius = 1
        headerView.layer.shadowOpacity = 0.2
        
        self.onMenuItemsClicked(sender: allPostingBtn)
        
        // Do any additional setup after loading the view.
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
    }
    
    override func viewDidLayoutSubviews() {
//        self.loadAllPostings();
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    //MARK: - IBAction instance functions.
    
    //Presents the filter view controller.
    @IBAction func onFilterClicked(sender: UIButton) {
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let filterControl = storyboard.instantiateViewController(withIdentifier: "filterControl") as! FilterController
//        NavigationModel.sharedInstance.homeControl?.contentNavController.view.frame = (NavigationModel.sharedInstance.homeControl?.contentView.bounds)!
//        NavigationModel.sharedInstance.homeControl?.contentNavController.isNavigationBarHidden = true
//        NavigationModel.sharedInstance.homeControl?.contentNavController.pushViewController(filterControl, animated: true)
        NavigationModel.sharedInstance.homeControl?.present(filterControl, animated: true, completion: nil)
    }
    
    //Presents the sort view controller.
    @IBAction func onSortClicked(sender: UIButton) {
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let sortControl = storyboard.instantiateViewController(withIdentifier: "sortControl") as! SortController
        sortControl.modalPresentationStyle = .overCurrentContext
        NavigationModel.sharedInstance.homeControl?.present(sortControl, animated: true, completion: nil)
//        filterControl.view.backgroundColor = UIColor.lightGray.withAlphaComponent(0.5)
//        filterControl.view.isOpaque = false
    }
    
    //Presents the Edit posting view control.
    @IBAction func getMyPost() {
        if editPostControl != nil {
            editPostControl = nil
        }
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        editPostControl = storyboard.instantiateViewController(withIdentifier: "editPostControl") as! EditPostingControl
        NavigationModel.sharedInstance.homeControl?.contentNavController.pushViewController(editPostControl, animated: true)
        NavigationModel.sharedInstance.homeControl?.showBackIconWithTitle(title: "EDIT POSTING")
//        editPostControl.postId = "296"
    }
    
    //Reloads the list based on the menu selected.
    @IBAction func onMenuItemsClicked(sender: UIButton) {
        selectedImgViewLeadingConstraint.constant = sender.frame.origin.x
        sender.isSelected = true
        switch sender {
        case allPostingBtn:
            allPostingBtn.isSelected = true
            myPurchasesBtn.isSelected = false
            myWishlistBtn.isSelected = false
            self.loadAllPostings()
            break
            
        case myPurchasesBtn:
            allPostingBtn.isSelected = false
            myPurchasesBtn.isSelected = true
            myWishlistBtn.isSelected = false
            self.loadmyPurchases()
            break
            
        case myWishlistBtn:
            allPostingBtn.isSelected = false
            myPurchasesBtn.isSelected = false
            myWishlistBtn.isSelected = true
            self.loadMyWishlist()
            break
            
        default:
            break
        }
    }
    
    func loadmyPurchases() {
//        if buyListViewCtrl != nil {
//            buyListViewCtrl.view.removeFromSuperview()
//            buyListViewCtrl = nil
//        }
        buyListViewCtrl.isActiveMenu = "buyagain"
        buyListViewCtrl.reloadListData()
    }
    
    func loadMyWishlist() {
        
//        if buyListViewCtrl != nil {
//            buyListViewCtrl.view.removeFromSuperview()
//            buyListViewCtrl = nil
//        }
        buyListViewCtrl.isActiveMenu = "mywishlist"
        buyListViewCtrl.reloadListData()
    }
    
    func loadAllPostings() {
        if buyListViewCtrl == nil {
//            buyListViewCtrl.view.removeFromSuperview()
//            buyListViewCtrl = nil
            let stortBd  = UIStoryboard(name: "Secondary", bundle: nil);
            buyListViewCtrl = stortBd.instantiateViewController(withIdentifier: "buyAllListViewCtrl") as! BuyListViewController;
            buyListViewCtrl.view.frame = self.contentView.bounds;
            buyListViewCtrl.isActiveMenu = ""
            self.contentView.addSubview(buyListViewCtrl.view);
            self.contentView.bringSubview(toFront: buyListViewCtrl.view)
            NavigationModel.sharedInstance.buyListControl = buyListViewCtrl
            GlobalData.sharedInstance.resetFilterDictionary()
            GlobalData.sharedInstance.resetSortDictionary()
            buyListViewCtrl.fetchMatketplaceDataService()
        }
        else {
            buyListViewCtrl.isActiveMenu = ""
            buyListViewCtrl.reloadListData()
        }
        /*let stortBd  = UIStoryboard(name: "Secondary", bundle: nil);
        buyListViewCtrl = stortBd.instantiateViewController(withIdentifier: "buyAllListViewCtrl") as! BuyListViewController;
        buyListViewCtrl.view.frame = self.contentView.bounds;
        buyListViewCtrl.isActiveMenu = ""
        self.contentView.addSubview(buyListViewCtrl.view);
        self.contentView.bringSubview(toFront: buyListViewCtrl.view)
        GlobalData.sharedInstance.resetFilterDictionary()
        GlobalData.sharedInstance.resetSortDictionary()
        buyListViewCtrl.fetchMatketplaceDataService()
        NavigationModel.sharedInstance.buyListControl = buyListViewCtrl*/
    }
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
