//
//  CartViewController.swift
//  MatchRX
//
//  Created by Poonkathirvelan on 19/02/18.
//  Copyright © 2018 HTC. All rights reserved.
//

import UIKit

class CartViewController: UIViewController,UITableViewDelegate,UITableViewDataSource, NetworkManagerDelegate, DropDownViewDelegate {
    
    //MARK: - IBOutlet references
    @IBOutlet weak var cartTableView: UITableView!
    @IBOutlet weak var tableviewHeightCons: NSLayoutConstraint!
    
    @IBOutlet weak var bottomViewHeightCons: NSLayoutConstraint!
    @IBOutlet weak var consPriceView: NSLayoutConstraint!
    @IBOutlet weak var priceConsView: UIView!
    @IBOutlet weak var grandTotalAmountlbl: UILabel!
    @IBOutlet weak var scrollView: UIScrollView!
    @IBOutlet weak var messageLbl: UILabel!
    @IBOutlet weak var informationLbl: UILabel!
    @IBOutlet weak var informationViewHeightConstraint: NSLayoutConstraint!
    
    //MARK: - Global variables declarations
    var cartArray = NSMutableArray.init()
    var tempHeight:CGFloat = 10.0
    var catSummaryViewCtrl:OrderSummaryController!
    var selectedIndex: Int?
    var selectedSection: Int?
    var selectedIndexPath: IndexPath?
    var selectedFooterIndexPath: IndexPath?
    
    let DEFAULT_FOOTER_HEIGHT: CGFloat = 320.0
    
    //MARK: - Default funtions
    
    override func viewDidLoad() {
        super.viewDidLoad()
        scrollView.isHidden = true
        messageLbl.isHidden = true
        informationViewHeightConstraint.constant = 0
        self.fetchCartData()
        // Do any additional setup after loading the view.
    }
    override func viewDidLayoutSubviews() {
        self.bottomViewHeightCons.constant = 300
        
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
    }
    
    
    //MARK: - IBAction function implementations.
    
    
    
    @IBAction func submitOrder(_ sender: UIButton) {
        let requestDict = self.getRequestToSumbitOrder()
        if NetworkManager.sharedInstance.isReachable(){
            _ = NetworkManager.sharedInstance.checkTypeOfNetwork()
            if NetworkManager.sharedInstance.isReachableViaWiFi || NetworkManager.sharedInstance.isReachableViaCellular{
                LoadingView.shareInstance.showLodingView(parentView: (NavigationModel.sharedInstance.homeControl?.view!)!)
                NetworkManager.sharedInstance.manageAPIRequest(requestType: .ServiceTypeSubmitOrder, requestString: RequestDataModel.sharedInstance.makeRequestToSumbitOrder(requestDict: requestDict), delegate: self)
            }
        }
        else{
            AlertControl.sharedInstance.presentAlertView(alertTitle: "Kindly check your internet connectivity", alertMessage: "", delegate: nil)
        }
    }
    
    //MARK: - Instance function implementations done here.
    
    func getRequestToSumbitOrder() -> NSMutableDictionary {
        let dataDict = NSMutableDictionary.init()
        let requestDict = NSMutableDictionary.init()
        let myCartArray = NSMutableArray.init()
        for i in 0..<cartArray.count {
            let sellerData = cartArray.object(at: i) as! CartSellerDataModel
            let shippingKeyArray = sellerData.shippingMethods.allKeys
            for j in 0..<shippingKeyArray.count {
                let shippingData = sellerData.shippingMethods.object(forKey: shippingKeyArray[j]) as! CartShippingDetailsDataModel
                let cartDict = NSMutableDictionary.init()
                if shippingData.isSelected! {
                    cartDict.setValue(shippingData.shippingPaidBy, forKey: "shipping_paid_by")
                    cartDict.setValue(shippingData.shippingId, forKey: "shipping_method")
                    cartDict.setValue(sellerData.cartId, forKey: "cart_id")
                    myCartArray.add(cartDict)
                    break
                }
            }
        }
        requestDict.setValue(myCartArray, forKey: "my_cart")
        requestDict.setValue(self.getGrandTotal(), forKey: "grand_total")
        dataDict.setValue(requestDict, forKey: "data")
        return dataDict
    }
    
    func fetchCartData() {
        if NetworkManager.sharedInstance.isReachable(){
            _ = NetworkManager.sharedInstance.checkTypeOfNetwork()
            if NetworkManager.sharedInstance.isReachableViaWiFi || NetworkManager.sharedInstance.isReachableViaCellular{
                LoadingView.shareInstance.showLodingView(parentView: (NavigationModel.sharedInstance.homeControl?.view!)!)
                NetworkManager.sharedInstance.manageAPIRequest(requestType: .ServiceTypeFetchMyCartDetails, requestString: "", delegate: self)
            }
        }
        else{
            AlertControl.sharedInstance.presentAlertView(alertTitle: "Kindly check your internet connectivity", alertMessage: "", delegate: nil)
        }
    }
    
    //Update cart service connection done.
    func updateCart(updateCartDict: NSMutableDictionary) {
        if NetworkManager.sharedInstance.isReachable(){
            _ = NetworkManager.sharedInstance.checkTypeOfNetwork()
            if NetworkManager.sharedInstance.isReachableViaWiFi || NetworkManager.sharedInstance.isReachableViaCellular{
                LoadingView.shareInstance.showLodingView(parentView: (NavigationModel.sharedInstance.homeControl?.view!)!)
                NetworkManager.sharedInstance.manageAPIRequest(requestType: .ServiceTypeUpdateQuantityInCart, requestString: RequestDataModel.sharedInstance.makeRequestForUpdatingAnItem(requestDict: updateCartDict), delegate: self)
            }
        }
        else{
            AlertControl.sharedInstance.presentAlertView(alertTitle: "Kindly check your internet connectivity", alertMessage: "", delegate: nil)
        }
    }
    
    //Triggered on clicking the delete button in cell.
    func onDeleteClicked(sender: UIButton) {
        selectedIndex = sender.tag
        selectedSection = sender.superview?.tag
        let cell = sender.superview?.superview?.superview?.superview as! CartCell
        let indexPath = cartTableView.indexPath(for: cell)
        let cartItemData = (cartArray.object(at: (indexPath?.section)!) as! CartSellerDataModel).cartDetailsArray.object(at: (indexPath?.row)!) as! CartItemDataModel
        
        let alertControl = UIAlertController(title: "Do you want to delete this item from your cart?", message: "", preferredStyle: UIAlertControllerStyle.alert)
        alertControl.addAction(UIAlertAction(title:"YES", style: UIAlertActionStyle.default, handler: {actions in self.deleteAnItem(data: cartItemData)}))
        alertControl.addAction(UIAlertAction(title: "NO", style: UIAlertActionStyle.cancel, handler: nil))
        UIApplication.topViewController()?.present(alertControl, animated: true, completion: nil)
        
        
    }
    
    //Prepares the service connection to delete the item from cart.
    func deleteAnItem(data: CartItemDataModel) {
        let updateCartDict = NSMutableDictionary.init()
        updateCartDict.setValue(data.postId, forKey: "post_id")
        if NetworkManager.sharedInstance.isReachable(){
            _ = NetworkManager.sharedInstance.checkTypeOfNetwork()
            if NetworkManager.sharedInstance.isReachableViaWiFi || NetworkManager.sharedInstance.isReachableViaCellular{
                LoadingView.shareInstance.showLodingView(parentView: (NavigationModel.sharedInstance.homeControl?.view!)!)
                NetworkManager.sharedInstance.manageAPIRequest(requestType: .ServiceTypeDeleteItemInCart, requestString: RequestDataModel.sharedInstance.makeRequestForUpdatingAnItem(requestDict: updateCartDict), delegate: self)
            }
        }
        else{
            AlertControl.sharedInstance.presentAlertView(alertTitle: "Kindly check your internet connectivity", alertMessage: "", delegate: nil)
        }
    }
    
    func onShippingTypeClicked(sender: UIButton) {
        let cell = sender.superview?.superview?.superview?.superview?.superview as! CartFooterTableViewCell
        selectedFooterIndexPath = cartTableView.indexPath(for: cell)
        
        let dataModel = cartArray.object(at: (sender.superview?.tag)!) as! CartSellerDataModel
        let shippingData = dataModel.shippingMethods.object(forKey: sender.titleLabel?.text ?? "") as! CartShippingDetailsDataModel
        var shippingCost = ""
        if shippingData.shippingPaidBy == "buyer" {
            shippingCost = "\(shippingData.shippingCost!)"
        }
        else {
            shippingCost = "Free"
        }
        if Double(shippingCost) != nil {
            cell.shippingCostLbl.text = "$\(String(format: "%.2f", arguments: [Double(shippingCost)!]))"
            dataModel.userModifiedSellerTotal = dataModel.itemTotalAmount!+Double(shippingCost)!
        }
        else {
            cell.shippingCostLbl.text = shippingCost
            dataModel.userModifiedSellerTotal = dataModel.totalAmount
            let sellerTotal = dataModel.itemTotalAmount!
            dataModel.userModifiedSellerTotal = sellerTotal
        }
        cell.sellerTotalLbl.text = "$\(String(format: "%.2f", arguments: [dataModel.userModifiedSellerTotal!]))"
//        cartArray.replaceObject(at: section, with: dataModel)
        
        cell.groundRadio.image = UIImage(named: "Radio off.png")
        cell.TwoDayRadioImg.image = UIImage(named: "Radio off.png")
        cell.priorityRadio.image = UIImage(named: "Radio off.png")
        switch sender.tag {
        case 500:
            cell.groundRadio.image = UIImage(named: "Radio on.png")
        case 501:
            cell.TwoDayRadioImg.image = UIImage(named: "Radio on.png")
        case 502:
            cell.priorityRadio.image = UIImage(named: "Radio on.png")
        default:
            cell.groundRadio.image = UIImage(named: "")
        }
        
        let keyArray = dataModel.shippingMethods.allKeys
        for i in 0..<keyArray.count {
            let shippingData = dataModel.shippingMethods.object(forKey: keyArray[i]) as! CartShippingDetailsDataModel
            if keyArray[i] as? String == sender.titleLabel?.text {
                shippingData.isSelected = true
            }
            else {
                shippingData.isSelected = false
            }
            dataModel.shippingMethods.setValue(shippingData, forKey: (keyArray[i] as? String)!)
        }
        
        dataModel.shippingAmount = shippingData.shippingCost
//        dataModel.totalAmount = dataModel.itemTotalAmount!+dataModel.shippingAmount!
        cartArray.replaceObject(at: (sender.superview?.tag)!, with: dataModel)
//        cell.shippingCostLbl.text = "$\(String(format: "%.2f", arguments: [dataModel.shippingAmount!]))"
//        cell.sellerTotalLbl.text = "$\(String(format: "%.2f", arguments: [dataModel.totalAmount!]))"
        
        /*let previousContentHeight = scrollView.contentSize.height
        let previousContentOffset = scrollView.contentOffset.y
        tempHeight = 10.0
        tableviewHeightCons.constant = 10
        LoadingView.shareInstance.showLodingView(parentView: (NavigationModel.sharedInstance.homeControl?.view!)!)
        self.reloadData {
            let currentContentOffset = self.scrollView.contentSize.height - previousContentHeight + previousContentOffset
            self.scrollView.contentOffset = CGPoint(x: 0, y: currentContentOffset)
            LoadingView.shareInstance.hideLoadingView()
        }
//        cartTableView.reloadData()*/
        self.loadAllPriceListView()
    }
    
    func reloadData(completion: @escaping ()->()) {
        UIView.animate(withDuration: 0, animations: { self.cartTableView.reloadData() })
        { _ in completion() }
    }
    
    func onQuantityClicked(sender: UIButton) {
        selectedIndex = sender.tag
        selectedSection = sender.superview?.tag
        let cell = sender.superview?.superview?.superview?.superview?.superview as! CartCell
        selectedIndexPath = cartTableView.indexPath(for: cell)
        let dataModel = cartArray.object(at: (sender.superview?.tag)!) as! CartSellerDataModel
        let cartItemModel = dataModel.cartDetailsArray.object(at: sender.tag) as! CartItemDataModel
        let dropDownData = NSMutableArray()
        for i in 1..<cartItemModel.availableQuantity!+1 {
            let model = DropDownModel()
            model.modelIdString = "\(i)" as NSString
            model.modelDataString = "\(i)" as NSString
            dropDownData.add(model)
        }
        DropDownView.defaultDropDownControl.showDropDownForData(dropDownData,DropDownType.SingleSelectionDefault, CGRect.zero, (NavigationModel.sharedInstance.homeControl?.view)!, self as DropDownViewDelegate, [])
        DropDownView.defaultDropDownControl.dropDownListSearch?.isHidden = true
    }
    
    
    func getGrandTotal() -> Double {
        var grandTotal = 0.0
        for i in 0..<cartArray.count {
            let dataModel = cartArray.object(at: i) as! CartSellerDataModel
//            grandTotal += dataModel.totalAmount!
            grandTotal += dataModel.userModifiedSellerTotal!
        }
        return grandTotal
    }
    
    func loadAllPriceListView() {
        self.view.layoutIfNeeded()
        
        for subview in priceConsView.subviews {
            if subview.isKind(of: UILabel.self){
                subview.removeFromSuperview()
            }
            if subview.isKind(of: UIImageView.self) {
                subview.removeFromSuperview()
            }
        }
        var grandTotal = 0.0
        var height = 20
        var heightCount = 1
        if cartArray.count > 1 {
            for i in 0..<cartArray.count {
                let dataModel = cartArray.object(at: i) as! CartSellerDataModel
                let sellarNamelbl:UILabel = UILabel(frame: CGRect(x: 20, y: 20*i+10, width: 200, height: 15))
                sellarNamelbl.text = "\(dataModel.sellerName!) total"
                sellarNamelbl.tag = 100+i
                sellarNamelbl.textColor = UIColor.darkGray
                sellarNamelbl.font  = UIFont(name: "Roboto-Light", size: 12.0)
                priceConsView.addSubview(sellarNamelbl)
                
                let sellerPriceLbl:UILabel = UILabel(frame: CGRect(x: Int(self.priceConsView.bounds.width-125), y: 20*i+10, width: 110, height: 15))
                //            sellerPriceLbl.text = "$ \(String(format: "%.2f", arguments: [dataModel.totalAmount!]))"
                sellerPriceLbl.text = "$ \(String(format: "%.2f", arguments: [dataModel.userModifiedSellerTotal!]))"
                sellerPriceLbl.tag = 100+i
                sellerPriceLbl.textColor = UIColor.darkGray
                sellerPriceLbl.font  = UIFont(name: "Roboto-Light", size: 12.0)
                sellerPriceLbl.textAlignment = NSTextAlignment.right
                priceConsView.addSubview(sellerPriceLbl)
                //            grandTotal += dataModel.totalAmount!
                grandTotal += dataModel.userModifiedSellerTotal!
            }
        }
        else {
            let dataModel = cartArray.object(at: 0) as! CartSellerDataModel
            height = 5
            heightCount = 0
            grandTotal = dataModel.userModifiedSellerTotal!
        }
        let Totallbl:UILabel = UILabel(frame: CGRect(x: 20, y: height*cartArray.count+height, width: 200, height: 15))
        Totallbl.text = "Grand Total"
        Totallbl.tag = 106
        Totallbl.textColor = UIColor.darkGray
        Totallbl.font  = UIFont(name: "Roboto-Medium", size: 12.0)
        priceConsView.addSubview(Totallbl)
        
        let totalPriceLbl:UILabel = UILabel(frame: CGRect(x: Int(self.priceConsView.bounds.width-125), y: height*cartArray.count+height, width: 110, height: 15))
        totalPriceLbl.text = "$\(String(format: "%.2f", arguments: [grandTotal]))"
        totalPriceLbl.tag = 106
        totalPriceLbl.textColor = UIColor.darkGray
        totalPriceLbl.font  = UIFont(name: "Roboto-Medium", size: 12.0)
        totalPriceLbl.textAlignment = NSTextAlignment.right
        
        if cartArray.count > 1 {
            let stripImgView = UIImageView(frame: CGRect(x: Int(self.priceConsView.bounds.width-95), y: 20*cartArray.count+10, width: 80, height: 1))
            stripImgView.backgroundColor = UIColor.lightGray
            priceConsView.addSubview(stripImgView)
        }
        
        priceConsView.addSubview(totalPriceLbl)
        
        
        let pricelistHeight =  (cartArray.count+heightCount)*25+10
        self.consPriceView.constant = CGFloat(pricelistHeight)
        self.bottomViewHeightCons.constant = 175.0 + CGFloat(pricelistHeight)
    }
    
    func navigateToSellerMarketPlace(sender: UIButton) {
        let dataModel = cartArray.object(at: sender.tag) as! CartSellerDataModel
        let sellerDict = NSMutableDictionary.init()
        sellerDict.setValue(dataModel.sellerId, forKey: "seller")
        let sellerArray = NSMutableArray.init()
        sellerArray.add(sellerDict)
        GlobalData.sharedInstance.sellerArray = sellerArray
        NavigationModel.sharedInstance.homeControl?.contentNavController.popToRootViewController(animated: true)
        NavigationModel.sharedInstance.homeControl?.headerLabel.text = "BUY"
        NavigationModel.sharedInstance.homeControl?.screenTitle = "BUY"
        NavigationModel.sharedInstance.homeControl?.headerMenuView.isHidden = false
        NavigationModel.sharedInstance.homeControl?.hamburgerView.isHidden = false
        NavigationModel.sharedInstance.homeControl?.backView.isHidden = true
        NavigationModel.sharedInstance.homeControl?.showBuyMenu()
//        if NavigationModel.sharedInstance.homeControl?.currentIndex == 0 {
//            NavigationModel.sharedInstance.homeControl?.showBuyMenu()
//        }
//        else {
//
//        }
    }
    
    func bindDataWithCartTableView(Data: Any!, requestType: ServiceRequestType) {
        let cartResponseModel = ResponseManager.sharedInstance.fetchMyCartDetails(responseData: Data)
        cartArray.removeAllObjects()
        if cartResponseModel.isCartObjectExists! {
            cartArray = cartResponseModel.cartSellerArray
        }
        if cartArray.count > 0 {
            scrollView.isHidden = false
            tempHeight = 10.0
            tableviewHeightCons.constant = 10
            cartTableView.reloadData()
            self.loadAllPriceListView()
            if requestType == .ServiceTypeFetchMyCartDetails && !(cartResponseModel.sellerInformation?.isEmpty)!{
                informationLbl.text = cartResponseModel.sellerInformation
                self.view.layoutIfNeeded()
                informationViewHeightConstraint.constant = informationLbl.frame.size.height+40
            }
            else if requestType == .ServiceTypeSubmitOrder && !(cartResponseModel.checkoutInformation?.isEmpty)!{
                informationLbl.text = cartResponseModel.checkoutInformation
                self.view.layoutIfNeeded()
                informationViewHeightConstraint.constant = informationLbl.frame.size.height+40
            }
        }
        else {
            scrollView.isHidden = true
            messageLbl.isHidden = false
            if requestType == .ServiceTypeFetchMyCartDetails || requestType == .ServiceTypeDeleteItemInCart{
                messageLbl.text = cartResponseModel.sellerInformation
            }
            else if requestType == .ServiceTypeSubmitOrder {
                messageLbl.text = cartResponseModel.checkoutInformation
            }
        }
        if GlobalData.sharedInstance.cartCount! > 0 {
            NavigationModel.sharedInstance.homeControl?.cartCountLbl.text = "\(GlobalData.sharedInstance.cartCount!)"
            NavigationModel.sharedInstance.homeControl?.cartCountLbl.isHidden = false
        }
        else {
            NavigationModel.sharedInstance.homeControl?.cartCountLbl.text = ""
            NavigationModel.sharedInstance.homeControl?.cartCountLbl.isHidden = true
        }
    }
    
    
    func resizeFooterCellView(footerCell: CartFooterTableViewCell, matchRxOffer: String, fedEXOffer: String, dataModel: CartSellerDataModel) -> CartFooterTableViewCell{
        if matchRxOffer == "" && fedEXOffer == "" {
            footerCell.fedexOfferInfoViewHeightConstraint.constant = 0
            footerCell.matchRxOfferLblViewHeightConstraint.constant = 0
            footerCell.matchRXOfferInfoContentViewHeightConstraint.constant = 0
//            footerCell.layoutIfNeeded()
//            self.view.layoutIfNeeded()
        }
        else {
            if matchRxOffer != "" {
                footerCell.matchRxOfferLblViewHeightConstraint.constant = footerCell.matchRXOfferLbl.frame.size.height+20.0
                var offerHeight: CGFloat = footerCell.matchRxOfferLblViewHeightConstraint.constant
                if fedEXOffer != "" {
                    offerHeight = footerCell.matchRxOfferLblViewHeightConstraint.constant+10.0
                }
                footerCell.matchRXOfferInfoContentViewHeightConstraint.constant = offerHeight
            }
            else {
                footerCell.matchRXOfferInfoContentViewHeightConstraint.constant = 0.0
            }
            if fedEXOffer != "" {
                footerCell.fedexOfferInfoViewHeightConstraint.constant = footerCell.fedexOfferLbl.frame.size.height+20.0
            }
            else {
                footerCell.fedexOfferInfoViewHeightConstraint.constant = 0
            }
//            footerCell.layoutIfNeeded()
//            self.view.layoutIfNeeded()
        }
        if dataModel.shippingMethods.object(forKey: "Ground") == nil {
            footerCell.groundViewHeightConstraint.constant = 0
        }
        if dataModel.shippingMethods.object(forKey: "2-Day") == nil {
            footerCell.twoDayHeightConstratint.constant = 0
        }
        footerCell.layoutIfNeeded()
        self.view.layoutIfNeeded()
        return footerCell
    }
    
    //MARK: - Drop down delegate functions.
    func selectedDropDownData(_ dropDownDataObject: DropDownModel) {
        DropDownView.defaultDropDownControl.hideDropDown()
        let cell = cartTableView.cellForRow(at: selectedIndexPath!) as! CartCell
        let quantity = dropDownDataObject.modelDataString as String?
//        cell.quantityLbl.text = dropDownDataObject.modelDataString as String?
        let cartItemData = (cartArray.object(at: (selectedIndexPath?.section)!) as! CartSellerDataModel).cartDetailsArray.object(at: (selectedIndexPath?.row)!) as! CartItemDataModel
        let updateCartDict = NSMutableDictionary.init()
        updateCartDict.setValue(cartItemData.postId, forKey: "post_id")
        updateCartDict.setValue(quantity, forKey: "quantity")
        self.perform(#selector(self.updateCart(updateCartDict:)), with: updateCartDict, afterDelay: 0.3)
    }
    
    
    func selectedDropDownDataArray(_ dropDownDataArray: NSMutableArray) {
        
        print("")
    }
    
    func selectedDropDownDataString(_ selectedDropDownDataString: NSString) {
        
        print("")
    }
    
    //MARK: - Table view delegate and data source functions.
    
    //Returns the number of sections in tableview
    func numberOfSections(in tableView: UITableView) -> Int {
        return cartArray.count
    }
    func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        let dataModel = cartArray.object(at: section) as! CartSellerDataModel
//        return "\(dataModel.sellerName!)"
        return ""
    }
    //Returns the number of rows in the section
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        let dataModel = cartArray.object(at: section) as! CartSellerDataModel
        return dataModel.cartDetailsArray.count
    }
    
    func tableView(_ tableView: UITableView, viewForFooterInSection section: Int) -> UIView? {
        var  footerCell = tableView.dequeueReusableCell(withIdentifier: "FooterCell") as! CartFooterTableViewCell
        let dataModel = cartArray.object(at: section) as! CartSellerDataModel
        var shippingCost = ""
        
        footerCell.itemTotalLbl.text = "$\(String(format: "%.2f", arguments: [dataModel.itemTotalAmount!]))"
//        footerCell.shippingCostLbl.text = "$\(String(format: "%.2f", arguments: [dataModel.shippingAmount!]))"
//        footerCell.sellerTotalLbl.text = "$\(String(format: "%.2f", arguments: [dataModel.totalAmount!]))"
        footerCell.sellerNameLbl.text = "\(dataModel.sellerName!) Total"
        
        
//        let dashedBorder = CAShapeLayer()
//        dashedBorder.strokeColor = UIColor.gray.cgColor
//        dashedBorder.lineDashPattern = [2, 2]
//        dashedBorder.fillColor = nil
//        dashedBorder.path = UIBezierPath(rect: footerCell.dashedImgView.bounds).cgPath
//        dashedBorder.frame = footerCell.dashedImgView.bounds
//        footerCell.dashedImgView.layer.borderColor = UIColor.lightGray.cgColor
//        footerCell.dashedImgView.layer.addSublayer(dashedBorder)
        
        var matchRxOffer = ""
        var fedEXOffer = ""
        
        if !(dataModel.overnightInfo?.isEmpty)! {
            matchRxOffer = dataModel.overnightInfo!
        }
        else {
            if !(dataModel.groundInfo?.isEmpty)! {
                matchRxOffer = dataModel.groundInfo!
            }
            if !(dataModel.twoDayInfo?.isEmpty)! {
                fedEXOffer = dataModel.twoDayInfo!
            }
        }
        footerCell.matchRXOfferLbl.text = matchRxOffer
        footerCell.fedexOfferLbl.text = fedEXOffer
        
        let url = URL(string: dataModel.fedexImageUrl!)
        let data = try? Data(contentsOf: url!)
        
        if let imageData = data {
            let image = UIImage(data: imageData)
            footerCell.fedexImgView.image = image
        }
        if dataModel.shippingMethods.object(forKey: "Ground") != nil {
            let groundData = dataModel.shippingMethods.object(forKey: "Ground") as! CartShippingDetailsDataModel
            footerCell.groundBtn.titleLabel?.text = groundData.shippingId
            footerCell.groundBtn.addTarget(self, action: #selector(self.onShippingTypeClicked(sender:)), for: .touchUpInside)
            footerCell.groundBtn.superview?.tag = section
            if groundData.isSelected! {
                footerCell.groundBtn.isSelected = true
                footerCell.shipingMethodSelctd(footerCell.groundBtn)
                if groundData.shippingPaidBy == "buyer" {
                    shippingCost = "\(groundData.shippingCost!)"
                }
                else {
                    shippingCost = "Free"
                }
            }
            footerCell.groundTitleLbl.text = groundData.shippingName
            footerCell.groundDeliveryLbl.text = groundData.deliveryDetails
            /*if groundData.matchRXShippingOffer != "" {
                footerCell.matchRXOfferLbl.text = groundData.matchRXShippingOffer
                matchRxOffer = groundData.matchRXShippingOffer!
            }
            if groundData.fedexShippingOffer != "" {
                footerCell.fedexOfferLbl.text = groundData.fedexShippingOffer
                fedEXOffer = groundData.fedexShippingOffer!
            }*/
        }
        else {
            footerCell.groundViewHeightConstraint.constant = 0
        }
        
        if dataModel.shippingMethods.object(forKey: "2-Day") != nil {
            
            let twoDayData = dataModel.shippingMethods.object(forKey: "2-Day") as! CartShippingDetailsDataModel
            footerCell.twoDayBtn.titleLabel?.text = twoDayData.shippingId
            footerCell.twoDayBtn.addTarget(self, action: #selector(self.onShippingTypeClicked(sender:)), for: .touchUpInside)
            footerCell.twoDayBtn.superview?.tag = section
            if twoDayData.isSelected! {
                footerCell.twoDayBtn.isSelected = true
                footerCell.shipingMethodSelctd(footerCell.twoDayBtn)
                if twoDayData.shippingPaidBy == "buyer" {
                    shippingCost = "\(twoDayData.shippingCost!)"
                }
                else {
                    shippingCost = "Free"
                }
            }
            footerCell.twoDayTitleLbl.text = twoDayData.shippingName
            footerCell.twoDayDeliveryLbl.text = twoDayData.deliveryDetails
        }
        else {
            footerCell.twoDayHeightConstratint.constant = 0
        }
        
        if dataModel.shippingMethods.object(forKey: "Overnight") != nil {
            let priorityOvernightData = dataModel.shippingMethods.object(forKey: "Overnight") as! CartShippingDetailsDataModel
            footerCell.priorityBtn.titleLabel?.text = priorityOvernightData.shippingId
            footerCell.priorityBtn.addTarget(self, action: #selector(self.onShippingTypeClicked(sender:)), for: .touchUpInside)
            footerCell.priorityBtn.superview?.tag = section
            if priorityOvernightData.isSelected! {
                footerCell.priorityBtn.isSelected = true
                footerCell.shipingMethodSelctd(footerCell.priorityBtn)
                if priorityOvernightData.shippingPaidBy == "buyer" {
                    shippingCost = "\(priorityOvernightData.shippingCost!)"
                }
                else {
                    shippingCost = "Free"
                }
            }
            footerCell.priorityTitleLbl.text = priorityOvernightData.shippingName
            footerCell.priorityDeliveryLbl.text = priorityOvernightData.deliveryDetails
            /*if priorityOvernightData.matchRXShippingOffer != "" {
                footerCell.matchRXOfferLbl.text = priorityOvernightData.matchRXShippingOffer
                matchRxOffer = priorityOvernightData.matchRXShippingOffer!
            }
            if priorityOvernightData.fedexShippingOffer != "" {
                footerCell.fedexOfferLbl.text = priorityOvernightData.fedexShippingOffer
                fedEXOffer = priorityOvernightData.fedexShippingOffer!
            }*/
        }
        if Double(shippingCost) != nil {
            footerCell.shippingCostLbl.text = "$\(String(format: "%.2f", arguments: [Double(shippingCost)!]))"
            dataModel.userModifiedSellerTotal = dataModel.itemTotalAmount!+Double(shippingCost)!
        }
        else {
            footerCell.shippingCostLbl.text = shippingCost
            dataModel.userModifiedSellerTotal = dataModel.itemTotalAmount
//            let sellerTotal = dataModel.itemTotalAmount!-dataModel.shippingAmount!
            let sellerTotal = dataModel.itemTotalAmount!
            dataModel.userModifiedSellerTotal = sellerTotal
        }
        footerCell.sellerTotalLbl.text = "$\(String(format: "%.2f", arguments: [dataModel.userModifiedSellerTotal!]))"
        cartArray.replaceObject(at: section, with: dataModel)
//        footerCell.shippingCostLbl.text = cost
        footerCell.layoutIfNeeded()
        
        footerCell = self.resizeFooterCellView(footerCell: footerCell, matchRxOffer: matchRxOffer, fedEXOffer: fedEXOffer, dataModel: dataModel)
        print("Footer height in view method is: \(footerCell.frame.size.height)")
        print("Footer basecontent height in view method is: \(footerCell.baseContentView.frame.size.height)")
        return footerCell
    }
    
    
    
    
    func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        var  footerCell = tableView.dequeueReusableCell(withIdentifier: "FooterCell") as! CartFooterTableViewCell
        let dataModel = cartArray.object(at: section) as! CartSellerDataModel
        var matchRxOffer = ""
        var fedEXOffer = ""
        
        if !(dataModel.overnightInfo?.isEmpty)! {
            matchRxOffer = dataModel.overnightInfo!
        }
        else {
            if !(dataModel.groundInfo?.isEmpty)! {
                matchRxOffer = dataModel.groundInfo!
            }
            if !(dataModel.twoDayInfo?.isEmpty)! {
                fedEXOffer = dataModel.twoDayInfo!
            }
        }
        footerCell.matchRXOfferLbl.text = matchRxOffer
        footerCell.fedexOfferLbl.text = fedEXOffer
        
        footerCell.layoutIfNeeded()
        footerCell = self.resizeFooterCellView(footerCell: footerCell, matchRxOffer: matchRxOffer, fedEXOffer: fedEXOffer, dataModel: dataModel)
        
        print("Footer height in height method is: \(footerCell.frame.size.height)")
        print("Footer basecontent height in height is: \(footerCell.baseContentView.frame.size.height)")
        return footerCell.baseContentView.frame.size.height
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 50.0
    }
    
    //Returns the cell for the indexpath.
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "cartCell", for: indexPath) as! CartCell
        cell.partialPackView.isHidden = true
        cell.quantityView.isHidden = true
        if cartArray.count != 0{
            let dataModel = cartArray.object(at: indexPath.section) as! CartSellerDataModel
            if indexPath.row < dataModel.cartDetailsArray.count {
                let cartItemModel = dataModel.cartDetailsArray.object(at: indexPath.row) as! CartItemDataModel
                let url = URL(string: cartItemModel.medispanImageURL!)
                let data = try? Data(contentsOf: url!)
                
                if let imageData = data {
                    let image = UIImage(data: imageData)
                    cell.medispanImageView.image = image
                }
                cell.deleteButton.tag = indexPath.row
                cell.deleteButton.superview?.tag = indexPath.section
                cell.deleteButton.addTarget(self, action: #selector(self.onDeleteClicked(sender:)), for: .touchUpInside)
                cell.ndcName.text = cartItemModel.postTitle
                cell.ndcNum_ManufacturerNameLbl.text = "\(cartItemModel.ndcNumber!) | \(cartItemModel.manufacturerName!)"
                if cartItemModel.packageOption == "F" {
                    cell.partialPackView.isHidden = true
                    cell.quantityView.isHidden = false
                    cell.quantityLbl.text = "\(cartItemModel.quantityAddedInCart!)"
//                    let price = Double(cartItemModel.unitPrice!*Double(cartItemModel.packageQuantity!)*Double(cartItemModel.actualPackageSize!)).rounded(toPlaces: 2)
                    let revisedPrice = String(format: "%.2f", arguments: [cartItemModel.revisedPrice!])
                    cell.priceLbl.text = "$\(revisedPrice)"
                    cell.quantityButton.layer.borderWidth = 1.0
                    cell.quantityButton.layer.borderColor = UIColor.init(red: 240.0/255.0, green: 240.0/255.0, blue: 240.0/255.0, alpha: 1.0).cgColor
                    cell.quantityButton.superview?.tag = indexPath.section
                    cell.quantityButton.tag = indexPath.row
                    cell.quantityButton.addTarget(self, action: #selector(self.onQuantityClicked(sender:)), for: .touchUpInside)
                }
                else {
                    cell.partialPackView.isHidden = false
                    cell.quantityView.isHidden = true
//                    cell.partialLbl.text = "Partial: \(cartItemModel.packageSize!)/\(cartItemModel.actualPackageSize!)"
                    cell.partialLbl.text = cartItemModel.packaging
//                    let price = Double(cartItemModel.unitPrice!*Double(cartItemModel.packageQuantity!)).rounded(toPlaces: 2)
                    let revisedPrice = String(format: "%.2f", arguments: [cartItemModel.revisedPrice!])
                    cell.priceLbl.text = "$\(revisedPrice)"
                }
            }
        }
        return cell
    }
    
    
    
    
    
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        tempHeight = tempHeight + cell.bounds.height
        // if tempHeight > self.tableviewHeightCons.constant {
        self.tableviewHeightCons.constant =  self.tableviewHeightCons.constant + cell.bounds.height
        // }
    }
    func tableView(_ tableView: UITableView, willDisplayFooterView view: UIView, forSection section: Int) {
        tempHeight = tempHeight + view.bounds.height
        self.tableviewHeightCons.constant = self.tableviewHeightCons.constant + view.bounds.height
    }
    
    func tableView(_ tableView: UITableView, willDisplayHeaderView view: UIView, forSection section: Int) {
        self.tableviewHeightCons.constant = self.tableviewHeightCons.constant + view.bounds.height
        let dataModel = cartArray.object(at: section) as! CartSellerDataModel
        
        let header: UITableViewHeaderFooterView = view as! UITableViewHeaderFooterView
        for view in header.subviews {
            if view.isKind(of: UILabel.self) {
                view.removeFromSuperview()
            }
        }
        header.contentView.backgroundColor = UIColor.init(red: 240/255, green: 240/255, blue: 240/255, alpha: 1.0); //UIColor.colorWithHexString(hexStr: "#408000")
        header.textLabel?.textColor = UIColor.init(red: 33/255, green: 33/255, blue: 33/255, alpha: 1.0)
        header.textLabel?.backgroundColor = UIColor.clear
        header.textLabel?.font = UIFont(name: "Roboto-Medium", size: 14.0)
//        header.textLabel?.text = dataModel.sellerName!//UIFont.fontNames(forFamilyName: "Roboto-Medium");
        
        let headerLbl = UILabel.init(frame: CGRect(x: 15.0, y: view.frame.height-50, width: 80, height: 50))
        headerLbl.textColor = UIColor.init(red: 33/255, green: 33/255, blue: 33/255, alpha: 1.0)
        headerLbl.backgroundColor = UIColor.clear
        headerLbl.font = UIFont(name: "Roboto-Medium", size: 14.0)
        headerLbl.text = dataModel.sellerName!
        header.addSubview(headerLbl)
        
//        let buttonTap:UIButton = UIButton(frame: CGRect(x: 150, y: view.frame.height-35, width: view.frame.width-160, height: 35))
        let buttonTap:UIButton = UIButton()
        buttonTap.tag = section;
        buttonTap.titleLabel?.font = UIFont(name: "Roboto-Light", size: 12.0)
        buttonTap.setTitleColor(UIColor.init(red: 33/255, green: 147/255, blue: 240/255, alpha: 1.0), for: .normal)
        buttonTap.setTitle("Shop seller’s other items", for: .normal)
        buttonTap.sizeToFit()
        self.view.layoutIfNeeded()
        buttonTap.addTarget(self, action: #selector(self.navigateToSellerMarketPlace(sender:)), for: .touchUpInside)
        buttonTap.frame = CGRect(x: view.frame.size.width-buttonTap.frame.size.width-5.0, y: view.frame.height-50, width: buttonTap.frame.width, height: 50)
        header.addSubview(buttonTap)
    }
    
    
    
    
    //MARK: - Web service response delegate function implementation.
    
    //Returns the response
    func webServiceResponse(for requestType: ServiceRequestType, responseData Data: Any!, withStatusCode statusCode: Int, errorMessage message: Any!) {
        LoadingView.shareInstance.hideLoadingView()
        if message as? Error != nil {
            AlertControl.sharedInstance.presentAlertView(alertTitle: "Something went wrong", alertMessage: "", delegate: self)
        }
        else{
            if !NetworkManager.sharedInstance.checkStatusCode(statusCode){
                if statusCode == 220  && requestType == .ServiceTypeSubmitOrder{
                    self.bindDataWithCartTableView(Data: Data, requestType: requestType)
                }
                else {
                    let error = NetworkManager.sharedInstance.getErrorModel(requestType: requestType, httpStatusCode: statusCode, responseData: Data)
                    AlertControl.sharedInstance.presentAlertView(alertTitle: (error.errorMessage!), alertMessage: "", delegate: self)
                }
            }
            else{
                if requestType == .ServiceTypeSubmitOrder {
                    let orderResponseModel = ResponseManager.sharedInstance.fetchMyOrderSummary(responseData: Data)
                    if self.catSummaryViewCtrl != nil{
                        self.catSummaryViewCtrl = nil
                    }
                    let storyBoard = UIStoryboard.init(name: "Secondary", bundle: nil)
                    self.catSummaryViewCtrl = storyBoard.instantiateViewController(withIdentifier: "orderSummaryCtrl") as! OrderSummaryController
                    catSummaryViewCtrl.summaryDataModel = orderResponseModel
//                    self.navigationController?.present(catSummaryViewCtrl, animated: true, completion: nil)
                    NavigationModel.sharedInstance.homeControl?.contentNavController.popToRootViewController(animated: false)
                    NavigationModel.sharedInstance.homeControl?.contentNavController.pushViewController(catSummaryViewCtrl, animated: true)
                    NavigationModel.sharedInstance.homeControl?.showBackIconWithTitle(title: "ORDER SUMMARY")
                    NavigationModel.sharedInstance.homeControl?.hamburgerView.isHidden = true
                    NavigationModel.sharedInstance.homeControl?.backView.isHidden = true
                }
                else {
                    if requestType == .ServiceTypeDeleteItemInCart {
                        AlertControl.sharedInstance.presentAlertView(alertTitle: "Item has been deleted successfully", alertMessage: "", delegate: nil)
                    }
                    self.bindDataWithCartTableView(Data: Data, requestType: requestType)
                }
                /*if GlobalData.sharedInstance.cartCount! > 0 {
                    NavigationModel.sharedInstance.homeControl?.cartCountLbl.text = "\(GlobalData.sharedInstance.cartCount!)"
                    NavigationModel.sharedInstance.homeControl?.cartCountLbl.isHidden = false
                }
                else {
                    NavigationModel.sharedInstance.homeControl?.cartCountLbl.text = ""
                    NavigationModel.sharedInstance.homeControl?.cartCountLbl.isHidden = true
                }*/
            }
        }
    }
    
    //    func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
    //        if section%2 == 0{
    //
    //
    //            return 230
    //        }
    //        else{
    //
    //            return 300
    //        }
    //    }
    
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
     // Get the new view controller using segue.destinationViewController.
     // Pass the selected object to the new view controller.
     }
     */
    
}

