//
//  MyOrdersCell.swift
//  MatchRX
//
//  Created by Vignesh on 4/12/18.
//  Copyright © 2018 HTC. All rights reserved.
//

import UIKit

class MyOrdersCell: UITableViewCell {
    
    @IBOutlet weak var orderNumberLbl: UILabel!
    @IBOutlet weak var orderDateLbl: UILabel!
    @IBOutlet weak var totalTitleLbl: UILabel!
    @IBOutlet weak var totalValueLbl: UILabel!
    @IBOutlet weak var shippingMethodLbl: UILabel!
    @IBOutlet weak var deliveryDateLbl: UILabel!
    @IBOutlet weak var shippingView: UIView!
    @IBOutlet weak var deliveryDateView: UIView!
    @IBOutlet weak var baseView: UIView!
    @IBOutlet weak var trackOrderBtn: UIButton!
    @IBOutlet weak var orderStatusBtn: UIButton!

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    override func layoutSubviews() {
        trackOrderBtn.layer.borderWidth = 1.0
        trackOrderBtn.layer.borderColor = UIColor(red: 33.0/255.0, green: 150.0/255.0, blue: 243.0/255.0, alpha: 1.0).cgColor
        self.baseView.layer.masksToBounds = false
        self.baseView.layer.shadowOffset = CGSize(width: 3, height: 3)
        self.baseView.layer.shadowRadius = 0.5
        self.baseView.layer.shadowOpacity = 0.1
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
