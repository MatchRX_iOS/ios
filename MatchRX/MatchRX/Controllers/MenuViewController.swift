//
//  MenuViewController.swift
//  ServiceTest
//
//  Created by Vignesh on 10/13/17.
//  Copyright © 2017 HTC. All rights reserved.
//

import UIKit

protocol menuDelegate {
    func selectedRowAtIndex(selectedIndex: Int, menuTitle: String, selectedData: Any?)
}

class MenuViewController: UITableViewController {
    
    //MARK: - Global variable declaration
    
    var menuArray = [[String]]()
    var parentDelegate: menuDelegate!
    var launchMenu: String?

    override func viewDidLoad() {
        super.viewDidLoad()

        // Uncomment the following line to preserve selection between presentations
        // self.clearsSelectionOnViewWillAppear = false

        // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
        // self.navigationItem.rightBarButtonItem = self.editButtonItem()
        menuArray.append(["BUY", "Buy_new.png"])
        menuArray.append(["SELL", "Sell_new.png"])
        menuArray.append(["MY ORDERS", "MyOrders_new.png"])
        menuArray.append(["WISHLIST", "Wishlist_new.png"])
        menuArray.append(["MY ACCOUNT", "MyAccount_new.png"])
        menuArray.append(["SETTINGS", "Settings_new.png"])
        
        var indexPath: IndexPath!
        if launchMenu != "myorders" {
            indexPath = IndexPath(row: 0, section: 0)
        }
        else{
            indexPath = IndexPath(row: 2, section: 0)
        }
        self.tableView.selectRow(at: indexPath, animated: true, scrollPosition: .bottom)
        self.tableView(self.tableView, didSelectRowAt: indexPath)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    // MARK: - Table view data source

    override func numberOfSections(in tableView: UITableView) -> Int {
        // #warning Incomplete implementation, return the number of sections
        return 1
    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // #warning Incomplete implementation, return the number of rows
        return menuArray.count
    }

    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        var cell: MenuListCell
        cell = tableView.dequeueReusableCell(withIdentifier: "menuCell", for: indexPath) as! MenuListCell

        // Configure the cell...
        if indexPath.row <= menuArray.count {
            cell.menuNameLabel.text = menuArray[indexPath.row][0]
            cell.imageView?.image = UIImage.init(named: menuArray[indexPath.row][1])
            cell.countLbl.layer.cornerRadius = cell.countLbl.frame.size.width/2.0
//            cell.countLbl.isHidden = true
            cell.layoutIfNeeded()
        }

        return cell
    }
    
    override func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 50.0
    }

    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        self.parentDelegate .selectedRowAtIndex(selectedIndex: indexPath.row, menuTitle: menuArray[indexPath.row][0], selectedData: menuArray) 
    }
    /*
    // Override to support conditional editing of the table view.
    override func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        // Return false if you do not want the specified item to be editable.
        return true
    }
    */

    /*
    // Override to support editing the table view.
    override func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCellEditingStyle, forRowAt indexPath: IndexPath) {
        if editingStyle == .delete {
            // Delete the row from the data source
            tableView.deleteRows(at: [indexPath], with: .fade)
        } else if editingStyle == .insert {
            // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
        }    
    }
    */

    /*
    // Override to support rearranging the table view.
    override func tableView(_ tableView: UITableView, moveRowAt fromIndexPath: IndexPath, to: IndexPath) {

    }
    */

    /*
    // Override to support conditional rearranging of the table view.
    override func tableView(_ tableView: UITableView, canMoveRowAt indexPath: IndexPath) -> Bool {
        // Return false if you do not want the item to be re-orderable.
        return true
    }
    */

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
