//
//  ReLoginController.swift
//  MatchRX
//
//  Created by Vignesh on 1/17/18.
//  Copyright © 2018 HTC. All rights reserved.
//

import UIKit

class ReLoginController: UIViewController, NetworkManagerDelegate {

    let myGroup = DispatchGroup()
    var serviceCount = 0
    var loginResponseModel = LoginResponseModel()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationController?.isNavigationBarHidden = true
        
        // Do any additional setup after loading the view.
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        let defaults = UserDefaults.standard
        if (defaults.object(forKey: "refreshToken") != nil) {
            let refreshToken = defaults.object(forKey: "refreshToken") as? String
            myGroup.enter()
            if NetworkManager.sharedInstance.isReachable(){
                _ = NetworkManager.sharedInstance.checkTypeOfNetwork()
                if NetworkManager.sharedInstance.isReachableViaWiFi || NetworkManager.sharedInstance.isReachableViaCellular{
                    serviceCount += 1
                    LoadingView.shareInstance.showLodingView(parentView: self.view.window!)
                    NetworkManager.sharedInstance.manageAPIRequest(requestType: .ServiceTypeReLogin, requestString: RequestDataModel.sharedInstance.createRequestForReLogin(refreshToken: refreshToken!), delegate: self)
                }
            }
            else{
                AlertControl.sharedInstance.presentAlertView(alertTitle: "Kindly check your internet connectivity", alertMessage: "", delegate: nil)
                let storyboard = UIStoryboard(name: "Main", bundle: nil)
                let appDelegate = UIApplication.shared.delegate as? AppDelegate
                appDelegate?.window?.rootViewController = storyboard.instantiateInitialViewController()
            }
            myGroup.notify(queue: DispatchQueue.main) {
                if NetworkManager.sharedInstance.isReachable(){
                    _ = NetworkManager.sharedInstance.checkTypeOfNetwork()
                    if NetworkManager.sharedInstance.isReachableViaWiFi || NetworkManager.sharedInstance.isReachableViaCellular{
                        LoadingView.shareInstance.showLodingView(parentView: self.view.window!)
                        NetworkManager.sharedInstance.manageAPIRequest(requestType: .ServiceTypeLOVDetails, requestString: "", delegate: self)
                    }
                }
                else{
                    AlertControl.sharedInstance.presentAlertView(alertTitle: "Kindly check your internet connectivity", alertMessage: "", delegate: nil)
                    let storyboard = UIStoryboard(name: "Main", bundle: nil)
                    let appDelegate = UIApplication.shared.delegate as? AppDelegate
                    appDelegate?.window?.rootViewController = storyboard.instantiateInitialViewController()
                }
            }
            
        }
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    //Presents the home view controller.
    func presentHomeController(responseData: Any!) {
        self.rewriteRefreshToken()
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let homeControl = storyboard.instantiateViewController(withIdentifier: "homeControl") as! HomeControl
        homeControl.loginResponseModel = loginResponseModel
        self.navigationController?.present(homeControl, animated: true, completion: nil)
    }
    
    //Rewrite the refresh token.
    func rewriteRefreshToken() {
        let defaults = UserDefaults.standard
        defaults.set(GlobalData.sharedInstance.refreshToken, forKey: "refreshToken")
        defaults.synchronize()
    }
    
    //MARK: - Network manager delegate functions.
    
    func webServiceResponse(for requestType: ServiceRequestType, responseData Data: Any!, withStatusCode statusCode: Int, errorMessage message: Any!) {
        LoadingView.shareInstance.hideLoadingView()
        if message as? Error != nil {
            AlertControl.sharedInstance.presentAlertView(alertTitle: "Something went wrong", alertMessage: "", delegate: self)
            let storyboard = UIStoryboard(name: "Main", bundle: nil)
            let appDelegate = UIApplication.shared.delegate as? AppDelegate
            appDelegate?.window?.rootViewController = storyboard.instantiateInitialViewController()
        }
        else{
            if !NetworkManager.sharedInstance.checkStatusCode(statusCode){
                let error = NetworkManager.sharedInstance.getErrorModel(requestType: requestType, httpStatusCode: statusCode, responseData: Data)
                AlertControl.sharedInstance.presentAlertView(alertTitle: error.errorMessage!, alertMessage: "", delegate: self)
                let storyboard = UIStoryboard(name: "Main", bundle: nil)
                //            self.window?.rootViewController = storyboard.instantiateInitialViewController()
                let appDelegate = UIApplication.shared.delegate as? AppDelegate
                appDelegate?.window?.rootViewController = storyboard.instantiateInitialViewController()
            }
            else{
                if requestType == .ServiceTypeReLogin {
                    myGroup.leave()
                    loginResponseModel = ResponseManager.sharedInstance.getLoginResponseData(responseData: Data)
                    GlobalData.sharedInstance.userId = loginResponseModel.userId
                    GlobalData.sharedInstance.accessToken = loginResponseModel.accessToken
                    GlobalData.sharedInstance.refreshToken = loginResponseModel.refreshToken
                    GlobalData.sharedInstance.pharmacyId = loginResponseModel.pharmacyId
                    GlobalData.sharedInstance.deaNumber = loginResponseModel.pharmacyDEA
                    GlobalData.sharedInstance.landingPage = loginResponseModel.landingPage
                    GlobalData.sharedInstance.salesInProgressCount = loginResponseModel.salesInProgressCount
                    GlobalData.sharedInstance.purchaseInProgressCount = loginResponseModel.purchaseInProgressCount
                    GlobalData.sharedInstance.stateWiseRestrictionMessage = loginResponseModel.stateWiseRestrictionMessage
                    GlobalData.sharedInstance.stateWiseRestriction = loginResponseModel.stateWiseRestriction
                    GlobalData.sharedInstance.cartCount = loginResponseModel.cartCount
                }
                else if requestType == .ServiceTypeLOVDetails {
                    GlobalData.sharedInstance.lovDataModel = ResponseManager.sharedInstance.getLOVDetails(responseData: Data)
                    self.presentHomeController(responseData: Data)
                }
            }
        }
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
