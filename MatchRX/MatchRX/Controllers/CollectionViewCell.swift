//
//  CollectionViewCell.swift
//  MatchRX
//
//  Created by Vignesh on 12/12/17.
//  Copyright © 2017 HTC. All rights reserved.
//

import UIKit

class CollectionViewCell: UICollectionViewCell {
    @IBOutlet var uploadButton: UIButton!
    @IBOutlet var cancelButton: UIButton!
}
