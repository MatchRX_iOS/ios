//
//  ItemDetailViewController.swift
//  MatchRXS5
//
//  Created by Poonkathirvelan on 23/01/18.
//  Copyright © 2018 HTCGlobalServices. All rights reserved.
//

import UIKit

protocol ItemDetailsDelegate {
    func loadResponseData(responseData: MarketPlaceDataModel, requestType: ServiceRequestType)
    func reloadBuyListFromItemDetail()
}

class ItemDetailViewController: UIViewController ,UITableViewDelegate,UITableViewDataSource,UIScrollViewDelegate, NetworkManagerDelegate, DropDownViewDelegate, EditPostingDelegate{
    
    
    
    //MARK: - IBOutlet references
    @IBOutlet weak var contentScrollView: UIScrollView!
    @IBOutlet weak var itemDesTable: UITableView!
    @IBOutlet weak var baseTblViewHeight: NSLayoutConstraint!
    @IBOutlet weak var imgScrollView: UIScrollView!
    @IBOutlet weak var imgPageControl: UIPageControl!
    @IBOutlet var comparePriceView: UIView!
    @IBOutlet weak var qtyLabel: UILabel!
    @IBOutlet weak var itemImgView: UIImageView!
    @IBOutlet weak var wacDiscountBtn: UIButton!
    @IBOutlet weak var ratingView: UIView!
    @IBOutlet weak var ratingParentView: UIView!
    @IBOutlet weak var ndcNameLbl: UILabel!
    @IBOutlet weak var ndcNumberLbl: UILabel!
    @IBOutlet weak var deliveryDateLbl: UILabel!
    @IBOutlet weak var shippingPromoLbl: UILabel!
    @IBOutlet weak var availableQtyLbl: UILabel!
    @IBOutlet weak var priceLbl: UILabel!
    @IBOutlet weak var quantityTxtField: UITextField!
    @IBOutlet weak var quantityBtn: UIButton!
    @IBOutlet weak var addToCartBtn: UIButton!
    @IBOutlet weak var wishlistBtn: UIButton!
    @IBOutlet weak var ratingLbl: UILabel!
    @IBOutlet weak var shippingPromoView: UIView!
    @IBOutlet weak var cartView: UIView!
    @IBOutlet weak var cartImage: UIImageView!
    @IBOutlet weak var cartLbl: UILabel!
    @IBOutlet weak var quantityView: UIView!
    var medispanImage: UIImage!
    
    var parentDelegate: ItemDetailsDelegate!
    
    //MARK: - Globally declared variables
    
    
    let TABLEVIEW_SECTION_HEIGHT: CGFloat = 35.0
    let myGroup = DispatchGroup()
    let CART_DISABLED_COLOR = UIColor(red: 170.0/255.0, green: 170.0/255.0, blue: 170.0/255.0, alpha: 1.0)
    let CART_MY_POSTING_COLOR = UIColor(red: 44.0/255.0, green: 185.0/255.0, blue: 227.0/255.0, alpha: 1.0)
    let CART_PREVIOUSLY_PURCHASED_COLOR = UIColor(red: 67.0/255.0, green: 160.0/255.0, blue: 71.0/255.0, alpha: 1.0)
    let CART_DEFAULT_COLOR = UIColor(red: 255.0/255.0, green: 148.0/255.0, blue: 0.0/255.0, alpha: 1.0)
    
    var sectionnames = ["Product Description","Package Details","Price Details","Seller Notes"];
    
    var SectionItems = [["Esomeprazole Magnesiyum item","600 mg","CPRD","Normal","ASTRAZENECA LP","NA"],["HM01700","BOTTLE(5 ML)","Original Non Sealed","8 EZ","12/2021"],["$6.977785","10%","Seller Pays (for items more than $50)"],["ONS, Original but not Sealed"]];
    var itemTitle = [["Generic Name","Strength","Form","Storage","Labeler/Mfg","Shape"],["Lot Number","Packaging","Package Condition","Package Quantity","Expiration Date"],["Unit Price","WAC Discount","Shipping Method"],["Note"]];
    var selectedSection = NSMutableArray();
    var expandedSectionHeaderNumber = -1;
    
    var compareListViCtrl: CompareListViewController!
    var theImageView: UIImageView!
    var postId: String?
    var prviousHeight: CGFloat = 200.0
    var tableCellNormal: UITableViewCell!
    var isComparePriceHidden: Bool = false
    var itemDataModel: ItemDetailsDataModel!
    var loopCount = 0
    var carouselImageArray = NSMutableArray.init()
    
    
    //MARK: - Override functions
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
        self.itemDesTable.estimatedRowHeight = 25
        self.itemDesTable.rowHeight = UITableViewAutomaticDimension
        self.itemDesTable.register(UITableViewCell.self , forCellReuseIdentifier: "CellNormal")
        if isComparePriceHidden {
            comparePriceView.isHidden = true
        }
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func viewDidLayoutSubviews() {
        self.comparePriceView.layer.borderWidth = 1.0;
        self.comparePriceView.layer.borderColor = UIColor.init(red: 1.0, green: 148.0/255.0, blue: 0.0, alpha: 1.0).cgColor
        
//        self.qtyLabel.layer.borderWidth = 1.0;
//        self.qtyLabel.layer.borderColor = UIColor.init(red: 240.0/255.0, green: 240.0/255.0, blue: 240.0/255.0, alpha: 1.0).cgColor
        self.itemDesTable.delegate = self
        self.itemDesTable.dataSource =  self
        quantityBtn.layer.borderWidth = 1.0
        quantityBtn.layer.borderColor = UIColor.init(red: 240.0/255.0, green: 240.0/255.0, blue: 240.0/255.0, alpha: 1.0).cgColor
        self.perform(#selector(self.loadScrollViweImages), with: nil, afterDelay: 0.5)
    }
    
    
    //MARK: - Instance functions
    
    //Shows and hides the compare price button based on the argument
    func showHideComparePriceView(flag: Bool) {
        if flag {
            comparePriceView.isHidden = false
        }
        else {
            comparePriceView.isHidden = true
        }
    }
    
    //Prepares connection to fetch data from server.
    func getItemDetailsForItem(itemId: String) {
        if NetworkManager.sharedInstance.isReachable(){
            _ = NetworkManager.sharedInstance.checkTypeOfNetwork()
            if NetworkManager.sharedInstance.isReachableViaWiFi || NetworkManager.sharedInstance.isReachableViaCellular{
//                LoadingView.shareInstance.showWhiteView(parentView: self.view)
                LoadingView.shareInstance.showLodingView(parentView: (NavigationModel.sharedInstance.homeControl?.view!)!)
                NetworkManager.sharedInstance.manageAPIRequest(requestType: .ServiceTypeFetchItemDetails, requestString: itemId, delegate: self)
            }
        }
        else{
            AlertControl.sharedInstance.presentAlertView(alertTitle: "Kindly check your internet connectivity", alertMessage: "", delegate: nil)
        }
    }
    
    //Binds the fetched data from server with the view.
    func bindDataWithView() {
        ndcNameLbl.text = itemDataModel.ndcName
        ndcNumberLbl.text = "NDC# \(itemDataModel.ndcNumber!)"
        
        if itemDataModel.isWishlisted! {
            wishlistBtn.isSelected = true
        }
        wacDiscountBtn.setTitle("\(itemDataModel.discountPercentage!)% WAC", for: .normal)
        
        deliveryDateLbl.text = itemDataModel.estimatedDeliveryDate
        ratingLbl.text = "Seller Rating \(itemDataModel.rating!)"
        self.loadStarRating(value: itemDataModel.rating!)
        if itemDataModel.shippingHandlingAmount != "0" {
            shippingPromoView.isHidden = false
            shippingPromoLbl.text = "$\(itemDataModel.shippingHandlingAmount!)"
        }
        else {
            shippingPromoView.isHidden = true
        }
        availableQtyLbl.text = "\(itemDataModel.availableQuantity!)"
//        priceLbl.text = "$ \(itemDataModel.unitPrice!*Double(itemDataModel.packageQuantity!))"
        let revisedPrice = String(format: "%.2f", arguments: [itemDataModel.revisedPrice!])
        priceLbl.text = "$ \(revisedPrice)"
        
        SectionItems = [[itemDataModel.genericName!, itemDataModel.strength!, itemDataModel.dosageForm!, itemDataModel.storage!, itemDataModel.manufacturerName!, itemDataModel.shape!],[itemDataModel.lotNumber!, itemDataModel.packaging!, itemDataModel.originalPackage!, "\(itemDataModel.packageSize!) \(itemDataModel.packageSizeUnitOfMeasure!)", itemDataModel.expiryDate!],["$\(itemDataModel.unitPrice!)", "\(itemDataModel.discountPercentage!)%", itemDataModel.shippingDisplayName!],[itemDataModel.packageConditionDescription!]]
        
        self.itemDesTable.reloadData()
        
        if itemDataModel.isMyPosting! {
            cartView.backgroundColor = CART_MY_POSTING_COLOR
            cartLbl.text = "My Posting"
            cartImage.image = UIImage.init(named: "B_Edit.png")
            quantityView.isHidden = true
            ratingParentView.isHidden = true
//            cartImage.isHidden = true
        }
        else if itemDataModel.isLastBought! {
            cartView.backgroundColor = CART_PREVIOUSLY_PURCHASED_COLOR
            cartLbl.text = "Add to Cart"
        }
        else {
            if itemDataModel.isInCart! && (itemDataModel.availableQuantity == itemDataModel.quantityInCart) {
                cartView.backgroundColor = CART_DISABLED_COLOR
                addToCartBtn.isUserInteractionEnabled = false
                cartLbl.text = "Add to Cart"
                availableQtyLbl.text = "\(itemDataModel.availableQuantity!-itemDataModel.quantityInCart!)"
                quantityBtn.isUserInteractionEnabled = false
                qtyLabel.text = "\(itemDataModel.availableQuantity!-itemDataModel.quantityInCart!)"
                quantityView.isHidden = true
            }
            else {
                cartView.backgroundColor = CART_DEFAULT_COLOR
                cartLbl.text = "Add to Cart"
                if itemDataModel.isInCart! {
                    availableQtyLbl.text = "\(itemDataModel.availableQuantity!-itemDataModel.quantityInCart!)"
                    qtyLabel.text = "\(itemDataModel.availableQuantity!-itemDataModel.quantityInCart!)"
                }
            }
        }
        
        if !itemDataModel.isComparePrice! {
            comparePriceView.isUserInteractionEnabled = false
            comparePriceView.alpha = 0.5
        }
        
        /*if itemDataModel.availableQuantity! > 1 {
            quantityBtn.isUserInteractionEnabled = true
        }
        else {
            quantityBtn.isUserInteractionEnabled = false
        }*/
//        qtyLabel.text = "\(itemDataModel.availableQuantity!)"
        
        let url = URL(string: itemDataModel.medispanImageURL!)
        let data = try? Data(contentsOf: url!)
        
        if let imageData = data {
            let image = UIImage(data: imageData)
            medispanImage = image
            carouselImageArray.add(image!)
        }
        
        if itemDataModel.imageArray.count > 0 {
            if itemDataModel.imageArray.count == 1 {
//                self.downloadImage()
                self.downloadImage(completion: {
                    self.loadScrollViweImages()
                })
            }
            else {
                myGroup.enter()
                self.downloadImage()
                myGroup.notify(queue: DispatchQueue.main) {
//                    self.downloadImage()
                    self.downloadImage(completion: {
                        self.loadScrollViweImages()
                    })
                }
            }
        }
    }
    
    //Download the image
    func downloadImage(completion:() -> ()) {
        let imageId = "\(itemDataModel.imageArray.object(at: loopCount))"
        if NetworkManager.sharedInstance.isReachable(){
            _ = NetworkManager.sharedInstance.checkTypeOfNetwork()
            if NetworkManager.sharedInstance.isReachableViaWiFi || NetworkManager.sharedInstance.isReachableViaCellular{
                loopCount += 1
//                LoadingView.shareInstance.showWhiteView(parentView: self.view)
                LoadingView.shareInstance.showLodingView(parentView: (NavigationModel.sharedInstance.homeControl?.view!)!)
                NetworkManager.sharedInstance.manageAPIRequestToDownloadImage(requestType: .ServiceTypeGetImage, requestString: imageId, delegate: self)
            }
        }
        else{
            AlertControl.sharedInstance.presentAlertView(alertTitle: "Kindly check your internet connectivity", alertMessage: "", delegate: nil)
        }
        completion()
    }
    
    //Downloads the image and load in scroll view
    func downloadImage() {
        let imageId = "\(itemDataModel.imageArray.object(at: loopCount))"
        if NetworkManager.sharedInstance.isReachable(){
            _ = NetworkManager.sharedInstance.checkTypeOfNetwork()
            if NetworkManager.sharedInstance.isReachableViaWiFi || NetworkManager.sharedInstance.isReachableViaCellular{
                loopCount += 1
                LoadingView.shareInstance.showLodingView(parentView: (NavigationModel.sharedInstance.homeControl?.view!)!)
                NetworkManager.sharedInstance.manageAPIRequestToDownloadImage(requestType: .ServiceTypeGetImage, requestString: imageId, delegate: self)
            }
        }
        else{
            AlertControl.sharedInstance.presentAlertView(alertTitle: "Kindly check your internet connectivity", alertMessage: "", delegate: nil)
        }
    }
    
    //Loads the downloaded images in the scroll view.
    func loadScrollViweImages()  {
        //self.imgScrollView.frame = CGRect(x:0, y:0, width:self.view.frame.width, height:self.view.frame.height)
        // let scrollViewWidth:CGFloat = self.imgScrollView.frame.width
        let scrollViewWidth:CGFloat = self.imgScrollView.frame.width - 32
        let scrollViewHeight:CGFloat = self.imgScrollView.frame.height - 20
        let scrollViewContentWidth:CGFloat = self.imgScrollView.frame.width //359
        
        let xAxis: CGFloat = 16.0
        //2
        
        for i in 0..<carouselImageArray.count {
            let image = UIImageView(frame: CGRect(x:xAxis+(scrollViewContentWidth*CGFloat(i)), y:10,width:scrollViewWidth , height:scrollViewHeight))
            image.image = carouselImageArray.object(at: i) as? UIImage
            image.contentMode = UIViewContentMode.scaleAspectFit
            self.imgScrollView.addSubview(image)
        }
        
        
        
        //3
        /*let imgOne = UIImageView(frame: CGRect(x:16, y:10,width:scrollViewWidth , height:scrollViewHeight))
        imgOne.image = UIImage(named: "Medi1.jpg")
        imgOne.contentMode = UIViewContentMode.scaleAspectFit;
        let imgTwo = UIImageView(frame: CGRect(x:scrollViewContentWidth+16, y:10,width:scrollViewWidth, height:scrollViewHeight))
        imgTwo.image = UIImage(named: "Medi2.jpg")
        imgOne.contentMode = UIViewContentMode.scaleAspectFit;
        let imgThree = UIImageView(frame: CGRect(x:(scrollViewContentWidth*2)+16, y:10,width:scrollViewWidth, height:scrollViewHeight))
        imgThree.image = UIImage(named: "Medi3.jpg")
        imgOne.contentMode = UIViewContentMode.scaleAspectFit;
        let imgFour = UIImageView(frame: CGRect(x:(scrollViewContentWidth*3)+16, y:10,width:scrollViewWidth, height:scrollViewHeight))
        imgFour.image = UIImage(named: "Medi4.jpg")
        imgOne.contentMode = UIViewContentMode.scaleAspectFit;
        self.imgScrollView.addSubview(imgOne)
        self.imgScrollView.addSubview(imgTwo)
        self.imgScrollView.addSubview(imgThree)
        self.imgScrollView.addSubview(imgFour)*/
        //4
        self.imgScrollView.contentSize = CGSize(width:scrollViewContentWidth * CGFloat(carouselImageArray.count), height:self.imgScrollView.frame.height)
        self.imgScrollView.delegate = self
        self.imgPageControl.numberOfPages = carouselImageArray.count
        self.imgPageControl.currentPage = 0
        
    }
    
    
    func scrollViewDidEndDecelerating(_ scrollView: UIScrollView){
        // Test the offset and calculate the current page after scrolling ends
        let pageWidth:CGFloat = self.view.frame.width-16
        let currentPage:CGFloat = floor((scrollView.contentOffset.x-pageWidth/2)/pageWidth)+1
        // Change the indicator
        self.imgPageControl.currentPage = Int(currentPage);
        let slideToX = self.imgScrollView.frame.width * (currentPage)
        self.imgScrollView.scrollRectToVisible(CGRect(x:slideToX, y:0, width:pageWidth, height:self.imgScrollView.frame.height), animated: true)
        // Change the text accordingly
        if Int(currentPage) == 0{
            print ("I write mobile tutorials mainly targeting iOS")
        }else if Int(currentPage) == 1{
            print ("I write mobile tutorials mainly targeting iOS")
        }else if Int(currentPage) == 2{
            print("And sometimes I write games tutorials about Unity")
            
        }else{
            print("Keep visiting sweettutos.com for new coming tutorials, and don't forget to subscribe to be notified by email :")
            
            
        }
    }
    
    
    //Reloads the table view to get the height.
    func reloadtableViewafterDelay(){
        //  self.itemDesTable!.beginUpdates()
        self.baseTblViewHeight.constant = 10
        //itemDesTable!.reloadData();
        
        //  self.itemDesTable!.endUpdates()
    }
    
    //Loads the rating view.
    func loadStarRating(value: Float)  {
        var btgregr: UIButton?
        for buttonVi in ratingView.subviews{
            if buttonVi.isKind(of: UIButton.self){
                btgregr = buttonVi as? UIButton
                btgregr?.setImage(UIImage.init(imageLiteralResourceName: "No_Rating.png"), for: UIControlState.normal)
                //  btgregr?.isSelected = false;
                let buttonFlo = Float(buttonVi.tag-99)
                if buttonFlo <= value{
                    btgregr?.setImage(UIImage.init(imageLiteralResourceName: "Full_Rating.png"), for: UIControlState.normal)
                }
                else if(buttonFlo > value  && buttonFlo < value+1){
                    btgregr?.setImage(UIImage.init(imageLiteralResourceName: "Half_Rating.png"), for: UIControlState.normal)
                }
            }
        }    }
    
    
    //MARK: - IBAction functions.
    
    //Display the dropdown to select the drug quantity.
    @IBAction func dropdownClicked(sender: UIButton) {
        let dropDownData = NSMutableArray()
        var dropDownQty = itemDataModel.availableQuantity!+1
        
        if itemDataModel.isInCart! {
            dropDownQty = itemDataModel.availableQuantity!-itemDataModel.quantityInCart!
        }
        
        for i in 1..<dropDownQty {
            let model = DropDownModel()
            model.modelIdString = "\(i)" as NSString
            model.modelDataString = "\(i)" as NSString
            dropDownData.add(model)
        }
        DropDownView.defaultDropDownControl.showDropDownForData(dropDownData,DropDownType.SingleSelectionDefault, CGRect.zero, (NavigationModel.sharedInstance.homeControl?.view)!, self as DropDownViewDelegate, [])
        DropDownView.defaultDropDownControl.dropDownListSearch?.isHidden = true
    }
    
    //On clicking on wishlisht button.
    @IBAction func onWishlistIconClicked(sender: UIButton) {
        if sender.isSelected {
            self.removeFromWishlist()
//            sender.isSelected = false
        }
        else {
            self.addToWishlist()
//            sender.isSelected = true
        }
    }
    
    //Removes the email ids and phone numbers in the given string
    func removePhoneNumberAndEmailId(sellerNotes: String) -> String {
        //        let result = self.validate(value: "313-450-5258")
        
        //        let string = "Good morning, 313-450-5258 \n Good morning, +34627a217s154 \n Good morning, 627 11 71 54  313 450 5258  "
        var string = sellerNotes as NSString
//        var string:NSString = "Sampel seller notes string 3134505258 to test value nuvivicky@gmail.com +1 313 450 5258  +34627217154 htc@matchrx.com, aldkf, lkjdfs@gmail.net, ram@yahoo.co.in"
        
        do {
            let detector = try NSDataDetector(types: NSTextCheckingResult.CheckingType.phoneNumber.rawValue)
            //            let matchArray = detector.matches(in: string as String, options: NSRegularExpression.MatchingOptions(rawValue: 0), range: NSMakeRange(0, string.length))
            let numberOfMatches = detector.numberOfMatches(in: string as String, range: NSMakeRange(0, string.length))
            print(numberOfMatches) // 3
            
            for _ in 0..<numberOfMatches {
                let matchArray = detector.matches(in: string as String, options: NSRegularExpression.MatchingOptions(rawValue: 0), range: NSMakeRange(0, string.length))
                if matchArray.count > 0{
                    let matchRange = matchArray[0].range
                    string = string.replacingCharacters(in: matchRange, with: "") as NSString
                }
            }
        } catch {
            print(error)
        }
        
        //        var results = [String]()
        
        let emailRegex = "[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,6}"
        //        let nsText = string
        do {
            let regExp = try NSRegularExpression(pattern: emailRegex, options: NSRegularExpression.Options.caseInsensitive)
            //            let matches = regExp.matches(in: string as String, options: .reportProgress, range: NSMakeRange(0, string.length))
            let numberOfEmails = regExp.numberOfMatches(in: string as String, options: .reportProgress, range: NSMakeRange(0, string.length))
            
            for _ in 0..<numberOfEmails {
                let matchArray = regExp.matches(in: string as String, options: .reportProgress, range: NSMakeRange(0, string.length))
                if matchArray.count > 0{
                    let matchRange = matchArray[0].range
                    string = string.replacingCharacters(in: matchRange, with: "") as NSString
                }
            }
            
            //            for match in matches {
            //                let matchRange = match.range
            //                results.append(nsText.substring(with: matchRange))
            //            }
            
        } catch _ {
        }
        
        print(string)
        return string as String
    }
    
    //Creates the service to add to wishlist
    func addToWishlist() {
        if NetworkManager.sharedInstance.isReachable(){
            _ = NetworkManager.sharedInstance.checkTypeOfNetwork()
            if NetworkManager.sharedInstance.isReachableViaWiFi || NetworkManager.sharedInstance.isReachableViaCellular{
                let ndcNumber = itemDataModel.ndcNumber?.replacingOccurrences(of: "-", with: "")
                let ndcDict = NSMutableDictionary.init()
                ndcDict.setValue(ndcNumber, forKey: "ndc_number")
                /*let dataArray = NSMutableArray.init(object: ndcDict)
                let wishlistDelete = NSMutableDictionary.init()
                wishlistDelete.setValue(dataArray, forKey: "wishlistdelete")
                wishlistDelete.setValue(false, forKey: "deletedall")
                let requestData = NSMutableDictionary.init()
                requestData.setValue(wishlistDelete, forKey: "data")*/
                LoadingView.shareInstance.showLodingView(parentView: self.view.window!)
                NetworkManager.sharedInstance.manageAPIRequest(requestType: .ServiceTypeAddToWishlist, requestString: RequestDataModel.sharedInstance.makeRequestToDeleteFromWishlist(requestDict: ndcDict ), delegate: self)
            }
        }
        else{
            AlertControl.sharedInstance.presentAlertView(alertTitle: "Kindly check your internet connectivity", alertMessage: "", delegate: nil)
        }
    }
    
    //Creates the service to remove from wishlist
    func removeFromWishlist() {
        if NetworkManager.sharedInstance.isReachable(){
            _ = NetworkManager.sharedInstance.checkTypeOfNetwork()
            if NetworkManager.sharedInstance.isReachableViaWiFi || NetworkManager.sharedInstance.isReachableViaCellular{
                let ndcNumber = itemDataModel.ndcNumber?.replacingOccurrences(of: "-", with: "")
                let ndcDict = NSMutableDictionary.init()
                ndcDict.setValue(ndcNumber, forKey: "ndc_number")
                let dataArray = NSMutableArray.init(object: ndcDict)
                let wishlistDelete = NSMutableDictionary.init()
                wishlistDelete.setValue(dataArray, forKey: "wishlistdelete")
                wishlistDelete.setValue(false, forKey: "deletedall")
                let requestData = NSMutableDictionary.init()
                requestData.setValue(wishlistDelete, forKey: "data")
                LoadingView.shareInstance.showLodingView(parentView: self.view.window!)
                NetworkManager.sharedInstance.manageAPIRequest(requestType: .ServiceTypeDeleteFromWishlist, requestString: RequestDataModel.sharedInstance.makeRequestToDeleteFromWishlist(requestDict: requestData ), delegate: self)
            }
        }
        else{
            AlertControl.sharedInstance.presentAlertView(alertTitle: "Kindly check your internet connectivity", alertMessage: "", delegate: nil)
        }
    }
    
    //Expand collapse the table view.
    @IBAction func sectionNameWasTouched(_ sender: UIButton)  {
        let headerView:UIView = sender.superview!
        
        let section    = sender.tag
        var imgView: UIImageView?
        for view in headerView.subviews {
//            view.removeFromSuperview()
            if(view.tag == section){
                if view.isKind(of: UIImageView.self) {
                    imgView = view as? UIImageView
                }
            }
        }
        
        var indexesPath = [IndexPath]()
        
        if (selectedSection.contains(section)){
            for i in 0 ..< SectionItems[section].count {
                let index = IndexPath(row: i, section: section)
                indexesPath.append(index)
                if let cell:UITableViewCell = self.itemDesTable.cellForRow(at: index)
                {
                    self.baseTblViewHeight.constant = self.baseTblViewHeight.constant - cell.bounds.height
                }
            }
            imgView?.image =  UIImage(named: "B_Down.png")
            selectedSection.remove(section)
            
            
            self.itemDesTable!.beginUpdates()
            self.itemDesTable.deleteRows(at: indexesPath, with: UITableViewRowAnimation.fade)
            //self.itemDesTable!.reloadRows(at: indexesPath, with: .none)
            
            
            self.itemDesTable!.endUpdates()
            
            
        }
        else{
            for i in 0 ..< SectionItems[section].count {
                let index = IndexPath(row: i, section: section)
                indexesPath.append(index)
            }
            selectedSection.add(section);
            imgView?.image =  UIImage(named: "B_Up.png")
            // self.expandedSectionHeaderNumber = section
            self.itemDesTable!.beginUpdates()
            self.itemDesTable!.insertRows(at: indexesPath, with: UITableViewRowAnimation.fade)
            self.itemDesTable!.endUpdates()
        }
        self.view.layoutIfNeeded()
    }
    
    
    //Initializes the compare price view on clicking the compare price button.
    @IBAction func comparePrice(_ sender: UIButton) {
        
        let storyboard = UIStoryboard(name: "Secondary", bundle: nil)
        compareListViCtrl = storyboard.instantiateViewController(withIdentifier: "compareTableCtrl") as! CompareListViewController
        NavigationModel.sharedInstance.homeControl?.contentNavController.pushViewController(compareListViCtrl, animated: true)
        NavigationModel.sharedInstance.homeControl?.showBackIconWithTitle(title: "COMPARE PRICE")
        GlobalData.sharedInstance.comparePricePostId = itemDataModel.postId
    }
    
    //Prepares network connection to add the item in cart.
    @IBAction func addCartAltBtn(_ sender: UIButton) {
        if itemDataModel.isMyPosting! {
            let storyboard = UIStoryboard(name: "Main", bundle: nil)
            let editPostControl = storyboard.instantiateViewController(withIdentifier: "editPostControl") as! EditPostingControl
            NavigationModel.sharedInstance.homeControl?.contentNavController.pushViewController(editPostControl, animated: true)
            NavigationModel.sharedInstance.homeControl?.showBackIconWithTitle(title: "EDIT POSTING")
            editPostControl.parentDelegate = self
            editPostControl.postId = "\(itemDataModel.postId!)"
        }
        else {
            if NetworkManager.sharedInstance.isReachable(){
                _ = NetworkManager.sharedInstance.checkTypeOfNetwork()
                if NetworkManager.sharedInstance.isReachableViaWiFi || NetworkManager.sharedInstance.isReachableViaCellular{
                    let cartDict = NSMutableDictionary.init()
                    cartDict.setValue(postId, forKey: "post_id")
                    cartDict.setValue(qtyLabel.text, forKey: "quantity")
                    cartDict.setValue(20, forKey: "per_page")
                    LoadingView.shareInstance.showLodingView(parentView: (NavigationModel.sharedInstance.homeControl?.view!)!)
                    NetworkManager.sharedInstance.manageAPIRequest(requestType: .ServiceTypeAddToCart, requestString: RequestDataModel.sharedInstance.makeRequestAddItemInCart(requestDict: cartDict), delegate: self)
                }
            }
            else{
                AlertControl.sharedInstance.presentAlertView(alertTitle: "Kindly check your internet connectivity", alertMessage: "", delegate: nil)
            }
        }
    }
    
    //Provides a pop up to display alert
    @IBAction func estDeliverHelpClk(_ sender: Any) {
        let alertt = UIAlertController(title: "Other conditions could impact the delivery date", message: "", preferredStyle: UIAlertControllerStyle.alert)
        alertt.addAction(UIAlertAction(title: "OK", style: .cancel, handler: nil))
        self.present(alertt, animated: true, completion: nil)
    }
    
    //Provides a pop up to display alert
    @IBAction func shipProHelpClk(_ sender: Any) {
        var shippingMessage = ""
        let shippingAmount = Int(itemDataModel.shippingHandlingAmount!)
        if shippingAmount == 50 {
            shippingMessage = "Free shipping on items $50 or more"
        }
        else if shippingAmount == 200 {
            shippingMessage = "Free shipping on orders $200 or more"
        }
        let alertt = UIAlertController(title: shippingMessage, message: "", preferredStyle: UIAlertControllerStyle.alert)
        alertt.addAction(UIAlertAction(title: "OK", style: .cancel, handler: nil))
        self.present(alertt, animated: true, completion: nil)
    }
    
    //Provides a dropdown to display the quantity.
    @IBAction func quantityDropdown(_ sender: UIButton) {
        self.view.endEditing(true)
        let dropDownData = NSMutableArray()
        var dropDownQty = itemDataModel.availableQuantity!
        
        if itemDataModel.isInCart! {
            dropDownQty = itemDataModel.availableQuantity!-itemDataModel.quantityInCart!
        }
        for i in 1..<dropDownQty+1 {
            let model = DropDownModel()
            model.modelIdString = "\(i)" as NSString
            model.modelDataString = "\(i)" as NSString
            dropDownData.add(model)
        }
        DropDownView.defaultDropDownControl.showDropDownForData(dropDownData,DropDownType.SingleSelectionDefault, CGRect.zero, (NavigationModel.sharedInstance.homeControl?.view)!, self as DropDownViewDelegate, [])
        //Call Webservice and get the maximum
        
    }
    
    //Prompts an alert to display the ingredients
    func showIngredients() {
        let alertt = UIAlertController(title: "Mulitple Ingredients", message: itemDataModel.ingredients, preferredStyle: UIAlertControllerStyle.alert)
        alertt.addAction(UIAlertAction(title: "OK", style: .cancel, handler: nil))
        let paragraphStyle = NSMutableParagraphStyle()
        paragraphStyle.alignment = NSTextAlignment.left
        
        let messageText = NSMutableAttributedString(
            string: itemDataModel.ingredients!,
            attributes: [
                NSParagraphStyleAttributeName: paragraphStyle,
                NSFontAttributeName: UIFont.systemFont(ofSize: 13.0)
            ]
        )
        
        alertt.setValue(messageText, forKey: "attributedMessage")
//        alertt.setValue("Multiple Ingredients", forKey: "attributedTitle")
        self.present(alertt, animated: true, completion: nil)
    }
    
    
    
    
    //MARK: - Tableview delegate functions.
    
    //Returns the number of sections displayed in the table view.
    func numberOfSections(in tableView: UITableView) -> Int {
        return sectionnames.count;
    }
    
    //Returns the number of rows to be declared in the section.
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        for expSection in selectedSection {
            let  i = expSection as? Int
            if(i == section){
                
                return SectionItems[section].count;
                
            }
            
        }
        return 0;
    }
    
    //Height of the tableview gets adjusted programatically here.
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        
        self.baseTblViewHeight.constant = self.baseTblViewHeight.constant + cell.bounds.height;
    }
    
    //Prepares the cell to be displayed.
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "reUsedIdDetail", for: indexPath) as! ItemDetailTableViewCell ;
        
        cell.ingredientsBtn.isHidden = true
//        cell.ingredientBtnWidth.constant = 0.0
        let setcion = SectionItems[indexPath.section] as Array;
        // cell.textLabel?.text = setcion[indexPath.row] as String;
        if (indexPath.section < 3){
            cell.titleLbl.text = itemTitle[indexPath.section][indexPath.row]
            cell.detailLbl.text = setcion[indexPath.row]
            if cell.titleLbl.text == "Strength" && itemDataModel.isMultiIngredients! {
                cell.ingredientsBtn.isHidden = false
//                cell.ingredientBtnWidth.constant = 20.0
                cell.ingredientsBtn.addTarget(self, action: #selector(self.showIngredients), for: .touchUpInside)
            }
            else {
                cell.ingredientsBtn.isHidden = true
//                cell.ingredientBtnWidth.constant = 0.0
            }
            
            //cell.layoutIfNeeded()
            return cell;
        }
        else{
            cell.tblCellwidth.constant = 0
            cell.detailLblLeadingConstant.constant = 0
            cell.titleLbl.text = ""
            let cellNormal  = tableView.dequeueReusableCell(withIdentifier: "CellNormal", for: indexPath)
            cellNormal.textLabel?.text = setcion[indexPath.row]
            cellNormal.textLabel?.font = UIFont(name: "Roboto-Medium", size: 12.0)
            cellNormal.textLabel?.textColor = UIColor(red: 33/255, green: 33/255, blue: 33/255, alpha: 1.0)
            cellNormal.textLabel?.lineBreakMode = .byWordWrapping
            cellNormal.textLabel?.numberOfLines = 0
            cellNormal.textLabel?.sizeToFit()
            //cell.titleLbl.text = ""
            return cellNormal
        }
    }
    
    //Returns the title to be displayed for each header.
    func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        return sectionnames[section] as String;
    }
    
    //Returns the height of the header
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return TABLEVIEW_SECTION_HEIGHT;
    }
    
    //Initialization of the header view
    func tableView(_ tableView: UITableView, willDisplayHeaderView view: UIView, forSection section: Int) {
        let header: UITableViewHeaderFooterView = view as! UITableViewHeaderFooterView
        header.contentView.backgroundColor = UIColor.init(red: 240/255, green: 240/255, blue: 240/255, alpha: 1.0); //UIColor.colorWithHexString(hexStr: "#408000")
        header.textLabel?.textColor = UIColor.darkGray;
        header.textLabel?.backgroundColor = UIColor.clear
        header.textLabel?.font = UIFont(name: "Roboto-Medium", size: 12.0) //UIFont.fontNames(forFamilyName: "Roboto-Medium");
        
        
        
        
        let theImageView:UIImageView = UIImageView(frame: CGRect(x: view.frame.width - 32, y: 5, width: 20, height: 20));
        theImageView.image = UIImage(named: "B_Down.png")
        theImageView.tag = section   ;
        
        for view in header.subviews {
            if(view.tag == section){
                if view.isKind(of: UIImageView.self) {
                    view.removeFromSuperview()
                }
            }
        }
        
        header.addSubview(theImageView)
        
        let buttonTap:UIButton = UIButton(frame: CGRect(x: 0, y: 0, width: view.frame.width, height: view.frame.height))
        buttonTap.tag = section;
        buttonTap.addTarget(self, action: #selector(sectionNameWasTouched(_:)), for: UIControlEvents.touchUpInside)
        // buttonTap.addSubview(theImageView)
        header.addSubview(buttonTap)
        
        buttonTap.layer.borderWidth = 2.0;
        buttonTap.layer.borderColor = UIColor.white.cgColor
        
    }
    
    
    
    //MARK: - Edit posting delegate call.
    func reloadListData() {
        NavigationModel.sharedInstance.homeControl?.onBackClicked(sender: UIButton.init())
        self.parentDelegate.reloadBuyListFromItemDetail()
    }
    
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
     // Get the new view controller using segue.destinationViewController.
     // Pass the selected object to the new view controller.
     }
     */
    
    //MARK: - Drop down delegate functions.
    func selectedDropDownData(_ dropDownDataObject: DropDownModel) {
        qtyLabel.text = dropDownDataObject.modelDataString as String?
        DropDownView.defaultDropDownControl.hideDropDown()
    }
    
    
    func selectedDropDownDataArray(_ dropDownDataArray: NSMutableArray) {
        
        print("")
    }
    
    func selectedDropDownDataString(_ selectedDropDownDataString: NSString) {
        
        print("")
    }
    
    //MARK: - Network manager delegate functions.
    func webServiceResponse(for requestType: ServiceRequestType, responseData Data: Any!, withStatusCode statusCode: Int, errorMessage message: Any!) {
        if message as? Error != nil {
            LoadingView.shareInstance.hideLoadingView()
//            LoadingView.shareInstance.hideWhiteView()
            AlertControl.sharedInstance.presentAlertView(alertTitle: "Something went wrong", alertMessage: "", delegate: nil)
            if itemDataModel != nil && (requestType == .ServiceTypeGetImage && itemDataModel.imageArray.count > 1 && loopCount == 1) {
                myGroup.leave()
            }
        }
        else{
            if !NetworkManager.sharedInstance.checkStatusCode(statusCode){
                LoadingView.shareInstance.hideLoadingView()
//                LoadingView.shareInstance.hideWhiteView()
                let error = NetworkManager.sharedInstance.getErrorModel(requestType: requestType, httpStatusCode: statusCode, responseData: Data)
                AlertControl.sharedInstance.presentAlertView(alertTitle: (error.errorMessage!), alertMessage: "", delegate: nil)
                if itemDataModel != nil && (requestType == .ServiceTypeGetImage && itemDataModel.imageArray.count > 1 && loopCount == 1) {
                    myGroup.leave()
                }
            }
            else{
                if requestType == .ServiceTypeFetchItemDetails {
                    itemDataModel = ResponseManager.sharedInstance.fetchItemDetails(responseData: Data) as! ItemDetailsDataModel
//                    LoadingView.shareInstance.hideWhiteView()
                    if itemDataModel.imageArray.count == 0 {
                        LoadingView.shareInstance.hideLoadingView()
//                        LoadingView.shareInstance.hideWhiteView()
                    }
                    self.bindDataWithView()
                }
                else if requestType == .ServiceTypeGetImage {
                    let imageData = Data as! Data
                    let image = UIImage(data: imageData)
//                    if let imageData = Data {
//                        let image = UIImage(data: imageData as! Data)
//                    }
                    if itemDataModel.imageArray.count == 1 {
                        LoadingView.shareInstance.hideLoadingView()
//                        LoadingView.shareInstance.hideWhiteView()
                        carouselImageArray.add(image!)
                        self.loadScrollViweImages()
                    }
                    else{
                        if loopCount == 1 {
//                            LoadingView.shareInstance.hideLoadingView()
//                            LoadingView.shareInstance.hideWhiteView()
                            carouselImageArray.add(image!)
                            myGroup.leave()
                        }
                        else {
                            LoadingView.shareInstance.hideLoadingView()
//                            LoadingView.shareInstance.hideWhiteView()
                            carouselImageArray.add(image!)
                            self.loadScrollViweImages()
                        }
                    }
                }
                else if requestType == .ServiceTypeAddToCart {
                    LoadingView.shareInstance.hideLoadingView()
                    GlobalData.sharedInstance.resetFilterDictionary()
                    GlobalData.sharedInstance.resetSortDictionary()
                    GlobalData.sharedInstance.searchText = ""
                    NavigationModel.sharedInstance.homeControl?.searchTxtField.text = ""
                    NavigationModel.sharedInstance.buyControl?.filterCountLbl.text = ""
                    let marketplaceData = ResponseManager.sharedInstance.getMarketPlaceListData(responseData: Data, requestType: requestType)
                     NavigationModel.sharedInstance.homeControl?.contentNavController.popToRootViewController(animated: true)
                     NavigationModel.sharedInstance.homeControl?.headerLabel.text = "BUY"
                     NavigationModel.sharedInstance.homeControl?.screenTitle = "BUY"
                    NavigationModel.sharedInstance.homeControl?.backView.isHidden = true
                    NavigationModel.sharedInstance.homeControl?.hamburgerView.isHidden = false
                     NavigationModel.sharedInstance.buyListControl?.loadResponseData(responseData: marketplaceData, requestType: requestType)
                    
                }
                else if requestType == .ServiceTypeDeleteFromWishlist {
                    LoadingView.shareInstance.hideLoadingView()
                    let wishlistData = ResponseManager.sharedInstance.deleteFromWishlist(responseData: Data)
                    AlertControl.sharedInstance.presentAlertView(alertTitle: wishlistData.message!, alertMessage: "", delegate: nil)
                    wishlistBtn.isSelected = false
                }
                else if requestType == .ServiceTypeAddToWishlist {
                    LoadingView.shareInstance.hideLoadingView()
                    let wishlistData = ResponseManager.sharedInstance.deleteFromWishlist(responseData: Data)
                    AlertControl.sharedInstance.presentAlertView(alertTitle: wishlistData.message!, alertMessage: "", delegate: nil)
                    wishlistBtn.isSelected = true
                }
            }
        }
    }
}

