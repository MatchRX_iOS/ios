//
//  LoginController.swift
//  MatchRX
//
//  Created by Vignesh on 11/13/17.
//  Copyright © 2017 HTC. All rights reserved.
//

import UIKit

class LoginController: UIViewController, UITextFieldDelegate, NetworkManagerDelegate {
    
    let myGroup = DispatchGroup()
    
    
    @IBOutlet var userName : UITextField!
    @IBOutlet var deaNumber: UITextField!
    @IBOutlet var password: UITextField!
    @IBOutlet var loginButton: UIButton!
    @IBOutlet var rememberButton: UIButton!
    @IBOutlet var logoImgView: UIImageView!
    @IBOutlet var serviceSegmentControl: UISegmentedControl!
    
    var loginModel: LoginDataModel?
    var loginResponseModel = LoginResponseModel()
    var selectedTextField: UITextField!
    
    //MARK: - Super class functions
    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationController?.isNavigationBarHidden = true
        let pressRecognizer = UITapGestureRecognizer.init(target: self, action: #selector(self.onLogoPressed(sender:)))
        pressRecognizer.numberOfTapsRequired = 4
        logoImgView.addGestureRecognizer(pressRecognizer)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.getLoginCredentials()
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    //MARK: - Instance functions
    
    //To change the service
    @IBAction func onSegmentValueChanged(sender: UISegmentedControl) {
        if sender.selectedSegmentIndex == 0 {
            NetworkManager.sharedInstance.webserviceURL = NetworkManager.sharedInstance.qaEnvirionmentURL
        }
        else if sender.selectedSegmentIndex == 1{
            NetworkManager.sharedInstance.webserviceURL = NetworkManager.sharedInstance.stagingURL
        }
        else {
            NetworkManager.sharedInstance.webserviceURL = NetworkManager.sharedInstance.devEnvironmentURL
        }
    }
    
    //To show hide the service type selector segment control.
    func onLogoPressed(sender: UIGestureRecognizer){
        if serviceSegmentControl.isHidden {
            serviceSegmentControl.isHidden = false
        }
        else {
            serviceSegmentControl.isHidden = true
        }
    }
    
    //Login to the application if the provided credentials are valid
    @IBAction func loginClicked(){
        if (selectedTextField != nil) {
            selectedTextField.resignFirstResponder()
        }
        loginModel = LoginDataModel()
        loginModel?.deaNumber = deaNumber.text
        loginModel?.userName = userName.text
        loginModel?.password = password.text
//        loginModel?.password = "Welcome@123"
        if !isValidDEANumber(deaNumber: (loginModel?.deaNumber)!) {
//            self.displayAlertControl(alertTitle: "Please enter a valid DEA number", alertMessage: "")
            AlertControl.sharedInstance.presentAlertView(alertTitle: "Please provide a valid DEA number", alertMessage: "", delegate: nil)
            return
        }
        if !isValidUserName(userNameText: (loginModel?.userName)!) {
//            self.displayAlertControl(alertTitle: "Please enter a valid username", alertMessage: "")
            AlertControl.sharedInstance.presentAlertView(alertTitle: "Please provide a valid username", alertMessage: "", delegate: nil)
            return
        }
        if !isValidPassword(passwordText: (loginModel?.password)!) {
//            self.displayAlertControl(alertTitle: "Please provide a valid password", alertMessage: "")
            AlertControl.sharedInstance.presentAlertView(alertTitle: "Please provide a valid password", alertMessage: "", delegate: nil)
            return
        }
        
        if NetworkManager.sharedInstance.isReachable(){
            _ = NetworkManager.sharedInstance.checkTypeOfNetwork()
            if NetworkManager.sharedInstance.isReachableViaWiFi || NetworkManager.sharedInstance.isReachableViaCellular{
                LoadingView.shareInstance.showLodingView(parentView: self.view.window!)
                NetworkManager.sharedInstance.manageAPIRequest(requestType: .ServiceTypeLogin, requestString: RequestDataModel.sharedInstance.createRequestForLogin(loginModel: self.loginModel!), delegate: self)
            }
        }
        else{
            AlertControl.sharedInstance.presentAlertView(alertTitle: "Kindly check your internet connectivity", alertMessage: "", delegate: nil)
        }
        
        
        /*myGroup.enter()
        if NetworkManager.sharedInstance.isReachable(){
            _ = NetworkManager.sharedInstance.checkTypeOfNetwork()
            if NetworkManager.sharedInstance.isReachableViaWiFi || NetworkManager.sharedInstance.isReachableViaCellular{
                LoadingView.shareInstance.showLodingView(parentView: self.view.window!)
                NetworkManager.sharedInstance.manageAPIRequest(requestType: .ServiceTypeLogin, requestString: RequestDataModel.sharedInstance.createRequestForLogin(loginModel: self.loginModel!), delegate: self)
            }
        }
        else{
            AlertControl.sharedInstance.presentAlertView(alertTitle: "Kindly check your internet connectivity", alertMessage: "", delegate: nil)
        }
        myGroup.notify(queue: DispatchQueue.main) {
            if NetworkManager.sharedInstance.isReachable(){
                _ = NetworkManager.sharedInstance.checkTypeOfNetwork()
                if NetworkManager.sharedInstance.isReachableViaWiFi || NetworkManager.sharedInstance.isReachableViaCellular{
                    LoadingView.shareInstance.showLodingView(parentView: self.view.window!)
                    NetworkManager.sharedInstance.manageAPIRequest(requestType: .ServiceTypeLOVDetails, requestString: "", delegate: self)
                }
            }
            else{
                AlertControl.sharedInstance.presentAlertView(alertTitle: "Kindly check your internet connectivity", alertMessage: "", delegate: nil)
            }
        }*/
        
    }
    
    func fetchShippingDetails () {
        if NetworkManager.sharedInstance.isReachable(){
            _ = NetworkManager.sharedInstance.checkTypeOfNetwork()
            if NetworkManager.sharedInstance.isReachableViaWiFi || NetworkManager.sharedInstance.isReachableViaCellular{
                LoadingView.shareInstance.showLodingView(parentView: self.view.window!)
                NetworkManager.sharedInstance.manageAPIRequest(requestType: .ServiceTypeLOVDetails, requestString: "", delegate: self)
            }
        }
        else{
            AlertControl.sharedInstance.presentAlertView(alertTitle: "Kindly check your internet connectivity", alertMessage: "", delegate: nil)
        }
    }
    
    //Pop up a static message about Username
    @IBAction func infoIconClicked(){
        let messageTitle = "If you do not have a username, enter first initial and lastname"
        let messageSubTitle = "(eg. John Smith's username is jsmith)"
        let alertView = UIAlertController(title: messageTitle, message: messageSubTitle, preferredStyle: UIAlertControllerStyle.alert)
        alertView.setValue(NSAttributedString(string: messageTitle, attributes: [NSFontAttributeName : UIFont(name: "Helvetica-Bold", size: 15) as Any,NSForegroundColorAttributeName : UIColor.black]), forKey: "attributedTitle")
        alertView.addAction(UIAlertAction(title: "OK", style: UIAlertActionStyle.cancel, handler:nil))
        self.present(alertView, animated: true, completion: nil)
    }
    
    //Checks to remember the credentials
    @IBAction func rememberMeClicked(sender: UIButton){
        if sender.isSelected{
            print("true")
            sender.isSelected = false
        }
        else{
            print("false")
            sender.isSelected = true
        }
    }
    
    //Validates the entered DEA Number
    func isValidDEANumber(deaNumber: String) -> Bool{
        if deaNumber.count == 9 {
            let firstCharacter = deaNumber.prefix(1)
            let numbers = deaNumber.index(deaNumber.startIndex, offsetBy: 2)
            if (firstCharacter.rangeOfCharacter(from: CharacterSet.init(charactersIn: "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ")) == nil){
                return false
            }
            else if (deaNumber.suffix(from: numbers).rangeOfCharacter(from: CharacterSet.init(charactersIn: "0123456789").inverted) != nil){
                return false
            }
            else{
                let index = deaNumber.index(after: deaNumber.startIndex)
                let secondCharacter = String(deaNumber[index])
                if (secondCharacter.rangeOfCharacter(from: CharacterSet.init(charactersIn: "0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ")) == nil){
                    return false
                }
            }
        }
        else{
            return false
        }
        return true
    }
    
    //Validates the entered Username
    func isValidUserName(userNameText: String) -> Bool{
        if userNameText.count <= 4 {
            return false
        }
        else{
            if (userNameText.prefix(1).rangeOfCharacter(from: CharacterSet.init(charactersIn: "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ")) == nil){
                return false
            }
            let set = CharacterSet(charactersIn: "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789.-_")
            if userNameText.rangeOfCharacter(from: set.inverted) != nil {
                return false
            }
        }
        return true
    }
    
    //Validates the entered Password
    func isValidPassword(passwordText: String) -> Bool {
//        if passwordText.count == 0 {
//            return false
//        }
        if passwordText.trimmingCharacters(in: CharacterSet.whitespaces).count == 0 {
            return false
        }
//        if (passwordText.rangeOfCharacter(from: CharacterSet.init(charactersIn: " $")) != nil){
//            return false
//        }
        return true
    }
    
    //Present a AlertController as pop up for validation
    func displayAlertControl(alertTitle: String, alertMessage: String) {
        let alertControl = UIAlertController(title: alertTitle, message: alertMessage, preferredStyle: UIAlertControllerStyle.alert)
        alertControl.addAction(UIAlertAction(title:"OK", style: UIAlertActionStyle.cancel, handler: nil))
        self.present(alertControl, animated: true, completion: nil)
    }
    
    //Remembers the DEA number and Username. Save those data in NSUserDefaults
    func rememberLoginCredentialsAction(){
        let defaults = UserDefaults.standard
        if rememberButton.isSelected {
            defaults.set(deaNumber.text, forKey: "deaNumber")
            defaults.set(userName.text, forKey: "userName")
        }
        else{
            if defaults.object(forKey: "deaNumber") != nil && defaults.object(forKey: "userName") != nil {
                if defaults.object(forKey: "deaNumber") as? String == deaNumber.text && defaults.object(forKey: "userName") as? String == userName.text{
                    defaults.removeObject(forKey: "deaNumber")
                    defaults.removeObject(forKey: "userName")
                }
            }
        }
        defaults.set(GlobalData.sharedInstance.refreshToken, forKey: "refreshToken")
        defaults.synchronize()
    }
    
    //Retrives the Login credentials and auto populate in appropriate fileds.
    func getLoginCredentials(){
        deaNumber.text = ""
        userName.text = ""
        password.text = ""
//        password.text = "Test@123"
        let defaults = UserDefaults.standard
        if (defaults.value(forKey: "deaNumber") != nil) {
            deaNumber.text = defaults.value(forKey: "deaNumber") as? String
            userName.text = defaults.value(forKey: "userName") as? String
            self.rememberButton.isSelected = true
        }
    }
    
    
    //MARK: - Textfield delegate functions implementation
    
    func textFieldDidBeginEditing(_ textField: UITextField) {
        selectedTextField = textField
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        let characterLength: Int?
        if textField == deaNumber{
            characterLength = 9
        }
        else if textField == userName {
            characterLength = 30
        }
        else{
            characterLength = 20
        }
        if characterLength! > 0 && !string.isEmpty
        {
            let text = "\(textField.text ?? "")\(string)"
//            if text.count >= characterLength!{
//                let index = text.index(text.startIndex, offsetBy: characterLength!)
//                textField.text = text.substring(to: index)
//                return false
//            }
            if text.count >= characterLength!
            {
                if string.count > 1{
//                    let text = "\(textField.text ?? "")\(string)"
                    let index = text.index(text.startIndex, offsetBy: characterLength!)
                    textField.text = text.substring(to: index)
                    return false
                }
                let currentString: NSString = textField.text! as NSString
                let newString: NSString =
                    currentString.replacingCharacters(in: range, with: string) as NSString
//                let flag = newString.length <= characterLength!
                return newString.length <= characterLength!
            }
        }
        return true
    }

    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        
        textField.resignFirstResponder()
        return true
    }
    
    //MARK: - API call response delegate from Network Manager
    //Receives the response
    func webServiceResponse(for requestType: ServiceRequestType, responseData Data: Any!, withStatusCode statusCode: Int, errorMessage message: Any!) {
        LoadingView.shareInstance.hideLoadingView()
        if message as? Error != nil {
            AlertControl.sharedInstance.presentAlertView(alertTitle: "Something went wrong", alertMessage: "", delegate: self)
        }
        else{
            if !NetworkManager.sharedInstance.checkStatusCode(statusCode){
                let error = NetworkManager.sharedInstance.getErrorModel(requestType: requestType, httpStatusCode: statusCode, responseData: Data)
                AlertControl.sharedInstance.presentAlertView(alertTitle: error.errorMessage!, alertMessage: "", delegate: self)
            }
            else{
                if requestType == .ServiceTypeLogin {
//                    myGroup.leave()
                    loginResponseModel = ResponseManager.sharedInstance.getLoginResponseData(responseData: Data)
                    print("Initiating the home control in login control")
                    GlobalData.sharedInstance.userId = loginResponseModel.userId
                    GlobalData.sharedInstance.accessToken = loginResponseModel.accessToken
                    GlobalData.sharedInstance.refreshToken = loginResponseModel.refreshToken
                    GlobalData.sharedInstance.pharmacyId = loginResponseModel.pharmacyId
                    GlobalData.sharedInstance.deaNumber = loginResponseModel.pharmacyDEA
                    GlobalData.sharedInstance.landingPage = loginResponseModel.landingPage
                    GlobalData.sharedInstance.salesInProgressCount = loginResponseModel.salesInProgressCount
                    GlobalData.sharedInstance.purchaseInProgressCount = loginResponseModel.purchaseInProgressCount
                    GlobalData.sharedInstance.stateWiseRestrictionMessage = loginResponseModel.stateWiseRestrictionMessage
                    GlobalData.sharedInstance.stateWiseRestriction = loginResponseModel.stateWiseRestriction
                    GlobalData.sharedInstance.cartCount = loginResponseModel.cartCount
                    
                    self.fetchShippingDetails()
                }
                else if requestType == .ServiceTypeLOVDetails {
                    GlobalData.sharedInstance.lovDataModel = ResponseManager.sharedInstance.getLOVDetails(responseData: Data)
                    self.rememberLoginCredentialsAction()
                    let storyboard = UIStoryboard(name: "Main", bundle: nil)
                    let homeControl = storyboard.instantiateViewController(withIdentifier: "homeControl") as! HomeControl
                    homeControl.loginResponseModel = loginResponseModel
                    self.navigationController?.present(homeControl, animated: true, completion: nil)
                }
            }
        }
    }
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
