//
//  FilterController.swift
//  MatchRX
//
//  Created by Vignesh on 3/13/18.
//  Copyright © 2018 HTC. All rights reserved.
//

import UIKit

class FilterController: UIViewController {
    
    @IBOutlet weak var resetButton: UIButton!
    
    var filterDictionary: NSMutableDictionary = NSMutableDictionary.init()
    var filterValueDictionary: NSMutableDictionary = NSMutableDictionary.init()
    var filterKeyDictionary: NSMutableDictionary = NSMutableDictionary.init()
    var isResetClicked: Bool = false
    
    let RESET_SELECTED_COLOR = UIColor(red: 255.0/255.0, green: 136.0/255.0, blue: 0.0/255.0, alpha: 1.0)
    let RESET_DEFAULT_COLOR = UIColor(red: 166.0/255.0, green: 166.0/255.0, blue: 166.0/255.0, alpha: 1.0)

    override func viewDidLoad() {
        super.viewDidLoad()
        resetButton.layer.borderWidth = 1.0
        resetButton.setTitleColor(RESET_DEFAULT_COLOR, for: .normal)
        resetButton.layer.borderColor = RESET_DEFAULT_COLOR.cgColor
        filterKeyDictionary.setValue("package_option", forKey: "1")
        filterKeyDictionary.setValue("expire_date", forKey: "2")
        filterKeyDictionary.setValue("wac_discount", forKey: "3")
        filterKeyDictionary.setValue("brand", forKey: "4")
        filterKeyDictionary.setValue("created_at", forKey: "5")
        filterKeyDictionary.setValue("original_package", forKey: "6")
        
        
        filterValueDictionary.setValue("A", forKey: "100")
        filterValueDictionary.setValue("F", forKey: "101")
        filterValueDictionary.setValue("P", forKey: "102")
        filterValueDictionary.setValue("A", forKey: "200")
        filterValueDictionary.setValue("1", forKey: "201")
        filterValueDictionary.setValue("3", forKey: "202")
        filterValueDictionary.setValue("6", forKey: "203")
        filterValueDictionary.setValue("A", forKey: "300")
        filterValueDictionary.setValue("25", forKey: "301")
        filterValueDictionary.setValue("50", forKey: "302")
        filterValueDictionary.setValue("75", forKey: "303")
        filterValueDictionary.setValue("A", forKey: "400")
        filterValueDictionary.setValue("B", forKey: "401")
        filterValueDictionary.setValue("G", forKey: "402")
        filterValueDictionary.setValue("A", forKey: "500")
        filterValueDictionary.setValue("1", forKey: "501")
        filterValueDictionary.setValue("3", forKey: "502")
        filterValueDictionary.setValue("7", forKey: "503")
        filterValueDictionary.setValue("A", forKey: "600")
        filterValueDictionary.setValue("sealed", forKey: "601")
        filterValueDictionary.setValue("nonsealed", forKey: "602")
        self.loadFilters()
        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    

    //MARK: - Load the filter buttons.
    
    //Loads the applied filters
    func loadFilters() {
        if isResetClicked {
            filterDictionary = GlobalData.sharedInstance.defaultFilters()
        }
        else {
            filterDictionary = GlobalData.sharedInstance.filterDictionary
        }
        let keyArray = filterDictionary.allKeys
        for i in 0..<keyArray.count {
            let key: Array = filterKeyDictionary.allKeys(for: keyArray[i] as! String)
            let value: String = filterDictionary.object(forKey: keyArray[i] as! String) as! String
            let startIndex = Int(key[0] as! String)
            var maxCount = 3
            if startIndex == 2 || startIndex == 3 || startIndex == 5 {
                maxCount = 4
            }
            for i in 0..<maxCount {
                let index = startIndex!*100+i
                if filterValueDictionary.object(forKey: String(index)) as! String == value {
                    (self.view.viewWithTag(startIndex!*100+i) as! UIButton).isSelected = true
                    if index%100 != 0 {
                        resetButton.isSelected = true
                        resetButton.setTitleColor(RESET_SELECTED_COLOR, for: .normal)
                        resetButton.layer.borderColor = RESET_SELECTED_COLOR.cgColor
                    }
                }
                else {
                    (self.view.viewWithTag(startIndex!*100+i) as! UIButton).isSelected = false
                }
            }
        }
    }
    
    //Resets the filters to default.
    func resetFilters() {
        let keyArray = filterKeyDictionary.allKeys
        for i in 0..<keyArray.count {
            var maxCount = 3
            let key = keyArray[i]
            let startIndex = Int(key as! String)
            if startIndex == 2 || startIndex == 3 || startIndex == 5 {
                maxCount = 4
            }
            for j in 0..<maxCount {
                if j == 0 {
                    (self.view.viewWithTag(startIndex!*100+j) as! UIButton).isSelected = true
                }
                else {
                    (self.view.viewWithTag(startIndex!*100+j) as! UIButton).isSelected = false
                }
            }
        }
        resetButton.isSelected = false
        resetButton.setTitleColor(RESET_DEFAULT_COLOR, for: .normal)
        resetButton.layer.borderColor = RESET_DEFAULT_COLOR.cgColor
    }
    
    //MARK: - IBAction functions
    
    //Resets the applied filter to default.
    @IBAction func onResetClicked(sender: UIButton) {
//        self.resetFilters()
        isResetClicked = true
        resetButton.setTitleColor(RESET_DEFAULT_COLOR, for: .normal)
        resetButton.layer.borderColor = RESET_DEFAULT_COLOR.cgColor
        
        self.loadFilters()
        resetButton.isSelected = false
       
    }
    
    @IBAction func onApplyFilterClicked(sender: UIButton) {
        if isResetClicked {
            filterDictionary = GlobalData.sharedInstance.resetFilterDictionary()
            GlobalData.sharedInstance.filterCount = 0
        }
        else {
            GlobalData.sharedInstance.filterDictionary = filterDictionary
        }
        let valueArray = filterDictionary.allValues
        var count: Int = 0
        for i in 0..<valueArray.count {
            if valueArray[i] as! String != "A" {
                count += 1
            }
        }
        GlobalData.sharedInstance.filterCount = count
        self.dismiss(animated: true, completion: nil)
        NavigationModel.sharedInstance.buyListControl?.reloadListData()
        if count > 0 {
            NavigationModel.sharedInstance.buyControl?.filterCountLbl.text = "(\(count))"
        }
        else {
            NavigationModel.sharedInstance.buyControl?.filterCountLbl.text = ""
        }
    }
    
    //Selects the applied filter and deselects the other ones.
    @IBAction func onFilterItemsClicked(sender: UIButton) {
        resetButton.isSelected = true
        resetButton.setTitleColor(RESET_SELECTED_COLOR, for: .selected)
        resetButton.layer.borderColor = RESET_SELECTED_COLOR.cgColor
        var maxCount = 3
        let startIndex = sender.tag/100
        if startIndex == 2 || startIndex == 3 || startIndex == 5 {
            maxCount = 4
        }
        for i in 0..<maxCount {
            (self.view.viewWithTag(startIndex*100+i) as! UIButton).isSelected = false
        }
        sender.isSelected = true
        filterDictionary.setValue(filterValueDictionary.object(forKey: String(sender.tag)), forKey: filterKeyDictionary.object(forKey: String(startIndex)) as! String)
    }
    
    //Dismiss the filter controller.
    @IBAction func onBackClicked(sender: UIButton) {
        self.dismiss(animated: true, completion: nil)
    }
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
