//
//  BuyerConfirmationControl.swift
//  MatchRX
//
//  Created by Vignesh on 5/17/18.
//  Copyright © 2018 HTC. All rights reserved.
//

import UIKit

class BuyerConfirmationControl: UIViewController, UITableViewDelegate, UITableViewDataSource, NetworkManagerDelegate, URLSessionDelegate {
    
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var listView: UIView!
    @IBOutlet weak var editedView: UIView!
    @IBOutlet weak var orderID_DateLbl: UILabel!
    @IBOutlet weak var confirmStatusBtn: UIButton!
    @IBOutlet weak var confirmOrderBtn: UIButton!
    @IBOutlet weak var nextButton: UIButton!
    @IBOutlet var oneLbl: UILabel!
    @IBOutlet var twoLbl: UILabel!
    @IBOutlet var wizardTick1Img: UIImageView!
    @IBOutlet var wizardTick2Img: UIImageView!
    @IBOutlet weak var avail_shippingMethodLbl: UILabel!
    @IBOutlet weak var edit_itemTotalLbl: UILabel!
    @IBOutlet weak var edit_shippingLbl: UILabel!
    @IBOutlet weak var edit_debitTotalLbl: UILabel!
    @IBOutlet weak var edit_SubmitBtn: UIButton!
    @IBOutlet weak var submit_MessageView: UIView!
    @IBOutlet weak var submit_CancellationView: UIView!
    @IBOutlet weak var submit_ReviseView: UIView!
    @IBOutlet weak var estimatedDeliveryDateLbl: UILabel!
    @IBOutlet weak var groundRadio: UIImageView!
    @IBOutlet weak var TwoDayRadioImg: UIImageView!
    @IBOutlet weak var priorityRadio: UIImageView!
    @IBOutlet weak var groundTitleLbl: UILabel!
    @IBOutlet weak var groundDeliveryLbl: UILabel!
    @IBOutlet weak var twoDayTitleLbl: UILabel!
    @IBOutlet weak var twoDayDeliveryLbl: UILabel!
    @IBOutlet weak var priorityTitleLbl: UILabel!
    @IBOutlet weak var priorityDeliveryLbl: UILabel!
    @IBOutlet weak var groundBtn: UIButton!
    @IBOutlet weak var twoDayBtn: UIButton!
    @IBOutlet weak var priorityBtn: UIButton!
    @IBOutlet weak var shippingMethodView: UIView!
    @IBOutlet weak var selectShippingMethodView: UIView!
    @IBOutlet weak var totalView: UIView!
    
    @IBOutlet weak var totalViewLeadingConstraint: NSLayoutConstraint!
    
    var dataArray: NSMutableArray = NSMutableArray.init()
    var orderId: String?
    var selectedIndex: Int?
    var submitStatus: String?
    var groundShippingCost: Double = 0.00
    var twoDayShippingCost: Double = 0.00
    var overnightShippingcost: Double = 0.00
    var orderDetailsDataModel: OrderDetailsDataModel?
    var confirmOrderType: OrderedItemStatus?
    
    var imageCache = NSCache<AnyObject, UIImage>()
    var operationQueue = OperationQueue()
    var operationListArray = Array<String>()
    
    let DEFAULT_WIZARD_BG_COLOR = UIColor(red: 238.0/255.0, green: 241.0/255.0, blue: 243.0/255.0, alpha: 1.0)
    let SELECTED_WIZARD_BG_COLOR = UIColor(red: 246.0/255.0, green: 208.0/255.0, blue: 181.0/255.0, alpha: 1.0)
    let DEFAULT_WIZARD_LABEL_BG_COLOR = UIColor.clear
    let SELECTED_WIZARD_LABEL_BG_COLOR = UIColor(red: 255.0/255.0, green: 142.0/255.0, blue: 0.0/255.0, alpha: 1.0)
    let DEFAULT_WIZARD_LABEL_TEXT_COLOR = UIColor(red: 164.0/255.0, green: 164.0/255.0, blue: 164.0/255.0, alpha: 1.0)
    let SELECTED_WIZARD_LABEL_TEXT_COLOR = UIColor(red: 255.0/255.0, green: 255.0/255.0, blue: 255.0/255.0, alpha: 1.0)
    let DEFAULT_WIZARD_LABEL_BORDER_COLOR = UIColor(red: 164.0/255.0, green: 164.0/255.0, blue: 164.0/255.0, alpha: 1.0).cgColor
    let SELECTED_WIZARD_LABEL_BORDER_COLOR = UIColor.clear.cgColor
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.view.bringSubview(toFront: listView)
        oneLbl.layer.cornerRadius = oneLbl.frame.size.width/2.0
        twoLbl.layer.cornerRadius = twoLbl.frame.size.width/2.0
        oneLbl.layer.borderColor = SELECTED_WIZARD_LABEL_BORDER_COLOR
        twoLbl.layer.borderColor = DEFAULT_WIZARD_LABEL_BORDER_COLOR
        twoLbl.layer.borderWidth = 1.0
        NavigationModel.sharedInstance.homeControl?.hamburgerView.isHidden = true
//        NavigationModel.sharedInstance.homeControl?.backView.isHidden = true
        NavigationModel.sharedInstance.homeControl?.headerMenuView.isHidden = true
        
        self.fetchOrderDetails()
        // Do any additional setup after loading the view.
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    //MARK: - Instance functions.
    
    //Prepares request to fetch the data from service.
    func fetchOrderDetails() {
        if NetworkManager.sharedInstance.isReachable(){
            _ = NetworkManager.sharedInstance.checkTypeOfNetwork()
            if NetworkManager.sharedInstance.isReachableViaWiFi || NetworkManager.sharedInstance.isReachableViaCellular{
                LoadingView.shareInstance.showLodingView(parentView: (NavigationModel.sharedInstance.homeControl?.view!)!)
                NetworkManager.sharedInstance.manageAPIRequest(requestType: .ServiceTypeOrderDetails, requestString: orderId!, delegate: self)
            }
        }
        else{
            AlertControl.sharedInstance.presentAlertView(alertTitle: "Kindly check your internet connectivity", alertMessage: "", delegate: nil)
        }
    }
    
    //Serialises the data fetched from service.
    func loadWithSerialisedData() {
        var shippingCost = ""
        
        
        orderID_DateLbl.text = "Order# \(orderDetailsDataModel?.orderNumber ?? "") | Order Date \(orderDetailsDataModel?.orderDate ?? "")"
//        estimatedDeliveryDateLbl.text = orderDetailsDataModel.
        if orderDetailsDataModel?.shippingMethods.object(forKey: "Ground") != nil {
            let shippingDetails = orderDetailsDataModel?.shippingMethods.object(forKey: "Ground") as! CartShippingDetailsDataModel
            if shippingDetails.isSelected! {
                avail_shippingMethodLbl.text = shippingDetails.shippingId
                estimatedDeliveryDateLbl.text = shippingDetails.deliveredBy
                groundBtn.isSelected = true
                self.shipingMethodSelctd(groundBtn)
                if shippingDetails.shippingPaidBy == "buyer" {
                    shippingCost = "\(shippingDetails.shippingCost!)"
                }
                else {
                    shippingCost = "Free"
                }
            }
            groundTitleLbl.text = shippingDetails.shippingName
            groundDeliveryLbl.text = shippingDetails.deliveryDetails
            groundShippingCost = shippingDetails.shippingCost!
        }
        if orderDetailsDataModel?.shippingMethods.object(forKey: "2-Day") != nil {
            let shippingDetails = orderDetailsDataModel?.shippingMethods.object(forKey: "2-Day") as! CartShippingDetailsDataModel
            if shippingDetails.isSelected! {
                avail_shippingMethodLbl.text = shippingDetails.shippingId
                estimatedDeliveryDateLbl.text = shippingDetails.deliveredBy
                twoDayBtn.isSelected = true
                self.shipingMethodSelctd(twoDayBtn)
                if shippingDetails.shippingPaidBy == "buyer" {
                    shippingCost = "\(shippingDetails.shippingCost!)"
                }
                else {
                    shippingCost = "Free"
                }
            }
            twoDayTitleLbl.text = shippingDetails.shippingName
            twoDayDeliveryLbl.text = shippingDetails.deliveryDetails
            twoDayShippingCost = shippingDetails.shippingCost!
        }
        if orderDetailsDataModel?.shippingMethods.object(forKey: "Overnight") != nil {
            let shippingDetails = orderDetailsDataModel?.shippingMethods.object(forKey: "Overnight") as! CartShippingDetailsDataModel
            if shippingDetails.isSelected! {
                avail_shippingMethodLbl.text = shippingDetails.shippingId
                estimatedDeliveryDateLbl.text = shippingDetails.deliveredBy
                priorityBtn.isSelected = true
                self.shipingMethodSelctd(priorityBtn)
                if shippingDetails.shippingPaidBy == "buyer" {
                    shippingCost = "\(shippingDetails.shippingCost!)"
                }
                else {
                    shippingCost = "Free"
                }
            }
            priorityTitleLbl.text = shippingDetails.shippingName
            priorityDeliveryLbl.text = shippingDetails.deliveryDetails
            overnightShippingcost = shippingDetails.shippingCost!
        }
        dataArray.removeAllObjects()
        dataArray.addObjects(from: orderDetailsDataModel?.orderItemsArray as! [Any])
        tableView.reloadData()
        nextButton.isUserInteractionEnabled = true
        nextButton.alpha = 1.0
        confirmOrderBtn.isUserInteractionEnabled = true
        confirmStatusBtn.isUserInteractionEnabled = true
    }
    
    
    
    //Downloads the image from URL
    func downloadImage(itemModel: OrderDetailsDataModel) {
        let config = URLSessionConfiguration.default
        let session = URLSession(configuration: config, delegate: self, delegateQueue: nil)
        let getImageTask = session.downloadTask(with: URL(string: itemModel.medispanImageURL!)!) { (url, response, error) in
            if url != nil{
                let data = try? Data(contentsOf: url!)
                let downloadedImage  = UIImage(data: data!)
                if (downloadedImage != nil) {
                    self.imageCache.setObject(downloadedImage!, forKey: itemModel.orderId!
                        as AnyObject)
                }
                self.tableView.performSelector(onMainThread: #selector(UITableView.reloadData), with: nil, waitUntilDone: false)
            }
            
        }
        getImageTask.resume()
    }
    
    @IBAction func shipingMethodSelctd(_ sender: UIButton) {
        var itemTotal = 0.00
        var shippingCharge = 0.00
        groundBtn.isSelected = false
        twoDayBtn.isSelected = false
        priorityBtn.isSelected = false
        groundRadio.image = UIImage(named: "Radio off.png")
        TwoDayRadioImg.image = UIImage(named: "Radio off.png")
        priorityRadio.image = UIImage(named: "Radio off.png")
        var shippingModel: CartShippingDetailsDataModel?
        
        
        for i in 0..<dataArray.count {
            let orderData = dataArray.object(at: i) as! OrderDetailsDataModel
            itemTotal += orderData.modifiedPrice!
        }
        
        switch sender.tag {
        case 500:
            groundRadio.image = UIImage(named: "Radio on.png")
            groundBtn.isSelected = true
            shippingModel = orderDetailsDataModel?.shippingMethods.object(forKey: "Ground") as? CartShippingDetailsDataModel
            if groundShippingCost != 0.00 {
                let userCharge = groundShippingCost+(orderDetailsDataModel?.userCalculatedShippingCharge)!
                edit_shippingLbl.text = "$"+String(format:"%.2f", arguments:[userCharge])
            }
            else {
                edit_shippingLbl.text = "Free"
            }
            break
            
        case 501:
            TwoDayRadioImg.image = UIImage(named: "Radio on.png")
            twoDayBtn.isSelected = true
            shippingModel = orderDetailsDataModel?.shippingMethods.object(forKey: "2-Day") as? CartShippingDetailsDataModel
            if twoDayShippingCost != 0.00 {
                let userCharge = twoDayShippingCost+(orderDetailsDataModel?.userCalculatedShippingCharge)!
                edit_shippingLbl.text = "$"+String(format:"%.2f", arguments:[userCharge])
            }
            else {
                edit_shippingLbl.text = "Free"
            }
            break
            
        case 502:
            priorityRadio.image = UIImage(named: "Radio on.png")
            priorityBtn.isSelected = true
            shippingModel = orderDetailsDataModel?.shippingMethods.object(forKey: "Overnight") as? CartShippingDetailsDataModel
            if overnightShippingcost != 0.00 {
                let userCharge = overnightShippingcost+(orderDetailsDataModel?.userCalculatedShippingCharge)!
                edit_shippingLbl.text = "$"+String(format:"%.2f", arguments:[userCharge])
            }
            else {
                edit_shippingLbl.text = "Free"
            }
            break
            
        default:
            groundRadio.image = UIImage(named: "Radio off.png")
            groundBtn.isSelected = true
            break
        }
        if (edit_shippingLbl.text?.contains("$"))! {
            shippingCharge = Double((edit_shippingLbl.text?.trimmingCharacters(in: CharacterSet.init(charactersIn: "$")))!)!
        }
//        let shippingCharge = Double((edit_shippingLbl.text?.trimmingCharacters(in: CharacterSet.init(charactersIn: "$")))!)
        let creditTotal = itemTotal+shippingCharge
        edit_debitTotalLbl.text = "$"+String(format: "%.2f", arguments:[creditTotal])
    }
    
    //On Next button clicked will decide which view to be displayed.
    @IBAction func onNextButtonClicked(sender: UIButton) {
        self.onHeaderButtonClicked(sender: confirmOrderBtn)
    }
    
    //Navigates to Buy marketplace
    @IBAction func onContinueShoppingClicked(sender: UIButton) {
        NavigationModel.sharedInstance.homeControl?.contentNavController.popToRootViewController(animated: true)
        NavigationModel.sharedInstance.homeControl?.headerLabel.text = "BUY"
        NavigationModel.sharedInstance.homeControl?.screenTitle = "BUY"
        NavigationModel.sharedInstance.homeControl?.headerMenuView.isHidden = false
        NavigationModel.sharedInstance.homeControl?.hamburgerView.isHidden = false
        NavigationModel.sharedInstance.homeControl?.backView.isHidden = true
        NavigationModel.sharedInstance.homeControl?.showBuyMenu()
    }
    
    //Navigates to My Orders
    @IBAction func onGoToMyOrdersClicked(sender: UIButton) {
        NavigationModel.sharedInstance.homeControl?.contentNavController.popToRootViewController(animated: true)
        NavigationModel.sharedInstance.homeControl?.headerLabel.text = "MY ORDERS"
        NavigationModel.sharedInstance.homeControl?.screenTitle = "MY ORDERS"
        NavigationModel.sharedInstance.homeControl?.headerMenuView.isHidden = false
        NavigationModel.sharedInstance.homeControl?.hamburgerView.isHidden = false
        NavigationModel.sharedInstance.homeControl?.backView.isHidden = true
        NavigationModel.sharedInstance.homeControl?.showMyOrders()
    }
    
    //Submits the order
    @IBAction func onSubmitOrderClicked(sender: UIButton) {
        
        let dataDict = NSMutableDictionary.init()
        dataDict.setValue("purchase", forKey: "type")
        dataDict.setValue(orderDetailsDataModel?.globalOrderId, forKey: "order_id")
        dataDict.setValue(submitStatus, forKey: "status")
        
        if submitStatus?.lowercased() == "pending seller confirmation" {
            if totalViewLeadingConstraint.constant == 100 {
                dataDict.setValue(avail_shippingMethodLbl.text, forKey: "shipping_method")
            }
            else {
                var shippingDetails: CartShippingDetailsDataModel!
                if groundBtn.isSelected {
                    shippingDetails  = orderDetailsDataModel?.shippingMethods.object(forKey: "Ground") as! CartShippingDetailsDataModel
//                    dataDict.setValue(groundTitleLbl.text, forKey: "shipping_method")
                }
                else if twoDayBtn.isSelected {
                    shippingDetails  = orderDetailsDataModel?.shippingMethods.object(forKey: "2-Day") as! CartShippingDetailsDataModel
//                    dataDict.setValue(twoDayTitleLbl.text, forKey: "shipping_method")
                }
                else if priorityBtn.isSelected {
                    shippingDetails  = orderDetailsDataModel?.shippingMethods.object(forKey: "Overnight") as! CartShippingDetailsDataModel
//                    dataDict.setValue(priorityTitleLbl.text, forKey: "shipping_method")
                }
                dataDict.setValue(shippingDetails.shippingId, forKey: "shipping_method")
            }
        }
        
        let orderArray = NSMutableArray.init()
        for i in 0..<dataArray.count {
            let orderDataDict = NSMutableDictionary.init()
            let orderData = dataArray.object(at: i) as! OrderDetailsDataModel
            
            orderDataDict.setValue(orderData.orderId, forKey: "id")
            
            if orderData.itemStatus == OrderedItemStatus.Accept {
                orderDataDict.setValue("Accepted", forKey: "confirmation_status")
                if orderData.packageOption?.lowercased() == "f" {
                    orderDataDict.setValue(orderData.modifiedQuantity, forKey: "quantity")
                }
                else {
                    orderDataDict.setValue(1, forKey: "quantity")
                    orderDataDict.setValue(orderData.modifiedPackageSize, forKey: "package_size")
                }
            }
            else if orderData.itemStatus == OrderedItemStatus.Reject {
                orderDataDict.setValue("Rejected", forKey: "confirmation_status")
                orderDataDict.setValue(0, forKey: "quantity")
            }
            else if orderData.itemStatus == OrderedItemStatus.OutOfStock {
                orderDataDict.setValue("Outofstock", forKey: "confirmation_status")
                orderDataDict.setValue(0, forKey: "quantity")
            }
            orderArray.add(orderDataDict)
        }
        dataDict.setValue(orderArray, forKey: "orderitems")
        
       /* if confirmOrderType == OrderedItemStatus.Accept {
            
            
        }
        else if confirmOrderType == OrderedItemStatus.Reject {
            let orderArray = NSMutableArray.init()
            for i in 0..<dataArray.count {
                let orderDataDict = NSMutableDictionary.init()
                let orderData = dataArray.object(at: i) as! OrderDetailsDataModel
                
                orderDataDict.setValue(orderData.orderId, forKey: "id")
                
                if orderData.itemStatus == OrderedItemStatus.Accept {
                    orderDataDict.setValue("Accepted", forKey: "confirmation_status")
                    orderDataDict.setValue(orderData.modifiedQuantity, forKey: "quantity")
                }
                else if orderData.itemStatus == OrderedItemStatus.Reject {
                    orderDataDict.setValue("Rejected", forKey: "confirmation_status")
                    orderDataDict.setValue(0, forKey: "quantity")
                }
                else if orderData.itemStatus == OrderedItemStatus.OutOfStock {
                    orderDataDict.setValue("Outofstock", forKey: "confirmation_status")
                    orderDataDict.setValue(0, forKey: "quantity")
                }
                
                /*if orderData.packageOption?.lowercased() == "f" {
                    orderDataDict.setValue(orderData.modifiedQuantity, forKey: "quantity")
                }
                else {
                    orderDataDict.setValue(1, forKey: "quantity")
                    orderDataDict.setValue(orderData.modifiedPackageSize, forKey: "package_size")
                }*/
                orderArray.add(orderDataDict)
            }
            dataDict.setValue(orderArray, forKey: "orderitems")
        }
        else {
            dataDict.setValue("Canceled", forKey: "status")
        }*/
        if NetworkManager.sharedInstance.isReachable(){
            _ = NetworkManager.sharedInstance.checkTypeOfNetwork()
            if NetworkManager.sharedInstance.isReachableViaWiFi || NetworkManager.sharedInstance.isReachableViaCellular{
                LoadingView.shareInstance.showLodingView(parentView: (NavigationModel.sharedInstance.homeControl?.view!)!)
                NetworkManager.sharedInstance.manageAPIRequest(requestType: .ServiceTypeSellerConfirmation, requestString: RequestDataModel.sharedInstance.makeRequestForSellerConfirmation(requestDict: dataDict), delegate: self)
            }
        }
        else{
            AlertControl.sharedInstance.presentAlertView(alertTitle: "Kindly check your internet connectivity", alertMessage: "", delegate: nil)
        }
    }
    
    
    @IBAction func onHeaderButtonClicked(sender: UIButton) {
        if !sender.isSelected {
            confirmStatusBtn.isSelected = false
            confirmOrderBtn.isSelected = false
            if sender == confirmStatusBtn {
                oneLbl.backgroundColor = SELECTED_WIZARD_LABEL_BG_COLOR
                twoLbl.backgroundColor = DEFAULT_WIZARD_LABEL_BG_COLOR
                oneLbl.textColor = SELECTED_WIZARD_LABEL_TEXT_COLOR
                twoLbl.textColor = DEFAULT_WIZARD_LABEL_TEXT_COLOR
                oneLbl.layer.borderColor = SELECTED_WIZARD_LABEL_BORDER_COLOR
                twoLbl.layer.borderColor = DEFAULT_WIZARD_LABEL_BORDER_COLOR
                confirmStatusBtn.backgroundColor = SELECTED_WIZARD_BG_COLOR
                confirmOrderBtn.backgroundColor = DEFAULT_WIZARD_BG_COLOR
                wizardTick2Img.isHidden = false
                wizardTick1Img.isHidden = true
                self.view.bringSubview(toFront: listView)
            }
            else {
                oneLbl.backgroundColor = DEFAULT_WIZARD_LABEL_BG_COLOR
                twoLbl.backgroundColor = SELECTED_WIZARD_LABEL_BG_COLOR
                oneLbl.textColor = DEFAULT_WIZARD_LABEL_TEXT_COLOR
                twoLbl.textColor = SELECTED_WIZARD_LABEL_TEXT_COLOR
                oneLbl.layer.borderColor = DEFAULT_WIZARD_LABEL_BORDER_COLOR
                twoLbl.layer.borderColor = SELECTED_WIZARD_LABEL_BORDER_COLOR
                confirmStatusBtn.backgroundColor = DEFAULT_WIZARD_BG_COLOR
                confirmOrderBtn.backgroundColor = SELECTED_WIZARD_BG_COLOR
                wizardTick1Img.isHidden = false
                wizardTick2Img.isHidden = true
                self.showConfirmOrderView()
            }
            sender.isSelected = true
        }
        sender.backgroundColor = SELECTED_WIZARD_BG_COLOR
    }
    
    //Decides which confirm order view has to be displayed.
    func showConfirmOrderView() {
        var acceptCount = 0
        var rejectCount = 0
        var totalRefrigeratedCount = 0
        var currentRefrigeratedCount = 0
        var itemTotal = 0.00
        
        for i in 0..<dataArray.count {
            let dataModel = dataArray.object(at: i) as! OrderDetailsDataModel
            if dataModel.itemStatus == OrderedItemStatus.Accept {
                acceptCount += 1
                if dataModel.storageCondition?.lowercased() != "n" {
                    currentRefrigeratedCount += 1
                }
            }
            else if dataModel.itemStatus == OrderedItemStatus.Reject {
                rejectCount += 1
            }
            if dataModel.storageCondition?.lowercased() != "n" {
                totalRefrigeratedCount += 1
            }
        }
        
        
        self.view.bringSubview(toFront: editedView)
        if totalRefrigeratedCount > 0 {
            if currentRefrigeratedCount > 0 {
                editedView.bringSubview(toFront: shippingMethodView)
                editedView.sendSubview(toBack: selectShippingMethodView)
                totalViewLeadingConstraint.constant = 100.0
            }
            else {
                editedView.bringSubview(toFront: selectShippingMethodView)
                totalViewLeadingConstraint.constant = 115.0
                
            }
        }
        else {
            editedView.bringSubview(toFront: shippingMethodView)
            editedView.sendSubview(toBack: selectShippingMethodView)
            totalViewLeadingConstraint.constant = 100.0
        }
        self.view.layoutIfNeeded()
        for i in 0..<dataArray.count {
            let orderData = dataArray.object(at: i) as! OrderDetailsDataModel
            itemTotal += orderData.modifiedPrice!
        }
        if acceptCount == dataArray.count {
            confirmOrderType = OrderedItemStatus.Accept
//            self.view.bringSubview(toFront: editedView)
            edit_itemTotalLbl.text = "$"+String(format:"%.2f", arguments:[itemTotal])
            edit_shippingLbl.text = "$"+String(format:"%.2f", arguments:[(orderDetailsDataModel?.shippingCharge)!])
            let shippingCharge = Double((edit_shippingLbl.text?.trimmingCharacters(in: CharacterSet.init(charactersIn: "$")))!)
            let creditTotal = itemTotal+shippingCharge!
            edit_debitTotalLbl.text = "$"+String(format: "%.2f", arguments:[creditTotal])
            submitStatus = "Pending Seller Confirmation"
            edit_SubmitBtn.setTitle("Submit Order", for: .normal)
        }
        else if rejectCount == dataArray.count  || acceptCount == 0 {
            //            self.view.bringSubview(toFront: outOfStockView)
            confirmOrderType = OrderedItemStatus.Canceled
//            self.view.bringSubview(toFront: editedView)
            edit_itemTotalLbl.text = "$0.00"
            edit_shippingLbl.text = "$0.00"
            edit_debitTotalLbl.text = "$0.00"
            submitStatus = "Canceled"
            edit_SubmitBtn.setTitle("Cancel Order", for: .normal)
            totalViewLeadingConstraint.constant = 0.0
            editedView.bringSubview(toFront: totalView)
        }
        else {
            confirmOrderType = OrderedItemStatus.Reject
//            self.view.bringSubview(toFront: editedView)
            edit_itemTotalLbl.text = "$"+String(format:"%.2f", arguments:[itemTotal])
            edit_shippingLbl.text = "$"+String(format:"%.2f", arguments:[(orderDetailsDataModel?.shippingCharge)!])
            let shippingCharge = Double((edit_shippingLbl.text?.trimmingCharacters(in: CharacterSet.init(charactersIn: "$")))!)
            let creditTotal = itemTotal+shippingCharge!
            edit_debitTotalLbl.text = "$"+String(format: "%.2f", arguments:[creditTotal])
            submitStatus = "Pending Seller Confirmation"
            edit_SubmitBtn.setTitle("Submit Order", for: .normal)
        }
        if totalViewLeadingConstraint.constant == 115.0 {
            self.resetShippingCost()
        }
    }
    
    
    func resetShippingCost() {
        var orderTotal = 0.00
        var shippingCharge = 0.00
        let shippingLOVModel = GlobalData.sharedInstance.lovDataModel
        let shippingMethodArray = NSMutableArray.init(array: (shippingLOVModel?.shippingMethodsArray)!)
//        let shippingHandlingArray = NSMutableArray.init(array: (shippingLOVModel?.shippingHandlingArray)!)
        let groundShippingModel = orderDetailsDataModel?.shippingMethods.object(forKey: "Ground") as! CartShippingDetailsDataModel
        let twoDayShippingModel = orderDetailsDataModel?.shippingMethods.object(forKey: "2-Day") as! CartShippingDetailsDataModel
        let overnightShippingModel = orderDetailsDataModel?.shippingMethods.object(forKey: "Overnight") as! CartShippingDetailsDataModel
        var isGroundFree = false
        var isTwoDayFree = false
        var isOvernightFree = false
        
        
        for i in 0..<dataArray.count {
            let orderData = dataArray.object(at: i) as! OrderDetailsDataModel
            orderTotal += orderData.modifiedPrice!
        }
        
        for i in 0..<dataArray.count {
            let dataModel = dataArray.object(at: i) as! OrderDetailsDataModel
            if dataModel.itemStatus == OrderedItemStatus.Accept {
                if (dataModel.item_shippingHandlingName?.lowercased() == "sellerpays200over" && orderTotal >= 200.0) || (dataModel.item_shippingHandlingName?.lowercased() == "sellerpays50over" && dataModel.originalPrice! >= 50.0){
//                    let shippingType = dataModel.item_shippingMethod
//                    var shippingMethod = ""
                    for i in 0..<shippingMethodArray.count {
                        let shippingDetails = shippingMethodArray.object(at: i) as! ShippingLOVDataModel
                        if dataModel.item_shippingMethod?.lowercased() == shippingDetails.shippingMethodName?.lowercased() {
                            if shippingDetails.shippingMethodSortOrder == 1 {
                                isGroundFree = true
                            }
                            else if shippingDetails.shippingMethodSortOrder == 2 {
                                isTwoDayFree = true
                            }
                            else if shippingDetails.shippingMethodSortOrder == 3 {
                                isOvernightFree = true
                            }
                            /*for j in 0..<shippingHandlingArray.count {
                                let handlingDetails = shippingHandlingArray.object(at: j) as! ShippingLOVDataModel
                                if dataModel.item_shippingHandlingName?.lowercased() == handlingDetails.shippingHandlingName?.lowercased() {
                                    if handlingDetails.shippingHandlingSortOrder == 1 {
                                            isGroundFree = true
                                    }
                                    else if handlingDetails.shippingHandlingSortOrder == 2 {
                                        isTwoDayFree = true
                                    }
                                    else if handlingDetails.shippingHandlingSortOrder == 3 {
                                        isOvernightFree = true
                                    }
                                }
                            }*/
                        }
                    }
                }
            }
        }
        if isGroundFree {
            groundDeliveryLbl.text = "( Est. Delivery \(groundShippingModel.deliveredBy!)) - Free"
            groundShippingCost = 0.00
        }
        else {
            groundDeliveryLbl.text = "( Est. Delivery \(groundShippingModel.deliveredBy!)) - $\(String(format: "%.2f", arguments: [groundShippingModel.shippingCost!]) )"
            groundShippingCost = groundShippingModel.shippingCost!
        }
        if isTwoDayFree {
            twoDayDeliveryLbl.text = "( Est. Delivery \(twoDayShippingModel.deliveredBy!)) - Free"
            twoDayShippingCost = 0.00
        }
        else {
            twoDayDeliveryLbl.text = "( Est. Delivery \(twoDayShippingModel.deliveredBy!)) - $\(String(format: "%.2f", arguments: [twoDayShippingModel.shippingCost!]) )"
            twoDayShippingCost = twoDayShippingModel.shippingCost!
        }
        if isOvernightFree {
            priorityDeliveryLbl.text = "( Est. Delivery \(overnightShippingModel.deliveredBy!)) - Free"
            overnightShippingcost = 0.00
        }
        else {
            priorityDeliveryLbl.text = "( Est. Delivery \(overnightShippingModel.deliveredBy!)) - $\(String(format: "%.2f", arguments: [overnightShippingModel.shippingCost!]) )"
            overnightShippingcost = overnightShippingModel.shippingCost!
        }
        if groundBtn.isSelected {
            if groundShippingCost != 0.00 {
                let userCharge = groundShippingCost+(orderDetailsDataModel?.userCalculatedShippingCharge)!
                edit_shippingLbl.text = "$"+String(format:"%.2f", arguments:[userCharge])
            }
            else {
                edit_shippingLbl.text = "Free"
            }
        }
        else if twoDayBtn.isSelected {
            if twoDayShippingCost != 0.00 {
                let userCharge = twoDayShippingCost+(orderDetailsDataModel?.userCalculatedShippingCharge)!
                edit_shippingLbl.text = "$"+String(format:"%.2f", arguments:[userCharge])
            }
            else {
                edit_shippingLbl.text = "Free"
            }
        }
        else if priorityBtn.isSelected {
            if overnightShippingcost != 0.00 {
                let userCharge = overnightShippingcost+(orderDetailsDataModel?.userCalculatedShippingCharge)!
                edit_shippingLbl.text = "$"+String(format:"%.2f", arguments:[userCharge])
            }
            else {
                edit_shippingLbl.text = "Free"
            }
        }
        
        if (edit_shippingLbl.text?.contains("$"))! {
            shippingCharge = Double((edit_shippingLbl.text?.trimmingCharacters(in: CharacterSet.init(charactersIn: "$")))!)!
        }
        let creditTotal = orderTotal+shippingCharge
        edit_debitTotalLbl.text = "$"+String(format: "%.2f", arguments:[creditTotal])
    }
    
    //When order item status clicked
    func onItemStatusChanged(sender: UIButton) {
        let orderData = dataArray.object(at: sender.tag) as! OrderDetailsDataModel
        let cell = sender.superview?.superview?.superview?.superview as! BuyerConfirmationTableViewCell
        selectedIndex = sender.tag
        if !sender.isSelected {
            cell.acceptBtn.isSelected = false
            cell.rejectBtn.isSelected = false
            if sender == cell.acceptBtn {
                cell.acceptBtn.isSelected = true
                self.onAvailableClicked(dataModel: orderData, cell: cell)
            }
            else if sender == cell.rejectBtn {
                cell.rejectBtn.isSelected = true
//                self.reviseItemDetails(dataModel: orderData, selectedCell: cell)
                orderData.itemStatus = OrderedItemStatus.Reject
                if orderData.packageOption?.lowercased() == "f" {
                    cell.quantityLbl.text = "0"
                    orderData.modifiedQuantity = 0
                }
                else {
                    cell.partialLbl.text = "0"
                    orderData.modifiedPackageSize = 0
                }
                cell.priceLbl.text = "$0.00"
                orderData.modifiedPrice = 0
                dataArray.replaceObject(at: sender.tag, with: orderData)
            }
//            tableView.reloadData()
        }
    }
    
    func calculateCreditTotal() -> Double{
        var itemTotal = 0.00
        
        for i in 0..<dataArray.count {
            let orderData = dataArray.object(at: i) as! OrderDetailsDataModel
            itemTotal += orderData.modifiedPrice!
        }
        let debitTotal = itemTotal+(orderDetailsDataModel?.shippingCharge)!
        return debitTotal
    }
    
    //On clicking the available button all the original values has to be restored.
    func resetToOriginalValues() {
        
    }
    
    //On available button clicked.
    func onAvailableClicked(dataModel: OrderDetailsDataModel, cell: BuyerConfirmationTableViewCell) {
        dataModel.itemStatus = OrderedItemStatus.Accept
        dataModel.modifiedQuantity = dataModel.originalQuantity
        dataModel.modifiedPackageSize = dataModel.originalPackageSize
        
        cell.acceptBtn.isSelected = true
        cell.rejectBtn.isSelected = false
        if dataModel.packageOption?.lowercased() == "f" {
            dataModel.modifiedPrice = dataModel.originalPackPrice!*Double(dataModel.modifiedQuantity!)
            cell.quantityLbl.text = "\(dataModel.modifiedQuantity!)"
            cell.priceLbl.text = "$"+String(format:"%.2f", arguments:[dataModel.modifiedPrice!])
        }
        else {
            dataModel.modifiedPrice = dataModel.unitPrice!*Double(dataModel.modifiedPackageSize!)
            cell.partialLbl.text = "Partial: \(dataModel.modifiedPackageSize!)/\(dataModel.actualPackageSize!)"
            cell.priceLbl.text = "$"+String(format:"%.2f", arguments:[dataModel.modifiedPrice!])
        }
        dataArray.replaceObject(at: selectedIndex!, with: dataModel)
    }
    
    //Sets the selected item as out of stock
    func outOfStockClicked(dataModel: OrderDetailsDataModel, cell: SellerConfirmationTableViewCell) {
        dataModel.itemStatus = OrderedItemStatus.OutOfStock
        if dataModel.packageOption?.lowercased() == "f" {
            dataModel.modifiedPrice = 0.00
            dataModel.modifiedQuantity = 0
            cell.quantityLbl.text = "\(dataModel.modifiedQuantity!)"
            cell.priceLbl.text = "$"+String(format:"%.2f", arguments:[dataModel.modifiedPrice!])
        }
        else {
            dataModel.modifiedPrice = 0.00
            dataModel.modifiedPackageSize = 0
            cell.partialLbl.text = "Partial: \(dataModel.modifiedPackageSize!)/\(dataModel.actualPackageSize!)"
            cell.priceLbl.text = "$"+String(format:"%.2f", arguments:[dataModel.modifiedPrice!])
        }
        dataArray.replaceObject(at: selectedIndex!, with: dataModel)
    }
    
    
    
    //MARK: - Revise control delegate
    func revisedItemDetails(itemDetails: OrderDetailsDataModel, forCell: BuyerConfirmationTableViewCell) {
        var flag = false
        if itemDetails.packageOption?.lowercased() == "f" {
            if itemDetails.originalQuantity == itemDetails.modifiedQuantity {
                flag = true
            }
        }
        else {
            if itemDetails.originalPackageSize == itemDetails.modifiedPackageSize {
                flag = true
            }
        }
        if flag {
            forCell.acceptBtn.isSelected = true
            forCell.rejectBtn.isSelected = false
            itemDetails.itemStatus = OrderedItemStatus.Accept
            itemDetails.modifiedQuantity = itemDetails.originalQuantity
            itemDetails.modifiedPackageSize = itemDetails.originalPackageSize
        }
        else {
            if itemDetails.packageOption?.lowercased() == "f" {
                itemDetails.modifiedPrice = itemDetails.originalPackPrice!*Double(itemDetails.modifiedQuantity!)
                forCell.quantityLbl.text = "\(itemDetails.modifiedQuantity!)"
                forCell.priceLbl.text = "$"+String(format:"%.2f", arguments:[itemDetails.modifiedPrice!])
            }
            else {
                itemDetails.modifiedPrice = itemDetails.unitPrice!*Double(itemDetails.modifiedPackageSize!)
                //                forCell.partialLbl.text = "\(itemDetails.modifiedPackageSize!)"
                forCell.partialLbl.text = "Partial: \(itemDetails.modifiedPackageSize!)/\(itemDetails.actualPackageSize!)"
                forCell.priceLbl.text = "$"+String(format:"%.2f", arguments:[itemDetails.modifiedPrice!])
            }
            itemDetails.itemStatus = OrderedItemStatus.Revised
        }
        dataArray.replaceObject(at: selectedIndex!, with: itemDetails)
    }
    
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
     // Get the new view controller using segue.destinationViewController.
     // Pass the selected object to the new view controller.
     }
     */
    
    //MARK: - Table view delegate and datasource functions.
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return dataArray.count
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 135.0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "buyerConfirmationCell", for: indexPath) as! BuyerConfirmationTableViewCell
        if indexPath.row < dataArray.count {
            let dataModel = dataArray.object(at: indexPath.row) as! OrderDetailsDataModel
            cell.ndcNameLbl.text = dataModel.ndcName
            cell.ndcNumberLbl.text = dataModel.ndcNumber
            cell.expiryDateLbl.text = dataModel.expiryDate
            cell.lotNumberLbl.text = dataModel.lotNumber
            cell.acceptBtn.tag = indexPath.row
            cell.rejectBtn.tag = indexPath.row
            cell.acceptBtn.addTarget(self, action: #selector(onItemStatusChanged(sender:)), for: .touchUpInside)
            cell.rejectBtn.addTarget(self, action: #selector(onItemStatusChanged(sender:)), for: .touchUpInside)
            if dataModel.itemStatus == OrderedItemStatus.OutOfStock {
                cell.outOfStockView.isHidden = false
                cell.statusBtnView.isHidden = true
            }
            else {
                cell.outOfStockView.isHidden = true
                cell.statusBtnView.isHidden = false
            }
            if dataModel.packageOption?.lowercased() == "f" {
                cell.partialPackView.isHidden = true
                cell.fullPackView.isHidden = false
                cell.quantityLbl.text = "\(dataModel.modifiedQuantity!)"
                if dataModel.isRevised! {
                    let orderedQty: NSMutableAttributedString =  NSMutableAttributedString(string: "\(dataModel.orderedQuantity!)")
                    orderedQty.addAttribute(NSStrikethroughStyleAttributeName, value: 2, range: NSMakeRange(0, orderedQty.length))
//                    cell.perviousQtyLbl.text = "\(dataModel.orderedQuantity!)"
                    cell.perviousQtyLbl.attributedText = orderedQty
                }
                else {
                    cell.perviousQtyLbl.text = ""
                }
            }
            else {
                cell.partialPackView.isHidden = false
                cell.fullPackView.isHidden = true
                cell.partialLbl.text = "Partial: \(dataModel.modifiedPackageSize!)"
//                cell.previousPartialLbl.text = "\(dataModel.orderedPackageSize!)"
                cell.actualPackageSizeLbl.text = "/\(dataModel.actualPackageSize!)"
                if dataModel.isRevised! {
                    let partialValue: NSMutableAttributedString =  NSMutableAttributedString(string: "\(dataModel.orderedPackageSize!)")
                    partialValue.addAttribute(NSStrikethroughStyleAttributeName, value: 2, range: NSMakeRange(0, partialValue.length))
                    
//                    cell.previousPartialLbl.text = "\(dataModel.orderedPackageSize!)"
                    cell.previousPartialLbl.attributedText = partialValue
                }
                else {
                    cell.previousPartialLbl.text = ""
                }
            }
            
            let previousPrice: NSMutableAttributedString =  NSMutableAttributedString(string: "$"+String(format: "%.2f", arguments:[dataModel.orderedPrice!]))
            previousPrice.addAttribute(NSStrikethroughStyleAttributeName, value: 2, range: NSMakeRange(0, previousPrice.length))
            
            cell.priceLbl.text = "$"+String(format: "%.2f", arguments:[dataModel.modifiedPrice!])
            cell.perviousPriceLbl.text = "$"+String(format: "%.2f", arguments:[dataModel.orderedPrice!])
            if dataModel.isRevised! {
//                cell.perviousPriceLbl.text = "$"+String(format: "%.2f", arguments:[dataModel.orderedPrice!])
                cell.perviousPriceLbl.attributedText = previousPrice
            }
            else {
                cell.perviousPriceLbl.text = ""
            }
            self.view.layoutIfNeeded()
            if dataModel.medispanImageURL != nil {
                if ((imageCache.object(forKey: dataModel.orderId! as AnyObject)) != nil){
                    cell.ndcImageView.image = imageCache.object(forKey: dataModel.orderId! as Int64 as AnyObject)
                }
                else {
                    if !((operationListArray.contains(String(dataModel.orderId!)))){
                        
                        let operation : BlockOperation = BlockOperation(block: {
                            self.downloadImage(itemModel: dataModel)
                        })
                        operation.completionBlock = {
                            //                            print("Operation completed")
                        }
                        
                        operationQueue.addOperation(operation)
                        
                        operationListArray.insert(String(dataModel.orderId!), at: (operationListArray.count))
                    }
                }
            }
            
        }
        return cell
    }
    
    //MARK: - Network manager delegate functions.
    
    //Returns the web service call
    func webServiceResponse(for requestType: ServiceRequestType, responseData Data: Any!, withStatusCode statusCode: Int, errorMessage message: Any!) {
        LoadingView.shareInstance.hideLoadingView()
        if message as? Error != nil {
            AlertControl.sharedInstance.presentAlertView(alertTitle: "Something went wrong", alertMessage: "", delegate: self)
        }
        else{
            if !NetworkManager.sharedInstance.checkStatusCode(statusCode){
                let error = NetworkManager.sharedInstance.getErrorModel(requestType: requestType, httpStatusCode: statusCode, responseData: Data)
                AlertControl.sharedInstance.presentAlertView(alertTitle: (error.errorMessage!), alertMessage: "", delegate: self)
            }
            else{
                if requestType == .ServiceTypeOrderDetails {
                    orderDetailsDataModel = ResponseManager.sharedInstance.getOrderDetails(responseData: Data, orderType: .Purchase)
                    self.loadWithSerialisedData()
                }
                else if requestType == .ServiceTypeSellerConfirmation {
                    NavigationModel.sharedInstance.homeControl?.backView.isHidden = true
                    if confirmOrderType == OrderedItemStatus.Accept {
//                        self.view.bringSubview(toFront: downloadPDFView)
                        self.view.bringSubview(toFront: submit_MessageView)
                        self.submit_MessageView.bringSubview(toFront: submit_ReviseView)
                    }
                    else if confirmOrderType == OrderedItemStatus.Reject {
                        self.view.bringSubview(toFront: submit_MessageView)
                        self.submit_MessageView.bringSubview(toFront: submit_ReviseView)
                    }
                    else {
                        self.view.bringSubview(toFront: submit_MessageView)
                        self.submit_MessageView.bringSubview(toFront: submit_CancellationView)
                    }
                }
            }
        }
    }
}

