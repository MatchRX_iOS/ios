//
//  ItemDetailTableViewCell.swift
//  MatchRX
//
//  Created by Poonkathirvelan on 31/01/18.
//  Copyright © 2018 HTC. All rights reserved.
//

import UIKit

class ItemDetailTableViewCell: UITableViewCell {
    
    
    @IBOutlet weak var titleLbl: UILabel!
    @IBOutlet weak var detailLbl: UILabel!
    @IBOutlet weak var ingredientsBtn: UIButton!
    @IBOutlet weak var tblCellwidth: NSLayoutConstraint!
    @IBOutlet weak var detailLblLeadingConstant: NSLayoutConstraint!
//    @IBOutlet weak var ingredientBtnWidth: NSLayoutConstraint!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
        // Configure the view for the selected state
    }

    
    override func layoutSubviews() {
        
        if(self.titleLbl.text == ""){
        self.tblCellwidth.constant = 0
        self.detailLblLeadingConstant.constant = 0
        }
    }


}
