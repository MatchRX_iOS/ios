//
//  HTCTextField.swift
//  HTCTextField
//
//  Created by CH1104 on 17/05/17.
//  Copyright © 2017 CH1104. All rights reserved.
//

import UIKit

enum textFieldType {
    case DEANumberTextField
    case UserNameTextField
    case PasswordTextField
}

class HTCTextField: UITextField, UITextFieldDelegate
{
    
    public var maximumCharacters : Int? = 0
    public var minimumCharacters : Int? = 0
    public var secureText : Bool?
    public var notEditable : Bool? = false
    public var allowNumbers : Bool? = false
    public var allowCharacters : Bool? = false
    //public var allowOnlySpecialCharacters : Bool? = false
    public var allowSpecialCharacters : Bool? = false
    public var allowNumbersAndCharacters : Bool? = false
    public var notAllowingWhiteSpace : Bool? = false
    
    
    var selectedTextField : UITextField?
    var supportedCharacterSetArray : NSMutableArray?
    var unSupportedCharacterSetArray : NSMutableArray?
    var type : textFieldType?
    
    
    override init(frame: CGRect)
    {
        super.init(frame: frame)
        self.delegate = self
  
    
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)

           }

    
    override func awakeFromNib() {
       // self.backgroundColor = UIColor .gray
        self.delegate = self as UITextFieldDelegate
       
    }
    
    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool
    {
        
       self.hideKeyboard()
        return true
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
     
        textField.resignFirstResponder()
        return true
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool
    {
        
     //   For Max Length
        if maximumCharacters! > 0
        {
        if (textField.text?.count)! >= maximumCharacters!
        {
        let currentString: NSString = textField.text! as NSString
        let newString: NSString =
            currentString.replacingCharacters(in: range, with: string) as NSString
        return newString.length <= maximumCharacters!
        }
        }
        
        //   For Min Length
        if minimumCharacters! > 0
        {
        
        if (textField.text?.count)! >= minimumCharacters!
        {
            let currentString: NSString = textField.text! as NSString
            let newString: NSString =
                currentString.replacingCharacters(in: range, with: string) as NSString
            return newString.length >= minimumCharacters!
        
        }
        }
        
        //FOR allowing only NUMBERS
        
       if allowNumbers!
       {
            if string.count > 0
                  {

            if (string.rangeOfCharacter(from: CharacterSet.init(charactersIn: "0123456789")) == nil)
            {
                return false
                
            }
        }
        }

        //FOR allowing only Characters
        
        if allowCharacters!
        {
            if string.count > 0
            {
                
                if (string.rangeOfCharacter(from: CharacterSet.init(charactersIn: "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ")) == nil)
                {
                    return false
                    
                }
            }
        }
        
        
        
        //FOR allowing only SpecialCharacters
        
//        if allowOnlySpecialCharacters!
//        {
//            if string.characters.count > 0
//            {
//                
//                if (string.rangeOfCharacter(from: CharacterSet.init(charactersIn: "!*'.();:@&=+$,/?%#[]{}<>")) == nil)
//                {
//                    return false
//                    
//                }
//            }
//        }
        
        
        //FOR allowing SpecialCharacters along with characters
      
        if allowSpecialCharacters!
        {
            if string.count > 0
            {
                
                if (string.rangeOfCharacter(from: CharacterSet.init(charactersIn: "0123456789")) != nil)
                {
                    return false
                    
                }
            }
        }
        
        
        //FOR allowing only numbers and characters
        
        if allowNumbersAndCharacters!
        {
            if string.count > 0
            {
                
                if (string.rangeOfCharacter(from: CharacterSet.init(charactersIn: "0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ")) == nil)
                {
                    return false
                    
                }
            }
        }
        
        
        //For Not allowing space
       
        if notAllowingWhiteSpace!
        {
            let text = string
            if text == " " {
                return false
            }
        }

        //For Not editing
             if notEditable!
             {
                textField.isUserInteractionEnabled = false
                return false
        }
        /*switch type {
        case .DEANumberTextField?:
            if !string.isEmpty {
                if range.location == 0{
                    if (string.rangeOfCharacter(from: CharacterSet.init(charactersIn: "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ")) == nil){
                        return false
                    }
                }
                else if range.location == 1{
                    if (string.rangeOfCharacter(from: CharacterSet.init(charactersIn: "0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ")) == nil){
                        return false
                    }
                }
                else{
                    if (string.rangeOfCharacter(from: CharacterSet.init(charactersIn: "0123456789")) == nil)
                    {
                        return false
                    }
                }
            }
            else{
                return true
            }
            break
        default:
            break
        }*/
        return true
    }
    
    

    func textFieldDidBeginEditing(_ textField: UITextField) {
            }
    func textFieldDidEndEditing(_ textField: UITextField) {
        /*textField.resignFirstResponder()
        switch type {
        case .DEANumberTextField?:
            if !validateDEANumber(deaNumber: textField.text!){
                let alertView = UIAlertController(title: nil, message: "Invalid DEA Number", preferredStyle: UIAlertControllerStyle.alert)
                //...
                alertView.addAction(UIAlertAction(title: "OK", style: UIAlertActionStyle.cancel, handler:nil))
                //        LoadingView.shareInstance.hideLoadingView()
                //textField.resignFirstResponder()
//                self.window.present(alertView!, animated: false, completion: nil)
                self.window?.rootViewController?.present(alertView, animated: true, completion: nil)
            }
            break
        default:
            break
        }
        */
    }
    
    func validateDEANumber(deaNumber: String) -> Bool{
        if deaNumber.count >= 9 {
            print(deaNumber.prefix(2))
            let firstCharacter = deaNumber.prefix(1)
            let numbers = deaNumber.index(deaNumber.startIndex, offsetBy: 2)
            if (firstCharacter.rangeOfCharacter(from: CharacterSet.init(charactersIn: "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ")) == nil){
                return false
            }
            else if (deaNumber.suffix(from: numbers).rangeOfCharacter(from: CharacterSet.init(charactersIn: "0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ")) == nil){
                return false
            }
            else{
                let secondCharacter = String(describing: deaNumber.index(deaNumber.startIndex, offsetBy: 1))
                if (secondCharacter.rangeOfCharacter(from: CharacterSet.init(charactersIn: "0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ")) == nil){
                    return false
                }
            }
        }
        else{
            return false
        }
        return true
    }
    
    // For changing the  Text color
    
    func changeTextColor(_ textField: UITextField , color: UIColor)
    {
        textField.textColor = color
    }

    // For changing the  Text Background  color
    
    func changeBackgroundColor(_ textField: UITextField , color: UIColor)
    {
        textField.backgroundColor = color
    }
    
    // For changing the text font size
    
    func changeFont(_ textField: UITextField , font : UIFont)
    {
        textField.font = font
    }
    
    
    // For adding placeholder
    
    func addPlaceholder(_ textField: UITextField , text : String )
    {
        textField.placeholder = text
        
    }
    
    
    // For adding placeholder with color
    
    
    func addPlaceholderWithColor(_ textField: UITextField , text : String , color : UIColor)
    {
        textField.attributedPlaceholder = NSAttributedString(string: text, attributes: [NSForegroundColorAttributeName: color])
    }

    
    //For clearing the text Box values
    
    func clearTextBox(_ textField: UITextField )
    {
        textField.text = nil;
    }
    
    //For password the text Box values
    
    func securePassword(_ textField: UITextField , value : Bool)
    {
        textField.isSecureTextEntry = value
    }
    
    
    //For not allowing user to enter text Box values
    
    func userInteraction(_ textField: UITextField , value : Bool)
    {
        textField.isUserInteractionEnabled = value
    }
    
    //For only uppercase letters
    func uppercaseLetters(_ textField: UITextField )
    {
        textField.autocapitalizationType = .allCharacters
    }

    //For only lower letters
    func lowercaseLetters(_ textField: UITextField )
    {
        textField.text?.lowercased()
    }

    //For only Upper Camel case letters
    func upperCamelcaseLetters(_ textField: UITextField )
    {
        textField.autocapitalizationType = UITextAutocapitalizationType.words;
    }
    

    //For emailID validation
    
    func isValidEmail(_ textField: UITextField ) -> Bool
    {
        let testStr : String = textField.text!
        let emailRegEx = "[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,4}"
      
        let emailTest = NSPredicate(format:"SELF MATCHES %@", emailRegEx)
        let result = emailTest.evaluate(with: testStr)
        return result
    }

    //For removing space
    
    func removeSpace(_ textField: UITextField) -> String
    {
        let testStr : String = textField.text!
        let formattedString = testStr.replacingOccurrences(of: " ", with: "")
        return formattedString
    }
    
    //For password validation
    //    8-> Min characters length
    //    1-> Min one letter in Upper Case
    //    1-> Min one Special Character (!@#$&*)
    //    1-> Min one number (0-9)
    //    1-> Min one letter in Lower Case
    
    func isPasswordValid(_ password : String) -> Bool
    {
        let passwordTest = NSPredicate(format: "SELF MATCHES %@","^(?=.*[A-Z])(?=.*[!@#$&*])(?=.*[0-9])(?=.*[a-z]).{8,}")
        return passwordTest.evaluate(with: password)

    }
    

    
   
    
    func hideKeyboard()
    {
        let tap: UITapGestureRecognizer = UITapGestureRecognizer(
            target: self,
            action: #selector(HTCTextField.dismissKeyboard))
        
        self.window?.addGestureRecognizer(tap)
    }
    
    func dismissKeyboard()
    {
        self.window?.endEditing(true)
    }

    
   }













