//
//  AlertControl.swift
//  MatchRX
//
//  Created by Vignesh on 11/14/17.
//  Copyright © 2017 HTC. All rights reserved.
//

import UIKit

protocol AlertDelegate {
    func onOkTouched()
}

class AlertControl: NSObject {

    static let sharedInstance = AlertControl()
    var parentDelegate: AlertDelegate?
    var isCancelButton: Bool = false
    var cancelTitle: String = "Cancel"
    
    /*override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }*/
    
    //MARK: - User defined functions.
    
    //Present the alert controller.
    
    func presentAlertView(alertTitle: String, alertMessage: String, delegate: Any?) {
        self.parentDelegate = delegate as? AlertDelegate
        let alertControl = UIAlertController(title: alertTitle, message: alertMessage, preferredStyle: UIAlertControllerStyle.alert)
        alertControl.addAction(UIAlertAction(title:"OK", style: UIAlertActionStyle.cancel, handler: {actions in self.parentDelegate?.onOkTouched()}))
        UIApplication.topViewController()?.present(alertControl, animated: true, completion: nil)
    }
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
