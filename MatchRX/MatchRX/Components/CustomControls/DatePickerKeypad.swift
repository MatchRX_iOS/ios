//
//  DatePickerKeypad.swift
//  DatePickerSwift
//
//  Created by CH1104 on 26/05/17.
//  Copyright © 2017 CH1104. All rights reserved.
//

import UIKit


class DatePickerKeypad: UITextField,UITextFieldDelegate {

    public var dateValue : String?
    public var dateFormat : String?
    public var timeKeyPad : Bool? = false
    
    override init(frame: CGRect)
    {
        super.init(frame: frame)
        self.delegate = self
      
       
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        
    }
    
    
    override func awakeFromNib() {
       
        self.delegate = self as UITextFieldDelegate
        
    }
    

    
    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
    
        let toolBar = UIToolbar()
        toolBar.barStyle = UIBarStyle.default
        toolBar.isTranslucent = true
       // toolBar.tintColor = UIColor(red: 76/255, green: 217/255, blue: 100/255, alpha: 1)
        toolBar.sizeToFit()
        let doneButton = UIBarButtonItem.init(title: "Done", style: UIBarButtonItemStyle.plain, target: nil, action: "donePicker")
        
        let spaceButton = UIBarButtonItem(barButtonSystemItem: UIBarButtonSystemItem.flexibleSpace, target: nil, action: nil)
        if !(timeKeyPad!)
        {
        let todayBtn = UIBarButtonItem(title: "Today", style: UIBarButtonItemStyle.plain, target: self, action:"tappedToolBarBtn")
        
        toolBar.setItems([todayBtn, spaceButton, doneButton], animated: false)
        }
        else{
            toolBar.setItems([doneButton], animated: false)
        }
        
        toolBar.isUserInteractionEnabled = true
       
        let datePickerView: UIDatePicker = UIDatePicker()

        if timeKeyPad!{
            datePickerView.datePickerMode = UIDatePickerMode.time
        }
        
        else{
            datePickerView.datePickerMode = UIDatePickerMode.date
        }
        
        textField.inputView = datePickerView
        
                textField.inputAccessoryView = toolBar
        let gregorian: NSCalendar = NSCalendar(calendarIdentifier: NSCalendar.Identifier.gregorian)!
        let currentDate: Date = Date()
        let components: NSDateComponents = NSDateComponents()
        components.year = -150
        let minDate: Date = gregorian.date(byAdding: components as DateComponents, to: currentDate, options: NSCalendar.Options(rawValue: 0))!
        
        //components.year = -16
        components.year = 0
        let maxDate: Date = gregorian.date(byAdding: components as DateComponents, to: currentDate, options: NSCalendar.Options(rawValue: 0))!
        
        datePickerView.minimumDate = minDate
        datePickerView.maximumDate = maxDate
        
        datePickerView.addTarget(self, action: #selector(datePickerValueChanged), for: UIControlEvents.valueChanged)
        self.hideKeyboard()
        
        
        return true

    }
    
    func datePickerValueChanged(_ sender: UIDatePicker)
    {
        
        var dateFormatter = DateFormatter()
        
        dateFormatter.dateStyle = DateFormatter.Style.medium
        
        dateFormatter.timeStyle = DateFormatter.Style.none
        
        if timeKeyPad!{
            dateFormat = "HH : mm"
            dateFormatter.dateFormat = dateFormat
        }
       // dateFormat="dd/MM/yy"
        dateFormatter.dateFormat = dateFormat
        dateValue = dateFormatter.string(for: sender.date)
        
        
    }
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        
        textField.text = dateValue
        
    }
    
    func donePicker()
    {
        self.resignFirstResponder()
        
    }
    
    // for getting today's date
    
    func tappedToolBarBtn()
    {
        let dateformatter = DateFormatter()
        
        dateformatter.dateStyle = DateFormatter.Style.medium
        
        dateformatter.timeStyle = DateFormatter.Style.none
        
        dateformatter.dateFormat = dateFormat
      
        dateValue = dateformatter.string(from: Date() as Date)
        
        self.resignFirstResponder()
       
    }
    
// For keyboard disable: 
    
    func hideKeyboard()
    {
        let tap: UITapGestureRecognizer = UITapGestureRecognizer(
            target: self,
            action: #selector(DatePickerKeypad.dismissKeyboard))
        
        self.window?.addGestureRecognizer(tap)
    }
    
    func dismissKeyboard()
    {
        self.window?.endEditing(true)
    }
    

    
    }
