//
//  DropDownView.swift
//  DropDown
//
//  Created by selvakumar on 23/05/17.
//  Copyright © 2017 chandramouli. All rights reserved.
//

import UIKit
import QuartzCore

protocol DropDownViewDelegate {
    
    func selectedDropDownData(_ dropDownDataObject : DropDownModel)
    func selectedDropDownDataArray(_ dropDownDataArray : NSMutableArray)
    func selectedDropDownDataString(_ selectedDropDownDataString : NSString)
}
class DropDownView: UIView,UITableViewDelegate,UITableViewDataSource,UISearchBarDelegate {

    /*
    // Only override draw() if you perform custom drawing.
    // An empty implementation adversely affects performance during animation.
    override func draw(_ rect: CGRect) {
        // Drawing code
    }
    */
    // Define properties of DropDown View
    
    open var dropDownDataTableView : UITableView = UITableView()  // Tableview for showing dropdown values
    var cancelButton : UIButton = UIButton()  // To cancel the dropdown options
    var okButton : UIButton = UIButton()
    var dropDownListSearch : UISearchBar?  // To search the dropdown list values
    var tableViewDataArray : NSMutableArray?  // Initialize a mutable array for storing dropdown data
    var tableViewSearchDataArray : NSMutableArray?  // Initialize a mutable array for storing searched data values
    var selectedModelIdArray : NSMutableArray?  // Initilaze a mutable array for storing the selected value in the dropdown.
    var dropDownType : DropDownType?  // Declare the type of dropdown
    var isSearching : Bool = false  // Bool instance used for searching drop down values
    var allowSearching : Bool = false  // Bool instance used for allowing to search
    var isAutoCorrectView : Bool = false 
    
    var keyBoardNotification : Notification? // Keyboard notification for showing and dismissing the keyboard
    var dropDownDelegate:DropDownViewDelegate? // DropDown delegate used to pass the value
    var serviceRequestType:ServiceRequestType? // Type of webservice request
    let cellReuseIdentifier = "dropDown"
    
    // Creates singleton object for DropDownView
    static let defaultDropDownControl : DropDownView = {
       
        var dropDownView = DropDownView()
        dropDownView.tableViewDataArray = NSMutableArray()
        
        // Set frame and properties for dropdown tableview
        dropDownView.dropDownDataTableView = UITableView.init(frame: CGRect.zero, style: UITableViewStyle.plain)
        dropDownView.dropDownDataTableView.backgroundColor = UIColor.white
        dropDownView.dropDownDataTableView.separatorStyle = UITableViewCellSeparatorStyle.none
        dropDownView.dropDownDataTableView.contentInset = UIEdgeInsetsMake(5, 0, 0, 0)
        dropDownView.dropDownDataTableView.layer.cornerRadius = 10.0
        
        // Set properties for cancel button in DropDownView
        dropDownView.cancelButton = UIButton.init(type: UIButtonType.custom)
        dropDownView.cancelButton.backgroundColor = UIColor.white
        dropDownView.cancelButton.setTitle("Cancel", for: UIControlState.normal)
        dropDownView.cancelButton.setTitleColor(UIColor.init(red: 24.0/255.0, green: 0.0/255.0, blue: 254.0/255.0, alpha: 1.0), for: UIControlState.normal)
        dropDownView.cancelButton.titleLabel?.font = UIFont.init(name: "Arial", size: 15.0)
        dropDownView.cancelButton.layer.cornerRadius = 10.0
        
        
        // Set properties for Ok button in DropDownView
        dropDownView.okButton = UIButton.init(type: UIButtonType.custom)
        dropDownView.okButton.backgroundColor = UIColor.white
        dropDownView.okButton.setTitle("Ok", for: UIControlState.normal)
        dropDownView.okButton.setTitleColor(UIColor.init(red: 24.0/255.0, green: 0.0/255.0, blue: 254.0/255.0, alpha: 1.0), for: UIControlState.normal)
        dropDownView.okButton.titleLabel?.font = UIFont.init(name: "Arial", size: 15.0)
        dropDownView.okButton.layer.cornerRadius = 10.0
        dropDownView.okButton.addTarget(dropDownView, action: #selector(didClickOk(_:)), for: UIControlEvents.touchUpInside)
        
        dropDownView.addSubview(dropDownView.okButton)
        dropDownView.addSubview(dropDownView.dropDownDataTableView)
        dropDownView.addSubview(dropDownView.cancelButton)
        dropDownView.cancelButton.addTarget(dropDownView, action: #selector(didClickCancel(_:)), for: UIControlEvents.touchUpInside)
        
        dropDownView.dropDownListSearch = UISearchBar.init()   // Initialize searchbar in dropdownview
        dropDownView.allowSearching = true
        
        return dropDownView
        
    }()
    
    //MARK: - Show dropdown view
    func showDropDownForData(_ dataArray:NSMutableArray,_ dropDownTypeValue:DropDownType,_ displayFrame:CGRect,_ parentView:UIView,_ delegateObject:DropDownViewDelegate,_ selectedModelID:NSMutableArray) {
        
        self.dropDownType = dropDownTypeValue      // Dropdown type
        var showDisplayFrame = displayFrame
        
        // Store the dropdown type
        
        switch dropDownTypeValue {
        case .SingleSelectionDefault:
            self.dropDownType = dropDownTypeValue
            break
        case .SingleSelectionWithImage:
            self.dropDownType = dropDownTypeValue
            break
        case .MultipleSelection:
            self.dropDownType = dropDownTypeValue
            self.dropDownDataTableView.allowsMultipleSelection = true
            break
        default:
            break
            
        }
        
        // Set the Dropdown frame
        showDisplayFrame = UIScreen.main.bounds
        self.frame = CGRect.init(x: 0, y: showDisplayFrame.size.height, width: showDisplayFrame.size.width, height: showDisplayFrame.size.height)
        self.backgroundColor = UIColor.init(red: 0.0, green: 0.0, blue: 0.0, alpha: 0.5)
        
        // Set datasource and delegate for dropdown tableview
        self.dropDownDataTableView.dataSource = self
        self.dropDownDataTableView.delegate = self
        self.dropDownDataTableView.register(UITableViewCell.self, forCellReuseIdentifier: cellReuseIdentifier)
        self.dropDownDelegate = delegateObject
        
        
        // Set the cancelbutton frame
        self.cancelButton.frame = CGRect.init(x: 5.0, y: showDisplayFrame.size.height - 55.0, width: showDisplayFrame.size.width - 10.0, height: 45.0)
        //self.addSubview(self.cancelButton)
        
        if !isAutoCorrectView {
            
            self.okButton.frame = CGRect.init(x: 5.0, y: showDisplayFrame.size.height - 105.0, width: showDisplayFrame.size.width - 10.0, height: 45.0)
        }
        
        self.tableViewDataArray?.removeAllObjects()
        //self.tableViewDataArray?.addObjects(from: [dataArray as Any])
        
        self.tableViewDataArray = dataArray
        //self.dropDownDataTableView?.dataSource = self.tableViewDataArray as? UITableViewDataSource
        let tableViewHeight = CGFloat(dataArray.count) * 50.0
        let availableHeight = showDisplayFrame.size.height - 65.0
        dropDownListSearch?.text = nil
        
        //Check the height and Dropdown selection type.
        if tableViewHeight > availableHeight && self.dropDownType == DropDownType.MultipleSelection {
            
            //Check if the dropdown list is allowing search options then set the frames for dropdowntableview, cancelbutton, searchBar accordingly.
            if self.allowSearching {
                
                // Set the tableview frame
                self.dropDownDataTableView.frame = CGRect.init(x: 5.0, y: 55.0, width: showDisplayFrame.size.width - 10.0, height: availableHeight - 100.0)
//                self.dropDownDataTableView?.frame = CGRect.init(x: 5.0, y: 55.0, width:Double(showDisplayFrame.size.width-10.0), height: availableHeight - 100.0)
                // Set the search bar frame
                self.dropDownListSearch?.frame = CGRect.init(x: 5.0, y: 5.0, width: showDisplayFrame.size.width-10.0, height: 50.0)
                self.dropDownListSearch?.searchBarStyle = UISearchBarStyle.default
                self.dropDownListSearch?.showsCancelButton = true
                self.dropDownListSearch?.placeholder = "Search"
                self.addSubview(self.dropDownListSearch!)
                self.dropDownListSearch?.delegate = self
                
                if !isAutoCorrectView {
                    
                    self.okButton.frame = CGRect.init(x: 5.0, y: showDisplayFrame.size.height - 105.0, width: showDisplayFrame.size.width - 10.0, height: 45.0)
                }
                
                self.okButton.backgroundColor = UIColor.red
                self.cancelButton.frame = CGRect.init(x: 5.0, y: showDisplayFrame.size.height - 55.0, width: showDisplayFrame.size.width - 10.0, height: 45.0)
                
            }
            else
            {
                self.dropDownDataTableView.frame = CGRect.init(x: 5.0, y: 55.0, width:showDisplayFrame.size.width - 10.0, height: availableHeight - 50.0)
                if !isAutoCorrectView {
                    
                    self.okButton.frame = CGRect.init(x: 5.0, y: showDisplayFrame.size.height - 105.0, width: showDisplayFrame.size.width - 10.0, height: 45.0)
                }
                
                self.dropDownListSearch?.removeFromSuperview()
                
            }
            
            
        }
        else if tableViewHeight > availableHeight
        {
            //Check if the dropdown list is allowing search options then set the frames for dropdowntableview, cancelbutton, searchBar accordingly.
            
            if self.allowSearching {
                
                self.dropDownDataTableView.frame = CGRect.init(x: 5.0, y: 55.0, width:showDisplayFrame.size.width - 10.0, height: availableHeight - 50.0)
                self.dropDownListSearch?.frame = CGRect.init(x: 5.0, y: 5.0, width: showDisplayFrame.size.width-10.0, height: 50.0)
                
                self.dropDownListSearch?.searchBarStyle = UISearchBarStyle.default
                self.dropDownListSearch?.showsCancelButton = true
                self.dropDownListSearch?.placeholder = "Search"
                self.addSubview(self.dropDownListSearch!)
                self.dropDownListSearch?.delegate = self
                self.cancelButton.frame = CGRect.init(x: 5.0, y: showDisplayFrame.size.height - 55.0, width: showDisplayFrame.size.width - 10.0, height: 45.0)
                
            }
            else
            {
                self.dropDownDataTableView.frame = CGRect.init(x: 5.0, y: 55.0, width:showDisplayFrame.size.width - 10.0, height: availableHeight - 50.0)
                
            }
        }
        else
        {
            let width  = tableViewHeight + 65.0
            
            self.dropDownDataTableView.frame = CGRect.init(x: 5.0, y: showDisplayFrame.size.height - width, width:showDisplayFrame.size.width - 10.0, height: tableViewHeight)
            
            print("\(self.dropDownDataTableView.frame)")
            
            if (self.dropDownListSearch != nil) {
                
                self.dropDownListSearch?.removeFromSuperview()
            }
        }
        print("search result..\(isSearching)")
        //self.addSubview(self.dropDownDataTableView!)
        self.dropDownDataTableView.reloadData()
        
        
        // Set animations for showing the dropdownlist
        UIView.beginAnimations("dropDown1", context: nil)
        UIView.setAnimationDuration(0.2)
        UIView.setAnimationTransition(UIViewAnimationTransition.none, for: self, cache: false)
        
        self.frame = showDisplayFrame
        parentView.addSubview(self)
        
        UIView.commitAnimations()
        
        // Add Notification observer for keyboard hide and show
        let notificationCenter:NotificationCenter = NotificationCenter.default
        notificationCenter.addObserver(self, selector: #selector(keyboardWillShow(_:)), name: .UIKeyboardWillShow, object: nil)
        notificationCenter.addObserver(self, selector: #selector(keyboardWillHide(_:)), name: .UIKeyboardWillHide, object: nil)
        
        if dataArray.count > 0 {
            
            //Scroll to tableview
            self.dropDownDataTableView.scrollToRow(at: IndexPath(row: 0, section: 0), at: UITableViewScrollPosition.top, animated: false)
        }
        
    }
    
    //MARK: - Keyboard will appear
    func keyboardWillShow(_ notification:Notification)  {
                
        self.keyBoardNotification = notification
        var keyBoardBounds:CGRect?
        
        //Get the frame of the keyboard while showing up and set the frames for DropDownTableView, Cancel Button
        let info = notification.userInfo! as NSDictionary//notification.userInfo as! NSDictionary;
        let keyBoardframe = info.value(forKey: UIKeyboardFrameBeginUserInfoKey) as! CGRect;
        let keyBoardsize = keyBoardframe.size
        
        var displayFrame:CGRect = UIScreen.main.bounds
        displayFrame = CGRect.init(x: 0.0, y: 0.0, width: displayFrame.size.width, height: displayFrame.size.height - keyBoardsize.height) //(keyBoardBounds?.size.height)
        self.cancelButton.frame = CGRect.init(x: 5.0, y: displayFrame.size.height - 55.0, width: displayFrame.size.width - 10.0, height: 45.0)
        if !isAutoCorrectView {
            
            self.okButton.frame = CGRect.init(x: 5.0, y: displayFrame.size.height - 105.0, width: displayFrame.size.width - 10.0, height: 45.0)
        }
        
        let tableHeight = CGFloat((tableViewDataArray?.count)!) * 50.0
        let availableHeight = displayFrame.size.height - 65.0
        
        // Check the DropDownTableview height & type of dropdown to alter the DropDownTableView frame
        if tableHeight > availableHeight && self.dropDownType == DropDownType.MultipleSelection {
            
            self.dropDownDataTableView.frame = CGRect.init(x: 5.0, y: 55.0, width: displayFrame.size.width - 10.0, height: availableHeight-100.0)
        }
        else if tableHeight>availableHeight
        {
            self.dropDownDataTableView.frame = CGRect.init(x: 5.0, y: 55.0, width: displayFrame.size.width - 10.0, height: availableHeight - 50.0)
        }
        else
        {
            self.dropDownDataTableView.frame = CGRect.init(x: 5.0, y: availableHeight - tableHeight, width: displayFrame.size.width - 10.0, height: tableHeight)
        }
        
    }
    //MARK: - Keyboard will disappear
    func keyboardWillHide(_ notification:Notification)  {
    
        var displayFrame:CGRect = UIScreen.main.bounds
        self.cancelButton.frame = CGRect.init(x: 5.0, y: displayFrame.size.height - 55.0, width: displayFrame.size.width - 10.0, height: 45.0)
        if !isAutoCorrectView {
            
            self.okButton.frame = CGRect.init(x: 5.0, y: displayFrame.size.height - 105.0, width: displayFrame.size.width - 10.0, height: 45.0)
        }
        
        
        
        let tableHeight = CGFloat((tableViewDataArray?.count)!) * 50.0
        let availableHeight = displayFrame.size.height - 65.0
        
        let totalHeight = tableHeight + availableHeight
        
        print("\(totalHeight)")
        
        
        // Check the DropDownTableView height & type of dropdown to alter the DropDownTableView frame
        if tableHeight > availableHeight && self.dropDownType == DropDownType.MultipleSelection {
            
            self.dropDownDataTableView.frame = CGRect.init(x: 5.0, y: 55.0, width: displayFrame.size.width-10.0, height: availableHeight-100.0)
        }
        else if tableHeight>availableHeight
        {
            self.dropDownDataTableView.frame = CGRect.init(x: 5.0, y: 55.0, width: displayFrame.size.width - 10.0, height: availableHeight - 50.0)
        }
        else
        {
            self.dropDownDataTableView.frame = CGRect.init(x: 5.0, y: availableHeight - tableHeight, width: displayFrame.size.width - 10.0, height: tableHeight)
            
        }
        
        self.dropDownDataTableView.reloadData()
    }
    
    //MARK: - Cancel the dropdown view
    func didClickCancel(_ sender: Any)  {
        
        print("Cancel dropdown")
        
        // Removing the list search values and hide the DropDowList
        dropDownListSearch?.text = nil
        isSearching = false
        dropDownDataTableView.reloadData()
        hideDropDown() // Will hide the dropDownList
        
    }
    
    //MARK: - Cancel the dropdown view
    func didClickOk(_ sender: Any)  {
        
        print("Cancel dropdown")
        
        self.selectedModelIdArray?.removeAllObjects()
        dropDownListSearch?.text = nil
        isSearching = false
        
        dropDownDataTableView.reloadData()
        hideDropDown()
        
    }
    
    
    // MARK: - Hide DropDownList
    func hideDropDown()
    {
        
        self.endEditing(true) // Set to end editing for UITextfield
        self.dropDownDataTableView.reloadData()
        
        // Set animations to hide the dropdown value
        UIView.beginAnimations("dropDown1", context: nil)
        UIView.setAnimationDuration(0.2)
        UIView.setAnimationTransition(UIViewAnimationTransition.none, for: self, cache: false)
        
        self.frame = CGRect.init(x: 0, y: self.frame.size.height, width: self.frame.size.width, height: self.frame.size.height)
         UIView.commitAnimations()
        self.perform(#selector(removeFromSuperview), with: nil, afterDelay: 0.3) // Remove the view attached to DropDownList
        
        
        // Remove all the keyboard notification observer to hide the keyboard
        let notificationCenter:NotificationCenter = NotificationCenter.default
        notificationCenter.removeObserver(self, name: .UIKeyboardDidShow, object: nil)
        notificationCenter.removeObserver(self, name: .UIKeyboardDidHide, object: nil)
        
       
    }
    
    
    
    //MARK: - UISearch bar delagate methods
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
     
        var searchTextString:NSString = searchText as NSString
        
        // Depends on the search text count DropDownList will be loaded
        if searchText.characters.count > 0 {
            
            isSearching = true
            if self.tableViewSearchDataArray == nil {
                
                self.tableViewSearchDataArray = NSMutableArray()
                
            }
            
            // Check the user typed text with the existing DropDownList values
            self.tableViewSearchDataArray?.removeAllObjects()
            let searchPredicate:NSPredicate = NSPredicate(format: "modelDataString BEGINSWITH[c] %@", "\(searchTextString)")
            self.tableViewSearchDataArray?.addObjects(from: (self.tableViewDataArray?.filtered(using: searchPredicate))!)
            self.dropDownDataTableView.reloadData()
        }
        else
        {
            // If the search text count is 0 DropDownList will be reloaded with existing DropDown Values.
            dropDownListSearch?.text = nil
            isSearching = false
            self.dropDownDataTableView.reloadData()
        }

    }
    
    //MARK: - Did click cancel button in searchBar
    func searchBarCancelButtonClicked(_ searchBar: UISearchBar) {
        
        dropDownListSearch?.text = nil  // Make the searchBar to nil while cancelling the search options and also make the textfield to resign.
        isSearching = false
        searchBar.resignFirstResponder()
    }
    
    //MARK: - Did click search button in searchBar
    func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
        
        searchBar.resignFirstResponder()
        LoadingView.shareInstance.showLodingView(parentView: self)
        print("\(self.dropDownDataTableView.frame)")
        
    }
    func getCustomResponse(requestType:ServiceRequestType, dataArray:String) -> NSMutableArray {
        let customsArray:NSMutableArray = NSMutableArray()
        let customObject = DropDownModel()
        customObject.modelDataString =  dataArray as NSString?
        customObject.modelIdString =  "-1"
        customsArray.add(customObject)
        return customsArray
    }
    
    //MARK: - Tableview Delegate & Datasource methods
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        if isSearching {
            
            return (self.tableViewSearchDataArray?.count)!
        }
        print("\(self.dropDownDataTableView.frame)")
        return (self.tableViewDataArray?.count)!
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
     
        LoadingView.shareInstance.hideLoadingView()
        //let cellIdentifier = "dropDown"
        let cell:UITableViewCell = tableView.dequeueReusableCell(withIdentifier: cellReuseIdentifier)!//tableView.dequeueReusableCell(withIdentifier: cellReuseIdentifier, for: indexPath)
        var dropDownObject:DropDownModel = DropDownModel()
        
        // Get the DropDownModel value using cell index based on the search criteria
        if isSearching {
            
            
            dropDownObject =  self.tableViewSearchDataArray?.object(at: indexPath.row) as! DropDownModel  // Store the tableview search dropdown value based on the tableview indexpath
        }
        else
        {
            dropDownObject =  self.tableViewDataArray?.object(at: indexPath.row) as! DropDownModel  // Store the tableview dropdown value based on the tableview indexpath
        }
        
        // Set the values to DropDownTableview cell from DropDownModel
        cell.imageView?.image = nil
        cell.textLabel?.text = nil
        
        //Check the data is available to display it in the cell
        if (dropDownObject.modelDataString != nil) {
            
            cell.textLabel?.text = dropDownObject.modelDataString! as String
            cell.textLabel?.numberOfLines = 2
        }
        
        // Adding cell values
        cell.textLabel?.font = UIFont.init(name: "Arial", size: 14.0)
        cell.textLabel?.textAlignment = NSTextAlignment.center
        cell.textLabel?.textColor = UIColor.init(red: 24.0/255.0, green: 0.0/255.0, blue: 254.0/255.0, alpha: 1.0)
        var accessoryImageView:UIImageView = UIImageView.init(frame: CGRect.init(x: 0, y: 0, width: 15.0, height: 15.0))
        //cell.backgroundColor = UIColor.blue
        cell.accessoryView = nil
        
        
        // Based on the DropDownType the DropDownTableView cell and it's propertis will be modified
        switch self.dropDownType!
        {
        case .SingleSelectionDefault:
            
            break
        case .SingleSelectionWithImage:
//            cell.imageView?.image = UIImage.init(named: "RadioSelected")
            cell.imageView?.image = UIImage.init(contentsOfFile: dropDownObject.image as! String)
            break
        case .MultipleSelection:
            if ((self.selectedModelIdArray?.count) != nil) {
                
                // Based on the selection type change the cell accessory image.
                if (self.selectedModelIdArray?.contains(dropDownObject.modelIdString ?? "0"))!
                {
                    accessoryImageView.image = UIImage.init(named: "RadioSelected")
                }
                else
                {
                    accessoryImageView.image = UIImage.init(named: "RadioUnselected")
                }
            }
            else
            {
                accessoryImageView.image = UIImage.init(named: "RadioUnselected")
            }
            cell.accessoryView = accessoryImageView
            break
        default:
            break
        }
        
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        var dropDownModelObject: DropDownModel = DropDownModel()
        
        
        // Based on the search criteria get the DropDowmModel using cell index
        if isSearching {
            
            dropDownModelObject = self.tableViewSearchDataArray?.object(at: indexPath.row) as! DropDownModel  // Get the tableview cell search data based on the selection of tableview index
        }
        else
        {
            dropDownModelObject = self.tableViewDataArray?.object(at: indexPath.row) as! DropDownModel  // Get the tableview cell data based on the selection of tableview index.
        }
        
        if self.selectedModelIdArray == nil {
            
            self.selectedModelIdArray = NSMutableArray()
        }
        
        // Based on the DropDownType the DropDownTableView cell and it's propertis will be modified
        switch self.dropDownType!
        {
        case .SingleSelectionDefault:
            self.selectedModelIdArray?.removeAllObjects()
            
            // Based on searching pass the selected data to respective class using delegate
            if isSearching {
                
                dropDownListSearch?.text = nil
                self.hideDropDown()
                self.dropDownDelegate?.selectedDropDownData(self.tableViewSearchDataArray?.object(at: indexPath.row) as! DropDownModel)  // Pass selected search data using delegate method
            }
            else
            {
                self.dropDownDelegate?.selectedDropDownData(self.tableViewDataArray?.object(at: indexPath.row) as! DropDownModel) // Pass selected data using delegate method
            }
            break
        case .SingleSelectionWithImage:
            
            if isSearching {
                
                self.dropDownDelegate?.selectedDropDownData(self.tableViewSearchDataArray?.object(at: indexPath.row) as! DropDownModel)
            }
            else
            {
                 self.dropDownDelegate?.selectedDropDownData(self.tableViewDataArray?.object(at: indexPath.row) as! DropDownModel)
            }
            print("")
            break
        case .MultipleSelection:
            
            //For multiple selection change the accessory image and selected data
            if (self.selectedModelIdArray?.contains(dropDownModelObject.modelIdString!))! {
                
                let selectedRow = dropDownModelObject.modelIdString?.integerValue
                //self.selectedModelIdArray?.removeObject(at:selectedRow!)
                self.selectedModelIdArray?.remove(dropDownModelObject.modelIdString!) // If the row is already selected then remove it while selecting it again.
            }
            else
            {
                //let selectedRow = dropDownModelObject.modelIdString?.integerValue
                self.selectedModelIdArray?.add(dropDownModelObject.modelIdString!)   // If the new row is selected then add it and make changes in the cell.
            }
            
            if (self.selectedModelIdArray?.count)! > 0 {
                
                self.dropDownDelegate?.selectedDropDownDataArray(self.selectedModelIdArray!)
            }
            
            
            break
        default:
            break
        }
        
        self.dropDownDataTableView.reloadData()
        
    }
    
    func tableView(_ tableView: UITableView, didDeselectRowAt indexPath: IndexPath) {
        
        
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        
        return 50.0
    }
    
    

    
    
}
