//
//  DropDownModel.swift
//  DropDown
//
//  Created by selvakumar on 23/05/17.
//  Copyright © 2017 chandramouli. All rights reserved.
//

import UIKit

enum DropDownType {

    case SingleSelectionDefault
    case SingleSelectionWithImage
    case MultipleSelection

}
class DropDownModel: NSObject {

    //Define DropDown model properties
    
    var modelIdString : NSString?
    var modelCostIdString : NSString?
    var modelDataString : NSString?
    var modelDataIdString : NSString?
    var modelAliasString : NSString?
    var modelCategoryString : NSString?
    var imagePath : NSString?
    var modelDescription : NSString?
    var image : NSString?
    
}
