//
//  AutoCorrectView.swift
//  DropDown
//
//  Created by selvakumar on 29/05/17.
//  Copyright © 2017 chandramouli. All rights reserved.
//

import UIKit

class AutoCorrectView: DropDownView,NetworkManagerDelegate {
    func webServiceResponse(for requestType: ServiceRequestType, responseData Data: Any!, withStatusCode statusCode: Int, errorMessage message: Any!) {
        
    }
    
    
     var defaultValue:String? // To store the user tyepd value
     var keyBoardNotifications : Notification?

    var alertView: UIAlertController?
    // Create Singleton object for AutoCorrectView
    static let defaultAutoCorrectView : DropDownView = {
       
        var autoCorrectDropDownView = AutoCorrectView()
        autoCorrectDropDownView.tableViewDataArray = NSMutableArray()
        
        // Set frame and properties for Dropdown tableview
        autoCorrectDropDownView.dropDownDataTableView = UITableView.init(frame: CGRect.zero, style: UITableViewStyle.plain)
        autoCorrectDropDownView.dropDownDataTableView.backgroundColor = UIColor.white
        autoCorrectDropDownView.dropDownDataTableView.separatorStyle = UITableViewCellSeparatorStyle.none
        autoCorrectDropDownView.dropDownDataTableView.contentInset = UIEdgeInsetsMake(5, 0, 0, 0)
        autoCorrectDropDownView.dropDownDataTableView.layer.cornerRadius = 10.0
        
        // Set properties for cancel button in DropDownview
        autoCorrectDropDownView.cancelButton.setTitle("Cancel", for: UIControlState.normal)
        autoCorrectDropDownView.cancelButton.backgroundColor = UIColor.white
        autoCorrectDropDownView.cancelButton.setTitleColor(UIColor.init(red: 24.0/255.0, green: 0.0/255.0, blue: 254.0/255.0, alpha: 1.0), for: UIControlState.normal)
        autoCorrectDropDownView.cancelButton.titleLabel?.font = UIFont.init(name: "Arial", size: 15.0)
        autoCorrectDropDownView.cancelButton.layer.cornerRadius = 10.0
        autoCorrectDropDownView.addSubview(autoCorrectDropDownView.dropDownDataTableView)
        autoCorrectDropDownView.addSubview(autoCorrectDropDownView.cancelButton)
        autoCorrectDropDownView.cancelButton.addTarget(autoCorrectDropDownView, action: #selector(clickCancel(_:)), for: UIControlEvents.touchUpInside)
        
        autoCorrectDropDownView.dropDownListSearch = UISearchBar.init() // Initialize search bar in dropdownview
        autoCorrectDropDownView.allowSearching = true
        
        return autoCorrectDropDownView
        
    }()
    
    //MARK: - Did click Cancel Button
    func clickCancel(_ sender: Any)  {
        
        super.didClickCancel(sender)
        NetworkManager.sharedInstance.invalidateURLSession()
        
    }
    
    //MARK: - Show DropDownList for auto populate view
    override func showDropDownForData(_ dataArray: NSMutableArray, _ dropDownTypeValue: DropDownType, _ displayFrame: CGRect, _ parentView: UIView, _ delegateObject: DropDownViewDelegate, _ selectedModelID: NSMutableArray) {
        
        var showDisplayFrame = displayFrame
        super.isAutoCorrectView = true
        super.showDropDownForData(dataArray, dropDownTypeValue, displayFrame, parentView, delegateObject, selectedModelID)
        
        showDisplayFrame = UIScreen.main.bounds
        
        let availableHight = showDisplayFrame.size.height - 65.0
        self.dropDownDataTableView.frame = CGRect.init(x: 5.0, y: 55.0, width: showDisplayFrame.size.width - 10.0, height: availableHight - 100.0)
        self.dropDownListSearch?.frame = CGRect.init(x: 5.0, y: 5.0, width: showDisplayFrame.size.width - 10.0 , height: 50.0)
        self.dropDownListSearch?.searchBarStyle = UISearchBarStyle.default
        self.dropDownListSearch?.showsCancelButton = true
        self.dropDownListSearch?.placeholder = "Search"
        self.addSubview(self.dropDownListSearch!)
        
        self.dropDownListSearch?.delegate = self
        self.cancelButton.frame = CGRect.init(x: 5.0, y: showDisplayFrame.size.height - 55.0, width: showDisplayFrame.size.width - 10.0, height: 45.0)
        // self.okButton.frame = CGRect.init(x: 5.0, y: showDisplayFrame.size.height - 105.0, width: showDisplayFrame.size.width - 10.0, height: 45.0)
        //self.addSubview(self.okButton)
        
        self.dropDownListSearch?.isHidden = false
        self.isSearching = true
        self.dropDownListSearch?.becomeFirstResponder()
        
    }
    
    
    //MARK: - Searchbar delegate methods
    func searchBar(_ searchBar: UISearchBar, shouldChangeTextIn range: NSRange, replacementText text: String) -> Bool {
     
        return ((searchBar.text?.characters.count)! + text.characters.count - range.length <= 100)
    }
    
    override func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
     
        // Based on the search text count the DropDownList will modified accordingly.
        if searchText.characters.count > 0 {
            
            self.isSearching = true
            defaultValue = searchText
            
            // Use webservice fetch the values that needs to be displayed in the DropDown List
            //LoadingView.shareInstance.showLodingView(parentView: self)
            NetworkManager.sharedInstance.invalidateURLSession()
            NetworkManager.sharedInstance.createConnectionAndProcessWebServiceRequest(.ServiceTypeGetDropDownValues, self)
            
        }
        else
        {
            // If the search text count is 0 then load the DropDownList with the default values.
            self.isSearching = false
            self.tableViewDataArray?.removeAllObjects()
            self.tableViewSearchDataArray?.removeAllObjects()
            
            self.dropDownDataTableView.reloadData()
            
            if (self.dropDownListSearch?.isFirstResponder)! {
                
                self.keyboardWillShow(self.keyBoardNotification! as Notification)
            }
        }
    }
    
    
    //MARK: - Webservice response delegate method
    func webServiceResponse(for requestType: ServiceRequestType, isSuccess: Bool, responseData Data: Any!, withStatusCode statusCode: Int, errorMessage message: Any!) {
        
        print("Response Data\(Data)")
        
        var conditionArray:NSArray = NSArray()
        
        //Check the condition array count (Conditon array contains dropdown values fetched from webservice response)
        if conditionArray.count > 0 {
            
            // Using the condition array data load the DropDwonTableView.
            self.tableViewDataArray = conditionArray as? NSMutableArray
            self.tableViewSearchDataArray = conditionArray as? NSMutableArray
            
            // Update the UI using
            DispatchQueue.main.async(execute: {
                
                self.dropDownDataTableView.reloadData()
                if (self.dropDownListSearch?.isFirstResponder)!
                {
                    self.keyboardWillShow(self.keyBoardNotification!)
                }
                
            })
            
        }
        else
        {
            // If the condition array doesn't contain any values then add the typed value in searchfield and reload the DropDownTable.
            self.tableViewDataArray = nil;
            self.tableViewSearchDataArray = nil;
            
            conditionArray = self.getCustomResponse(requestType: .ServiceTypeGetDropDownValues, dataArray: defaultValue!)
            self.tableViewDataArray = conditionArray as? NSMutableArray
            self.tableViewSearchDataArray = conditionArray as? NSMutableArray
            
            // Update the UI asynchronously
            DispatchQueue.main.async(execute: {
                
                self.dropDownDataTableView.reloadData()
                if (self.dropDownListSearch?.isFirstResponder)!
                {
                    self.keyboardWillShow(self.keyBoardNotification!)
                }
                
            })
            
        }
        
        if isSuccess {
            
            
            print("Success respone")
        }
        else
        {
            self.showAlertMessage("Error in web-service response. Select the auto populated value")
        }
    }
    
    // MARK: -  Show Alert Message
    func showAlertMessage(_ messageString:String)
    {
        DispatchQueue.main.async {
            
            self.alertView = UIAlertController(title: "Alert", message: "\(messageString)", preferredStyle: UIAlertControllerStyle.alert)
            //...
            self.alertView?.addAction(UIAlertAction(title: "OK", style: UIAlertActionStyle.cancel, handler:{ (ACTION :UIAlertAction!)in
            }))
            
            UIApplication.shared.keyWindow?.rootViewController?.present(self.alertView!, animated: true, completion: nil)
            //self.present(alertView!, animated: false, completion: nil)
        }
        
    }
    
    //MARK: - Get custom response for User searched text
    override func getCustomResponse(requestType:ServiceRequestType, dataArray:String) -> NSMutableArray {
        
        // If there is no response from webservice then load the user searched text to show in the DropDownList
        let customsArray:NSMutableArray = NSMutableArray()
        let customObject = DropDownModel()
        customObject.modelDataString =  dataArray as NSString
        customObject.modelIdString =  "-1"
        customsArray.add(customObject)
        return customsArray
    }
    
    
    
    /*
    // Only override draw() if you perform custom drawing.
    // An empty implementation adversely affects performance during animation.
    override func draw(_ rect: CGRect) {
        // Drawing code
    }
    */

}
