//
//  RequestDataModel.swift
//  MatchRX
//
//  Created by Vignesh on 11/16/17.
//  Copyright © 2017 HTC. All rights reserved.
//

import UIKit

class RequestDataModel: NSObject {
    static let sharedInstance = RequestDataModel()
    
    //Serializes the dictionary to String
    func getRequestString(requestDictionary: NSMutableDictionary) -> String{
        var requestString: String = ""
        do{
            let requestData: NSData = try JSONSerialization.data(withJSONObject: requestDictionary, options: []) as NSData
            requestString = String.init(data: requestData as Data, encoding: String.Encoding.utf8)!
        }
        catch{
            print("Error occured")
        }
        return requestString
    }
    
    //Creates the login request data
    func createRequestForLogin(loginModel: LoginDataModel) -> String{
        let loginDict = NSMutableDictionary()
        loginDict.setValue(loginModel.deaNumber, forKey: "dea")
        loginDict.setValue(loginModel.userName, forKey: "username")
        loginDict.setValue(loginModel.password, forKey: "password")
        return getRequestString(requestDictionary: loginDict)
    }
    
    //Creates the relogin request data
    func createRequestForReLogin(refreshToken: String) -> String {
        let reloginDict = NSMutableDictionary()
        reloginDict.setValue(refreshToken, forKey: "refresh_token")
        return getRequestString(requestDictionary: reloginDict)
    }
    
    //Creates the forgot password request data
    func forgotPassword(forgotPwdModel: ForgotPasswordDataModel) -> String{
        let forgotPwdDict = NSMutableDictionary()
        forgotPwdDict.setValue(forgotPwdModel.deaNumber, forKey: "dea")
        forgotPwdDict.setValue(forgotPwdModel.userName, forKey: "username")
        return getRequestString(requestDictionary: forgotPwdDict)
    }
    
    //Creates the pharmacy information request data
    func getPharmacyInformation() -> String{
        return String(GlobalData.sharedInstance.pharmacyId!)
    }
    
    //Creates the NDC number verification request
    func makeRequestForNDCVerification(ndcNumber: String) -> String {
        let ndcDict = NSMutableDictionary()
        ndcDict.setValue(ndcNumber, forKey: "ndc_number")
        return getRequestString(requestDictionary: ndcDict)
    }
    
    //Creates the request model to fetch the data for Buy list
    func makeRequestForBuyList(requestDict: NSMutableDictionary) -> String {
        return getRequestString(requestDictionary: requestDict)
    }
    
    func makeRequestForMyPostingList(requestDict: NSMutableDictionary) -> String {
        return getRequestString(requestDictionary: requestDict)
    }
    
    func makeRequestAddItemInCart(requestDict: NSMutableDictionary) -> String {
        return getRequestString(requestDictionary: requestDict)
    }
    
    func makeRequestForUpdatingAnItem(requestDict: NSMutableDictionary) -> String {
        return getRequestString(requestDictionary: requestDict)
    }
    
    func makeRequestToSumbitOrder(requestDict: NSMutableDictionary) -> String {
        return getRequestString(requestDictionary: requestDict)
    }
    
    func makeRequestToFetchWishlist(requestDict: NSMutableDictionary) -> String {
        return getRequestString(requestDictionary: requestDict)
    }
    
    func makeRequestToDeleteFromWishlist(requestDict: NSMutableDictionary) -> String {
        return getRequestString(requestDictionary: requestDict)
    }
    
    func makeRequestToFetchMyOrders(requestDict: NSMutableDictionary) -> String {
        return getRequestString(requestDictionary: requestDict)
    }
    
    func makeRequestForSellerConfirmation(requestDict: NSMutableDictionary) -> String {
        return getRequestString(requestDictionary: requestDict)
    }
}
