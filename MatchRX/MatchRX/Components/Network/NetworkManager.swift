//
//  NetworkManager.swift
//  Sample
//
//  Created by selvakumar on 16/05/17.
//  Copyright © 2017 chandramouli. All rights reserved.
//

import UIKit
import CoreTelephony
import MapKit



// Create protocol for NetworkManager class
protocol NetworkManagerDelegate {
    
    func webServiceResponse(for requestType: ServiceRequestType, responseData Data: Any!, withStatusCode statusCode: Int, errorMessage message: Any!)
    
    
}
//Service request type.
enum ServiceRequestType {
    
    case ServiceTypeLogin
    case ServiceTypeReLogin
    case ServiceTypeForgotPassword
    case ServiceTypePharmacyInfo
    case ServiceTypeGetDropDownValues
    case ServiceTypeNDCVerification
    case ServiceTypeLOVDetails
    case ServiceTypePostAnItem
    case ServiceTypeUpdatePosting
    case ServiceTypeEditAnItem
    case ServiceTypeFetchItemDetails
    case ServiceTypeDeletePost
    case ServiceTypeGetImage
    case ServiceTypeBuyMarketplace
    case ServiceTypeMyPostings
    case ServiceTypeComparePricing
    case ServiceTypeAddToCart
    case ServiceTypeFetchMyCartDetails
    case ServiceTypeUpdateQuantityInCart
    case ServiceTypeDeleteItemInCart
    case ServiceTypeSubmitOrder
    case ServiceTypeFetchWislist
    case ServiceTypeAddToWishlist
    case ServiceTypeDeleteFromWishlist
    case ServiceTypeMyOrdersList
    case ServiceTypeOrderCount
    case ServiceTypeOrderDetails
    case ServiceTypeSellerConfirmation
}
// Create network model for background request processing
class NetworkRequestModel: NSObject {
    
    var networkRequestType: ServiceRequestType?
    var backgroundDelegate: NetworkManagerDelegate?
    var backgroundUrlString : String?
    
    
}

class NetworkManager: NSObject,CLLocationManagerDelegate {
    
    
    // Base url for the web service
    var devEnvironmentURL:String = "https://matchrx.htcindia.com/matchrxv3/mrx/api/v3/"     //----Development URL   https://matchrx.htcindia.com/mrx/api/v3/
    var qaEnvirionmentURL:String = "https://matchrxqa.htcindia.com/mrx/matchrxv3/api/v3/"     //----QA URL  https://matchrxqa.htcindia.com/mrx/api/v3/
    var stagingURL:String = "https://mrx3.matchrx.com/matchrxv3/mrx/api/v3/"       //----Staging URL  https://mrx3.matchrx.com/mrx/api/v3/
    
    var webserviceURL = "https://matchrxqa.htcindia.com/matchrxv3/mrx/api/v3/"
//    var webserviceURL:String = "https://mrx3.matchrx.com/mrx/api/v3/"       //----Staging URL
//    var webserviceURL = "https://matchrx.htcindia.com/mrx/api/v3/"
    
    //Device details
    var deviceId : String?
    var deviceName : String?
    var deviceModel : String?
    var osVersion : String?
    var deviceUTCTime : String?
    var appBuildVersion : String?
    
    var networkIpAddress : String?
    var reachability = Reachability()
    var latLongValue : CLLocationCoordinate2D?
    var currentLocationState : String?
    var networkStatusResult : String?
    
    // To Check network types
    var isReachableViaWiFi : Bool!
    var isReachableViaCellular : Bool!
    var isNotReachable : Bool!
    
    var sessionDelegate:NetworkManagerDelegate? // Current session delegate
    var currentRequestType : ServiceRequestType? // Current service type request
    
    var currentUrlSession :URLSession!
    var currentUrlSharedSession :URLSession!
    
    // Creating singleton object for NetworkManager class and initial
    static let sharedInstance : NetworkManager = {
        var networkManagerObj = NetworkManager()
        networkManagerObj.initiateNetworkModels()
        
        if !networkManagerObj.isReachable()
        {
            print("Network not available")
        }
        return networkManagerObj
        
    }()
    var locationManager:CLLocationManager? = CLLocationManager()   // To get the current location
    var networkRequestModelObj : NetworkRequestModel?
    var backgroundRequestDictionary:NSMutableDictionary = NSMutableDictionary()
    var deviceDetailsDictionary:NSMutableDictionary = NSMutableDictionary()
    
    //MARK: - Initiate Required Models
    func initiateNetworkModels()  {
        
        self.backgroundRequestDictionary = NSMutableDictionary()
        self.findCurrentLocationIdentifier()
    }
    
    //MARK: - Check internet connection
    func checkTypeOfNetwork() -> String {
        
        NotificationCenter.default.addObserver(self, selector: #selector(reachabilityChanged(notificationObj:)), name:ReachabilityChangedNotification, object: nil) // Notify if there is any change in the network
        
        do {
            try reachability?.startNotifier()
            
            let networkStatus:Reachability.NetworkStatus = reachability!.currentReachabilityStatus // Get the current network status
            
            //Check which type of network is available
            switch networkStatus{
            case .notReachable:
                networkStatusResult = "Not Reachable"
                print("Network Status..\(String(describing: networkStatusResult))")
                self.isReachableViaWiFi = false
                self.isReachableViaCellular = false
                self.isNotReachable = true
                break
            case .reachableViaWiFi:
                networkStatusResult = "Wi-Fi"
                print("Network Status..\(String(describing: networkStatusResult))")
                self.isReachableViaWiFi = true
                self.isReachableViaCellular = false
                self.isNotReachable = false
                break
            case .reachableViaWWAN:
                networkStatusResult = "Cellular"
                print("Network Status..\(String(describing: networkStatusResult))")
                self.isReachableViaCellular = true
                self.isReachableViaWiFi = false
                self.isNotReachable = false
                break
            default:
                print("Network Status.")
                networkStatusResult = "Not Reachable"
                break
            }
            
        } catch  {
            
            print("Error in network")
        }
        
        return networkStatusResult!
    }
    
    //MARK: - Check the reachability of the network connection
    func isReachable() -> Bool {
        
        return self.reachability?.currentReachabilityStatus != .notReachable
    }
    
    //MARK: - Reachability Change Notification Method
    func reachabilityChanged(notificationObj: NSNotification) {
        let reachabilityStatus = notificationObj.object as! Reachability     // Reachability status from notification object
        if reachabilityStatus.isReachable {
            if reachabilityStatus.isReachableViaWiFi {
                print("Reachable via WiFi")
            } else {
                print("Reachable via Cellular")
            }
        } else {
            print("Network not reachable")
        }
    }
    
    //MARK: - Find the current location
    func findCurrentLocationIdentifier()  {
        
        if CLLocationManager.locationServicesEnabled()
        {
            if locationManager == nil
            {
                locationManager = CLLocationManager()
            }
            locationManager!.delegate = self
            locationManager!.desiredAccuracy = kCLLocationAccuracyNearestTenMeters
            locationManager?.requestWhenInUseAuthorization()
            locationManager!.startUpdatingLocation()
            switch(CLLocationManager.authorizationStatus()) {
                
            case .authorizedAlways, .authorizedWhenInUse:
                locationManager!.startUpdatingLocation()
                break
            default:
                break
            }
            
        }
        
    }
    
    //MARK: - Get IP Address
    func getNetworkIPAddress() -> String? {
        var address : String?
        
        // Get list of all interfaces on the local machine:
        var ifaddr : UnsafeMutablePointer<ifaddrs>?
        guard getifaddrs(&ifaddr) == 0 else { return nil }
        guard let firstAddr = ifaddr else { return nil }
        
        // For each interface ...
        for ifptr in sequence(first: firstAddr, next: { $0.pointee.ifa_next }) {
            let interface = ifptr.pointee
            
            // Check for IPv4 or IPv6 interface:
            let addrFamily = interface.ifa_addr.pointee.sa_family
            if addrFamily == UInt8(AF_INET) || addrFamily == UInt8(AF_INET6) {
                
                // Check interface name:
                let name = String(cString: interface.ifa_name)
                if  name == "en0" {
                    
                    // Convert interface address to a human readable string:
                    var addr = interface.ifa_addr.pointee
                    var hostname = [CChar](repeating: 0, count: Int(NI_MAXHOST))
                    getnameinfo(&addr, socklen_t(interface.ifa_addr.pointee.sa_len),
                                &hostname, socklen_t(hostname.count),
                                nil, socklen_t(0), NI_NUMERICHOST)
                    address = String(cString: hostname)
                }
            }
        }
        freeifaddrs(ifaddr)
        networkIpAddress = address
        return address
    }
    
    //MARK: - Get Link with the parameters for the request
    func getURLForRequest(requestType: ServiceRequestType, requestString: String) -> String{
        var serviceName : String?
        switch requestType {
        case .ServiceTypeLogin:
            serviceName = "users/login"
            break
            
        case .ServiceTypeReLogin:
            serviceName = "users/bypasslogin"
            break
            
        case .ServiceTypeForgotPassword:
            serviceName = "users/forgotpassword"
            break
            
        case .ServiceTypePharmacyInfo:
            serviceName = "users/\(requestString)"
            break
            
        case .ServiceTypeNDCVerification:
            serviceName = "posts/ndc_verify"
            break
            
        case .ServiceTypeLOVDetails:
            serviceName = "posts/shippinglov"
            break
            
        case .ServiceTypePostAnItem:
            serviceName = "posts"
            break
            
        case .ServiceTypeUpdatePosting:
            serviceName = "posts"
            break
            
        case .ServiceTypeEditAnItem:
            serviceName = "posts/edit/\(requestString)"
            break
            
        case .ServiceTypeFetchItemDetails:
            serviceName = "posts/detail/\(requestString)"
            break
            
        case .ServiceTypeDeletePost:
            serviceName = "posts/delete/\(requestString)"
            break
            
        case .ServiceTypeGetImage:
            serviceName = "posts/image/\(requestString)"
            break
            
        case .ServiceTypeBuyMarketplace:
            serviceName = "posts/getallpost"
            break
            
        case .ServiceTypeMyPostings:
            serviceName = "posts/getmypost"
            break
            
        case .ServiceTypeAddToCart:
            serviceName = "shoppingcart"
            break
            
        case .ServiceTypeComparePricing:
            serviceName = "posts/compare/\(GlobalData.sharedInstance.comparePricePostId!)"
            break
            
        case .ServiceTypeFetchMyCartDetails:
            serviceName = "checkout"
            break
            
            
        case .ServiceTypeUpdateQuantityInCart:
            serviceName = "checkout"
            break
            
        case .ServiceTypeDeleteItemInCart:
            serviceName = "checkout/delete"
            break
            
        case .ServiceTypeSubmitOrder:
            serviceName = "order"
            break
            
        case .ServiceTypeFetchWislist:
            serviceName = "wishlists"
            break
            
        case .ServiceTypeDeleteFromWishlist:
            serviceName = "wishlistdelete"
            break
            
        case .ServiceTypeAddToWishlist:
            serviceName = "addwishlist"
            break
            
        case .ServiceTypeMyOrdersList:
            serviceName = "order/actionorders"
            break
            
        case .ServiceTypeOrderCount:
            serviceName = "order/getOrdersCount"
            break
            
        case .ServiceTypeOrderDetails:
            serviceName = "order/detail/\(requestString)"
            break
            
        case .ServiceTypeSellerConfirmation:
            serviceName = "order"
            break
            
        default:
            break
        }
        let requestURLString:String = "\(webserviceURL)\(serviceName!)"  //Full webservice link with parameters
        return requestURLString
    }
    
    //MARK: - Get webservice link with the parameters available for the request
    func getLinkForRequestType(_ requestType : ServiceRequestType) -> String {
        var serviceName :String?
        switch requestType {
        case .ServiceTypeLogin:
            serviceName = "login"
            break
        default:
            break
        }
        
        let finalServiceRequet:String = "\(webserviceURL)\(serviceName!)"  //Full webservice link with parameters
        return finalServiceRequet
    }
    
    //Creates the request URL with required header parameters and the body parameters.
    func prepareAPIRequest(_ requestType: ServiceRequestType, _ requestData: String) -> URLRequest{
        
        
        let accessToken: String = "Bearer \( GlobalData.sharedInstance.accessToken ?? "")"
        let callURL = URL.init(string: self.getURLForRequest(requestType: self.currentRequestType!, requestString: requestData))
        var request = URLRequest.init(url: callURL!)
        request.timeoutInterval = 120.0 // TimeoutInterval in Seconds
        request.cachePolicy = URLRequest.CachePolicy.reloadIgnoringLocalCacheData
//        request = self.getHeadersAndMethodsForRequestType(self.currentRequestType!, request)
        request.addValue("application/json", forHTTPHeaderField: "Content-Type")
        switch requestType {
        case .ServiceTypeLogin:
            request.httpMethod = "POST"
            request.httpBody = requestData.data(using: String.Encoding.utf8)
            break
            
        case .ServiceTypeReLogin:
            request.httpMethod = "POST"
            request.httpBody = requestData.data(using: String.Encoding.utf8)
            break
            
        case .ServiceTypeForgotPassword:
            request.httpMethod = "POST"
            request.httpBody = requestData.data(using: String.Encoding.utf8)
            break
            
        case .ServiceTypePharmacyInfo:
            request.httpMethod = "GET"
            request.addValue(accessToken, forHTTPHeaderField: "Authorization")
            break
            
        case .ServiceTypeNDCVerification:
            request.httpMethod = "POST"
            request.httpBody = requestData.data(using: String.Encoding.utf8)
            request.addValue(accessToken, forHTTPHeaderField: "Authorization")
            break
            
        case .ServiceTypeLOVDetails:
            request.httpMethod = "GET"
            request.addValue(accessToken, forHTTPHeaderField: "Authorization")
            break
            
        
        case .ServiceTypePostAnItem:
            request.httpMethod = "POST"
            break
            
        case .ServiceTypeUpdatePosting:
            request.httpMethod = "POST"
            break
            
        case .ServiceTypeEditAnItem:
            request.httpMethod = "GET"
            request.addValue(accessToken, forHTTPHeaderField: "Authorization")
            break
            
        case .ServiceTypeFetchItemDetails:
            request.httpMethod = "GET"
            request.addValue(accessToken, forHTTPHeaderField: "Authorization")
            break
            
        case .ServiceTypeDeletePost:
            request.httpMethod = "DELETE"
            request.addValue(accessToken, forHTTPHeaderField: "Authorization")
            break
            
        case .ServiceTypeGetImage:
            request.httpMethod = "GET"
            request.addValue(accessToken, forHTTPHeaderField: "Authorization")
            break
            
        case .ServiceTypeBuyMarketplace:
            request.httpMethod = "POST"
            request.httpBody = requestData.data(using: String.Encoding.utf8)
            request.addValue(accessToken, forHTTPHeaderField: "Authorization")
            break
            
        case .ServiceTypeMyPostings:
            request.httpMethod = "POST"
            request.httpBody = requestData.data(using: String.Encoding.utf8)
            request.addValue(accessToken, forHTTPHeaderField: "Authorization")
            break
            
        case .ServiceTypeAddToCart:
            request.httpMethod = "POST"
            request.httpBody = requestData.data(using: String.Encoding.utf8)
            request.addValue(accessToken, forHTTPHeaderField: "Authorization")
            break
            
        case .ServiceTypeComparePricing:
            request.httpMethod = "POST"
            request.httpBody = requestData.data(using: String.Encoding.utf8)
            request.addValue(accessToken, forHTTPHeaderField: "Authorization")
            break
            
            
        case .ServiceTypeFetchMyCartDetails:
            request.httpMethod = "GET"
            request.addValue(accessToken, forHTTPHeaderField: "Authorization")
            break
            
        case .ServiceTypeUpdateQuantityInCart:
            request.httpMethod = "PUT"
            request.httpBody = requestData.data(using: String.Encoding.utf8)
            request.addValue(accessToken, forHTTPHeaderField: "Authorization")
            break
            
        case .ServiceTypeDeleteItemInCart:
            request.httpMethod = "POST"
            request.httpBody = requestData.data(using: String.Encoding.utf8)
            request.addValue(accessToken, forHTTPHeaderField: "Authorization")
            break
            
        case .ServiceTypeSubmitOrder:
            request.httpMethod = "POST"
            request.httpBody = requestData.data(using: String.Encoding.utf8)
            request.addValue(accessToken, forHTTPHeaderField: "Authorization")
            break
            
        case .ServiceTypeFetchWislist:
            request.httpMethod = "POST"
            request.httpBody = requestData.data(using: String.Encoding.utf8)
            request.addValue(accessToken, forHTTPHeaderField: "Authorization")
            break
            
        case .ServiceTypeDeleteFromWishlist:
            request.httpMethod = "POST"
            request.httpBody = requestData.data(using: String.Encoding.utf8)
            request.addValue(accessToken, forHTTPHeaderField: "Authorization")
            break
            
        case .ServiceTypeAddToWishlist:
            request.httpMethod = "POST"
            request.httpBody = requestData.data(using: String.Encoding.utf8)
            request.addValue(accessToken, forHTTPHeaderField: "Authorization")
            break
            
        case .ServiceTypeMyOrdersList:
            request.httpMethod = "POST"
            request.httpBody = requestData.data(using: String.Encoding.utf8)
            request.addValue(accessToken, forHTTPHeaderField: "Authorization")
            break
            
        case .ServiceTypeOrderCount:
            request.httpMethod = "GET"
            request.addValue(accessToken, forHTTPHeaderField: "Authorization")
            break
            
        case .ServiceTypeOrderDetails:
            request.httpMethod = "GET"
            request.addValue(accessToken, forHTTPHeaderField: "Authorization")
            break
            
        case .ServiceTypeSellerConfirmation:
            request.httpMethod = "PUT"
            request.httpBody = requestData.data(using: String.Encoding.utf8)
            request.addValue(accessToken, forHTTPHeaderField: "Authorization")
            break
            
        default:
            break
        }
        return request
    }
    
    //MARK: - Add additonal values (Headers & Method type)for the request
    func getHeadersAndMethodsForRequestType(_ requestType : ServiceRequestType, _ urlRequest:URLRequest) -> URLRequest{
        
        var serviceUrlRequest = urlRequest
        
        // Adding values to web-service request and type of httpMethod
        switch requestType {
            
        case .ServiceTypeLogin:
            serviceUrlRequest.addValue("application/json", forHTTPHeaderField: "Content-Type")
            serviceUrlRequest.httpMethod = "POST"
            break
        default: break
            
        }
        return serviceUrlRequest
    }
    
    //Creates the reqeust to download image.
    func manageAPIRequestToDownloadImage(requestType:ServiceRequestType, requestString: String, delegate: Any?){
        self.sessionDelegate = delegate as! NetworkManagerDelegate?
        self.currentRequestType = requestType
        var config :URLSessionConfiguration!
        
        // Set up session
        config = URLSessionConfiguration.default
        currentUrlSession = URLSession(configuration: config)
        
        let urlRequest = self.prepareAPIRequest(self.currentRequestType!, requestString)
        
        do {
            let dataTask = currentUrlSession.dataTask(with: urlRequest) { (data,response,error) in
                
                let httpResponse = response as? HTTPURLResponse
                let responseModel = ResponseModel()
                responseModel.requestType = self.currentRequestType!
                responseModel.statusCode = httpResponse?.statusCode
                guard error == nil else {
                    print("\(String(describing: error))")
                    responseModel.error = error
                    responseModel.responseData = ""
                    if (responseModel.statusCode == nil) {
                        responseModel.statusCode = 1000
                    }
                    //                    self.sessionDelegate?.webServiceResponse(for: self.currentRequestType!, responseData: "", withStatusCode:(httpResponse?.statusCode)! , errorMessage: error)
                    self.performSelector(onMainThread: #selector(self.processResponse(responseModel:)), with: responseModel, waitUntilDone: false)
                    return
                }
                // Check whether response data is available
                guard let responseData = data else {
                    print("Error: did not receive data")
                    return
                }
                do {
                    //Parse the response as JSON
                    /*guard let resultJson = try JSONSerialization.jsonObject(with: responseData, options: []) as? /[String:AnyObject] else
                    {
                        //                        AlertControl.sharedInstance.presentAlertView(alertTitle: "Something went wrong with response. Invalid JSON format", alertMessage: "", delegate: nil)
                        responseModel.responseData = ""
                        responseModel.error = error
                        self.performSelector(onMainThread: #selector(self.processResponse(responseModel:)), with: responseModel, waitUntilDone: true)
                        //                        self.sessionDelegate?.webServiceResponse(for: self.currentRequestType!, responseData: "", withStatusCode:(httpResponse?.statusCode)! , errorMessage: error)
                        return
                    }*/
                    // Send the data to the respective class from which they made service request
                    responseModel.responseData = responseData
                    responseModel.error = error
                    self.performSelector(onMainThread: #selector(self.processResponse(responseModel:)), with: responseModel, waitUntilDone: false)
                    //                    self.sessionDelegate?.webServiceResponse(for: self.currentRequestType!, responseData: resultJson, withStatusCode:(httpResponse?.statusCode)! , errorMessage: error)
                    //                    print("Result",resultJson)
                    print("Next line to perform selector in Manage API in Network manager")
                    
                }
            }
            dataTask.resume()
            
        }
    }
    
    //MARK: - Create the URL request, hits the API
    func manageAPIRequest(requestType:ServiceRequestType, requestString: String, delegate: Any?){
        self.sessionDelegate = delegate as! NetworkManagerDelegate?
        self.currentRequestType = requestType
        var config :URLSessionConfiguration!
        
        // Set up session
        config = URLSessionConfiguration.default
        currentUrlSession = URLSession(configuration: config)
        
        let urlRequest = self.prepareAPIRequest(self.currentRequestType!, requestString)
        
        /*if self.currentRequestType == ServiceRequestType.ServiceTypeBuyMarketplace {
            let storyboard = UIStoryboard(name: "Main", bundle: nil)
            let appDelegate = UIApplication.shared.delegate as? AppDelegate
            appDelegate?.window?.rootViewController = storyboard.instantiateInitialViewController()
            AlertControl.sharedInstance.presentAlertView(alertTitle: "Your password has been changed", alertMessage: "", delegate: nil)
        }*/
        do {
            let dataTask = currentUrlSession.dataTask(with: urlRequest) { (data,response,error) in
                
                let httpResponse = response as? HTTPURLResponse
                let responseModel = ResponseModel()
                responseModel.requestType = self.currentRequestType!
                responseModel.statusCode = httpResponse?.statusCode
                guard error == nil else {
                    print("\(String(describing: error))")
                    responseModel.error = error
                    responseModel.responseData = ""
                    if (responseModel.statusCode == nil) {
                        responseModel.statusCode = 1000
                    }
//                    self.sessionDelegate?.webServiceResponse(for: self.currentRequestType!, responseData: "", withStatusCode:(httpResponse?.statusCode)! , errorMessage: error)
                    self.performSelector(onMainThread: #selector(self.processResponse(responseModel:)), with: responseModel, waitUntilDone: false)
                    return
                }
                // Check whether response data is available
                guard let responseData = data else {
                    print("Error: did not receive data")
                    return
                }
                do {
                    //Parse the response as JSON
                    guard let resultJson = try JSONSerialization.jsonObject(with: responseData, options: []) as? [String:AnyObject] else
                    {
//                        AlertControl.sharedInstance.presentAlertView(alertTitle: "Something went wrong with response. Invalid JSON format", alertMessage: "", delegate: nil)
                        responseModel.responseData = ""
                        responseModel.error = error
                        self.performSelector(onMainThread: #selector(self.processResponse(responseModel:)), with: responseModel, waitUntilDone: true)
//                        self.sessionDelegate?.webServiceResponse(for: self.currentRequestType!, responseData: "", withStatusCode:(httpResponse?.statusCode)! , errorMessage: error)
                        return
                    }
                    // Send the data to the respective class from which they made service request
                    responseModel.responseData = resultJson
                    responseModel.error = error
                    self.performSelector(onMainThread: #selector(self.processResponse(responseModel:)), with: responseModel, waitUntilDone: false)
//                    self.sessionDelegate?.webServiceResponse(for: self.currentRequestType!, responseData: resultJson, withStatusCode:(httpResponse?.statusCode)! , errorMessage: error)
//                    print("Result",resultJson)
                    print("Next line to perform selector in Manage API in Network manager")
                    
                } catch {
                    
                    responseModel.responseData = ""
                    responseModel.error = error
                    self.performSelector(onMainThread: #selector(self.processResponse(responseModel:)), with: responseModel, waitUntilDone: true)
//                    self.sessionDelegate?.webServiceResponse(for: self.currentRequestType!, responseData: "", withStatusCode:(httpResponse?.statusCode)! , errorMessage: error)
                    print("Error -> \(error)")
                }
            }
            dataTask.resume()
            
        }
    }
    
    func processResponse(responseModel: ResponseModel){
        print("Process response in Network manager")
        self.sessionDelegate?.webServiceResponse(for: responseModel.requestType, responseData: responseModel.responseData, withStatusCode:responseModel.statusCode! , errorMessage: responseModel.error)
    }
    
    //MARK: -  Create URL Session and process webservice request with the body parameter
    func createConnectionAndProcessWebServiceRequestWithBody(_ requestType:ServiceRequestType, _ requestData:NSDictionary, _ delegate:Any?)
    {
        self.sessionDelegate = delegate as! NetworkManagerDelegate?
        self.currentRequestType = requestType
        
        var config :URLSessionConfiguration!
        
        // Set up session
        config = URLSessionConfiguration.default
        currentUrlSession = URLSession(configuration: config)
        
        //Create url with the parameters
        guard var callURL = URL.init(string: self.getLinkForRequestType(self.currentRequestType!)) else
        {
            return
        }
//        guard var callURL = URL.init(string: self.getURLForRequest(requestType: self.currentRequestType!, requestString: "")) else
//        {
//            return
//        }
        
//        if self.currentRequestType == ServiceRequestType.ServiceTypeDeleterUserDetails {
//            var urlString: String = callURL.absoluteString
//            urlString = "\(urlString)/\(requestData["userID"] as! Int)"
//            callURL = URL(string: urlString)!
//        }
        
        // Set up web-service request
        var request = URLRequest.init(url: callURL)
        request.timeoutInterval = 120.0 // TimeoutInterval in Seconds
        request.cachePolicy = URLRequest.CachePolicy.reloadIgnoringLocalCacheData
//        request.addValue("application/json", forHTTPHeaderField: "Content-Type")
//        request.httpMethod = "POST"
        request = self.getHeadersAndMethodsForRequestType(self.currentRequestType!, request)
//        self.getHeadersAndMethodsForRequestType(self.currentRequestType!, request) // Get the full webservice link to process the request
        
        do {
            //Add body parameters
            let requestDataObj :NSData = try JSONSerialization.data(withJSONObject: requestData, options: []) as NSData
            let requestStringObj : String = String.init(data: requestDataObj as Data, encoding: String.Encoding.utf8)!
            request.httpBody = requestStringObj.data(using: String.Encoding.utf8)
            
            let dataTask = currentUrlSession.dataTask(with: request) { (data,response,error) in
                
                let httpResponse = response as? HTTPURLResponse
                
                guard error == nil else {
                    print("\(String(describing: error))")
                    return
                }
                // Check whether response data is available
                guard let responseData = data else {
                    print("Error: did not receive data")
                    return
                }
                do {
                    //Parse the response as JSON
                    guard let resultJson = try JSONSerialization.jsonObject(with: responseData, options: []) as? [String:AnyObject] else
                    {
                        return
                    }
                    // Send the data to the respective class from which they made service request
                    self.sessionDelegate?.webServiceResponse(for: self.currentRequestType!, responseData: resultJson, withStatusCode:(httpResponse?.statusCode)! , errorMessage: error)
                    print("Result",resultJson)
                    
                } catch {
                    
                    self.sessionDelegate?.webServiceResponse(for: self.currentRequestType!, responseData: "", withStatusCode:(httpResponse?.statusCode)! , errorMessage: error)
                    print("Error -> \(error)")
                }
            }
            
            dataTask.resume()
            
        } catch  {
            
            
        }
    }
    
    //MARK: - Create URL Session and process webservice request without body parameter
    func createConnectionAndProcessWebServiceRequest(_ requestType:ServiceRequestType, _ delegate:Any?)
    {
        self.sessionDelegate = delegate as! NetworkManagerDelegate?
        self.currentRequestType = requestType
        
        
        var config :URLSessionConfiguration!
        
        // Set up session
        config = URLSessionConfiguration.default
        currentUrlSession = URLSession(configuration: config)
        
        //Create url with the parameters
        guard let callURL = URL.init(string: self.getLinkForRequestType(self.currentRequestType!)) else
        {
            return
        }
        
        // Set up web-service request
        var request = URLRequest.init(url: callURL)
        request.timeoutInterval = 60.0 // TimeoutInterval in Seconds
        request.cachePolicy = URLRequest.CachePolicy.reloadIgnoringLocalCacheData
        self.getHeadersAndMethodsForRequestType(self.currentRequestType!, request) // Get the full webservice link to process the request
        
        let dataTask = currentUrlSession.dataTask(with: request) { (data,response,error) in
            
            let httpResponse = response as? HTTPURLResponse
            
            guard error == nil else {
                print("\(String(describing: error))")
                return
            }
            // Check whether response data is available
            guard let responseData = data else {
                print("Error: did not receive data")
                return
            }
            do {
                //Parse the response as JSON
                guard let resultJson = try JSONSerialization.jsonObject(with: responseData, options: []) as? [String:AnyObject] else
                {
                    return
                }
                print("1. Request Type...\(String(describing: self.currentRequestType))")
                // Send the data to the respective class from which they made service request
                self.sessionDelegate?.webServiceResponse(for: self.currentRequestType!, responseData: resultJson, withStatusCode:(httpResponse?.statusCode)! , errorMessage: error)
                print("Result",resultJson)
                
            } catch {
                
                print("Error -> \(error)")
                self.sessionDelegate?.webServiceResponse(for: self.currentRequestType!, responseData: "", withStatusCode:(httpResponse?.statusCode)! , errorMessage: error)
            }
        }
        
        dataTask.resume()
        
    }
    
    //MARK: - Create URL Session and process request to download
    func createConnectionAndProcessWebServiceRequestForDownload(_ requestType:ServiceRequestType,_ requestString: String, _ delegate:Any?)
    {
        self.sessionDelegate = delegate as! NetworkManagerDelegate?
        self.currentRequestType = requestType
        
        var config :URLSessionConfiguration!
        
        // Set up session
        config = URLSessionConfiguration.default
        currentUrlSession = URLSession(configuration: config)
        
        //Create url with the parameters
//        guard let callURL = URL.init(string: self.getLinkForRequestType(self.currentRequestType!)) else
//        {
//            return
//        }
        
        // Set up web-service request
//        var request = URLRequest.init(url: callURL)
//        request.timeoutInterval = 60.0 // TimeoutInterval in Seconds
//        request.cachePolicy = URLRequest.CachePolicy.reloadIgnoringLocalCacheData
        let urlRequest = self.prepareAPIRequest(self.currentRequestType!, requestString)
//        self.getHeadersAndMethodsForRequestType(self.currentRequestType!, request) // Get the full webservice link to process the request
        
        let downloadTasks = currentUrlSession.downloadTask(with: urlRequest) { (data,response,error) in
            
            let httpResponse = response as? HTTPURLResponse
            let responseModel = ResponseModel()
            responseModel.requestType = self.currentRequestType!
            responseModel.statusCode = httpResponse?.statusCode
            guard error == nil else {
                print("\(String(describing: error))")
                
                // Pass the error data
                // Send the data to the respective class from which they made service request
//                self.sessionDelegate?.webServiceResponse(for: self.currentRequestType!, responseData: "", withStatusCode:(httpResponse?.statusCode)! , errorMessage: error)
                responseModel.error = error
                responseModel.responseData = ""
                if (responseModel.statusCode == nil) {
                    responseModel.statusCode = 1000
                }
                //                    self.sessionDelegate?.webServiceResponse(for: self.currentRequestType!, responseData: "", withStatusCode:(httpResponse?.statusCode)! , errorMessage: error)
                self.performSelector(onMainThread: #selector(self.processResponse(responseModel:)), with: responseModel, waitUntilDone: false)
                return
            }
            // Check whether response data is available
            guard let responseData = data else {
                print("Error: did not receive data")
                return
            }
            // Send the data to the respective class from which they made service request
            responseModel.responseData = responseData
            responseModel.error = error
            self.performSelector(onMainThread: #selector(self.processResponse(responseModel:)), with: responseModel, waitUntilDone: false)
            print("Result",responseData)
        }
        
        downloadTasks.resume()
        
    }
    
    //MARK: - Create URL Session and process for multiple request in background
    func createConnectionAndProcessRequestInBackgroundSession(_ requestType:ServiceRequestType,_ requestModel:NetworkRequestModel, _ delegate:Any?) {
        
        var config :URLSessionConfiguration!
        
        self.currentRequestType = requestType
        // Set up session
        config = URLSessionConfiguration.default
        
        if currentUrlSharedSession == nil {
            
            currentUrlSharedSession = URLSession(configuration: config)
        }
        
        //Create url with the parameters
        guard let callURL = URL.init(string: requestModel.backgroundUrlString!) else
        {
            return
        }
        
        // Set up web-service request
        var request = URLRequest.init(url: callURL)
        request.timeoutInterval = 60.0 // TimeoutInterval in Seconds
        request.cachePolicy = URLRequest.CachePolicy.reloadIgnoringLocalCacheData
        self.getHeadersAndMethodsForRequestType(self.currentRequestType!, request) // Get the full webservice link to process the request
        
        let dataTask = currentUrlSharedSession.dataTask(with: request) { (data,response,error) in
            
            let httpResponse = response as? HTTPURLResponse
            var newRequestModel = NetworkRequestModel()
            
            let objectKey = response!.url?.absoluteString
            
            if (self.backgroundRequestDictionary.object(forKey: objectKey!) != nil)
            {
                newRequestModel = self.backgroundRequestDictionary.object(forKey: response?.url?.absoluteString as Any) as! NetworkRequestModel
            }
            
            
            // Get values from Dictionary to process the background operations
            //newRequestModel = self.backgroundRequestDictionary.object(forKey: response?.url?.absoluteString as Any) as! NetworkRequestModel
            guard error == nil else {
                print("\(String(describing: error))")
                return
            }
            // Check whether response data is available
            guard let responseData = data else {
                print("Error: did not receive data")
                return
            }
            do {
                //Parse the response as JSON
                guard let resultJson = try JSONSerialization.jsonObject(with: responseData, options: []) as? [String:AnyObject] else
                {
                    return
                }
                print("2. Request Type...\(String(describing: self.currentRequestType))")
                // Send the data to the respective class from which they made service request
                newRequestModel.backgroundDelegate?.webServiceResponse(for: self.currentRequestType!, responseData: resultJson, withStatusCode:(httpResponse?.statusCode)! , errorMessage: error)
                print("Result",resultJson)
                
            } catch {
                newRequestModel.backgroundDelegate?.webServiceResponse(for: self.currentRequestType!, responseData: "", withStatusCode:(httpResponse?.statusCode)! , errorMessage: error)
                print("Error -> \(error)")
            }
            
            self.performSelector(onMainThread: #selector(self.removeTopTask(_:)), with: response?.url?.absoluteString, waitUntilDone: true)
        }
        dataTask.resume()
        
    }
    
    //MARK: - Add background task
    func addBackgroundTaskWithRequestType(_ requestType:ServiceRequestType,_ delegate:Any?)  {
        
        // Create the new request model for background operations
        let newRequestModel : NetworkRequestModel = NetworkRequestModel()
        newRequestModel.networkRequestType = requestType
        newRequestModel.backgroundDelegate = delegate as! NetworkManagerDelegate?
        
        // Assign the request model in queue
        self.startNextTaskInQueue(newRequestModel)
    }
    
    //MARK: - Start next task in the queue
    func startNextTaskInQueue(_ requestModel:NetworkRequestModel) {
        
        guard let callURL = URL.init(string: self.getLinkForRequestType(requestModel.networkRequestType!)) else
        {
            return
        }
        
        requestModel.backgroundUrlString = callURL.absoluteString
        self.backgroundRequestDictionary.setObject(requestModel, forKey: callURL.absoluteString as NSCopying)
        self.createConnectionAndProcessRequestInBackgroundSession(requestModel.networkRequestType!, requestModel, requestModel.backgroundDelegate)
        
    }
    
    //MARK: - Remove the Task
    func removeTopTask(_ urlString:String) {
        
        if urlString != "" {
            
            self.backgroundRequestDictionary.removeObject(forKey: urlString)
        }
        
    }
    
    //MARK: -  Get device details
    func getDeviceDetails() -> NSDictionary {
        
        self.deviceId = UIDevice.current.identifierForVendor?.uuidString
        self.deviceName = UIDevice.current.name
        self.deviceModel = UIDevice.current.model
        self.osVersion = UIDevice.current.systemVersion
        self.deviceUTCTime = self.getCurrentUTCDateTime()
        self.appBuildVersion = Bundle.main.infoDictionary?["CFBundleShortVersionString"] as! String?
        
        let deviceDictionaryDetails:NSDictionary = ["deviceID":self.deviceId as Any,"deviceName":self.deviceName as Any,"deviceModel":self.deviceModel as Any,"osVersion":self.osVersion as Any,"deviceUTCTime":self.deviceUTCTime as Any,"appBuildVersion":self.appBuildVersion as Any]
        
        return deviceDictionaryDetails
        
    }
    
    
    //MARK: - Accessing status code
    func checkStatusCode(_ statusCode:Int) -> Bool {
        
        if statusCode == 200 {
            /*if self.currentRequestType == ServiceRequestType.ServiceTypeMyOrdersList {
                if NavigationModel.sharedInstance.homeControl != nil {
                    let storyboard = UIStoryboard(name: "Main", bundle: nil)
                    let appDelegate = UIApplication.shared.delegate as? AppDelegate
                    appDelegate?.window?.rootViewController = storyboard.instantiateInitialViewController()
                }
                AlertControl.sharedInstance.presentAlertView(alertTitle: "", alertMessage:
                    "You have been logged out due a recent password change. Please login again.", delegate: nil)
            }*/
            return true
        }
        else
        {
            return false
        }
    }
    
    //MARK: - Get current UTC Time
    func getCurrentUTCDateTime()-> String {
        let date = Date()
        let formatter = DateFormatter()
        formatter.calendar = Calendar(identifier: .iso8601)
        formatter.locale = Locale(identifier: "en_US_POSIX")
        formatter.timeZone = TimeZone(secondsFromGMT: 0)
        formatter.dateFormat = "yyyy-MM-dd HH:mm:ss"
        let currentDateTime = formatter.string(from: date)
        return currentDateTime
    }
    
    //MARK: - Location Manager delegate methods
    func locationManager(_ manager: CLLocationManager, didChangeAuthorization status: CLAuthorizationStatus) {
        switch(CLLocationManager.authorizationStatus()) {
            
        case .authorizedAlways, .authorizedWhenInUse:
            locationManager!.startUpdatingLocation()
            break
        default:
            break
        }
    }
    
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        
        let reverseGeoCoder:CLGeocoder = CLGeocoder()
        let locationObj:CLLocation = locations.last!
        self.latLongValue = locationObj.coordinate
        
        print("Lat Value..\(self.latLongValue?.latitude)..Long Value\(self.latLongValue?.longitude)")
        print(self.latLongValue?.latitude)
        if locations.count == 0{
            return;
        }
        reverseGeoCoder.reverseGeocodeLocation(locations[locations.count-1]) { (placeMarks:[CLPlacemark]?, error:Error?) in
            if let _:Error = error
            {
                print("Can't get location")
                
            }
            else
            {
                if (placeMarks?.count)! > 0
                {
                    self.currentLocationState = placeMarks?[0].administrativeArea
                    print("Current Location...\(self.currentLocationState!)")
                    
                }
            }
        }
        manager.stopUpdatingLocation()
        if locations.count > 0{
            manager.delegate = nil
        }
        
    }
    
    
    func locationManager(_ manager: CLLocationManager, didFailWithError error: Error) {
        manager.stopUpdatingLocation()
    }
    
    //MARK: - Invalidate URL session
    func invalidateURLSession()   {
        
        if (currentUrlSession != nil) {
            currentUrlSession.invalidateAndCancel()
            currentUrlSession = nil
        }
        
        if (currentUrlSharedSession != nil) {
            currentUrlSharedSession.invalidateAndCancel()
            currentUrlSharedSession = nil
        }
        
    }
    

    
    //MARK: - Handling multipart.
    
    //Generates the boundary string.
    func generateBoundaryString() -> String {
        return "Boundary-\(NSUUID().uuidString)"
    }
    
    //Creates a function to handle multi form data type.
    func manageAPIForMultiFormDataRequest(requestType:ServiceRequestType, requestParam: NSMutableDictionary, imageArray: NSMutableArray, delegate: Any?){
        self.sessionDelegate = delegate as! NetworkManagerDelegate?
        self.currentRequestType = requestType
        var config :URLSessionConfiguration!
        
        // Set up session
        config = URLSessionConfiguration.default
        currentUrlSession = URLSession(configuration: config)
        
        let boundary = self.generateBoundaryString()
        let accessToken: String = "Bearer \( GlobalData.sharedInstance.accessToken ?? "")"
        let callURL = URL.init(string: self.getURLForRequest(requestType: self.currentRequestType!, requestString: ""))
        var request = URLRequest.init(url: callURL!)
        request.addValue("multipart/form-data; boundary=\(boundary)", forHTTPHeaderField: "Content-Type")
        request.addValue(accessToken, forHTTPHeaderField: "Authorization")
        request.timeoutInterval = 120.0 // TimeoutInterval in Seconds
        request.cachePolicy = URLRequest.CachePolicy.reloadIgnoringLocalCacheData
        request.httpMethod = "POST"
        request.httpBody = self.createBodyWithBoundary(boundary as NSString, requestParam, imageArray) as Data
        
        
        do {
            let dataTask = currentUrlSession.dataTask(with: request) { (data,response,error) in
                
                let httpResponse = response as? HTTPURLResponse
                let responseModel = ResponseModel()
                responseModel.requestType = self.currentRequestType!
                responseModel.statusCode = httpResponse?.statusCode
                guard error == nil else {
                    print("\(String(describing: error))")
                    responseModel.error = error
                    responseModel.responseData = ""
                    if (responseModel.statusCode == nil) {
                        responseModel.statusCode = 1000
                    }
                    //                    self.sessionDelegate?.webServiceResponse(for: self.currentRequestType!, responseData: "", withStatusCode:(httpResponse?.statusCode)! , errorMessage: error)
                    self.performSelector(onMainThread: #selector(self.processResponse(responseModel:)), with: responseModel, waitUntilDone: false)
                    return
                }
                // Check whether response data is available
                guard let responseData = data else {
                    print("Error: did not receive data")
                    return
                }
                do {
                    //Parse the response as JSON
                    guard let resultJson = try JSONSerialization.jsonObject(with: responseData, options: []) as? [String:AnyObject] else
                    {
                        //                        AlertControl.sharedInstance.presentAlertView(alertTitle: "Something went wrong with response. Invalid JSON format", alertMessage: "", delegate: nil)
                        responseModel.responseData = ""
                        responseModel.error = error
                        self.performSelector(onMainThread: #selector(self.processResponse(responseModel:)), with: responseModel, waitUntilDone: true)
                        //                        self.sessionDelegate?.webServiceResponse(for: self.currentRequestType!, responseData: "", withStatusCode:(httpResponse?.statusCode)! , errorMessage: error)
                        return
                    }
                    // Send the data to the respective class from which they made service request
                    responseModel.responseData = resultJson
                    responseModel.error = error
                    self.performSelector(onMainThread: #selector(self.processResponse(responseModel:)), with: responseModel, waitUntilDone: false)
                    //                    self.sessionDelegate?.webServiceResponse(for: self.currentRequestType!, responseData: resultJson, withStatusCode:(httpResponse?.statusCode)! , errorMessage: error)
                    //                    print("Result",resultJson)
                    print("Next line to perform selector in Manage API in Network manager")
                    
                } catch {
                    
                    responseModel.responseData = ""
                    responseModel.error = error
                    self.performSelector(onMainThread: #selector(self.processResponse(responseModel:)), with: responseModel, waitUntilDone: true)
                    //                    self.sessionDelegate?.webServiceResponse(for: self.currentRequestType!, responseData: "", withStatusCode:(httpResponse?.statusCode)! , errorMessage: error)
                    print("Error -> \(error)")
                }
            }
            dataTask.resume()
            
        }
    }
    
    func createBodyWithBoundary(_ boundary:NSString,_ parameters:NSDictionary,_ imageArray: NSMutableArray) -> NSData {
        
        
        let httpBody:NSMutableData = NSMutableData.init()
        
        
        let mimeType = "imgae/png"
        
        for (key, value) in parameters
        {
            httpBody.append("--\(boundary)\r\n".data(using: String.Encoding.utf8)!)
            httpBody.append("Content-Disposition: form-data; name=\"\(key)\"\r\n\r\n".data(using: String.Encoding.utf8)!)
            httpBody.append("\(value)\r\n".data(using: String.Encoding.utf8)!)
        }
        
        
        for i in 0..<imageArray.count {
            let image = (imageArray[i] as! Array<Any>)[1] as! UIImage
            let imageData = UIImageJPEGRepresentation(image, 1.0)!
//            let data:NSData = NSData.init(data: (imageArray[i] as! Array<Any>)[1] as! Data)
            
//            let image1 = UIImage.init(named: "Upload_Btn.png")
//            let imageData1 = UIImageJPEGRepresentation(image1!, 1.0)
//            let imageData1 = UIImagePNGRepresentation(image1!)
            
            httpBody.append("--\(boundary)\r\n".data(using: String.Encoding.utf8)!)
            //        httpBody.append("Content-Disposition:form-data; name=\"\(fieldName)\"; filename=\"\(fieldName)\"\r\n".data(using: String.Encoding.utf8)!)
            httpBody.append("Content-Disposition:form-data; name=\"post_images[\(i)]\"; filename=\"\((imageArray[i] as! Array<Any>)[0] as! String)\"\r\n".data(using: String.Encoding.utf8)!)
//            httpBody.append("Content-Disposition:form-data; name=\"post_images[\(i)]\"; filename=\"imagei.png\"\r\n".data(using: String.Encoding.utf8)!)
            httpBody.append("Content-Type: \(mimeType)\r\n\r\n".data(using: String.Encoding.utf8)!)
            httpBody.append(imageData)
            httpBody.append("\r\n".data(using: String.Encoding.utf8)!)
            httpBody.append("--\(boundary)\r\n".data(using: String.Encoding.utf8)!)
        }
        
        
        return httpBody
    }
    
    func checkError(receivedData:Any?) -> ErrorModel {
        
        let errorModel = ErrorModel()
        if receivedData != nil
        {
            if let recievedDictionary:NSDictionary = receivedData as? NSDictionary{
                
                if let errorMessage = recievedDictionary["error"] as? String  {
                    errorModel.errorMessage = errorMessage
                }
                else if let errorMessage = recievedDictionary["message"] as? String {
                    errorModel.errorMessage = errorMessage
                }
                else if let errorstatus = recievedDictionary["status"] as? String {
                    errorModel.errorStatus = errorstatus
                }
            }
        }
        return errorModel
        
    }
    
    func getErrorModel(requestType:ServiceRequestType, httpStatusCode: Int, responseData: Any!) -> ErrorModel {
        let errorModel = ErrorModel()
        errorModel.errorStatus = "error"
        errorModel.errorCode = httpStatusCode
        switch httpStatusCode {
        case 401:
            let responseDictionary: NSDictionary = responseData as! NSDictionary
            errorModel.errorMessage = responseDictionary["message"] as? String ?? ""
            errorModel.errorMessage = "You have been logged out due a recent password change. Please login again."
            if NavigationModel.sharedInstance.homeControl != nil {
                let storyboard = UIStoryboard(name: "Main", bundle: nil)
                let appDelegate = UIApplication.shared.delegate as? AppDelegate
                appDelegate?.window?.rootViewController = storyboard.instantiateInitialViewController()
            }
//            AlertControl.sharedInstance.presentAlertView(alertTitle: "", alertMessage:
//                "You have been logged out due a recent password change. Please login again.", delegate: nil)
            break
            
        case 403:
//            let responseDictionary: NSDictionary = responseData as! NSDictionary
//            let response: NSDictionary = responseDictionary["data"] as! NSDictionary
            errorModel.errorMessage = "Forbidden"
            break
            
        case 404:
            let responseDictionary: NSDictionary = responseData as! NSDictionary
            errorModel.errorMessage = responseDictionary["message"] as? String ?? ""
            break
            
        case 420:
            let responseDictionary: NSDictionary = responseData as! NSDictionary
            let response: NSDictionary = responseDictionary["data"] as! NSDictionary
            if requestType == .ServiceTypeNDCVerification {
                if (((response["error"]) as? String) != nil) {
                    errorModel.errorMessage = response["error"] as? String ?? ""
                }
                else if (((response["error"]) as? NSDictionary) != nil){
                    let ndcNumberResponse: NSDictionary = response["error"] as! NSDictionary
                    errorModel.errorMessage = (ndcNumberResponse["ndc_number"] as! NSArray)[0] as? String ?? ""
                }
            }
            else if requestType == .ServiceTypeUpdatePosting {
                if (((response["error"]) as? NSDictionary) != nil) {
                    let errorResponse: NSDictionary = response["error"] as! NSDictionary
                    let errorMessage = errorResponse.allValues[0] as! NSArray
                    errorModel.errorMessage = errorMessage[0] as? String
//                    errorModel.errorMessage = errorMessage
//                    for values in errorResponse.allValues {
//                        let messageArray = values[0] as? String
//                    }
                }
            }
            else {
                errorModel.errorMessage = ""
            }
            break
            
        case 422:
            let responseDictionary: NSDictionary = responseData as! NSDictionary
            let response: NSDictionary = responseDictionary["data"] as! NSDictionary
            errorModel.errorMessage = response["message"] as? String ?? ""
            if requestType == .ServiceTypeNDCVerification {
                
                if (((response["error"]) as? String) != nil) {
                    errorModel.errorMessage = response["error"] as? String ?? ""
                }
                else if (((response["error"]) as? NSDictionary) != nil){
                    let ndcNumberResponse: NSDictionary = response["error"] as! NSDictionary
                    errorModel.errorMessage = (ndcNumberResponse["ndc_number"] as! NSArray)[0] as? String ?? ""
                }
            }
            else {
                errorModel.errorMessage = response["message"] as? String ?? ""
            }
            break
            
        case 1000:
            errorModel.errorMessage = "Request time out"
            break
            
        default:
            errorModel.errorMessage = "Something went wrong with the data"
            break
        }
        return errorModel
    }
    
    deinit {
        NotificationCenter.default.removeObserver(self)
        reachability?.stopNotifier()
    }
    
    
}
