	//
//  LoadingView.swift
//  DigiClinic
//
//  Created by Shyamchandar on 18/01/17.
//  Copyright © 2017 chandramouli. All rights reserved.
//

import UIKit

class LoadingView: UIView {

    /*
    // Only override draw() if you perform custom drawing.
    // An empty implementation adversely affects performance during animation.
    override func draw(_ rect: CGRect) {
        // Drawing code
    }
    */
    var isShown:Bool = false
    var whiteView: UIView?
    var messageLabel: UILabel?
    
    static let shareInstance = LoadingView(frame: CGRect.zero)
    
    let activityIndicator:UIActivityIndicatorView = UIActivityIndicatorView(activityIndicatorStyle: .white)
    
    func showLodingView(parentView:UIView) {
        self.isShown = true
        self.addSubview(activityIndicator)
        self.backgroundColor = UIColor(colorLiteralRed: 0, green: 0, blue: 0, alpha: 0.5)
        self.frame = UIScreen.main.bounds
        activityIndicator.center = CGPoint(x: self.frame.size.width/2.0, y: self.frame.size.height/2.0)
        activityIndicator.startAnimating()
        parentView.addSubview(self);
    }
    
    func showWhiteView(parentView: UIView){
        whiteView = UIView.init(frame: parentView.bounds)
        whiteView?.backgroundColor = UIColor.white
        parentView.addSubview(whiteView!)
        parentView.bringSubview(toFront: whiteView!)
    }
    
    func displayAlertMessage(message: String, parentView: UIView) {
        whiteView = UIView.init(frame: parentView.bounds)
        whiteView?.backgroundColor = UIColor.white
        parentView.addSubview(whiteView!)
        parentView.bringSubview(toFront: whiteView!)
        let alertLabel = UILabel.init()
        alertLabel.font = UIFont(name: "Roboto-Medium", size: 14.0)
        alertLabel.textColor = UIColor.lightGray
        alertLabel.text = message
        alertLabel.numberOfLines = 0
        alertLabel.frame = CGRect(x: 15.0, y: self.frame.size.height/2.0, width: self.frame.size.width-30.0, height: 20.0)
        alertLabel.textAlignment = NSTextAlignment.center
        alertLabel.sizeToFit()
        alertLabel.center = CGPoint(x: parentView.frame.size.width/2.0, y: parentView.frame.size.height/2.0)
        parentView.addSubview(alertLabel)
    }
    
    func hideAlertMessage() {
        for views in (whiteView?.subviews)! {
            views.removeFromSuperview()
        }
        whiteView?.removeFromSuperview()
        whiteView = nil
    }
    
    func hideWhiteView(){
        whiteView?.removeFromSuperview()
        whiteView = nil
    }
    
    func hideLoadingView() {
        self.performSelector(onMainThread: #selector(removeActivityIndicator), with: nil, waitUntilDone: false)
    }
    
    func removeActivityIndicator(){
        if self.isShown == true
        {
            self.isShown = false
            activityIndicator.stopAnimating()
            if self.superview != nil
            {
                self.removeFromSuperview()
            }
        }
    }

}
