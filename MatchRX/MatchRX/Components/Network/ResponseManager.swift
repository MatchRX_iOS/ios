//
//  ResponseManager.swift
//  MatchRX
//
//  Created by Vignesh on 11/20/17.
//  Copyright © 2017 HTC. All rights reserved.
//

import UIKit


class ResponseModel: NSObject{
    var requestType: ServiceRequestType!
    var responseData: Any?
    var statusCode: Int?
    var error: Error!
}

enum MyError : Error {
    case conversionError
}

enum Request {
    case LoginRequest
    case FetchCartRequest
    case MarketplaceRequest
}

enum ExceptionHandling: Error{
    case NotFoundError
}

class ResponseManager: NSObject {
    static let sharedInstance = ResponseManager()
    var request: Request!
    
    func getLoginResponseData(responseData: Any!) -> LoginResponseModel{
        let loginModel = LoginResponseModel()
        if responseData != nil {
            let responseDictionary: NSDictionary = responseData as! NSDictionary
            let response: NSDictionary = responseDictionary["data"] as! NSDictionary
            let pharmacyInfo: NSDictionary = response["pharmacy"] as! NSDictionary
            loginModel.userId = response["user_id"] as? Int
            loginModel.userName = response["username"] as? String ?? ""
            loginModel.firstName = response["first_name"] as? String ?? ""
            loginModel.lastName = response["last_name"] as? String ?? ""
//            loginModel.loggedUserName = "Hi, \(response["first_name"] as! String) \(response["last_name"] as! String)"
            loginModel.pharmacyId = pharmacyInfo["pharmacy_id"] as? Int
            loginModel.pharmacyName = pharmacyInfo["legal_business_name"] as? String ?? ""
            loginModel.pharmacyDEA = pharmacyInfo["dea"] as? String ?? ""
            loginModel.accessToken = response["access_token"] as? String ?? ""
            loginModel.refreshToken = response["refresh_token"] as? String ?? ""
            loginModel.landingPage = response["landing_page"] as? String ?? ""
            loginModel.notificationCount = response["notification_count"] as? Int
            loginModel.cartCount = response["cart_count"] as? Int ?? 0
            loginModel.salesInProgressCount = response["sales_inprogress_count"] as? Int
            loginModel.purchaseInProgressCount = response["purchase_inprogress_count"] as? Int
            loginModel.stateWiseRestrictionMessage = response["statewise_restriction_error"] as? String ?? ""
            
            let cartArray  = response["cart_sellers"] as? NSArray ?? NSArray.init()
            self.loadCartSellerDetails(cartArray: cartArray, request: .LoginRequest)
            /*let cartSellerArray = NSMutableArray.init()
            let postSellerArray = NSMutableArray.init()
            for i in 0..<cartArray.count {
                let cartData = cartArray.object(at: i) as! NSDictionary
                if cartData["seller_id"] is Int {
                    let cartSellerModel = CartSellerDataModel()
                    cartSellerModel.sellerId = cartData["seller_id"] as? Int ?? 0
                    cartSellerModel.sellerName = cartData["seller_display_name"] as? String ?? ""
                    cartSellerModel.isActive = cartData["active"] as? Bool ?? false
                    GlobalData.sharedInstance.marketplaceModel.cartArray.add(cartSellerModel)
                }
                
                
                let sellerDict = cartArray.object(at: i) as! NSDictionary
                let cartSellerDict = NSMutableDictionary.init()
                if sellerDict["seller_id"] is Int
                {
                    cartSellerDict.setValue(sellerDict["seller_id"] as? Int, forKey: "seller")
                    //                cartSellerDict.setValue(sellerDict["seller_id"] as? Int, forKey: "seller")
                    if (sellerDict["active"] as? Bool)! {
                        postSellerArray.add(cartSellerDict)
                        GlobalData.sharedInstance.sellerArray = postSellerArray
                    }
                    cartSellerArray.add(cartSellerDict)
                }
            }
            GlobalData.sharedInstance.cartArray = cartSellerArray*/
            
            
            let statewisePackageCondition = response["statewise_package_condition"] as? String ?? ""
            if statewisePackageCondition == "bothAllowed" {
                loginModel.stateWiseRestriction = .allowBoth
            }
            else if statewisePackageCondition == "onlySealedAllowed"{
                loginModel.stateWiseRestriction = .allowOnlySealed
            }
            else if statewisePackageCondition == "onlyNonSealedAllowed"{
                loginModel.stateWiseRestriction = .allowOnlyNonSealed
            }
            else{
                loginModel.stateWiseRestriction = .restrictBoth
            }
        }
        return loginModel
    }
    
    func loadCartSellerDetails(cartArray: NSArray, request: Request) {
        GlobalData.sharedInstance.marketplaceModel.cartArray.removeAllObjects()
        let cartSellerArray = NSMutableArray.init()
        let postSellerArray = NSMutableArray.init()
        for i in 0..<cartArray.count {
            let cartData = cartArray.object(at: i) as! NSDictionary
            let cartSellerDict = NSMutableDictionary.init()
            let cartSellerModel = CartSellerDataModel()
            if cartData["seller_id"] is Int {
                cartSellerModel.commonSellerId = String(cartData["seller_id"] as? Int ?? 0)
                cartSellerDict.setValue(cartData["seller_id"] as? Int, forKey: "seller")
            }
            else {
                cartSellerModel.commonSellerId = cartData["seller_id"] as? String ?? ""
                cartSellerDict.setValue(cartData["seller_id"] as? String, forKey: "seller")
            }
            cartSellerModel.sellerName = cartData["seller_display_name"] as? String ?? ""
            cartSellerModel.isActive = cartData["active"] as? Bool ?? false
            if cartData["is_last_active"] != nil {
                cartSellerModel.isLastActive = cartData["is_last_active"] as? Bool ?? false
            }
            GlobalData.sharedInstance.marketplaceModel.cartArray.add(cartSellerModel)
            
            if (cartData["active"] as? Bool)! {
//                if request != .MarketplaceRequest {
//                    GlobalData.sharedInstance.sellerArray = postSellerArray
//                }
                if cartData["seller_id"] is Int {
                    postSellerArray.add(cartSellerDict)
                    GlobalData.sharedInstance.sellerArray = postSellerArray
                }
            }
            cartSellerArray.add(cartSellerDict)
        }
        if cartArray.count == 0 {
            GlobalData.sharedInstance.sellerArray = postSellerArray
        }
        GlobalData.sharedInstance.cartArray = cartSellerArray
    }
    
    func forgotPasswordResponse(responseData: Any!) -> ForgotPasswordDataModel{
        let forgotPwdModel = ForgotPasswordDataModel()
        if responseData != nil {
            let responseDictionary: NSDictionary = responseData as! NSDictionary
            let response: NSDictionary = responseDictionary["data"] as! NSDictionary
            forgotPwdModel.responseMessage = response["message"] as? String ?? ""
        }
        return forgotPwdModel
    }
    
    func getPharmacyData(responseData: Any!) -> PharmacyDataModel{
        let pharmacyModel = PharmacyDataModel()
        if responseData != nil {
            let responseDictionary: NSDictionary = responseData as! NSDictionary
            let basicResponse = responseDictionary["data"] as! NSDictionary
            let response: NSDictionary = basicResponse["pharmacy"] as! NSDictionary
            let pharmacyInfo: NSDictionary = response["pharmacy_information"] as! NSDictionary
            let pharmacyOwnerInfo: NSDictionary = response["pharmacy_owner_info"] as! NSDictionary
            let pharmacyLicenseInfo: NSDictionary = response["pharmacy_license_info"] as! NSDictionary
            
            
            pharmacyModel.loggedUserFirstName = basicResponse["first_name"] as? String ?? ""
            pharmacyModel.loggedUserLastName = basicResponse["last_name"] as? String ?? ""
            pharmacyModel.userEmailID = basicResponse["email"] as? String ?? ""
            
            pharmacyModel.legalBusinessName = pharmacyInfo["legal_business_name"] as? String ?? ""
            pharmacyModel.doingBusinessName = pharmacyInfo["doing_business_as"] as? String ?? ""
            pharmacyModel.shippingAddress1 = pharmacyInfo["shipping_address1"] as? String ?? ""
            pharmacyModel.shippingAddress2 = pharmacyInfo["shipping_address2"] as? String ?? ""
            pharmacyModel.shippingCity = pharmacyInfo["shipping_city"] as? String ?? ""
            pharmacyModel.shippingState = pharmacyInfo["shipping_state"] as? String ?? ""
            pharmacyModel.shippingZipCode = pharmacyInfo["shipping_zip_code"] as? String ?? ""
            pharmacyModel.shippingPhone = pharmacyInfo["phone"] as? String ?? ""
            pharmacyModel.shippingFax = pharmacyInfo["fax"] as? String ?? ""
            pharmacyModel.mailingAddress1 = pharmacyInfo["mailing_address1"] as? String ?? ""
            pharmacyModel.mailingAddress2 = pharmacyInfo["mailing_address2"] as? String ?? ""
            pharmacyModel.mailingCity = pharmacyInfo["mailing_city"] as? String ?? ""
            pharmacyModel.mailingState = pharmacyInfo["mailing_state"] as? String ?? ""
            pharmacyModel.mailingZipCode = pharmacyInfo["mailing_zip_code"] as? String ?? ""
            
            pharmacyModel.ownerFirstName = pharmacyOwnerInfo["owner_first_name"] as? String ?? ""
            pharmacyModel.ownerLastName = pharmacyOwnerInfo["owner_last_name"] as? String ?? ""
            pharmacyModel.ownerMobileNumber = pharmacyOwnerInfo["owner_mobile"] as? String ?? ""
            pharmacyModel.ownerEmailID = pharmacyOwnerInfo["owner_email"] as? String ?? ""
            
            pharmacyModel.deaNumber = pharmacyLicenseInfo["dea"] as? String ?? ""
            pharmacyModel.ncpdpNumber = pharmacyLicenseInfo["ncpdp_number"] as? String ?? ""
            pharmacyModel.npiNumber = pharmacyLicenseInfo["npi_number"] as? Int
            pharmacyModel.stateLicenseNumber = pharmacyLicenseInfo["state_license_number"] as? String ?? ""
            
            let dateFormatter = DateFormatter()
            dateFormatter.dateFormat = "YYYY-mm-dd"
            let dateFormatter1 = DateFormatter()
            dateFormatter1.dateFormat = "mm/dd/YYYY"
            if (pharmacyLicenseInfo["dea_exp_date"] as? String ?? "").count > 0{
                let deaExpDate = dateFormatter.date(from: (pharmacyLicenseInfo["dea_exp_date"] as? String)!)
                pharmacyModel.deaExpiryDate = dateFormatter1.string(from: deaExpDate!)
            }
            else{
                pharmacyModel.deaExpiryDate = ""
            }
            if (pharmacyLicenseInfo["state_license_exp_date"] as? String ?? "").count > 0{
                let licenseExpDate = dateFormatter.date(from: (pharmacyLicenseInfo["state_license_exp_date"] as? String)!)
                pharmacyModel.licenseExpiryDate = dateFormatter1.string(from: licenseExpDate!)
            }
            else{
                pharmacyModel.licenseExpiryDate = ""
            }
            
            
            
//            pharmacyModel.deaExpiryDate = response["dea_exp_date"] as? String
//            pharmacyModel.licenseExpiryDate = response["state_license_exp_date"] as? String
        }
        return pharmacyModel
    }
    
    func getNDCDetails(responseData: Any!) -> Any{
        let ndcResponseModel = NDCDetailsDataModel()
        if responseData != nil {
            let responseDictionary: NSDictionary = responseData as! NSDictionary
            let responseData = responseDictionary["data"] as! NSDictionary
            let medispanData: NSDictionary = responseData["medispan_details"] as! NSDictionary
            let matchRXData: NSDictionary = responseData["matchrx_details"] as! NSDictionary
            let priceDetailsData: NSDictionary = medispanData["price_details"] as! NSDictionary
            ndcResponseModel.isSuccess = true
            ndcResponseModel.ndcNumber = medispanData["ndc_number"] as? String ?? ""
            ndcResponseModel.formattedNDCNumber = medispanData["formatted_id_number"] as? String ?? ""
            ndcResponseModel.ndcName = medispanData["product_title"] as? String ?? ""
            ndcResponseModel.medispanImageURL = medispanData["image_url"] as? String ?? ""
            ndcResponseModel.medispanImageName = medispanData["image_name"] as? String ?? ""
            ndcResponseModel.packageStrength = medispanData["metric_strength"] as? NSNumber
            ndcResponseModel.strengthUnitOfMeasure = medispanData["strength_unit_of_measure"] as? String ?? ""
            ndcResponseModel.storageCondition = medispanData["storage_condition_code"] as? String ?? ""
            ndcResponseModel.packageType = medispanData["package_type"] as? String ?? ""
            ndcResponseModel.packageSize = medispanData["package_size"] as? String ?? ""
//            ndcResponseModel.maximumumPackageCount = Int(medispanData["total_package_quantity"] as? String ?? "0")!
            ndcResponseModel.maximumumPackageCount = medispanData["total_package_quantity"] as? Int ?? 0
            ndcResponseModel.packageQuantity = medispanData["package_quantity"] as? Int ?? 0
            ndcResponseModel.packageUnitOfMeasure = medispanData["package_size_unit_of_measure"] as? String ?? ""
            ndcResponseModel.packageDosageForm = medispanData["dosage_form"] as? String ?? ""
            
            ndcResponseModel.packPrice = Double(priceDetailsData["package_price"] as? String ?? "0.00")
            ndcResponseModel.unitPrice = Double(priceDetailsData["unit_price"] as? String ?? "0.00")
            ndcResponseModel.priceType = medispanData["price_type"] as? String ?? ""
            
            /*ndcResponseModel.aawpPackPrice = Double(medispanData["awp_package_price"] as? String ?? "0.00")
            ndcResponseModel.aawpUnitPrice = Double(medispanData["awp_unit_price"] as? String ?? "0.00")
            ndcResponseModel.wacPackPrice = Double(medispanData["wac_package_price"] as? String ?? "0.00")
            ndcResponseModel.wacUnitPrice = Double(medispanData["wac_unit_price"] as? String ?? "0.00")*/
            
//            ndcResponseModel.similarItems = Int(matchRXData["similar_items"] as? String ?? "0")
            ndcResponseModel.similarItems = matchRXData["similar_items"] as? Int ?? 0
            ndcResponseModel.packAvailable = Int(matchRXData["package_available"] as? String ?? "0")
//            ndcResponseModel.packAvailable = matchRXData["package_available"] as? Int ?? 0
            ndcResponseModel.isPartialPackExists = matchRXData["partialpackExists"] as? Bool
            ndcResponseModel.partialPackMessage = matchRXData["partialpackerrorMessage"] as? String ?? ""
            ndcResponseModel.isNonSealedPartialExists = matchRXData["nonsealedpackExists"] as? Bool
            ndcResponseModel.nonSealedPartialErrorMessage = matchRXData["nonsealedpackerrorMessage"] as? String ?? ""
//            ndcResponseModel.isNonSealedPartialExists = true
//            ndcResponseModel.nonSealedPartialErrorMessage = "Sample test message"
            ndcResponseModel.minimumPrice = matchRXData["minprice"] as? String ?? "0"
            ndcResponseModel.maximumPrice = matchRXData["maxprice"] as? String ?? "0"
            ndcResponseModel.unitPrice = Double(matchRXData["unit_price"] as? String ?? "0.00")
            ndcResponseModel.minimumExpiryDate = matchRXData["min_exp_date"] as? String ?? ""
            ndcResponseModel.maximumExpiryDate = matchRXData["max_exp_date"] as? String ?? ""
            ndcResponseModel.wacMaximumPrice = Double(matchRXData["wac_max_discount"] as? String ?? "0.00")
            ndcResponseModel.aawpMaximumPrice = Double(matchRXData["aawp_max_discount"] as? String ?? "0.00")
            ndcResponseModel.quantitySoldMessage = matchRXData["quantity_sold_message"] as? String ?? ""
            ndcResponseModel.stateRestrictionType = GlobalData.sharedInstance.stateWiseRestriction
            
            if ndcResponseModel.packageQuantity == 1 {
                ndcResponseModel.packaging = "\(ndcResponseModel.packageType!) (\(ndcResponseModel.packageSize!) \(ndcResponseModel.packageUnitOfMeasure!))"
            }
            else {
                ndcResponseModel.packaging = "\(ndcResponseModel.packageType!) (\(ndcResponseModel.packageSize!) \(ndcResponseModel.packageUnitOfMeasure!)) X \(ndcResponseModel.packageQuantity!) EA"
            }
            
            if ndcResponseModel.priceType == "wac" {
                ndcResponseModel.minimumDiscount = matchRXData["wac_min_discount"] as? String ?? ""
                ndcResponseModel.maximumDiscount = matchRXData["wac_max_discount"] as? String ?? ""
            }
            else {
                ndcResponseModel.minimumDiscount = matchRXData["aawp_min_discount"] as? String ?? ""
                ndcResponseModel.maximumDiscount = matchRXData["aawp_max_discount"] as? String ?? ""
            }
            
            let dateFormatter = DateFormatter()
            dateFormatter.dateFormat = "YYYY-mm-dd"
            let displayDateFormat = DateFormatter()
            displayDateFormat.dateFormat = "mm/YYYY"
            let minDate = dateFormatter.date(from: matchRXData["min_exp_date"] as? String ?? "")
            let maxDate = dateFormatter.date(from: matchRXData["max_exp_date"] as? String ?? "")
            var minExpDate = ""
            var maxExpDate = ""
            if minDate != nil {
                minExpDate = displayDateFormat.string(from: minDate!)
            }
            if maxDate != nil {
                maxExpDate = displayDateFormat.string(from: maxDate!)
            }
//            ndcResponseModel.minimumExpiryDate = minExpDate
//            ndcResponseModel.maximumExpiryDate = maxExpDate
            
            ndcResponseModel.minimumExpiryDate = matchRXData["min_exp_date"] as? String ?? ""
            ndcResponseModel.maximumExpiryDate = matchRXData["max_exp_date"] as? String ?? ""
            
        }
        else{
            ndcResponseModel.isSuccess = false
        }
        
        return ndcResponseModel
    }
    
    func getLOVDetails(responseData: Any!) -> ShippingLOVDataModel {
        let LOVDataModel = ShippingLOVDataModel()
        if responseData != nil {
            let responseDictionary: NSDictionary = responseData as! NSDictionary
            let responseData = responseDictionary["data"] as! NSDictionary
//            let shippingMethodsData = responseData["shipping_methods"] as! NSArray
//            let shippingHandlingData = responseData["shipping_handling"] as! NSArray
            
            let shippingMethodsData = NSMutableArray.init(array: responseData["shipping_methods"] as! NSArray)
            let shippingHandlingData = NSMutableArray.init(array: responseData["shipping_handling"] as! NSArray)
            
            let tempShippingMethodArray = NSMutableArray.init()
            let tempShippingHandlingArray = NSMutableArray.init()
            for i in 0..<shippingMethodsData.count {
                let shippingMethodModel = ShippingLOVDataModel()
                let shippingDict = shippingMethodsData[i] as! NSDictionary
                shippingMethodModel.shippingMethodId = shippingDict["shipping_id"] as? Int
                shippingMethodModel.shippingMethodTitle = shippingDict["title"] as? String ?? ""
                shippingMethodModel.shippingMethodName = shippingDict["name"] as? String ?? ""
                shippingMethodModel.shippingMethodShortName = shippingDict["short_name"] as? String ?? ""
                shippingMethodModel.shippingMethodIconURL = shippingDict["icon"] as? String ?? ""
                shippingMethodModel.shippingMethodSortOrder = shippingDict["sort_order"] as? Int
//                LOVDataModel.shippingMethodsArray.add(shippingMethodModel)
                tempShippingMethodArray.add(shippingMethodModel)
            }
            LOVDataModel.shippingMethodsArray.setArray(tempShippingMethodArray.sorted(by: {($0 as! ShippingLOVDataModel).shippingMethodSortOrder! < ($1 as! ShippingLOVDataModel).shippingMethodSortOrder!}))
            for j in 0..<shippingHandlingData.count {
                let shippingHandlingModel = ShippingLOVDataModel()
                let shippingDict = shippingHandlingData[j] as! NSDictionary
                shippingHandlingModel.shippingHandlingId = shippingDict["ship_handling_id"] as? Int
                shippingHandlingModel.shippingHandlingTitle = shippingDict["title"] as? String ?? ""
                shippingHandlingModel.shippingHandlingName = shippingDict["name"] as? String ?? ""
                shippingHandlingModel.shippingAmount = Float(shippingDict["amount"] as? String ?? "0.00")
                shippingHandlingModel.shippingHandlingSortOrder = shippingDict["sort_order"] as? Int
//                LOVDataModel.shippingHandlingArray.add(shippingHandlingModel)
                tempShippingHandlingArray.add(shippingHandlingModel)
            }
            LOVDataModel.shippingHandlingArray.setArray(tempShippingHandlingArray.sorted(by: {($0 as! ShippingLOVDataModel).shippingHandlingSortOrder! < ($1 as! ShippingLOVDataModel).shippingHandlingSortOrder!}))
        }
        return LOVDataModel
    }
    
    
    func fetchMyPostItem(responseData: Any!) -> Any{
        let myPostResponseModel = MyPostDataModel()
        if responseData != nil {
            let responseDictionary: NSDictionary = responseData as! NSDictionary
            let responseData = responseDictionary["data"] as! NSDictionary
            let myPosting: NSDictionary = responseData ["myposting"] as! NSDictionary
            let medispanData: NSDictionary = myPosting["medispan_details"] as! NSDictionary
            let matchRXData: NSDictionary = myPosting["matchrx_details"] as! NSDictionary
            let priceDetailsData: NSDictionary = medispanData["price_details"] as! NSDictionary
            myPostResponseModel.isSuccess = true
            myPostResponseModel.postId = myPosting["post_id"] as? Int
            myPostResponseModel.ndcName = myPosting["post_title"] as? String ?? ""
            myPostResponseModel.similarItems = matchRXData["similar_items"] as? Int ?? 0
            myPostResponseModel.ndcNumber = myPosting["ndc_number"] as? String ?? ""
            myPostResponseModel.lotNumber = myPosting["lot_number"] as? String ?? ""
            myPostResponseModel.expiryDate = myPosting["expire_date"] as? String ?? ""
            myPostResponseModel.packageOption = myPosting["package_option"] as? String ?? ""
            myPostResponseModel.packageUnit = myPosting["package_unit"] as? Int ?? 0
            myPostResponseModel.actualPackageSize = myPosting["actual_package_size"] as? Int ?? 0
            myPostResponseModel.optionalPackageCondition  = myPosting["condition"] as? String ?? ""
            myPostResponseModel.currentPrice = myPosting["current_price"] as? Double ?? 0.00
            myPostResponseModel.sellerNotes = myPosting["seller_notes"] as? String ?? ""
            myPostResponseModel.priceOption = myPosting["price_option"] as? String ?? ""
            myPostResponseModel.myMinPrice = myPosting["min_price"] as? Double ?? 0.00
            myPostResponseModel.myMaxPrice = myPosting["max_price"] as? Double ?? 0.00
            myPostResponseModel.revisedPrice = Double(myPosting["revised_price"] as? String ?? "0.00")
//            myPostResponseModel.currentPrice = 1.00
//            myPostResponseModel.myMinPrice = 12.04
            
            myPostResponseModel.updatedDate  = myPosting["updated_at"] as? String ?? ""
            myPostResponseModel.originalPackage = myPosting["original_package"] as? String ?? ""
            myPostResponseModel.shippingHandling = myPosting["shipping_handling_name"] as? String ?? ""
            myPostResponseModel.shippingMethod = myPosting["shipping_method"] as? String ?? ""
            
            myPostResponseModel.medispanImageURL = medispanData["image_url"] as? String ?? ""
            myPostResponseModel.packageStrength = myPosting["package_strength"] as? NSNumber
            myPostResponseModel.strengthUnitOfMeasure = medispanData["strength_unit_of_measure"] as? String ?? ""
            
//            myPostResponseModel.strengthToBeDisplayed = myPostResponseModel.packageStrength!+myPostResponseModel.strengthUnitOfMeasure!
            
            myPostResponseModel.storageCondition = myPosting["storage"] as? String ?? ""
            
            
            myPostResponseModel.packageType = myPosting["package_type"] as? String ?? ""
            myPostResponseModel.packageSize = myPosting["package_size"] as? Int ?? 0
            myPostResponseModel.medispanPackageSize = medispanData["package_size"] as? Int ?? 0
            myPostResponseModel.maximumumPackageCount = medispanData["total_package_quantity"] as? Int
            myPostResponseModel.packageQuantity = myPosting["package_quantity"] as? Int
            myPostResponseModel.availableQuantity = myPosting["available_quantity"] as? Int
            myPostResponseModel.medispanPackageQuantity = medispanData["package_quantity"] as? Int
            myPostResponseModel.packageUnitOfMeasure = medispanData["package_size_unit_of_measure"] as? String ?? ""
            myPostResponseModel.packageDosageForm = myPosting["package_form"] as? String ?? ""
            
            myPostResponseModel.packPrice = Double(priceDetailsData["package_price"] as? String ?? "0.00")
            myPostResponseModel.revisedPrice = Double(myPosting["revised_price"] as? String ?? "0.00")
            myPostResponseModel.unitPrice = Double(priceDetailsData["unit_price"] as? String ?? "0.00")
            myPostResponseModel.priceType = medispanData["price_type"] as? String ?? ""
            
            myPostResponseModel.packAvailable = Int(matchRXData["package_available"] as? String ?? "0")
            myPostResponseModel.isPartialPackExists = matchRXData["partialpackExists"] as? Bool
            myPostResponseModel.partialPackMessage = matchRXData["partialpackerrorMessage"] as? String ?? ""
            myPostResponseModel.isNonSealedPartialExists = matchRXData["nonsealedpackExists"] as? Bool
            myPostResponseModel.nonSealedPartialErrorMessage = matchRXData["nonsealedpackerrorMessage"] as? String ?? ""
            myPostResponseModel.minimumPrice = matchRXData["minprice"] as? String ?? "0"
            myPostResponseModel.maximumPrice = matchRXData["maxprice"] as? String ?? "0"
            myPostResponseModel.minimumExpiryDate = matchRXData["min_exp_date"] as? String ?? ""
            myPostResponseModel.maximumExpiryDate = matchRXData["max_exp_date"] as? String ?? ""
            myPostResponseModel.quantitySoldMessage = matchRXData["quantity_sold_message"] as? String ?? ""
            myPostResponseModel.stateRestrictionType = GlobalData.sharedInstance.stateWiseRestriction
            
            
            if myPostResponseModel.medispanPackageQuantity == 1 {
                myPostResponseModel.packaging = "\(myPostResponseModel.packageType!) (\(myPostResponseModel.medispanPackageSize!) \(myPostResponseModel.packageUnitOfMeasure!))"
            }
            else {
                myPostResponseModel.packaging = "\(myPostResponseModel.packageType!) (\(myPostResponseModel.medispanPackageSize!) \(myPostResponseModel.packageUnitOfMeasure!)) X \(myPostResponseModel.medispanPackageQuantity!) EA"
            }
            
            if myPostResponseModel.priceType == "wac" {
                myPostResponseModel.minimumDiscount = matchRXData["wac_min_discount"] as? String ?? ""
                myPostResponseModel.maximumDiscount = matchRXData["wac_max_discount"] as? String ?? ""
            }
            else {
                myPostResponseModel.minimumDiscount = matchRXData["aawp_min_discount"] as? String ?? ""
                myPostResponseModel.maximumDiscount = matchRXData["aawp_max_discount"] as? String ?? ""
            }
            
            if (myPostResponseModel.optionalPackageCondition?.contains("1"))! {
                myPostResponseModel.isTornPkgSelected = true
            }
            if (myPostResponseModel.optionalPackageCondition?.contains("2"))! {
                myPostResponseModel.isStickerSelected = true
            }
            if (myPostResponseModel.optionalPackageCondition?.contains("3"))! {
                myPostResponseModel.isXContainerSelected = true
            }
            if (myPostResponseModel.optionalPackageCondition?.contains("4"))! {
                myPostResponseModel.isWritingOnContainerSelected = true
            }
            if (myPostResponseModel.optionalPackageCondition?.contains("5"))! {
                myPostResponseModel.isOthersSelected = true
            }
            
            
            let images = NSMutableArray.init(array: myPosting["images"] as! NSArray)
            let imageArray = NSMutableArray.init()
            for i in 0..<images.count {
                let imageDict = images[i] as! NSDictionary
                let imageId = imageDict["post_image_id"] as? Int
                imageArray.add(imageId!)
            }
            myPostResponseModel.imageArray = imageArray
        }
        else{
            myPostResponseModel.isSuccess = false
        }
        
        return myPostResponseModel
    }
    
    func bindCartSellerBuyerData(cartArray: NSMutableArray, buyListModel: MarketPlaceDataModel) {
        let cartSellerArray = NSMutableArray.init()
        let postSellerArray = NSMutableArray.init()
        for i in 0..<cartArray.count {
            let cartData = cartArray.object(at: i) as! NSDictionary
            let cartSellerModel = CartSellerDataModel()
            if cartData["seller_id"] is Int {
                cartSellerModel.sellerId = cartData["seller_id"] as? Int ?? 0
                cartSellerModel.sellerName = cartData["seller_display_name"] as? String ?? ""
                cartSellerModel.isActive = cartData["active"] as? Bool ?? false
                buyListModel.cartArray.add(cartSellerModel)
            }
            
            let cartSellerDict = NSMutableDictionary.init()
            if cartData["seller_id"] is Int
            {
                cartSellerDict.setValue(cartData["seller_id"] as? Int, forKey: "seller")
            }
            else if cartData["seller_id"] is String{
                cartSellerDict.setValue(cartData["seller_id"] as? String, forKey: "seller")
            }
            if (cartData["active"] as? Bool)! {
                postSellerArray.add(cartSellerDict)
                GlobalData.sharedInstance.sellerArray = postSellerArray
            }
            cartSellerArray.add(cartSellerDict)
        }
        GlobalData.sharedInstance.cartArray = cartSellerArray
    }
    
    func getMarketPlaceListData(responseData: Any!, requestType: ServiceRequestType) -> MarketPlaceDataModel{
        let marketPlaceArray = NSMutableArray.init()
        let marketplaceDataModel = MarketPlaceDataModel()
        if responseData != nil {
            let responseDictionary: NSDictionary = responseData as! NSDictionary
            let responseDict = responseDictionary["data"] as! NSDictionary
            let marketPlaceDict = responseDict["marketplace"] as! NSDictionary
            if responseDict["cart_sellers"] != nil {
                let cartArray = NSMutableArray.init(array: responseDict["cart_sellers"] as! NSArray)
                //            let responseArray = NSMutableArray.init(array: ((responseDictionary["data"] as! NSDictionary)["marketplace"] as! NSDictionary)["data"] as! NSArray)
                let responseArray = NSMutableArray.init(array: marketPlaceDict["data"] as! NSArray)
                marketplaceDataModel.cartCount = responseDict["cart_qty_count"] as? Int ?? 0
                self.loadCartSellerDetails(cartArray: cartArray, request: .MarketplaceRequest)
                
                if responseArray.count == 0 {
                    if responseDict["message"] != nil {
                        marketplaceDataModel.message = responseDict["message"] as? String ?? ""
                    }
                }
                
                for i in 0..<responseArray.count {
                    let buyListModel = BuyListDataModel()
                    let drugData = responseArray.object(at: i) as! NSDictionary
                    let medispanData = drugData["medispan_details_list"] as! NSDictionary
                    buyListModel.postId = drugData["post_id"] as? Int ?? 0
                    buyListModel.medispanImageURL = medispanData["image_url"] as? String ?? ""
                    buyListModel.imageCount = drugData["image_count"] as? Int ?? 0
                    buyListModel.drugName = drugData["post_title"] as? String ?? ""
                    buyListModel.ndcNumber = drugData["ndc_number"] as? String ?? ""
                    buyListModel.manufacturerName = medispanData["manufacturer_name"] as? String ?? ""
                    buyListModel.expiryDate = drugData["expire_date"] as? String ?? ""
                    buyListModel.metricStrength = medispanData["metric_strength"] as? NSNumber
                    buyListModel.strenghtUnitOfMeasure = medispanData["strength_unit_of_measure"] as? String ?? ""
                    buyListModel.packageOption = drugData["package_option"] as? String ?? ""
                    buyListModel.packageSize = drugData["package_size"] as? Int ?? 0
                    buyListModel.actualPackageSize = drugData["actual_package_size"] as? Int ?? 0
                    buyListModel.packageUnitOfMeasure = medispanData["package_size_unit_of_measure"] as? String ?? ""
                    buyListModel.packageType = medispanData["package_type"] as? String ?? ""
                    buyListModel.packageQuantity = drugData["package_quantity"] as? Int ?? 0
                    buyListModel.currentPrice = drugData["current_price"] as? Double ?? 0.00
                    buyListModel.revisedPrice = Double(drugData["revised_price"] as? String ?? "0.00")
                    buyListModel.isMyposting = drugData["is_myposting"] as? Bool
                    buyListModel.estimatedDeliveryDate = drugData["estimate_delivery"] as? String ?? ""
                    buyListModel.rating = drugData["rating"] as? NSNumber
                    buyListModel.aawpDiscount = drugData["aawp_discount"] as? Double ?? 0.00
                    buyListModel.wacDiscount = drugData["wac_discount"] as? Double ?? 0.00
                    buyListModel.updatedDate = drugData["updated_at"] as? String ?? ""
                    buyListModel.createdDate = drugData["created_at"] as? String ?? ""
                    buyListModel.lastBoughtDate = drugData["last_bought"] as? String ?? ""
                    buyListModel.isInWishList = drugData["wishlist"] as? Bool ?? false
                    buyListModel.shippingAmount = drugData["shipping_handling_amount"] as? String ?? "0"
                    buyListModel.isMultiIngredient = medispanData["is_multi_ingredient"] as? Bool
                    //                buyListModel.totalDataCount = marketPlaceDict["total"] as? Int ?? 0
                    buyListModel.isInCart = drugData["is_added_cart"] as? Bool ?? false
                    buyListModel.packaging = drugData["packaging"] as? String ?? ""
                    buyListModel.strength = "\(buyListModel.metricStrength!) \(buyListModel.strenghtUnitOfMeasure!)"
                    buyListModel.availableQuantity = drugData["available_quantity"] as? Int ?? 0
                    
                    if buyListModel.isMultiIngredient! {
                        buyListModel.strength = "MULTI"
                    }
                    
                    if buyListModel.wacDiscount != 0 {
                        buyListModel.discount = "\(Int((buyListModel.wacDiscount?.rounded())!))"
                    }
                    else {
                        buyListModel.discount = "\(Int((buyListModel.aawpDiscount?.rounded())!))"
                    }
                    
                    let images = NSMutableArray.init(array: drugData["images"] as! NSArray)
                    let imageArray = NSMutableArray.init()
                    for i in 0..<images.count {
                        let imageDict = images[i] as! NSDictionary
                        let imageId = imageDict["post_image_id"] as? Int
                        imageArray.add(imageId!)
                    }
                    buyListModel.imageArray = imageArray
                    
                    /*if buyListModel.packageOption == "F" {
                        buyListModel.packaging = "\(buyListModel.packageType!)(\(buyListModel.packageSize!) \(buyListModel.packageUnitOfMeasure!))"
                    }
                    else {
                        buyListModel.packaging = "Partial: \(buyListModel.packageSize!) / \(buyListModel.actualPackageSize!)"
                    }*/
                    if buyListModel.packageOption == "P" {
                        buyListModel.packaging = "Partial: \(buyListModel.packaging!)"
                    }
                    
                    if buyListModel.isMyposting! {
                        let dateFormatter = DateFormatter()
                        dateFormatter.dateFormat = "YYYY-MM-dd HH:mm:ss"
                        let displayDateFormat = DateFormatter()
                        displayDateFormat.dateFormat = "MM-dd-YYYY"
                        var myPostingDate = buyListModel.updatedDate
//                        var dateValue = "Modified on"
                        if (myPostingDate?.isEmpty)! {
                            myPostingDate = buyListModel.createdDate
//                            dateValue = "Created on"
                        }
                        
                        let modDate = dateFormatter.date(from: myPostingDate!)
                        buyListModel.modifiedDate = "Last Modified on \(displayDateFormat.string(from: modDate!))"
                        /*let modDate = dateFormatter.date(from: drugData["updated_at"] as? String ?? "")
                        var modifiedDate = ""
                        if modDate != nil {
                            buyListModel.isModified = true
                            modifiedDate = "Modified on \(displayDateFormat.string(from: modDate!))"
                        }
                        else {
                            buyListModel.isModified = false
                        }
                        buyListModel.modifiedDate = modifiedDate*/
                    }
                    marketPlaceArray.add(buyListModel)
                    marketplaceDataModel.marketPlaceArray.add(buyListModel)
                }
                GlobalData.sharedInstance.cartCount = marketplaceDataModel.cartCount
            }
            if responseDict["message"] != nil {
                marketplaceDataModel.message = responseDict["message"] as? String ?? ""
            }
            else {
                marketplaceDataModel.message = ""
            }
        }
        return marketplaceDataModel
    }
    
    func getComparePriceData(responseData: Any!) -> ComparePriceDataModel {
        let comparePriceDataModel = ComparePriceDataModel()
        if responseData != nil {
            let responseDictionary: NSDictionary = responseData as! NSDictionary
            let responseDict = responseDictionary["data"] as! NSDictionary
            let currentPostArray = NSMutableArray.init(array: responseDict["current_post"] as! NSArray)
            let matchesArray = NSMutableArray.init(array: responseDict["matches"] as! NSArray)
            
            for i in 0..<currentPostArray.count {
                let drugData = currentPostArray.object(at: i) as! NSDictionary
                comparePriceDataModel.currentPostArray.add(self.fetchBindedDataInArray(drugData: drugData))
            }
            for i in 0..<matchesArray.count {
                let drugData = matchesArray.object(at: i) as! NSDictionary
                comparePriceDataModel.matchesArray.add(self.fetchBindedDataInArray(drugData: drugData))
            }
        }
        return comparePriceDataModel
    }
    
    func fetchBindedDataInArray(drugData: NSDictionary) -> BuyListDataModel {
        let buyListModel = BuyListDataModel()
        buyListModel.postId = drugData["post_id"] as? Int ?? 0
        buyListModel.medispanImageURL = drugData["medispan_image_url"] as? String ?? ""
        buyListModel.imageCount = drugData["image_count"] as? Int ?? 0
        buyListModel.drugName = drugData["post_title"] as? String ?? ""
        buyListModel.ndcNumber = drugData["ndc_number"] as? String ?? ""
        buyListModel.manufacturerName = drugData["manufacturer_name"] as? String ?? ""
        buyListModel.expiryDate = drugData["expire_date"] as? String ?? ""
        buyListModel.metricStrength = drugData["metric_strength"] as? NSNumber
        buyListModel.strenghtUnitOfMeasure = drugData["strength_unit_of_measure"] as? String ?? ""
        buyListModel.packageOption = drugData["package_option"] as? String ?? ""
        buyListModel.packageSize = drugData["package_size"] as? Int ?? 0
        buyListModel.actualPackageSize = drugData["actual_package_size"] as? Int ?? 0
        buyListModel.packageUnitOfMeasure = drugData["package_size_unit_of_measure"] as? String ?? ""
        buyListModel.revisedPrice = Double(drugData["revised_price"] as? String ?? "0.00")
        buyListModel.packageType = drugData["package_type"] as? String ?? ""
        buyListModel.packageQuantity = drugData["package_quantity"] as? Int ?? 0
        buyListModel.currentPrice = drugData["current_price"] as? Double ?? 0.00
        buyListModel.isMyposting = drugData["is_myPosting"] as? Bool ?? false
        buyListModel.isLastBought = drugData["is_lastBought"] as? Bool ?? false
        buyListModel.estimatedDeliveryDate = drugData["estimate_delivery"] as? String ?? ""
        buyListModel.rating = drugData["rating"] as? NSNumber
        buyListModel.aawpDiscount = drugData["aawp_discount"] as? Double ?? 0.00
        buyListModel.wacDiscount = drugData["wac_discount"] as? Double ?? 0.00
        buyListModel.updatedDate = drugData["updated_at"] as? String ?? ""
        buyListModel.createdDate = drugData["created_at"] as? String ?? ""
        buyListModel.lastBoughtDate = drugData["last_bought"] as? String ?? ""
        buyListModel.isInWishList = drugData["wishlist"] as? Bool ?? false
        buyListModel.shippingAmount = drugData["shipping_handling_amount"] as? String ?? "0"
        buyListModel.isInCart = drugData["is_added_cart"] as? Bool ?? false
        buyListModel.strength = "\(buyListModel.metricStrength!) \(buyListModel.strenghtUnitOfMeasure!)"
        buyListModel.packaging = drugData["packaging"] as? String ?? ""
        buyListModel.isMultiIngredient = drugData["is_multi_ingredient"] as? Bool
        buyListModel.availableQuantity = drugData["available_quantity"] as? Int ?? 0
        if buyListModel.isMultiIngredient! {
            buyListModel.strength = "MULTI"
        }
        
        
        if buyListModel.wacDiscount != 0 {
            buyListModel.discount = "\(Int((buyListModel.wacDiscount?.rounded())!))"
        }
        else {
            buyListModel.discount = "\(Int((buyListModel.aawpDiscount?.rounded())!))"
        }
        
        
        if buyListModel.packageOption == "P" {
            buyListModel.packaging = "Partial: \(buyListModel.packaging!)"
        }
        
        
        if buyListModel.isMyposting! {
            let dateFormatter = DateFormatter()
            dateFormatter.dateFormat = "YYYY-MM-dd HH:mm:ss"
            let displayDateFormat = DateFormatter()
            displayDateFormat.dateFormat = "MM-dd-YYYY"
            var myPostingDate = buyListModel.updatedDate
//            var dateValue = "Modified on"
            if (myPostingDate?.isEmpty)! {
                myPostingDate = buyListModel.createdDate
//                dateValue = "Created on"
            }
            
            let modDate = dateFormatter.date(from: myPostingDate!)
            buyListModel.modifiedDate = "Last Modified on \(displayDateFormat.string(from: modDate!))"
            
        }
        return buyListModel
    }
    
    func getMyPostingsListData(responseData: Any!) -> NSMutableArray{
        let marketPlaceArray = NSMutableArray.init()
        if responseData != nil {
            let responseDictionary: NSDictionary = responseData as! NSDictionary
            let responseDict = responseDictionary["data"] as! NSDictionary
            let marketPlaceDict = responseDict["myposting"] as! NSDictionary
            //            let responseArray = NSMutableArray.init(array: ((responseDictionary["data"] as! NSDictionary)["marketplace"] as! NSDictionary)["data"] as! NSArray)
            let responseArray = NSMutableArray.init(array: marketPlaceDict["data"] as! NSArray)
            for i in 0..<responseArray.count {
                let buyListModel = BuyListDataModel()
                let drugData = responseArray.object(at: i) as! NSDictionary
                let medispanData = drugData["medispan_details_list"] as! NSDictionary
                
                
                
                buyListModel.postId = drugData["post_id"] as? Int ?? 0
                buyListModel.medispanImageURL = medispanData["image_url"] as? String ?? ""
                buyListModel.imageCount = drugData["image_count"] as? Int ?? 0
                buyListModel.drugName = drugData["post_title"] as? String ?? ""
                buyListModel.ndcNumber = drugData["ndc_number"] as? String ?? ""
                buyListModel.lotNumber = drugData["lot_number"] as? String ?? ""
                buyListModel.expiryDate = drugData["expire_date"] as? String ?? ""
                buyListModel.metricStrength = medispanData["metric_strength"] as? NSNumber
                buyListModel.strenghtUnitOfMeasure = medispanData["strength_unit_of_measure"] as? String ?? ""
                buyListModel.packageOption = drugData["package_option"] as? String ?? ""
                buyListModel.packageSize = drugData["package_size"] as? Int ?? 0
                buyListModel.actualPackageSize = drugData["actual_package_size"] as? Int ?? 0
                buyListModel.packageUnitOfMeasure = medispanData["package_size_unit_of_measure"] as? String ?? ""
                buyListModel.packageType = medispanData["package_type"] as? String ?? ""
                buyListModel.packageQuantity = drugData["package_quantity"] as? Int ?? 0
                buyListModel.currentPrice = drugData["current_price"] as? Double ?? 0.00
                buyListModel.revisedPrice = Double(drugData["revised_price"] as? String ?? "0.00")
                buyListModel.isMyposting = drugData["is_myposting"] as? Bool
                buyListModel.estimatedDeliveryDate = drugData["estimate_delivery"] as? String ?? ""
                buyListModel.rating = drugData["rating"] as? NSNumber
                buyListModel.aawpDiscount = drugData["aawp_discount"] as? Double ?? 0.00
                buyListModel.wacDiscount = drugData["wac_discount"] as? Double ?? 0.00
                buyListModel.updatedDate = drugData["updated_at"] as? String ?? ""
                buyListModel.lastBoughtDate = drugData["last_bought"] as? String ?? ""
                buyListModel.isInWishList = drugData["is_wishlist"] as? Bool ?? false
                buyListModel.shippingAmount = drugData["shipping_handling_amount"] as? String ?? "0"
//                buyListModel.totalDataCount = marketPlaceDict["total"] as? Int ?? 0
                buyListModel.strength = "\(buyListModel.metricStrength!) \(buyListModel.strenghtUnitOfMeasure!)"
                buyListModel.packaging = drugData["packaging"] as? String ?? ""
                buyListModel.isMultiIngredient = medispanData["is_multi_ingredient"] as? Bool
                buyListModel.availableQuantity = drugData["available_quantity"] as? Int ?? 0
                if buyListModel.isMultiIngredient! {
                    buyListModel.strength = "MULTI"
                }
                
                var discountValue = buyListModel.wacDiscount
                
                /*if buyListModel.wacDiscount != 0 {
                    buyListModel.discount = "\(buyListModel.wacDiscount!)"
                }
                else {
                    buyListModel.discount = "\(buyListModel.aawpDiscount!)"
                    discountValue = buyListModel.aawpDiscount
                }*/
                
                if buyListModel.wacDiscount != 0 {
                    buyListModel.discount = "\(Int((buyListModel.wacDiscount?.rounded())!))"
                }
                else {
                    buyListModel.discount = "\(Int((buyListModel.aawpDiscount?.rounded())!))"
                    discountValue = buyListModel.aawpDiscount
                }
                
                if i == 0 && discountValue! < 10{
                    buyListModel.isWacDiscountLessThanTenExists = true
                }
                else {
                    buyListModel.isWacDiscountLessThanTenExists = false
                }
                
                let images = NSMutableArray.init(array: drugData["images"] as! NSArray)
                let imageArray = NSMutableArray.init()
                for i in 0..<images.count {
                    let imageDict = images[i] as! NSDictionary
                    let imageId = imageDict["post_image_id"] as? Int
                    imageArray.add(imageId!)
                }
                buyListModel.imageArray = imageArray
                
                /*if buyListModel.packageOption == "F" {
                    buyListModel.packaging = "\(buyListModel.packageType!)(\(buyListModel.packageSize!) \(buyListModel.packageUnitOfMeasure!))"
                }
                else {
                    buyListModel.packaging = "Partial: \(buyListModel.packageSize!) / \(buyListModel.actualPackageSize!)"
                }*/
                
                if buyListModel.packageOption == "P" {
                    buyListModel.packaging = "Partial: \(buyListModel.packaging!)"
                }
                
                let dateFormatter = DateFormatter()
                dateFormatter.dateFormat = "YYYY-MM-dd HH:mm:ss"
                let displayDateFormat = DateFormatter()
                displayDateFormat.dateFormat = "MM-dd-YYYY"
                let modDate = dateFormatter.date(from: drugData["updated_at"] as? String ?? "")
                var modifiedDate = ""
                if modDate != nil {
                    buyListModel.isModified = true
                    modifiedDate = "Modified on \(displayDateFormat.string(from: modDate!))"
                }
                else {
                    buyListModel.isModified = false
                }
                buyListModel.modifiedDate = modifiedDate
                
                marketPlaceArray.add(buyListModel)
            }
        }
        return marketPlaceArray
    }
    
    func fetchItemDetails(responseData: Any!) -> Any{
        let dataModel = ItemDetailsDataModel()
        if responseData != nil {
            let responseDictionary: NSDictionary = responseData as! NSDictionary
            let itemDetails = responseDictionary["data"] as! NSDictionary
            if itemDetails.allKeys.count != 0 {
                dataModel.postId = itemDetails["post_id"] as? Int ?? 0
                dataModel.wacDiscount = itemDetails["wac_discount"] as? Double ?? 0.00
                dataModel.aawpDiscount = itemDetails["aawp_discount"] as? Double ?? 0.00
                dataModel.ndcName = itemDetails["post_title"] as? String ?? ""
                dataModel.ndcNumber = itemDetails["ndc_number"] as? String ?? ""
                dataModel.lotNumber = itemDetails["lot_number"] as? String ?? ""
                dataModel.packageOption = itemDetails["package_option"] as? String ?? ""
                dataModel.actualPackageSize = itemDetails["actual_package_size"] as? Int ?? 0
                dataModel.packageQuantity = itemDetails["package_quantity"] as? Int ?? 0
                dataModel.originalPackage = itemDetails["original_package"] as? String ?? ""
                dataModel.expiryDate = itemDetails["expire_date"] as? String ?? ""
                dataModel.packageSize = itemDetails["package_size"] as? Int ?? 0
                dataModel.unitPrice = itemDetails["unit_price"] as? Double ?? 0.00
                dataModel.revisedPrice = Double(itemDetails["revised_price"] as? String ?? "0.00")
                dataModel.shippingHandlingName = itemDetails["shipping_handling_name"] as? String ?? ""
                dataModel.shippingDisplayName = itemDetails["shipping_handling_display_name"] as? String ?? ""
                dataModel.condition = itemDetails["condition"] as? String ?? ""
                dataModel.sellerNotes = itemDetails["seller_notes"] as? String ?? ""
                dataModel.lastBoughtOn = itemDetails["last_bought"] as? String ?? ""
                dataModel.isWishlisted = itemDetails["wishlist"] as? Bool ?? false
                dataModel.rating = itemDetails["rating"] as? Float ?? 0.0
                dataModel.shippingHandlingAmount = itemDetails["shipping_handling_amount"] as? String ?? ""
                dataModel.estimatedDeliveryDate = itemDetails["estimate_delivery"] as? String ?? ""
                dataModel.packageType = itemDetails["package_type"] as? String ?? ""
                dataModel.medispanImageURL = itemDetails["medispan_image_url"] as? String ?? ""
                dataModel.genericName = itemDetails["gpi_generic_name"] as? String ?? ""
                dataModel.storageCondition = itemDetails["storage_condition_code"] as? String ?? ""
                dataModel.manufacturerName = itemDetails["manufacturer_name"] as? String ?? ""
                dataModel.packageSizeUnitOfMeasure = itemDetails["package_size_unit_of_measure"] as? String ?? ""
                dataModel.strengthUnitOfMeasure = itemDetails["strength_unit_of_measure"] as? String ?? ""
                dataModel.metricStrength = itemDetails["metric_strength"] as? NSNumber
                dataModel.dosageForm = itemDetails["dosage_form"] as? String ?? ""
                dataModel.shape = itemDetails["shape"] as? String ?? ""
                dataModel.isLastBought = itemDetails["is_last_bought"] as? Bool ?? false
                dataModel.isMyPosting = itemDetails["is_myPosting"] as? Bool ?? false
                dataModel.isInCart = itemDetails["is_added_cart"] as? Bool ?? false
                dataModel.availableQuantity = itemDetails["available_quantity"] as? Int ?? 0
                dataModel.quantityInCart = itemDetails["quantity_added_in_cart"] as? Int ?? 0
                dataModel.isComparePrice = itemDetails["is_compare_price"] as? Bool ?? false
                dataModel.isMultiIngredients = itemDetails["is_multi_ingredient"] as? Bool ?? false
                dataModel.packaging = itemDetails["packaging"] as? String ?? ""
                dataModel.packageConditionDescription = itemDetails["package_condition_description"] as? String ?? ""
                
                if dataModel.shape == "" {
                    dataModel.shape = "NA"
                }
                
                var sellerNotes = ""
                if (dataModel.condition?.contains("1"))! {
                    sellerNotes += "Torn Package Label\n\n"
                }
                if (dataModel.condition?.contains("2"))! {
                    sellerNotes += "Sticker / Glue residue\n\n"
                }
                if (dataModel.condition?.contains("3"))! {
                    sellerNotes += "\"X\" on container\n\n"
                }
                if (dataModel.condition?.contains("4"))! {
                    sellerNotes += "Writing on container\n\n"
                }
                if !(dataModel.sellerNotes?.isEmpty)! {
                    sellerNotes += dataModel.sellerNotes!
                }
                
                if sellerNotes.last == "," {
                    sellerNotes = String(sellerNotes.dropLast())
                }
                if sellerNotes.first == "," {
                    sellerNotes = String(sellerNotes.dropFirst())
                }
                dataModel.sellerNotes = sellerNotes
//                dataModel.sellerNotes = dataModel.packageConditionDescription
                
                if dataModel.isMultiIngredients! {
                    let ingredientsArray = NSMutableArray.init(array: itemDetails["ingredient_details"] as! NSArray)
                    var ingredients = ""
                    for j in 0..<ingredientsArray.count {
                        let ingredientDict = ingredientsArray[j] as! NSDictionary
                        let ingredientData = ingredientDict["ingredient_name"] as? String ?? ""
                        ingredients += "-"+ingredientData+"\n"
                    }
                    dataModel.ingredients = ingredients
                }
                
                let images = NSMutableArray.init(array: itemDetails["images"] as! NSArray)
                let imageArray = NSMutableArray.init()
                for i in 0..<images.count {
                    let imageDict = images[i] as! NSDictionary
                    let imageId = imageDict["post_image_id"] as? Int
                    imageArray.add(imageId!)
                }
                dataModel.imageArray = imageArray
                
                if dataModel.wacDiscount! != 0 {
                     dataModel.discountPercentage = "\(Int((dataModel.wacDiscount?.rounded())!))"
                }
                else {
                     dataModel.discountPercentage = "\(Int((dataModel.aawpDiscount?.rounded())!))"
                }
                
                if dataModel.storageCondition == "N" {
                    dataModel.storage = "Normal"
                }
                else if dataModel.storageCondition == "R"{
                    dataModel.storage = "Refrigerated"
                }
                else {
                    dataModel.storage = "Frozen"
                }
                
                if dataModel.isMultiIngredients! {
                    dataModel.strength = "Multi"
                }
                else {
                    dataModel.strength = "\(dataModel.metricStrength!) \(dataModel.strengthUnitOfMeasure!)"
                }
                
                
                /*if dataModel.packageOption == "F" {
                    dataModel.packaging = "\(dataModel.packageType!)(\(dataModel.packageSize!) \(dataModel.packageSizeUnitOfMeasure!))"
                }
                else {
                    dataModel.packaging = "Partial: \(dataModel.packageSize!)/\(dataModel.actualPackageSize!)"
                }*/
                if dataModel.packageOption == "P" {
                    dataModel.packaging = "Partial: \(dataModel.packaging!)"
                }
            }
        }
        return dataModel
    }
    
    func fetchMyCartDetails (responseData: Any!) -> CartDataModel {
        let cartModel = CartDataModel()
        if responseData != nil {
            let responseDictionary: NSDictionary = responseData as! NSDictionary
            let responseDict = responseDictionary["data"] as! NSDictionary
            
            if responseDict["checkout_info"] != nil {
                cartModel.checkoutInformation = responseDict["checkout_info"] as? String ?? ""
            }
            if responseDict["seller_info"] != nil {
                cartModel.sellerInformation = responseDict["seller_info"] as? String ?? ""
            }
            
            if responseDict["cart_seller"] != nil {
//                let marketplaceDataModel = MarketPlaceDataModel()
                let cartArray = NSMutableArray.init(array: responseDict["cart_seller"] as! NSArray)
                /*let cartSellerArray = NSMutableArray.init()
                let postSellerArray = NSMutableArray.init()
                for i in 0..<cartArray.count {
                    let cartData = cartArray.object(at: i) as! NSDictionary
                    let cartSellerModel = CartSellerDataModel()
                    if cartData["seller_id"] is Int {
                        cartSellerModel.sellerId = cartData["seller_id"] as? Int ?? 0
                        cartSellerModel.sellerName = cartData["seller_display_name"] as? String ?? ""
                        cartSellerModel.isActive = cartData["active"] as? Bool ?? false
                        GlobalData.sharedInstance.marketplaceModel.cartArray.add(cartSellerModel)
//                        marketplaceDataModel.cartArray.add(cartSellerModel)
                    }
                    
                    let cartSellerDict = NSMutableDictionary.init()
                    if cartData["seller_id"] is Int
                    {
                        cartSellerDict.setValue(cartData["seller_id"] as? Int, forKey: "seller")
                    }
                    else if cartData["seller_id"] is String{
                        cartSellerDict.setValue(cartData["seller_id"] as? String, forKey: "seller")
                    }
                    if (cartData["active"] as? Bool)! {
                        postSellerArray.add(cartSellerDict)
                        GlobalData.sharedInstance.sellerArray = postSellerArray
                    }
                    cartSellerArray.add(cartSellerDict)
                }
                GlobalData.sharedInstance.cartArray = cartSellerArray*/
                self.loadCartSellerDetails(cartArray: cartArray, request: .FetchCartRequest)
            }
            
            if responseDict["my_cart"] != nil {
                cartModel.isCartObjectExists = true
                cartModel.totalQuantity = responseDict["total_quantity"] as? String ?? ""
                cartModel.grandTotal = responseDict["grand_total"] as? Double ?? 0
                cartModel.cartCount = responseDict["total_quantity"] as? String ?? "0"
                if (cartModel.cartCount?.isEmpty)! {
                    cartModel.cartCount = "0"
                }
                let cartCount = Int(cartModel.cartCount!)
                GlobalData.sharedInstance.cartCount = cartCount
                let cartArray = NSMutableArray.init(array: responseDict["my_cart"] as! NSArray)
                for i in 0..<cartArray.count {
                    let dataModel = CartSellerDataModel()
                    let sellerData = cartArray.object(at: i) as! NSDictionary
                    let shippingMethods = sellerData["shipping_methods"] as! NSDictionary
                    let cartItemArray = NSMutableArray.init(array: sellerData["cart_items"]  as! NSArray)
                    dataModel.sellerId = sellerData["seller_id"] as? Int
                    dataModel.cartId = sellerData["cart_id"] as? Int
                    dataModel.sellerName = sellerData["seller_display_name"] as? String ?? ""
                    dataModel.itemTotalAmount = sellerData["gross_amount"] as? Double ?? 0
                    dataModel.totalAmount = sellerData["debit_amount"] as? Double ?? 0
                    dataModel.shippingAmount = sellerData["buying_shipping_fee"] as? Double ?? 0.00
                    dataModel.fedexImageUrl = sellerData["shipping_image_url"] as? String ?? ""
                    dataModel.matchRXFee = sellerData["mrx_fee"] as? Double ?? 0
                    dataModel.groundInfo = sellerData["ground_shipping_info"] as? String ?? ""
                    dataModel.twoDayInfo = sellerData["twoday_shipping_info"] as? String ?? ""
                    dataModel.overnightInfo = sellerData["overnight_shipping_info"] as? String ?? ""
                    
                    for j in 0..<cartItemArray.count {
                        let itemModel = CartItemDataModel()
                        let itemData = cartItemArray.object(at: j) as! NSDictionary
                        itemModel.postId = itemData["post_id"] as? Int
                        itemModel.postTitle = itemData["post_title"] as? String ?? ""
                        itemModel.ndcNumber = itemData["ndc_number"] as? String ?? ""
                        itemModel.packageQuantity = itemData["package_quantity"] as? Int ?? 0
                        itemModel.availableQuantity = itemData["available_quantity"] as? Int ?? 0
                        itemModel.quantityAddedInCart = itemData["quantity"] as? Int ?? 0
                        itemModel.unitPrice = itemData["unit_price"] as? Double ?? 0
                        itemModel.packageOption = itemData["package_option"] as? String ?? ""
                        itemModel.medispanImageURL = itemData["medispan_image_url"] as? String ?? ""
                        itemModel.manufacturerName = itemData["manufacturer_name"] as? String ?? ""
                        itemModel.packageSize = itemData["package_size"] as? Int ?? 0
                        itemModel.actualPackageSize = itemData["actual_package_size"] as? Int ?? 0
                        itemModel.revisedPrice = Double(itemData["revised_price"] as? String ?? "0.00")
                        itemModel.packaging = itemData["packaging"] as? String ?? ""
                        dataModel.cartDetailsArray.add(itemModel)
                        //                    cartModel.cartItemArray.add(itemModel)
                    }
                    
                    if shippingMethods.object(forKey: "Ground") != nil {
                        let ground = shippingMethods["Ground"] as! NSDictionary
                        dataModel.shippingMethods.setValue(self.bindShippingDetails(shippingDict: ground), forKey: "Ground")
                    }
                    if shippingMethods.object(forKey: "2-Day") != nil {
                        let twoDay = shippingMethods["2-Day"] as! NSDictionary
                        dataModel.shippingMethods.setValue(self.bindShippingDetails(shippingDict: twoDay), forKey: "2-Day")
                    }
                    if shippingMethods.object(forKey: "Overnight") != nil {
                        let priorityOvernight = shippingMethods["Overnight"] as! NSDictionary
                        dataModel.shippingMethods.setValue(self.bindShippingDetails(shippingDict: priorityOvernight), forKey: "Overnight")
                    }
                    
                    cartModel.cartSellerArray.add(dataModel)
                    //                cartDetailsArray.add(cartModel)
                }
            }
            else {
                GlobalData.sharedInstance.cartCount = 0
                GlobalData.sharedInstance.sellerArray = NSMutableArray.init()
                cartModel.isCartObjectExists = false
            }
        }
        return cartModel
    }
    
    func bindShippingDetails(shippingDict: NSDictionary) -> CartShippingDetailsDataModel {
        let shippingModel = CartShippingDetailsDataModel()
        shippingModel.shippingId = shippingDict["shipping_id"] as? String ?? ""
        shippingModel.shippingName = shippingDict["shipping_name"] as? String ?? ""
        shippingModel.deliveredBy = shippingDict["delivered_by"] as? String ?? ""
        shippingModel.shippingCost = Double(shippingDict["shipping_cost"] as? String ?? "0.00")
        shippingModel.sortOrder = shippingDict["sort_order"] as? Int
        shippingModel.shippingPaidBy = shippingDict["shipping_paid_by"] as? String ?? ""
        shippingModel.matchRXShippingOffer = shippingDict["matchrx_shipping_offer"] as? String ?? ""
        shippingModel.fedexShippingOffer = shippingDict["fedex_shipping_offer"] as? String ?? ""
        shippingModel.isSelected = shippingDict["is_selected"] as? Bool
        
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "YYYY-MM-dd" //YYYY-MM-dd HH:mm:ss
        let displayDateFormat = DateFormatter()
        displayDateFormat.dateFormat = "MMM dd"
        let modDate = dateFormatter.date(from: shippingDict["delivered_by"] as? String ?? "")
        var modifiedDate = ""
        if modDate != nil {
//            modifiedDate = "( Est. Delivery \(displayDateFormat.string(from: modDate!))) - $\(String(format: "%.2f", arguments: [shippingModel.shippingCost!]) )"
        }
        modifiedDate = "( Est. Delivery \(shippingModel.deliveredBy!)) - $\(String(format: "%.2f", arguments: [shippingModel.shippingCost!]) )"
        shippingModel.deliveryDetails = modifiedDate
        return shippingModel
    }
    
    func fetchMyOrderSummary(responseData: Any!) -> OrderSummaryDataModel {
        let orderData = OrderSummaryDataModel()
        if responseData != nil {
            let responseDictionary: NSDictionary = responseData as! NSDictionary
            let responseDataDict = responseDictionary["data"]  as! NSDictionary
            let responseArray = responseDataDict["mrx_orders"] as! NSArray
            orderData.bankDetails = responseDataDict["bank_info"] as? String ?? ""
//            let cartArray = NSMutableArray.init(array: responseDict["my_cart"] as! NSArray)
            for i in 0..<responseArray.count {
                let dataModel = OrderSummaryDataModel()
                let summaryData = responseArray.object(at: i) as! NSDictionary
                dataModel.orderId = summaryData["order_id"] as? Int
                dataModel.orderNumber = summaryData["order_number"] as? String ?? ""
                dataModel.sellerId = summaryData["seller_pharmacy_id"] as? Int
                dataModel.sellerName = summaryData["seller_display_name"] as? String ?? ""
                dataModel.itemTotalAmount = summaryData["gross_amount"] as? Double
                dataModel.totalAmount = summaryData["debit_amount"] as? Double
                dataModel.shippingMethod = summaryData["shipping_method"] as? String ?? ""
                dataModel.buyerShippingFee = summaryData["buyer_shipping_fee"] as? Double ?? 0.00
                dataModel.sellerShippingFee = summaryData["seller_shipping_fee"] as? Double ?? 0.00
//                if dataModel.buyerShippingFee == 0 {
//                    dataModel.shippingCost = 0
//                }
                dataModel.shippingCost = dataModel.buyerShippingFee
//                else if dataModel.sellerShippingFee == 0 {
//                    dataModel.shippingCost = dataModel.buyerShippingFee
//                }
//                else {
//                    dataModel.shippingCost = 0.00
//                }
                
                let itemDataArray = summaryData["order_item"] as! NSArray
                for j in 0..<itemDataArray.count {
                    let itemDataModel = OrderItemModel()
                    let itemData = itemDataArray.object(at: j) as! NSDictionary
                    itemDataModel.ndcNumber = itemData["ndc_number"] as? String ?? ""
                    itemDataModel.postTitle = itemData["post_title"] as? String ?? ""
                    itemDataModel.packaging = itemData["packaging"] as? String ?? ""
                    itemDataModel.packageOption = itemData["package_option"] as? String ?? ""
                    itemDataModel.packageQuantity = itemData["quantity"] as? Int
                    itemDataModel.packageSize = itemData["package_size"] as? Int
                    itemDataModel.actualPackageSize = itemData["actual_package_size"] as? Int
                    itemDataModel.unitPrice = itemData["unit_price"] as? Double ?? 0.00
                    itemDataModel.itemPrice = itemData["price"] as? Double ?? 0.00
                    itemDataModel.totalPrice = itemData["item_total"] as? Double ?? 0.00
                    dataModel.cartItemArray.add(itemDataModel)
                }
                orderData.orderSummaryArray.add(dataModel)
            }
        }
        return orderData
    }
    
    func fetchWishlistData(responseData: Any) -> WishlistDataModel {
        let wishlistModel = WishlistDataModel()
        do {
            let responseDictionary: NSDictionary = responseData as! NSDictionary
            let responseData = (responseDictionary["data"] as! NSDictionary)
            if responseData["wishlist"] != nil {
                let wishlistDict = responseData["wishlist"] as! NSDictionary
                let wishlistDataArray = wishlistDict["data"] as! NSArray
                for i in 0..<wishlistDataArray.count {
                    let wishlistData = wishlistDataArray.object(at: i) as! NSDictionary
                    let dataModel = WishlistDataModel()
                    dataModel.wishlistId = wishlistData["id"] as? Int
                    dataModel.ndcNumber = wishlistData["ndc_number"] as? String
                    dataModel.formattedNdcNumber = wishlistData["formatted_id_number"] as? String
                    dataModel.ndcName = wishlistData["product_title1"] as? String
                    dataModel.ndcImageURL = wishlistData["image_url"] as? String
                    dataModel.quantity = wishlistData["available_quantity"] as? Int
                    
                    dataModel.wishlistedDate = ""
                    let dateFormatter = DateFormatter()
                    dateFormatter.dateFormat = "YYYY-MM-dd HH:mm:ss" //"YYYY-MM-dd"
                    let displayDateFormat = DateFormatter()
                    displayDateFormat.dateFormat = "MM/dd/YYYY"
                    let modDate = dateFormatter.date(from: wishlistData["created_at"] as? String ?? "")
                    if modDate != nil {
                        dataModel.wishlistedDate = displayDateFormat.string(from: modDate!)
                    }
                    
                    wishlistModel.wishlistArray.add(dataModel)
                }
            }
        }
        catch {
            wishlistModel.message = "Something went wrong with the data"
        }
        return wishlistModel
    }
    
    func deleteFromWishlist(responseData: Any!) -> WishlistDataModel {
        let wishlistModel = WishlistDataModel()
        do {
            let responseDictionary: NSDictionary = responseData as! NSDictionary
            let responseData = (responseDictionary["data"] as! NSDictionary)
            if responseData["message"] != nil {
                wishlistModel.message = responseData["message"] as? String ?? ""
            }
        }
        catch {
            wishlistModel.message = "Something went wrong with the data"
        }
        return wishlistModel
    }
    
    func addToWishlist(responseData: Any!) -> WishlistDataModel {
        let wishlistModel = WishlistDataModel()
        do {
            let responseDictionary: NSDictionary = responseData as! NSDictionary
            let responseData = (responseDictionary["data"] as! NSDictionary)
            if responseData["message"] != nil {
                wishlistModel.message = responseData["message"] as? String ?? ""
            }
        }
        catch {
            wishlistModel.message = "Something went wrong with the data"
        }
        return wishlistModel
    }
    
    func fetchMyOrders(responseData: Any!, orderType: TypeOfOrder) -> MyOrdersDataModel {
        let myOrdersModel = MyOrdersDataModel()
        do {
            let responseDictionary: NSDictionary = responseData as! NSDictionary
            let responseData = responseDictionary["data"] as! NSDictionary
            let actionDataArray = responseData["action_orders"] as! NSArray
            let openDataArray = responseData["noaction_orders"] as! NSArray
            myOrdersModel.actionOrdersArray = self.bindMyOrderData(dataArray: actionDataArray, orderType: orderType, isActionRequired: true)
            myOrdersModel.openOrdersArray = self.bindMyOrderData(dataArray: openDataArray, orderType: orderType, isActionRequired: false)
            
        } catch  MyError.conversionError{
            myOrdersModel.message = "Something went wrong with the data"
        }
        
        return myOrdersModel
    }
    
    func bindMyOrderData(dataArray: NSArray, orderType: TypeOfOrder, isActionRequired: Bool) -> NSMutableArray {
        let dataModelArray = NSMutableArray.init()
        for i in 0..<dataArray.count {
            let dataModel = MyOrdersDataModel()
            let orderData = dataArray.object(at: i) as! NSDictionary
            dataModel.orderId = orderData["order_id"] as? Int64 ?? 0
            dataModel.orderNumber = orderData["order_number"] as? String ?? ""
            dataModel.orderDate = orderData["created_at"] as? String ?? ""
//            dataModel.shippingAmount = Double(orderData["order_number"] as? String ?? "0")
            dataModel.creditTotalAmount = Double(orderData["credit_amount"] as? String ?? "0")
            dataModel.debitTotalAmount = Double(orderData["debit_amount"] as? String ?? "0")
            dataModel.grossAmount = Double(orderData["gross_amount"] as? String ?? "0")
            dataModel.sellerShippingFee = Double(orderData["seller_shipping_fee"] as? String ?? "0")
            dataModel.additionalSellerShippingFee = Double(orderData["additional_seller_shipping_fee"] as? String ?? "0")
            dataModel.additionalSellerPickupFee = Double(orderData["additional_seller_pickup_fee"] as? String ?? "0")
            dataModel.refundSellerShippingFee = Double(orderData["refund_seller_shipping_fee"] as? String ?? "0")
            dataModel.fedexShippingInsurance = Double(orderData["fedex_shipping_insurance"] as? String ?? "0")
            dataModel.shippingMethod = orderData["shipping_method"] as? String ?? ""
            dataModel.shippingDate = orderData["shipping_date"] as? String ?? ""
            dataModel.expectedDeliveryDate = orderData["expected_delivery_date"] as? String ?? ""
            dataModel.isActionRequired = isActionRequired
            dataModel.orderType = orderType
            dataModel.shippingPaidBy = orderData["shipping_paid_by"] as? String ?? ""
            dataModel.sellerConfirmationDate = orderData["seller_confirmation_date"] as? String ?? ""
            dataModel.trackingId = orderData["tracking_id"] as? String ?? ""
            dataModel.createdDate = orderData["created_at"] as? String ?? ""
            dataModel.promoText = orderData["promo_text"] as? String ?? ""
            dataModel.buyerShippingFee = Double(orderData["buyer_shipping_fee"] as? String ?? "0")
            dataModel.orderStatus = orderData["status"] as? String ?? ""
            dataModel.shippingStatusTime = orderData["shipping_status_time"] as? String ?? ""
            
            let dateFormatter = DateFormatter()
            dateFormatter.dateFormat = "YYYY-MM-dd  HH:mm:ss"
            let dateFormatter1 = DateFormatter()
            dateFormatter1.dateFormat = "MM/dd/YYYY"
            if (orderData["created_at"] as? String ?? "").count > 0{
                let createdDate = dateFormatter.date(from: (orderData["created_at"] as? String)!)
                if createdDate != nil {
                    dataModel.createdDate = dateFormatter1.string(from: createdDate!)
                }
                else {
                    dataModel.createdDate = ""
                }
            }
            else{
                dataModel.createdDate = ""
            }
            
            if (orderData["expected_delivery_date"] as? String ?? "").count > 0{
                let deliveryDate = dateFormatter.date(from: (orderData["expected_delivery_date"] as? String)!)
                if deliveryDate != nil {
                    dataModel.expectedDeliveryDate = dateFormatter1.string(from: deliveryDate!)
                }
                else {
                    dataModel.expectedDeliveryDate = ""
                }
            }
            else{
                dataModel.expectedDeliveryDate = ""
            }
            
            if (orderData["shipping_status_time"] as? String ?? "").count > 0{
                let shippingTime = dateFormatter.date(from: (orderData["shipping_status_time"] as? String)!)
                if shippingTime != nil {
                    dataModel.shippingStatusTime = dateFormatter1.string(from: shippingTime!)
                }
                else {
                    dataModel.shippingStatusTime = ""
                }
            }
            else{
                dataModel.shippingStatusTime = ""
            }
            
            dataModelArray.add(dataModel)
        }
        return dataModelArray
    }
    
    func getOrderCount(responseData: Any!) -> OrderCountDataModel {
        let countModel = OrderCountDataModel()
        do {
            let responseDictionary: NSDictionary = responseData as! NSDictionary
            let responseData = responseDictionary["data"] as! NSDictionary
            if responseData != nil {
                countModel.salesCount = responseData["sale"] as? Int
                countModel.purchaseCount = responseData["purchase"] as? Int
                countModel.totalCount = responseData["total_order"] as? Int
            }
            
        } catch  MyError.conversionError{
            
        }
        
        return countModel
    }
    
    func getOrderDetails(responseData: Any!, orderType: TypeOfOrder) -> OrderDetailsDataModel {
        let detailsModel = OrderDetailsDataModel()
        do {
            let responseDictionary: NSDictionary = responseData as! NSDictionary
            let responseData = responseDictionary["data"] as? NSDictionary
            if responseData != nil {
                let dateFormatter = DateFormatter()
                dateFormatter.dateFormat = "YYYY-MM-dd  HH:mm:ss"
                let dateFormatter1 = DateFormatter()
                dateFormatter1.dateFormat = "MM/dd/YYYY"
                var shippingCharge = 0.00
                
                let orderDetails = responseData!["order"] as! NSDictionary
                let orderItemsArray = orderDetails["orderitems"] as! NSArray
                detailsModel.globalOrderId = orderDetails["order_id"] as? Int64
                detailsModel.orderNumber = orderDetails["order_number"] as? String ?? ""
                detailsModel.fedexInsuranceAmount = responseData!["shipping_insurance_cost"] as? Double ?? 0.00
                detailsModel.matchRXFee = Double(orderDetails["mrx_fee"] as? String ?? "0.00")
                detailsModel.matchRXFeePercentage = Double(orderDetails["mrx_fee_percentage"] as? String ?? "0.00")
                detailsModel.matchMoney = Double(responseData!["available_match_money"] as? String ?? "0.00")
                detailsModel.sellerShippingFee = Double(orderDetails["seller_shipping_fee"] as? String ?? "0.00")
                detailsModel.additionalSellerShippingFee = Double(orderDetails["additional_seller_shipping_fee"] as? String ?? "0.00")
                detailsModel.additionalSellerPickupFee = Double(orderDetails["additional_seller_pickup_fee"] as? String ?? "0.00")
                detailsModel.refundSellerShippingFee = Double(orderDetails["refund_seller_shipping_fee"] as? String ?? "0.00")
                detailsModel.additionalFedexShippingInsurance = Double(orderDetails["additional_seller_shipping_insurance"] as? String ?? "0.00")
                detailsModel.refundFededShippingInsurance = Double(orderDetails["refund_seller_shipping_insurance"] as? String ?? "0.00")
                detailsModel.fedexShippingInsurance = orderDetails["fedex_shipping_insurance"] as? Double ?? 0.00
                
                detailsModel.buyerShippingFee = Double(orderDetails["buyer_shipping_fee"] as? String ?? "0.00")
                detailsModel.additionalBuyerShippingFee = Double(orderDetails["additional_buyer_shipping_fee"] as? String ?? "0.00")
                detailsModel.additionalBuyerPickupFee = Double(orderDetails["additional_buyer_pickup_fee"] as? String ?? "0.00")
                detailsModel.refundBuyerShippingFee = Double(orderDetails["refund_buyer_shipping_fee"] as? String ?? "0.00")
                detailsModel.additionalBuyerShippingInsurance = Double(orderDetails["additional_buyer_shipping_insurance"] as? String ?? "0.00")
                detailsModel.refundBuyerShippingInsurance = Double(orderDetails["refund_buyer_shipping_insurance"] as? String ?? "0.00")
                
                if orderType == .Sales {
                    shippingCharge = (detailsModel.sellerShippingFee!+detailsModel.additionalSellerShippingFee!+detailsModel.additionalSellerPickupFee!-detailsModel.refundSellerShippingFee!)+(detailsModel.fedexShippingInsurance!+detailsModel.additionalFedexShippingInsurance!-detailsModel.refundFededShippingInsurance!)
                }
                else {
                    shippingCharge = (detailsModel.buyerShippingFee!+detailsModel.additionalBuyerShippingFee!+detailsModel.additionalBuyerPickupFee!-detailsModel.refundBuyerShippingFee!)+(detailsModel.additionalBuyerShippingInsurance!-detailsModel.refundBuyerShippingInsurance!)
                    
                    let userShippingCharge = (detailsModel.additionalBuyerShippingFee!+detailsModel.additionalBuyerPickupFee!-detailsModel.refundBuyerShippingFee!)+(detailsModel.additionalBuyerShippingInsurance!-detailsModel.refundBuyerShippingInsurance!)
                    detailsModel.userCalculatedShippingCharge = userShippingCharge
                }
                detailsModel.shippingCharge = shippingCharge
                
                let shippingMethods = orderDetails["shipping_method"] as! NSDictionary
                if shippingMethods.object(forKey: "Ground") != nil {
                    let ground = shippingMethods["Ground"] as! NSDictionary
                    detailsModel.shippingMethods.setValue(self.bindShippingDetails(shippingDict: ground), forKey: "Ground")
                }
                if shippingMethods.object(forKey: "2-Day") != nil {
                    let twoDay = shippingMethods["2-Day"] as! NSDictionary
                    detailsModel.shippingMethods.setValue(self.bindShippingDetails(shippingDict: twoDay), forKey: "2-Day")
                }
                if shippingMethods.object(forKey: "Overnight") != nil {
                    let priorityOvernight = shippingMethods["Overnight"] as! NSDictionary
                    detailsModel.shippingMethods.setValue(self.bindShippingDetails(shippingDict: priorityOvernight), forKey: "Overnight")
                }
                
                if responseData!["shipping_dates"] != nil {
                    let shippingSlotArray = responseData!["shipping_dates"] as! NSArray
                    for i in 0..<shippingSlotArray.count {
                        let dformat = DateFormatter()
                        dformat.dateFormat = "YYYY-MM-dd"
                        let shippingDateFormat = DateFormatter()
                        shippingDateFormat.dateFormat = "MM/dd/YYYY (EEE)"
                        let slotDict = shippingSlotArray.object(at: i) as! NSDictionary
                        var sample = detailsModel.shippingSlot
                        sample.shippingDate = slotDict["date"] as? String ?? ""
                        sample.receivedDate = slotDict["date"] as? String ?? ""
                        sample.keys = slotDict["keys"] as? String ?? ""
                        sample.shippingTimeSlot = slotDict["time"] as! NSDictionary
                        
                        if (slotDict["date"] as? String ?? "").count > 0{
                            let shippingDate = dformat.date(from: (slotDict["date"] as? String)!)
                            if shippingDate != nil {
                                sample.shippingDate = shippingDateFormat.string(from: shippingDate!)
                            }
                            else {
                                sample.shippingDate = ""
                            }
                        }
                        else{
                            sample.shippingDate = ""
                        }
                        detailsModel.shippingSlotArray.add(sample)
                    }
                }
                
                for i in 0..<orderItemsArray.count {
                    let itemData = orderItemsArray.object(at: i) as! NSDictionary
                    let medispanData = itemData["medispan_details"] as! NSDictionary
                    let itemsModel = OrderDetailsDataModel()
                    itemsModel.orderId = itemData["id"] as? Int64
                    itemsModel.ndcName = medispanData["product_title"] as? String ?? ""
                    itemsModel.ndcNumber = medispanData["ndc_number"] as? String ?? ""
                    itemsModel.storageCondition = medispanData["storage_condition_code"] as? String ?? ""
                    itemsModel.medispanImageURL = medispanData["image_url"] as? String ?? ""
                    itemsModel.expiryDate = itemData["expire_date"] as? String ?? ""
                    itemsModel.lotNumber = itemData["lot_number"] as? String ?? ""
                    itemsModel.originalQuantity = itemData["quantity"] as? Int ?? 0
                    itemsModel.modifiedQuantity = itemData["quantity"] as? Int ?? 0
                    itemsModel.orderedQuantity = itemData["ordered_quantity"] as? Int ?? 0
                    itemsModel.originalPackageSize = itemData["package_size"] as? Int ?? 0
                    itemsModel.modifiedPackageSize = itemData["package_size"] as? Int ?? 0
                    itemsModel.orderedPackageSize = itemData["ordered_package_size"] as? Int ?? 0
                    itemsModel.actualPackageSize = itemData["actual_package_size"] as? Int ?? 0
                    itemsModel.packageOption = itemData["package_option"] as? String ?? ""
                    itemsModel.originalPrice = Double(itemData["item_total"] as? String ?? "0.00")
                    itemsModel.modifiedPrice = Double(itemData["item_total"] as? String ?? "0.00")
                    itemsModel.orderedPrice = itemData["ordered_item_total"] as? Double ?? 0.00
                    itemsModel.originalPackPrice = Double(itemData["price"] as? String ?? "0.00")
                    itemsModel.modifiedPackPrice = Double(itemData["price"] as? String ?? "0.00")
                    itemsModel.unitPrice = Double(itemData["unit_price"] as? String ?? "0.00")
                    itemsModel.item_shippingMethod = itemData["ordered_shipping_method"] as? String ?? ""
                    itemsModel.item_shippingHandlingName = itemData["ordered_shipping_handling_name"] as? String ?? ""
                    if itemData["confirmation_status"] != nil && ((itemData["confirmation_status"] as? String)?.lowercased() == "revised" || (itemData["confirmation_status"] as? String)?.lowercased() == "outofstock") {
                        itemsModel.isRevised = true
                    }
                    else {
                        itemsModel.isRevised = false
                    }
//                    itemsModel.isRevised = itemData["confirmation_status"] as? Bool
                    if orderType == .Sales {
                        itemsModel.itemStatus = OrderedItemStatus.Available
                    }
                    else if orderType == .Purchase {
                        if itemsModel.packageOption?.lowercased() == "f" {
                            if itemsModel.originalQuantity == 0 {
                                itemsModel.itemStatus = OrderedItemStatus.OutOfStock
                            }
                            else {
                                itemsModel.itemStatus = OrderedItemStatus.Accept
                            }
                        }
                        else {
                            if itemsModel.originalPackageSize == 0 {
                                itemsModel.itemStatus = OrderedItemStatus.OutOfStock
                            }
                            else {
                                itemsModel.itemStatus = OrderedItemStatus.Accept
                            }
                        }
                    }
                    else if orderType == .Purchase_Confirm_Receipt {
                        itemsModel.itemStatus = OrderedItemStatus.Accept
                    }
                    detailsModel.orderItemsArray.add(itemsModel)
                }
                
                
                
                if (orderDetails["created_at"] as? String ?? "").count > 0{
                    let createdDate = dateFormatter.date(from: (orderDetails["created_at"] as? String)!)
                    if createdDate != nil {
                        detailsModel.orderDate = dateFormatter1.string(from: createdDate!)
                    }
                    else {
                        detailsModel.orderDate = ""
                    }
                }
                else{
                    detailsModel.orderDate = ""
                }
            }
            
        } catch  MyError.conversionError{
            
        }
        
        return detailsModel
    }
    
    func getSubmitOrderResponse(responseData: Any!) -> OrderResponseDataModel {
        let dataModel = OrderResponseDataModel()
        do {
            let responseDictionary: NSDictionary = responseData as! NSDictionary
            let responseData = responseDictionary["data"] as! NSDictionary
            if responseData != nil {
                dataModel.message = responseData["message"] as? String ?? ""
                if responseData["tracking_id"] != nil {
                    dataModel.trackingId = responseData["tracking_id"] as? String ?? ""
                }
                if responseData["shipping_document"] != nil {
                    dataModel.shippingDocumentURL = responseData["shipping_document"] as? String ?? ""
                }
            }
            
        } catch  MyError.conversionError{
            
        }
        
        return dataModel
    }
    
}
    

