//
//  imageCapture.m
//  InsuranceDemo
//
//  Created by Jothiram on 02/03/14.
//  Copyright (c) 2014 jothiram. All rights reserved.
//

#import "imageCapture.h"
#import <AssetsLibrary/AssetsLibrary.h>

@interface imageCapture ()

@end

@implementation imageCapture

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark -  Image Capture

-(void)showLibrary:(UIViewController*)modal fromview:(UIView*)views
{
    if(picker!=nil)
    {
        //        [picker release];÷
        picker=nil;
    }
    picker=[[UIImagePickerController alloc]init];
    picker.allowsEditing = YES;
    picker.delegate=self;
    picker.sourceType=UIImagePickerControllerSourceTypePhotoLibrary;
    [modal presentModalViewController:picker animated:YES];
}

-(void)showcamera:(UIViewController*)modal fromview:(UIView*)views
{
    if(picker!=nil)
    {
//        [picker release];÷
        picker=nil;
    }
    picker=[[UIImagePickerController alloc]init];
    picker.delegate=self;
    picker.allowsEditing = YES;
    // picker.allowsImageEditing=NO;
    
    //
    
    if ([UIImagePickerController isSourceTypeAvailable:UIImagePickerControllerSourceTypeCamera] == YES)
    {
        picker.sourceType=UIImagePickerControllerSourceTypeCamera;
        [modal presentModalViewController:picker animated:YES];
    }
    else
    {
        picker.sourceType=UIImagePickerControllerSourceTypePhotoLibrary;
//        picker.mediaTypes = [[NSArray alloc] initWithObjects:(NSString *)kCIAttributeTypeImage, nil];
        [modal presentModalViewController:picker animated:YES];
        /*picker.view.frame=CGRectMake(0, 0, modal.view.frame.size.width, modal.view.frame.size.height);
        picker.wantsFullScreenLayout = YES;
        if(UI_USER_INTERFACE_IDIOM()==UIUserInterfaceIdiomPad)
        {
            if(popover==nil)
            {
                popover = [[UIPopoverController alloc]
                           initWithContentViewController:picker];
            }
            popover.contentViewController.view.frame=CGRectMake(0, 0, modal.view.frame.size.width, 700);
            
            //        [popover presentPopoverFromBarButtonItem: permittedArrowDirections:UIPopoverArrowDirectionUp animated:YES];
            [popover presentPopoverFromRect:CGRectMake(views.center.x-130,views.center.y, modal.view.frame.size.width, modal.view.frame.size.height) inView:modal.view permittedArrowDirections:UIPopoverArrowDirectionDown animated:YES];
            
            
        }*/
    }
    
    ////
    //    [modal.view addSubview:self.picker.view];
    //    picker.view.frame=CGRectMake(0, 0, modal.view.frame.size.width, modal.view.frame.size.height);
    
    
    [picker viewWillAppear:YES];
    [picker viewDidAppear:YES];
    CABasicAnimation *anim=[CABasicAnimation animationWithKeyPath:@"position"];
    anim.delegate=self;
    anim.duration=.3;
    anim.fromValue=[ NSValue valueWithCGPoint:CGPointMake(picker.view.center.x,picker.view.frame.size.height)];
    [picker.view.layer addAnimation:anim forKey:@"postion"];
    
    //picker.hidesBottomBarWhenPushed=YES;
}

-(void)removeController
{
    picker=Nil;
}
#pragma mark - Picker Delegate

- (void) imagePickerController:(UIImagePickerController *)thePicker didFinishPickingMediaWithInfo:(NSDictionary *)imageInfo
{
    [thePicker dismissViewControllerAnimated:YES completion:nil];
    /*if ([thePicker sourceType]==UIImagePickerControllerSourceTypeCamera) {
        [thePicker dismissModalViewControllerAnimated:YES];
    }
    else
    {
        [popover dismissPopoverAnimated:YES];
    }*/
    //    // NSLog(@"%@",imageInfo);
    NSURL *refURL = [imageInfo valueForKey:UIImagePickerControllerReferenceURL];
    
    // define the block to call when we get the asset based on the url (below)
    self.fileName = @"";
    ALAssetsLibraryAssetForURLResultBlock resultblock = ^(ALAsset *imageAsset)
    {
        ALAssetRepresentation *imageRep = [imageAsset defaultRepresentation];
        NSLog(@"[imageRep filename] : %@", [imageRep filename]);
        self.fileName = [imageRep filename];
        
        Byte *buffer = (Byte*)malloc(imageRep.size);
        NSUInteger buffered = [imageRep getBytes:buffer fromOffset:0.0 length:imageRep.size error:nil];
        NSData *libraryImageData = [NSData dataWithBytesNoCopy:buffer length:buffered freeWhenDone:YES];
        
        
        UIImage *imag = [imageInfo objectForKey:UIImagePickerControllerEditedImage];
        NSData *cameraImageData = UIImageJPEGRepresentation(imag,1.0);
        /*float imageSize = (float)cameraImageData.length/1024.0/1024.0;
        if (imageSize > 0.5)
        {
            float reducingpercentage = (0.5/imageSize)*0.5;
            cameraImageData = UIImageJPEGRepresentation(imag,reducingpercentage);
            
        }*/
        //    NSString *dataStr = [NSString stringWithCString:[imageData bytes] length:[imageData length]];//[[NSString alloc] initWithBytes:[imageData bytes]length:[imageData length] encoding:NSUTF8StringEncoding];
        //    // NSLog(@"%@",dataStr);
        //// NSLog(@"%@",[imageData base64Encoding]);
        
        if ([thePicker sourceType]==UIImagePickerControllerSourceTypeCamera) {
//            [_parentViewCont capturedImage:cameraImageData];
            [self performSelector:@selector(sendImageToParent:) withObject:cameraImageData afterDelay:0.5];
        }
        else
        {
//            [_parentViewCont capturedImage:libraryImageData];
            double selectedImageSize = libraryImageData.length/1024.0/1024.0;
            if (selectedImageSize >= 10.0) {
                [self performSelector:@selector(sendImageToParent:) withObject:libraryImageData afterDelay:0.5];
            }
            else{
                [self performSelector:@selector(sendImageToParent:) withObject:cameraImageData afterDelay:0.5];
            }
        }
        
    };

    
    // get the asset library and fetch the asset based on the ref url (pass in block above)
    ALAssetsLibrary* assetslibrary = [[ALAssetsLibrary alloc] init];
    [assetslibrary assetForURL:refURL resultBlock:resultblock failureBlock:nil];
    
}

-(void)sendImageToParent:(NSData *)imageData {
    [_parentViewCont capturedImage:imageData];
}

- (void)imagePickerControllerDidCancel:(UIImagePickerController *)thepicker
{
    [thepicker dismissViewControllerAnimated:YES completion:nil];
//    if ([thepicker sourceType]==UIImagePickerControllerSourceTypeCamera) {
//        [thepicker dismissModalViewControllerAnimated:YES];
//    }
//    else
//    {
//        [popover dismissPopoverAnimated:YES];
//    }
    //    [parentviewcontroller resizetheview:picker.view.frame];
    
    //    [picker dismissModalViewControllerAnimated:YES];
    [picker.view removeFromSuperview];
    CABasicAnimation *anim=[CABasicAnimation animationWithKeyPath:@"position"];
    anim.delegate=self;
    anim.duration=.2;
    anim.toValue=[ NSValue valueWithCGPoint:CGPointMake(picker.view.center.x,picker.view.frame.size.height)];
    [picker.view.layer addAnimation:anim forKey:@"postion"];
    //    [picker.view performSelector:@selector(removeFromSuperview) withObject:nil afterDelay:.2];
    
}
- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    // Return YES for supported orientations
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}



@end
