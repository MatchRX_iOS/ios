//
//  imageCapture.h
//  InsuranceDemo
//
//  Created by Jothiram on 02/03/14.
//  Copyright (c) 2014 jothiram. All rights reserved.
//
#import <Foundation/Foundation.h>
@protocol imageCaptureDelegate <NSObject>

@optional
-(void)capturedImage:(NSData*)imageData;
@end
#import <UIKit/UIKit.h>

@interface imageCapture : UIViewController<UIImagePickerControllerDelegate>
{
    UIImagePickerController *picker;
    
    UIPopoverController* popover;
}
@property (nonatomic, strong)NSString *fileName;
@property(nonatomic,assign)id<imageCaptureDelegate>parentViewCont;
-(void)showcamera:(UIViewController*)modal fromview:(UIView*)views;
-(void)showLibrary:(UIViewController*)modal fromview:(UIView*)views;
-(void)removeController;
@end
