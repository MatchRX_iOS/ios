//
//  ViewController.swift
//  ServiceTest
//
//  Created by Vignesh on 10/11/17.
//  Copyright © 2017 HTC. All rights reserved.
//

import UIKit

class ViewController: UIViewController, menuDelegate {
    
    @IBOutlet var leadingConstraint: NSLayoutConstraint!
    @IBOutlet var menuView: UIView!
    @IBOutlet var menuContentView: UIView!
    @IBOutlet var contentView: UIView!
    @IBOutlet var headerLabel: UILabel!
    var swipeGestureLeft: UISwipeGestureRecognizer!
    var menuController : MenuViewController!
    
    //MARK: - Override functions.
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.leadingConstraint.constant = -self.view.frame.size.width
        swipeGestureLeft = UISwipeGestureRecognizer(target: self, action: #selector(self.hideMenuController))
        swipeGestureLeft.direction = UISwipeGestureRecognizerDirection.left
        menuView.addGestureRecognizer(swipeGestureLeft)
        
        let storyboard = UIStoryboard(name: "Main", bundle:nil)
        menuController = storyboard.instantiateViewController(withIdentifier: "menuController") as! MenuViewController
        menuController.parentDelegate = self
        menuController.view.frame = menuContentView.bounds
        menuContentView.addSubview(menuController.view)
        // Do any additional setup after loading the view, typically from a nib.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.navigationController?.isNavigationBarHidden = true
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    //MARK: - IBAction functions.
    
    @IBAction func showHideMenu(){
        UIView.animate(withDuration: 0.5, delay: 0.0, options: .transitionFlipFromLeft, animations: {
            self.leadingConstraint.constant = 0
            self.view.layoutIfNeeded()
        }) { (animationComplete) in
            print("The animation is complete!")
        }
    }
    
    //MARK: - Local accessibel functions.
    
    func respondToSelector(gesture: UIGestureRecognizer){
        UIView.animate(withDuration: 0.5, delay: 0.0, options: .transitionFlipFromRight, animations: {
            self.leadingConstraint.constant = -self.view.frame.size.width
            self.view.layoutIfNeeded()
        }) { (animationComplete) in
            print("The animation is complete!")
        }
    }
    
    func hideMenuController(){
        UIView.animate(withDuration: 0.5, delay: 0.0, options: .transitionFlipFromRight, animations: {
            self.leadingConstraint.constant = -self.view.frame.size.width
            self.view.layoutIfNeeded()
        }) { (animationComplete) in
            print("The animation is complete!")
        }
    }
    
    //MARK: - Menu delegate functions
    
    func selectedRowAtIndex(selectedIndex: Int, menuTitle: String, selectedData: Any?) {
        print(menuTitle)
        self.hideMenuController()
        /*headerLabel.text = menuTitle
         let storyboard = UIStoryboard(name: "Main", bundle: nil)
         let controller = storyboard.instantiateViewController(withIdentifier: "userDetail") as! UserViewController
         controller.view.frame = contentView.bounds
         self.contentView.addSubview(controller.view)*/
        
        //        self.present(controller, animated: false, completion: nil)
        //        let title : String = menuTitle
        //        performSegue(withIdentifier: "userDetail", sender: title)
    }
    
    
    // MARK: - Navigation
    
    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
//        let destinationController = segue.destination as! UserViewController
//        destinationController.headerTitle = sender as? String
    }
    
}

