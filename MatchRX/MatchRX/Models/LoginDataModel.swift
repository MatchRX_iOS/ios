//
//  LoginDataModel.swift
//  MatchRX
//
//  Created by Vignesh on 11/16/17.
//  Copyright © 2017 HTC. All rights reserved.
//

import UIKit

class LoginDataModel: NSObject {
    var deaNumber:String?
    var userName:String?
    var password:String?
    var rememberCredentials:Bool?
}

class ForgotPasswordDataModel: NSObject{
    var deaNumber: String?
    var userName: String?
    var responseMessage: String?
}

class LoginResponseModel: NSObject{
    var userId: Int?
    var pharmacyId: Int?
    var pharmacyName: String?
    var pharmacyDEA: String?
    var userName: String?
    var firstName: String?
    var lastName: String?
    var loggedUserName: String?
    var landingPage: String?
    var notificationCount: Int?
    var cartCount: Int?
    var accessToken: String?
    var refreshToken: String?
    var salesInProgressCount: Int?
    var purchaseInProgressCount: Int?
    var pharmacyInfo: PharmacyDataModel!
    var loginCredentials: LoginDataModel!
    var errorMessage: String?
    var stateWiseRestriction: StateRestriction?
    var stateWiseRestrictionMessage: String?
}


