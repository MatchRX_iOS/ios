//
//  WishlistDataModel.swift
//  MatchRX
//
//  Created by Vignesh on 3/8/18.
//  Copyright © 2018 HTC. All rights reserved.
//

import UIKit

class WishlistDataModel: NSObject {
    var wishlistId: Int?
    var ndcImageURL: String?
    var ndcNumber: String?
    var formattedNdcNumber: String?
    var ndcName: String?
    var quantity: Int?
    var wishlistedDate: String?
    var message: String?
    var wishlistArray: NSMutableArray = NSMutableArray.init()
}
