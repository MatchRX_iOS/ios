//
//  PharmacyDataModel.swift
//  MatchRX
//
//  Created by Vignesh on 11/20/17.
//  Copyright © 2017 HTC. All rights reserved.
//

import UIKit

enum CellType{
    case cellTypeSingleColumn
    case cellTypeDoubleColumn
}

class PharmacyDataModel: NSObject {
    var celltype: CellType?
    var firstLabelText: String?
    var secondLabelText: String?
    var firstTxtFieldText: String?
    var secondTxtFieldText: String?
    var pharmacyId: Int?
    var legalBusinessName: String?
    var doingBusinessName: String?
    var loggedUserFirstName: String?
    var loggedUserLastName: String?
    var loggedUserName: String?
    var deaNumber: String?
    var userEmailID: String?
    var shippingAddress1: String?
    var shippingAddress2: String?
    var shippingCity: String?
    var shippingState: String?
    var shippingZipCode: String?
    var shippingPhone: String?
    var shippingFax: String?
    var shippingLocation: String?
    var shippingLattitude: String?
    var shippingLongitude: String?
    var mailingAddress1: String?
    var mailingAddress2: String?
    var mailingCity: String?
    var mailingState: String?
    var mailingZipCode: String?
    var ownerFirstName: String?
    var ownerLastName: String?
    var ownerMobileNumber: String?
    var ownerEmailID:String?
    var ncpdpNumber: String?
    var deaLicenseNumber: String?
    var deaExpiryDate: String?
    var npiNumber: Int?
    var stateLicenseNumber: String?
    var licenseExpiryDate: String?
}
