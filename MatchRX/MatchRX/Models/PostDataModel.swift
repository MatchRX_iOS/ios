//
//  PostDataModel.swift
//  MatchRX
//
//  Created by Vignesh on 12/12/17.
//  Copyright © 2017 HTC. All rights reserved.
//

import UIKit

enum StateRestriction {
    case allowOnlySealed
    case allowOnlyNonSealed
    case allowBoth
    case restrictBoth
}

class PostDataModel: NSObject {

}




class NDCDetailsDataModel: NSObject {
    var isSuccess: Bool?
    var ndcNumber: String?
    var formattedNDCNumber: String?
    var ndcName: String?
    var similarItems: Int?
    var medispanImageURL: String?
    var packageStrength: NSNumber?
    var strengthUnitOfMeasure: String?
    var storageCondition: String?
    var packageType: String?
    var packageSize: String?
    var maximumumPackageCount: Int?
    var packageQuantity: Int?
    var packageUnitOfMeasure: String?
    var packageDosageForm: String?
    var packAvailable: Int?
    var packaging: String?
    var isBrand: Bool?
    var isPartialPackExists: Bool?
    var partialPackMessage: String?
    var isNonSealedPartialExists: Bool?
    var nonSealedPartialErrorMessage: String?
    var minimumPrice: String?
    var maximumPrice: String?
    var unitPrice: Double?
    var packPrice: Double?
    var priceType: String?
    var minimumDiscount: String?
    var maximumDiscount: String?
    var minimumExpiryDate: String?
    var maximumExpiryDate: String?
    var wacMaximumPrice: Double?
    var aawpMaximumPrice: Double?
    var quantitySoldMessage: String?
    var stateWisePackageCondition: String?
    var stateRestrictionType: StateRestriction?
    var maximumDiscountPercentage: Float?
    var minimumDiscountPercentage: Float?
    
    var medispanImageName: String?
    var aawpPackPrice: Double?
    var aawpUnitPrice: Double?
    var wacPackPrice: Double?
    var wacUnitPrice: Double?
    var isNDCScreenValidated: Bool = false
    var isSetPriceScreenValidated: Bool = false
    var isSelectShippingScreenValidated: Bool = false
    
    var lotNumber: String?
    var expiryDate: String?
    var yourPartialQty: String?
    var yourPricePerUnit: String?
    var yourDiscount: String?
}

class MyPostDataModel: NSObject {
    var isSuccess: Bool?
    var postId: Int?
    var ndcName: String?
    var similarItems: Int?
    var ndcNumber: String?
    var medispanImageURL: String?
    var packageStrength: NSNumber?
    var strengthUnitOfMeasure: String?
    var storageCondition: String?
    var packageType: String?
    var packageSize: Int?
    var availableQuantity: Int?
    var maximumumPackageCount: Int?
    var packageQuantity: Int?
    var medispanPackageQuantity: Int?
    var medispanPackageSize: Int?
    var packageUnitOfMeasure: String?
    var packageDosageForm: String?
    var packAvailable: Int?
    var packaging: String?
    var isBrand: Bool?
    var isPartialPackExists: Bool?
    var partialPackMessage: String?
    var isNonSealedPartialExists: Bool?
    var nonSealedPartialErrorMessage: String?
    var minimumPrice: String?
    var maximumPrice: String?
    var unitPrice: Double?
    var packPrice: Double?
    var priceType: String?
    var minimumDiscount: String?
    var maximumDiscount: String?
    var minimumExpiryDate: String?
    var maximumExpiryDate: String?
    var wacMaximumPrice: Double?
    var aawpMaximumPrice: Double?
    var quantitySoldMessage: String?
    var stateWisePackageCondition: String?
    var stateRestrictionType: StateRestriction?
    var maximumDiscountPercentage: Float?
    var minimumDiscountPercentage: Float?
    
    var aawpPackPrice: Double?
    var aawpUnitPrice: Double?
    var wacPackPrice: Double?
    var wacUnitPrice: Double?
    var isNDCScreenValidated: Bool = false
    var isSetPriceScreenValidated: Bool = false
    var isSelectShippingScreenValidated: Bool = false
    
    var lotNumber: String?
    var expiryDate: String?
    var yourPartialQty: String?
    var yourPricePerUnit: String?
    var yourDiscount: String?
    var myMinPrice: Double?
    var myMaxPrice: Double?
    var strengthToBeDisplayed: String?
    var packageOption: String?
    var packageUnit: Int?
    var actualPackageSize: Int?
    var optionalPackageCondition: String?
    var isTornPkgSelected: Bool = false
    var isStickerSelected: Bool = false
    var isXContainerSelected: Bool = false
    var isWritingOnContainerSelected: Bool = false
    var isOthersSelected: Bool = false
    var sellerNotes: String?
    var currentPrice: Double?
    var revisedPrice: Double?
    var updatedDate: String?
    var originalPackage: String?
    var shippingHandling: String?
    var shippingMethod: String?
    var priceOption:String?
    var imageArray: NSMutableArray = NSMutableArray.init()
}

class ShippingLOVDataModel: NSObject {
    var shippingMethodId: Int?
    var shippingMethodTitle: String?
    var shippingMethodName: String?
    var shippingMethodShortName: String?
    var shippingMethodIconURL: String?
    var shippingMethodSortOrder: Int?
    
    var shippingHandlingId: Int?
    var shippingHandlingTitle: String?
    var shippingHandlingName: String?
    var shippingAmount: Float?
    var shippingHandlingSortOrder: Int?
    
    var shippingMethodsArray: NSMutableArray = NSMutableArray.init()
    var shippingHandlingArray: NSMutableArray = NSMutableArray.init()
    class ShippingMethodsModel: NSObject {
        var shippingId: Int?
        var title: String?
        var name: String?
        var iconURL: String?
        var sortOrder: Int?
    }
    class ShippingHandlingModel: NSObject {
        var shippingHandlingId: Int?
        var title: String?
        var name: String?
        var sortOrder: Int?
    }
}

class ItemDetailsDataModel: NSObject {
    var postId: Int?
    var wacDiscount: Double?
    var aawpDiscount: Double?
    var discountPercentage: String?
    var ndcName: String?
    var ndcNumber: String?
    var lotNumber: String?
    var packageOption: String?
    var actualPackageSize: Int?
    var packageQuantity: Int?
    var originalPackage: String?
    var expiryDate: String?
    var packageSize: Int?
    var unitPrice: Double?
    var revisedPrice: Double?
    var shippingHandlingName: String?
    var shippingDisplayName: String?
    var condition: String?
    var sellerNotes: String?
    var lastBoughtOn: String?
    var isWishlisted: Bool?
    var rating: Float?
    var shippingHandlingAmount: String?
    var estimatedDeliveryDate: String?
    var packageType: String?
    var imageArray: NSMutableArray = NSMutableArray.init()
    var medispanImageURL: String?
    var genericName: String?
    var storageCondition: String?
    var storage: String?
    var manufacturerName: String?
    var packageSizeUnitOfMeasure: String?
    var metricStrength: NSNumber?
    var strengthUnitOfMeasure: String?
    var dosageForm: String?
    var shape: String?
    var isLastBought: Bool?
    var isMyPosting: Bool?
    var isInCart: Bool?
    var strength: String?
    var packaging: String?
    var availableQuantity: Int?
    var quantityInCart: Int?
    var isComparePrice: Bool?
    var ingredients: String?
    var isMultiIngredients: Bool?
    var packageConditionDescription: String?
}
