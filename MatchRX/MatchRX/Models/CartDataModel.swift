//
//  CartDataModel.swift
//  MatchRX
//
//  Created by Vignesh on 2/16/18.
//  Copyright © 2018 HTC. All rights reserved.
//

import UIKit

enum ShippingType {
    case ShippingTypeGround
    case ShippingTypeTwoDay
    case ShippingTypePriorityOvernight
}

class CartDataModel: NSObject {
    var cartSellerArray: NSMutableArray = NSMutableArray.init()
    var shippingArray: NSMutableArray = NSMutableArray.init()
    var typeOfShipping: ShippingType?
    var checkoutInformation: String?
    var sellerInformation: String?
    var totalQuantity: String?
    var grandTotal: Double?
    var cartCount: String?
    var isCartObjectExists: Bool?
}

class CartItemDataModel: NSObject {
    var postId: Int?
    var postTitle: String?
    var ndcNumber: String?
    var manufacturerName: String?
    var medispanImageURL: String?
    var packageQuantity: Int?
    var availableQuantity: Int?
    var quantityAddedInCart: Int?
    var unitPrice: Double?
    var packageOption: String?
    var packageSize: Int?
    var actualPackageSize: Int?
    var itemPrice: Double?
    var revisedPrice: Double?
    var packaging: String?
}

class CartShippingDetailsDataModel: NSObject {
    var shippingId: String?
    var shippingName: String?
    var deliveredBy: String?
    var shippingCost: Double?
    var sortOrder: Int?
    var shippingPaidBy: String?
    var isSelected: Bool?
    var matchRXShippingOffer: String?
    var fedexShippingOffer: String?
    var deliveryDetails: String?
}

class CartSellerDataModel: NSObject {
    var sellerId: Int?
    var commonSellerId: String?
    var cartId: Int?
    var isActive: Bool?
    var isLastActive: Bool = false
    var sellerName: String?
    var itemTotalAmount: Double?
    var shippingAmount: Double?
    var userModifiedSellerTotal : Double?
    var totalAmount: Double?
    var shippingMethod: String?
    var fedexImageUrl: String?
    var matchRXFee: Double?
    var groundInfo: String?
    var twoDayInfo: String?
    var overnightInfo: String?
    var grandTotal: Double?
    var cartDetailsArray: NSMutableArray = NSMutableArray.init()
    var shippingMethods: NSMutableDictionary = NSMutableDictionary.init()
}
