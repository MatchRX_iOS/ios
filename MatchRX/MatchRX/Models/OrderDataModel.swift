//
//  OrderDataModel.swift
//  MatchRX
//
//  Created by Vignesh on 2/19/18.
//  Copyright © 2018 HTC. All rights reserved.
//

import UIKit

enum TypeOfOrder {
    case Sales
    case Purchase
    case Purchase_Revise
    case Purchase_Confirm_Receipt
}

enum OrderedItemStatus {
    case Available
    case Revised
    case OutOfStock
    case Accept
    case Reject
    case Canceled
}

class OrderDataModel: NSObject {

}

class OrderSummaryDataModel: NSObject {
    var sellerId: Int?
    var sellerName: String?
    var orderId: Int?
    var orderNumber: String?
    var totalAmount: Double?
    var shippingPaidBy: String?
    var itemTotalAmount: Double?
    var shippingCost: Double?
    var shippingMethod: String?
    var fedexImageUrl: String?
    var cartItemArray: NSMutableArray = NSMutableArray.init()
    var orderSummaryArray: NSMutableArray = NSMutableArray.init()
    var bankDetails: String?
    var buyerShippingFee: Double?
    var sellerShippingFee: Double?
}

class OrderItemModel: NSObject {
    var postId: Int?
    var postTitle: String?
    var ndcNumber: String?
    var packageQuantity: Int?
    var unitPrice: Double?
    var packageOption: String?
    var packageSize: Int?
    var actualPackageSize: Int?
    var itemPrice: Double?
    var totalPrice: Double?
    var packaging: String?
}

class OrderCountDataModel: NSObject {
    var salesCount: Int?
    var purchaseCount: Int?
    var totalCount: Int?
}

class MyOrdersDataModel: NSObject {
    var orderId: Int64?
    var orderNumber: String?
    var orderDate: String?
    var shippingAmount: Double?
    var sellerShippingFee: Double?
    var additionalSellerShippingFee: Double?
    var additionalSellerPickupFee: Double?
    var refundSellerShippingFee: Double?
    var creditTotalAmount: Double?
    var fedexShippingInsurance: Double?
    var shippingMethod: String?
    var shippingDate: String?
    var expectedDeliveryDate: String?
    var debitTotalAmount: Double?
    var grossAmount: Double?
    var isActionRequired: Bool?
    var orderType: TypeOfOrder?
    var message: String?
    var actionOrdersArray = NSMutableArray.init()
    var openOrdersArray = NSMutableArray.init()
    var shippingPaidBy: String?
    var buyerShippingFee: Double?
    var sellerConfirmationDate: String?
    var trackingId: String?
    var createdDate: String?
    var promoText: String?
    var orderStatus: String?
    var shippingStatusTime: String?
}

struct ShippingDates {
    var shippingDate: String?
    var receivedDate: String?
    var keys: String?
    var shippingTimeSlot: NSDictionary = NSDictionary.init()
}

class OrderDetailsDataModel: NSObject {
    var orderNumber: String?
    var orderDate: String?
    var orderId: Int64?
    var globalOrderId: Int64?
    var ndcName: String?
    var ndcNumber: String?
    var medispanImageURL: String?
    var expiryDate: String?
    var lotNumber: String?
    var originalQuantity: Int?
    var modifiedQuantity: Int?
    var orderedQuantity: Int?
    var modifiedPackageSize: Int?
    var originalPackageSize: Int?
    var orderedPackageSize: Int?
    var actualPackageSize: Int?
    var packageOption: String?
    var partialQuantity: String?
    var unitPrice: Double?
    var originalPrice: Double?
    var modifiedPrice: Double?
    var originalPackPrice: Double?
    var modifiedPackPrice: Double?
    var orderedPrice: Double?
    var fedexInsuranceAmount: Double?
    var matchRXFee: Double?
    var matchMoney: Double?
    var grossTotal: Double?
    var shippingCharge: Double?
    var sellerShippingFee: Double?
    var additionalSellerShippingFee: Double?
    var additionalSellerPickupFee: Double?
    var refundSellerShippingFee: Double?
    var fedexShippingInsurance: Double?
    var additionalFedexShippingInsurance: Double?
    var refundFededShippingInsurance: Double?
    
    
    var buyerShippingFee: Double?
    var additionalBuyerShippingFee: Double?
    var additionalBuyerPickupFee: Double?
    var refundBuyerShippingFee: Double?
    var additionalBuyerShippingInsurance: Double?
    var refundBuyerShippingInsurance: Double?
    var userCalculatedShippingCharge: Double?
    
    var orderItemsArray: NSMutableArray = NSMutableArray.init()
    var shippingMethods: NSMutableDictionary = NSMutableDictionary.init()
    var shippingSlotArray: NSMutableArray = NSMutableArray.init()
    var storageCondition: String?
    var shippingSlot: ShippingDates = ShippingDates()
    var itemStatus: OrderedItemStatus?
    var isRevised: Bool?
    var matchRXFeePercentage: Double?
    
    var item_shippingHandlingName: String?
    var item_shippingMethod: String?
}

class OrderResponseDataModel: NSObject {
    var trackingId: String?
    var message: String?
    var shippingDocumentURL: String?
}
