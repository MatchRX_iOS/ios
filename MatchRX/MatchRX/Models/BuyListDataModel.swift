//
//  BuyListDataModel.swift
//  MatchRXS5
//
//  Created by Poonkathirvelan on 22/01/18.
//  Copyright © 2018 HTCGlobalServices. All rights reserved.
//

import UIKit

class BuyListDataModel: NSObject {

    var postId: Int?
    var postTitle: String?
    var drugName: String?
    var ndcNumber: String?
    var manufacturerName: String?
    var drugImgUrl: String?
    var expiryDate: String?
    var strengthMetric: Int?
    var strengthUnitMsr: String?
    var updatedDate: String?
    var createdDate: String?
    var footerDate: String?
    
    var packageOption: String?
    var packageUnit: Int?
    var packageType: String?
    var packageSize: Int?
    var actualPackageSize: Int?
    var packageQuantity: Int?
    var packageUnitMsr: String?
    var packageLabText: String?
    
    var estDelivery: String?
    var rating: NSNumber?
    var shipPromo: String?
    var aawpDiscount: Double?
    var wacDiscount: Double?
    var price: Int?
    
    var isMyposting: Bool?
    var isModified: Bool?
    var lastBoughtDate: String?
    var isLastBought: Bool?
    var isInWishList: Bool?
    
    var medispanImageURL: String?
    var imageCount: Int?
    var metricStrength: NSNumber?
    var strenghtUnitOfMeasure: String?
    var packageUnitOfMeasure: String?
    var currentPrice: Double?
    var revisedPrice: Double?
    var imageArray: NSMutableArray = NSMutableArray.init()
    var packaging: String?
    var strength: String?
    var estimatedDeliveryDate: String?
    var modifiedDate: String?
    var discount: String?
    var shippingAmount: String?
    
    var lotNumber: String?
    var isWacDiscountLessThanTenExists: Bool?
    var isInCart: Bool?
    var isMultiIngredient: Bool?
    var availableQuantity: Int?
    
}

class ComparePriceDataModel: NSObject {
    var currentPostArray = NSMutableArray.init()
    var matchesArray = NSMutableArray.init()
}

class MarketPlaceDataModel: NSObject {
    var marketPlaceArray = NSMutableArray.init()
    var cartArray = NSMutableArray.init()
    var totalDataCount: Int?
    var cartCount: Int?
    var message: String?
}
