//
//  GlobalData.swift
//  MatchRX
//
//  Created by Vignesh on 11/21/17.
//  Copyright © 2017 HTC. All rights reserved.
//

import UIKit

class GlobalData: NSObject {
    static let sharedInstance = GlobalData()
    
    var userId: Int?
    var accessToken: String?
    var refreshToken: String?
    var pharmacyId: Int?
    var deaNumber: String?
    var landingPage: String?
    var salesInProgressCount: Int?
    var purchaseInProgressCount: Int?
    var stateWiseRestriction: StateRestriction?
    var stateWiseRestrictionMessage: String?
    var lovDataModel: ShippingLOVDataModel?
    var sellerArray: NSMutableArray = NSMutableArray.init()
    var cartArray: NSMutableArray = NSMutableArray.init()
    var comparePricePostId: Int?
    var cartCount: Int?
    var notificationCount: Int?
    var marketplaceModel = MarketPlaceDataModel()
    var searchText: String = ""
    var filterDictionary = NSMutableDictionary.init()
    var sortDictionary = NSMutableDictionary.init()
    var filterCount: Int = 0
    
    func resetFilterDictionary () -> NSMutableDictionary{
        filterDictionary.setValue("A", forKey: "package_option")
        filterDictionary.setValue("A", forKey: "original_package")
        filterDictionary.setValue("A", forKey: "brand")
        filterDictionary.setValue("A", forKey: "expire_date")
        filterDictionary.setValue("A", forKey: "wac_discount")
        filterDictionary.setValue("A", forKey: "created_at")
        return filterDictionary
    }
    
    func defaultFilters () -> NSMutableDictionary{
        let filterDict = NSMutableDictionary.init()
        filterDict.setValue("A", forKey: "package_option")
        filterDict.setValue("A", forKey: "original_package")
        filterDict.setValue("A", forKey: "brand")
        filterDict.setValue("A", forKey: "expire_date")
        filterDict.setValue("A", forKey: "wac_discount")
        filterDict.setValue("A", forKey: "created_at")
        return filterDict
    }
    
    func resetSortDictionary () {
        sortDictionary.setValue("desc", forKey: "updated_at")
    }
}
