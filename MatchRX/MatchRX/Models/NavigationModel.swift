//
//  NavigationModel.swift
//  MatchRX
//
//  Created by Vignesh on 11/30/17.
//  Copyright © 2017 HTC. All rights reserved.
//

import UIKit

class NavigationModel: NSObject {
    static let sharedInstance = NavigationModel()
    weak var homeControl: HomeControl?
    weak var homeNavigationControl: UINavigationController!
    weak var buyControl: BuyController?
    weak var buyListControl: BuyListViewController?
    weak var messageControl: StaticMessageController?
    
    override init() {
        homeNavigationControl = homeControl?.contentNavController
        buyListControl = homeControl?.buyController.buyListViewCtrl
    }
    
    func showAlertMessage(presentView: UIView, message: String) {
        let storyboard = UIStoryboard(name: "Secondary", bundle: nil)
        messageControl = storyboard.instantiateViewController(withIdentifier: "messageControl") as? StaticMessageController
        messageControl?.view.frame = presentView.bounds
        presentView.addSubview((messageControl?.view)!)
//        messageControl?.messageLbl.text = message
//        presentView.layoutIfNeeded()
    }
}
